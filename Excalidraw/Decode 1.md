---

excalidraw-plugin: parsed

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Decode ^dqFoLQC7

7 ^iTihcd10

4 ^9RyQfXjp

Decode ^aPaRpFuu

7 ^zlZTTz6d

4 ^S6I7aq9T

Decode ^5JcelWlM

7 ^jwR2nM2j

4 ^0C00oIJy

Decode ^VJzP8T7u

7 ^D1VfLta7

4 ^PjRxmtgm

MUX ^8CE76NJP

4 ^vAnWGF3B

7 ^m0QPo5Nm

Decode Number 1
Interconnection between 4 decodes for create a more complex decode
with a muxtiplixer inside.  ^maej18of

2 ^CNsqW9gg

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "rectangle",
			"version": 190,
			"versionNonce": 1331089131,
			"isDeleted": false,
			"id": "jMaKVXM0fpigkzJYnzf9b",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -259.8346053169415,
			"y": -44.25491073957534,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 193.75,
			"height": 107.375,
			"seed": 333252517,
			"groupIds": [
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "Rx-OEmNrjCI6Sc9vlCi1S",
					"type": "arrow"
				},
				{
					"id": "aDeRk0ZtiDIqELgT7uqJt",
					"type": "arrow"
				}
			],
			"updated": 1646802929255,
			"link": null
		},
		{
			"type": "text",
			"version": 137,
			"versionNonce": 292694341,
			"isDeleted": false,
			"id": "dqFoLQC7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -212.4596053169415,
			"y": -8.067410739575337,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 99,
			"height": 36,
			"seed": 1779724587,
			"groupIds": [
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929255,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Decode",
			"rawText": "Decode",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Decode"
		},
		{
			"type": "arrow",
			"version": 256,
			"versionNonce": 353614219,
			"isDeleted": false,
			"id": "Rx-OEmNrjCI6Sc9vlCi1S",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -58.39809738043354,
			"y": 9.452246853402983,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 93.49952453412115,
			"height": 0,
			"seed": 1149684485,
			"groupIds": [
				"betvV_pqkqckiMz1Y3434",
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929255,
			"link": null,
			"startBinding": {
				"elementId": "jMaKVXM0fpigkzJYnzf9b",
				"focus": 0.00036614841403155996,
				"gap": 7.686507936507951
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					93.49952453412115,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 139,
			"versionNonce": 1111931045,
			"isDeleted": false,
			"id": "bM86CvXD0ws_wserFvh5C",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -30.436859745282646,
			"y": -9.187355307889277,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 1147343819,
			"groupIds": [
				"betvV_pqkqckiMz1Y3434",
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929255,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 169,
			"versionNonce": 1964658731,
			"isDeleted": false,
			"id": "xiLgTRppnJx45ifRw-e4x",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -15.529452337875256,
			"y": -8.539207159741125,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 1977688677,
			"groupIds": [
				"betvV_pqkqckiMz1Y3434",
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929255,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "arrow",
			"version": 313,
			"versionNonce": 1016224773,
			"isDeleted": false,
			"id": "aDeRk0ZtiDIqELgT7uqJt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -367.5794760982659,
			"y": 7.178402504646044,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 93.49952453412115,
			"height": 0,
			"seed": 772039275,
			"groupIds": [
				"GAZkILoJb4czf6wJyam4B",
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929255,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "jMaKVXM0fpigkzJYnzf9b",
				"focus": 0.041987180550009204,
				"gap": 14.245346247203258
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					93.49952453412115,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 193,
			"versionNonce": 1364529867,
			"isDeleted": false,
			"id": "UglVQLdInyVluHlQP7NNM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -339.6182384631151,
			"y": -11.461199656646215,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 1824099781,
			"groupIds": [
				"GAZkILoJb4czf6wJyam4B",
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929255,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 223,
			"versionNonce": 1750242149,
			"isDeleted": false,
			"id": "XwHG7uZzIEsRHlGvS-OMO",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -324.7108310557076,
			"y": -10.813051508498063,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 2143477003,
			"groupIds": [
				"GAZkILoJb4czf6wJyam4B",
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929255,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "text",
			"version": 122,
			"versionNonce": 1784578411,
			"isDeleted": false,
			"id": "iTihcd10",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -34.780456071421014,
			"y": -49.26178122235359,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 16,
			"height": 36,
			"seed": 2016251173,
			"groupIds": [
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "7",
			"rawText": "7",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "7"
		},
		{
			"type": "text",
			"version": 105,
			"versionNonce": 1372905157,
			"isDeleted": false,
			"id": "9RyQfXjp",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -340.56992975563145,
			"y": -49.26178122235359,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 19,
			"height": 36,
			"seed": 353960875,
			"groupIds": [
				"VG7HisvKOPC9LE3bPwBN9"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "4",
			"rawText": "4",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4"
		},
		{
			"type": "rectangle",
			"version": 185,
			"versionNonce": 159456267,
			"isDeleted": false,
			"id": "IkOZ7iT8YhTHZ0gaJNrLr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -259.0826112536554,
			"y": 116.6097477500835,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 193.75,
			"height": 107.375,
			"seed": 316095659,
			"groupIds": [
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "D1_SfqVlUObQqMGds6Wco",
					"type": "arrow"
				},
				{
					"id": "4hBuHIJznbA96nbGPxas2",
					"type": "arrow"
				}
			],
			"updated": 1646802929256,
			"link": null
		},
		{
			"type": "text",
			"version": 131,
			"versionNonce": 532069925,
			"isDeleted": false,
			"id": "aPaRpFuu",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -211.7076112536554,
			"y": 152.7972477500835,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 99,
			"height": 36,
			"seed": 382236549,
			"groupIds": [
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Decode",
			"rawText": "Decode",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Decode"
		},
		{
			"type": "arrow",
			"version": 245,
			"versionNonce": 1790403243,
			"isDeleted": false,
			"id": "D1_SfqVlUObQqMGds6Wco",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -57.646103317147436,
			"y": 170.31690534306182,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 93.49952453412115,
			"height": 0,
			"seed": 1908436811,
			"groupIds": [
				"h3L7Qa6yohHhmaRI_Ais1",
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"startBinding": {
				"elementId": "IkOZ7iT8YhTHZ0gaJNrLr",
				"focus": 0.00036614841403155996,
				"gap": 7.686507936507951
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					93.49952453412115,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 134,
			"versionNonce": 1223012741,
			"isDeleted": false,
			"id": "CUAeGgAsPHpdRPPbU1aQa",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -29.684865681996598,
			"y": 151.67730318176956,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 1547255525,
			"groupIds": [
				"h3L7Qa6yohHhmaRI_Ais1",
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 164,
			"versionNonce": 1722537291,
			"isDeleted": false,
			"id": "xlt_NdHEPGBcnLWbM5p2B",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -14.777458274589094,
			"y": 152.3254513299177,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 237653483,
			"groupIds": [
				"h3L7Qa6yohHhmaRI_Ais1",
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "arrow",
			"version": 302,
			"versionNonce": 178206949,
			"isDeleted": false,
			"id": "4hBuHIJznbA96nbGPxas2",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -366.82748203497977,
			"y": 168.04306099430488,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 93.49952453412115,
			"height": 0,
			"seed": 2061113925,
			"groupIds": [
				"nyFk2MAcw5Huf6mf2p9W9",
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "IkOZ7iT8YhTHZ0gaJNrLr",
				"focus": 0.041987180550009204,
				"gap": 14.245346247203202
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					93.49952453412115,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 188,
			"versionNonce": 1555624939,
			"isDeleted": false,
			"id": "Tat0PleZcg6gc_Nz0D_-H",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -338.86624439982893,
			"y": 149.40345883301262,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 1260345483,
			"groupIds": [
				"nyFk2MAcw5Huf6mf2p9W9",
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 218,
			"versionNonce": 510710853,
			"isDeleted": false,
			"id": "FHU9_vUD72tsodJRlGInl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -323.95883699242154,
			"y": 150.05160698116066,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 2072545701,
			"groupIds": [
				"nyFk2MAcw5Huf6mf2p9W9",
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "text",
			"version": 115,
			"versionNonce": 1558789771,
			"isDeleted": false,
			"id": "zlZTTz6d",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -34.028462008134966,
			"y": 111.60287726730513,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 16,
			"height": 36,
			"seed": 1180307243,
			"groupIds": [
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "7",
			"rawText": "7",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "7"
		},
		{
			"type": "text",
			"version": 98,
			"versionNonce": 808977317,
			"isDeleted": false,
			"id": "S6I7aq9T",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -339.8179356923454,
			"y": 111.60287726730513,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 19,
			"height": 36,
			"seed": 1869745413,
			"groupIds": [
				"7rpMdRBcu3-I87axLGJvF"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929256,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "4",
			"rawText": "4",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4"
		},
		{
			"type": "rectangle",
			"version": 278,
			"versionNonce": 301413675,
			"isDeleted": false,
			"id": "G7Rk0m9VmMBXvVsQzE2m1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -259.4194631461546,
			"y": 276.0458348111708,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 193.75,
			"height": 107.375,
			"seed": 38072779,
			"groupIds": [
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "Bs249fhycM8qadowalaNG",
					"type": "arrow"
				},
				{
					"id": "Q5ntqYa9njOvDS0Z-7BHj",
					"type": "arrow"
				}
			],
			"updated": 1646802929257,
			"link": null
		},
		{
			"type": "text",
			"version": 178,
			"versionNonce": 337790725,
			"isDeleted": false,
			"id": "5JcelWlM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -212.04446314615464,
			"y": 312.2333348111708,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 99,
			"height": 36,
			"seed": 1712596069,
			"groupIds": [
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "e91G26GVCs7ky6rhr0yHv",
					"type": "arrow"
				}
			],
			"updated": 1646802929257,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Decode",
			"rawText": "Decode",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Decode"
		},
		{
			"type": "arrow",
			"version": 434,
			"versionNonce": 1800230859,
			"isDeleted": false,
			"id": "Bs249fhycM8qadowalaNG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -57.98295520964669,
			"y": 329.7529924041491,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 93.49952453412115,
			"height": 0,
			"seed": 1341816939,
			"groupIds": [
				"Aghw0DKQ95hH_cdXAcU3K",
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929257,
			"link": null,
			"startBinding": {
				"elementId": "G7Rk0m9VmMBXvVsQzE2m1",
				"focus": 0.00036614841403155996,
				"gap": 7.686507936507951
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					93.49952453412115,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 226,
			"versionNonce": 1017978469,
			"isDeleted": false,
			"id": "VTM7vtcrzKSS4CjJKZ6Xp",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -30.021717574495852,
			"y": 311.11339024285684,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 591155141,
			"groupIds": [
				"Aghw0DKQ95hH_cdXAcU3K",
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929257,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 256,
			"versionNonce": 117843563,
			"isDeleted": false,
			"id": "wq-ispWgYvKIgQ26kfSuv",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -15.114310167088462,
			"y": 311.761538391005,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 2034580235,
			"groupIds": [
				"Aghw0DKQ95hH_cdXAcU3K",
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929257,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "arrow",
			"version": 489,
			"versionNonce": 1889929669,
			"isDeleted": false,
			"id": "Q5ntqYa9njOvDS0Z-7BHj",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -367.16433392747905,
			"y": 327.47914805539216,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 93.49952453412115,
			"height": 0,
			"seed": 352849701,
			"groupIds": [
				"e9aRGujmyx6bPzF_FQooJ",
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929257,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "G7Rk0m9VmMBXvVsQzE2m1",
				"focus": 0.041987180550009204,
				"gap": 14.245346247203258
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					93.49952453412115,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 280,
			"versionNonce": 1542201611,
			"isDeleted": false,
			"id": "DmHXlcZRn7-wKwFBra-_b",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -339.2030962923283,
			"y": 308.8395458940999,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 715104683,
			"groupIds": [
				"e9aRGujmyx6bPzF_FQooJ",
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929257,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 310,
			"versionNonce": 1093306661,
			"isDeleted": false,
			"id": "JWPFNmLLluTwCSXtZNuiT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -324.2956888849208,
			"y": 309.48769404224805,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 165995141,
			"groupIds": [
				"e9aRGujmyx6bPzF_FQooJ",
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929257,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "text",
			"version": 162,
			"versionNonce": 595183531,
			"isDeleted": false,
			"id": "jwR2nM2j",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -34.36531390063422,
			"y": 271.0389643283925,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 16,
			"height": 36,
			"seed": 489960523,
			"groupIds": [
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929257,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "7",
			"rawText": "7",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "7"
		},
		{
			"type": "text",
			"version": 145,
			"versionNonce": 1067981957,
			"isDeleted": false,
			"id": "0C00oIJy",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -340.15478758484466,
			"y": 271.0389643283925,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 19,
			"height": 36,
			"seed": 646990309,
			"groupIds": [
				"_NSpK6aMYQslWBqamn-8X"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929257,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "4",
			"rawText": "4",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4"
		},
		{
			"type": "rectangle",
			"version": 372,
			"versionNonce": 64592459,
			"isDeleted": false,
			"id": "Y2MrKBn-vaSPo95ELqc3Z",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -258.4670705373891,
			"y": 436.1962075865438,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 193.75,
			"height": 107.375,
			"seed": 2140374885,
			"groupIds": [
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "VlTaELMj6rl00OyNnMS5O",
					"type": "arrow"
				},
				{
					"id": "Si6tb85Q_JD3E5Hoj5qM3",
					"type": "arrow"
				}
			],
			"updated": 1646802929257,
			"link": null
		},
		{
			"type": "text",
			"version": 239,
			"versionNonce": 1943815141,
			"isDeleted": false,
			"id": "VJzP8T7u",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -211.09207053738908,
			"y": 472.38370758654384,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 99,
			"height": 36,
			"seed": 450284907,
			"groupIds": [
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "e91G26GVCs7ky6rhr0yHv",
					"type": "arrow"
				}
			],
			"updated": 1646802929257,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Decode",
			"rawText": "Decode",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Decode"
		},
		{
			"type": "arrow",
			"version": 627,
			"versionNonce": 2045122795,
			"isDeleted": false,
			"id": "VlTaELMj6rl00OyNnMS5O",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -57.030562600881126,
			"y": 489.90336517952215,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 93.49952453412115,
			"height": 0,
			"seed": 1020744389,
			"groupIds": [
				"_mW2lU672L8wbDFpPf1YE",
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"startBinding": {
				"elementId": "Y2MrKBn-vaSPo95ELqc3Z",
				"focus": 0.00036614841403155996,
				"gap": 7.686507936507951
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					93.49952453412115,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 319,
			"versionNonce": 299138885,
			"isDeleted": false,
			"id": "Q_Nek82PrBxhGfbT1WmBB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -29.06932496573029,
			"y": 471.2637630182299,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 487581707,
			"groupIds": [
				"_mW2lU672L8wbDFpPf1YE",
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 349,
			"versionNonce": 53068683,
			"isDeleted": false,
			"id": "zEfSqs-Mu3gw95HK97a4s",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -14.161917558322898,
			"y": 471.91191116637793,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 1783280165,
			"groupIds": [
				"_mW2lU672L8wbDFpPf1YE",
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "arrow",
			"version": 682,
			"versionNonce": 1877888677,
			"isDeleted": false,
			"id": "Si6tb85Q_JD3E5Hoj5qM3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -366.21194131871357,
			"y": 487.6295208307652,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 93.49952453412115,
			"height": 0,
			"seed": 982298283,
			"groupIds": [
				"jQ7Y2kL5YXldJ3JhnuJnT",
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "Y2MrKBn-vaSPo95ELqc3Z",
				"focus": 0.04198718055000921,
				"gap": 14.245346247203315
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					93.49952453412115,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 373,
			"versionNonce": 132061739,
			"isDeleted": false,
			"id": "ZJcbLVaLG7zzF12Unatk_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -338.25070368356273,
			"y": 468.98991866947296,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 81470853,
			"groupIds": [
				"jQ7Y2kL5YXldJ3JhnuJnT",
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 403,
			"versionNonce": 1020736005,
			"isDeleted": false,
			"id": "Czci_5uT4I7YG4Oo_lS8D",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -323.34329627615534,
			"y": 469.638066817621,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 636985675,
			"groupIds": [
				"jQ7Y2kL5YXldJ3JhnuJnT",
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "text",
			"version": 222,
			"versionNonce": 82877643,
			"isDeleted": false,
			"id": "D1VfLta7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -33.412921291868656,
			"y": 431.1893371037655,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 16,
			"height": 36,
			"seed": 264645861,
			"groupIds": [
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "7",
			"rawText": "7",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "7"
		},
		{
			"type": "text",
			"version": 205,
			"versionNonce": 1972152677,
			"isDeleted": false,
			"id": "PjRxmtgm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -339.2023949760792,
			"y": 431.1893371037655,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 19,
			"height": 36,
			"seed": 1487618027,
			"groupIds": [
				"qtwANlH_arKUqNRA8WIR5"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "4",
			"rawText": "4",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4"
		},
		{
			"type": "line",
			"version": 135,
			"versionNonce": 1979239275,
			"isDeleted": false,
			"id": "1PA08CHh1ynloOJzrzzxJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 38.70383581626015,
			"y": 11.490465448408571,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 476.2526967648417,
			"seed": 1859444907,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					476.2526967648417
				]
			]
		},
		{
			"type": "line",
			"version": 196,
			"versionNonce": 459027653,
			"isDeleted": false,
			"id": "AReynQQWyhafwP_Is-uIr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -366.47812131111965,
			"y": 6.269192731523418,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 0.1408450704225288,
			"height": 480.7597390183628,
			"seed": 947561387,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929258,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.1408450704225288,
					480.7597390183628
				]
			]
		},
		{
			"type": "arrow",
			"version": 213,
			"versionNonce": 2135041547,
			"isDeleted": false,
			"id": "ITx7aGMezddEIWWAClJc_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 40.45797266593445,
			"y": 250.9618411202815,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 60.74547723019329,
			"height": 1.4036639789287904,
			"seed": 992090821,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802929259,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "b0LpR09O95WkjYxNHGyzv",
				"gap": 9.258491023774777,
				"focus": 0.00008462679183855324
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					60.74547723019329,
					1.4036639789287904
				]
			]
		},
		{
			"type": "rectangle",
			"version": 187,
			"versionNonce": 932124773,
			"isDeleted": false,
			"id": "b0LpR09O95WkjYxNHGyzv",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 110.46194091990253,
			"y": 177.79612791177726,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 178.48357424441525,
			"height": 150.25,
			"seed": 1332457227,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "ITx7aGMezddEIWWAClJc_",
					"type": "arrow"
				},
				{
					"id": "f1M01bLboUwMZyr28ROft",
					"type": "arrow"
				},
				{
					"id": "Jat6RBntqUY5peQKDq_aT",
					"type": "arrow"
				},
				{
					"id": "ovpeA0QG1_sNztk-aYx0a",
					"type": "arrow"
				}
			],
			"updated": 1646803325873,
			"link": null
		},
		{
			"type": "text",
			"version": 30,
			"versionNonce": 1292863365,
			"isDeleted": false,
			"id": "8CE76NJP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 169.7662288961004,
			"y": 235.4339554097307,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 59,
			"height": 36,
			"seed": 1240971397,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802929259,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "MUX",
			"rawText": "MUX",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "MUX"
		},
		{
			"type": "rectangle",
			"version": 126,
			"versionNonce": 1932681035,
			"isDeleted": false,
			"id": "i6ZyD0jlWQ4EunbDxeFQ8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -447.1168879870165,
			"y": -93.58119610542133,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 801.6666666666667,
			"height": 711.6666666666667,
			"seed": 373098219,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "yWoDLxMWwySWKNkxQKRrA",
					"type": "arrow"
				},
				{
					"id": "f1M01bLboUwMZyr28ROft",
					"type": "arrow"
				},
				{
					"id": "abSyFZTAfS84UQEuAunIC",
					"type": "arrow"
				},
				{
					"id": "Jat6RBntqUY5peQKDq_aT",
					"type": "arrow"
				}
			],
			"updated": 1646802929259,
			"link": null
		},
		{
			"type": "arrow",
			"version": 449,
			"versionNonce": 364779,
			"isDeleted": false,
			"id": "abSyFZTAfS84UQEuAunIC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -562.7922217585956,
			"y": 253.0702791552122,
			"strokeColor": "#5c940d",
			"backgroundColor": "transparent",
			"width": 191.49952453412118,
			"height": 1,
			"seed": 486680037,
			"groupIds": [
				"akiQ9LBx2Lihdngj4JHTp",
				"oOn3xLR4Fl-a5lPNcQJrJ"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802992040,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "i6ZyD0jlWQ4EunbDxeFQ8",
				"focus": 0.025803816597751293,
				"gap": 22.175809237457884
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					191.49952453412118,
					-1
				]
			]
		},
		{
			"type": "line",
			"version": 253,
			"versionNonce": 1849679333,
			"isDeleted": false,
			"id": "F2xtyCOG68QiLhbUQ3E5d",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -534.8309841234448,
			"y": 234.43067699391995,
			"strokeColor": "#5c940d",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 1208440555,
			"groupIds": [
				"akiQ9LBx2Lihdngj4JHTp",
				"oOn3xLR4Fl-a5lPNcQJrJ"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802961402,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 283,
			"versionNonce": 2090933995,
			"isDeleted": false,
			"id": "o92-wnXw4vtgFDT17ZSg8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -519.9235767160374,
			"y": 235.07882514206798,
			"strokeColor": "#5c940d",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 535699781,
			"groupIds": [
				"akiQ9LBx2Lihdngj4JHTp",
				"oOn3xLR4Fl-a5lPNcQJrJ"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802961402,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "arrow",
			"version": 486,
			"versionNonce": 211547883,
			"isDeleted": false,
			"id": "Jat6RBntqUY5peQKDq_aT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 298.3744907738311,
			"y": 245.31850706597209,
			"strokeColor": "#5c940d",
			"backgroundColor": "transparent",
			"width": 161.405493705955,
			"height": 0,
			"seed": 1670857323,
			"groupIds": [
				"Wp2AUhIJP8rcTBUYW06Eo",
				"xfxAorN5mSAFaLRW-ADPJ"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802938127,
			"link": null,
			"startBinding": {
				"elementId": "b0LpR09O95WkjYxNHGyzv",
				"focus": -0.10119961192419538,
				"gap": 9.428975609513316
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					161.405493705955,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 312,
			"versionNonce": 1064515659,
			"isDeleted": false,
			"id": "_3e_1NnFQPEW1s5oh26jF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 394.24169758081575,
			"y": 226.67890490467983,
			"strokeColor": "#5c940d",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 910470597,
			"groupIds": [
				"Wp2AUhIJP8rcTBUYW06Eo",
				"xfxAorN5mSAFaLRW-ADPJ"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802937759,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "line",
			"version": 342,
			"versionNonce": 129753707,
			"isDeleted": false,
			"id": "e0ZxVDpB8ULYa78sAfAkr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 409.14910498822314,
			"y": 234.00903256644165,
			"strokeColor": "#5c940d",
			"backgroundColor": "transparent",
			"width": 15,
			"height": 40.185185185185176,
			"seed": 1073331467,
			"groupIds": [
				"Wp2AUhIJP8rcTBUYW06Eo",
				"xfxAorN5mSAFaLRW-ADPJ"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646802947737,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-15,
					40.185185185185176
				]
			]
		},
		{
			"type": "text",
			"version": 29,
			"versionNonce": 1035415365,
			"isDeleted": false,
			"id": "vAnWGF3B",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -532.3019339352195,
			"y": 188.85710361291277,
			"strokeColor": "#5c940d",
			"backgroundColor": "transparent",
			"width": 19,
			"height": 36,
			"seed": 95643813,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802969279,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "4",
			"rawText": "4",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4"
		},
		{
			"type": "text",
			"version": 37,
			"versionNonce": 170015429,
			"isDeleted": false,
			"id": "m0QPo5Nm",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 396.78440565112004,
			"y": 180.80347205928132,
			"strokeColor": "#5c940d",
			"backgroundColor": "transparent",
			"width": 16,
			"height": 36,
			"seed": 1811611301,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646802978867,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "7",
			"rawText": "7",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "7"
		},
		{
			"type": "text",
			"version": 223,
			"versionNonce": 958594821,
			"isDeleted": false,
			"id": "maej18of",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 60,
			"angle": 0,
			"x": -446.1821229893218,
			"y": -247.0831739034519,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 967,
			"height": 107,
			"seed": 1562982917,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646803126636,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "Decode Number 1\nInterconnection between 4 decodes for create a more complex decode\nwith a muxtiplixer inside. ",
			"rawText": "Decode Number 1\nInterconnection between 4 decodes for create a more complex decode\nwith a muxtiplixer inside. ",
			"baseline": 97,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Decode Number 1\nInterconnection between 4 decodes for create a more complex decode\nwith a muxtiplixer inside. "
		},
		{
			"type": "line",
			"version": 72,
			"versionNonce": 1335676773,
			"isDeleted": false,
			"id": "ywiVKVbuF1QWlpVipVGcr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 60,
			"angle": 0,
			"x": -222.07821120062601,
			"y": 694.1838911570911,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 420.9090909090909,
			"height": 0,
			"seed": 1893127781,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646803341666,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					420.9090909090909,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 86,
			"versionNonce": 2094144683,
			"isDeleted": false,
			"id": "ovpeA0QG1_sNztk-aYx0a",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 60,
			"angle": 0,
			"x": 200.40847889925848,
			"y": 693.3933773230988,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 333.6363636363635,
			"seed": 1182994917,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646803335689,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "b0LpR09O95WkjYxNHGyzv",
				"focus": 0.042334278541427205,
				"gap": 31.710885774958
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					-333.6363636363635
				]
			]
		},
		{
			"type": "text",
			"version": 12,
			"versionNonce": 842095563,
			"isDeleted": false,
			"id": "CNsqW9gg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 60,
			"angle": 0,
			"x": -262.84138071608396,
			"y": 675.1127090338492,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 21,
			"height": 36,
			"seed": 1282906821,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646803353263,
			"link": null,
			"fontSize": 28,
			"fontFamily": 1,
			"text": "2",
			"rawText": "2",
			"baseline": 25,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "2"
		},
		{
			"type": "line",
			"version": 38,
			"versionNonce": 818654853,
			"isDeleted": false,
			"id": "9fNVZIWVIEZv3BxZyR3GE",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 60,
			"angle": 0,
			"x": -61.174714049417275,
			"y": 661.7793757005159,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 34.16666666666663,
			"height": 59,
			"seed": 1523165803,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646803366233,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-34.16666666666663,
					59
				]
			]
		},
		{
			"type": "line",
			"version": 55,
			"versionNonce": 850548363,
			"isDeleted": false,
			"id": "h8TgEkqBhyKUTI4L_sY0W",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 60,
			"angle": 0,
			"x": -39.92471404941733,
			"y": 658.9460423671826,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 34.16666666666663,
			"height": 59,
			"seed": 637200107,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646803375099,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-34.16666666666663,
					59
				]
			]
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 60,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 28,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%