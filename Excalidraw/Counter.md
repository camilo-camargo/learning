---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
Frequency
50MHZ ^urXR7tBt

Counter_decimal ^sZLzvkfq

Counter_binary ^cRtvrsvO

Decode_7seg ^WcMRE2AO

top_level ^iyUmYaH9

top_level_clk ^XfV3vdog

top_level_reset ^ucR4AylY

top_level_mode ^ZR3v6NpX

top_level_stop ^n4r5ARFr

frequency
1. frequency_clk_in
2. frequency_clk_out ^La4AbOje

1 ^gazq4xTo

2 ^39DpyJaw

1 ^qDrrmG39

2 ^NNmqHOy4

3 ^7S3GJyYN

4 ^7TaVJHrJ

Counter_decimal
1. Couter_decimal_clk
2. Counter_decimal_reset
3. Counter_decimal_mode
4. Counter_decimal_stop
5. Counter_decimal_out ^OA2pgm4n

5 ^bmF4yfCl

1 ^kQbACk2W

2 ^VwhcbHAX

3 ^HPwCLfa3

4 ^wtwVkTFU

Counter_binary
1. Couter_binary_clk
2. Counter_binary_reset
3. Counter_binary_mode
4. Counter_binary_stop
5. Counter_binary_out ^m8SgHJZt

5 ^Tj3znCTP

Decode_7seg
1. Decode_7seg_in
2. Decode_7seg_out ^vFUFk6xR

9 ^wRzasENA

4 ^y9dvXDsW

s0 ^8EKT2Uhh

s0 ^yKq1oSdW

s1 ^d98peW5d

top_level_out_7seg ^XCnDRS3t

top_level_out_led ^bIbm7jw8

1 ^4kfMZWEr

2 ^so12LroP

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "rectangle",
			"version": 155,
			"versionNonce": 259930036,
			"isDeleted": false,
			"id": "aiNQCIaOAvc8r1awW3BGc",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -154.2584581251269,
			"y": -142.44711501877026,
			"strokeColor": "#5f3dc4",
			"backgroundColor": "transparent",
			"width": 171.96971546519882,
			"height": 87.87877863103691,
			"seed": 8325556,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "DCzecHsga_dG-NiR78Noz",
					"type": "arrow"
				},
				{
					"id": "G2ZYcVwxtQFkZMKywAtZh",
					"type": "arrow"
				}
			],
			"updated": 1648693783906,
			"link": null
		},
		{
			"type": "text",
			"version": 229,
			"versionNonce": 1483296140,
			"isDeleted": false,
			"id": "urXR7tBt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -115.37278978752369,
			"y": -120.33307287703339,
			"strokeColor": "#5f3dc4",
			"backgroundColor": "transparent",
			"width": 93,
			"height": 50,
			"seed": 236229004,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Frequency\n50MHZ",
			"rawText": "Frequency\n50MHZ",
			"baseline": 43,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Frequency\n50MHZ"
		},
		{
			"type": "rectangle",
			"version": 369,
			"versionNonce": 935665972,
			"isDeleted": false,
			"id": "TIsPj8kmeYSgJq3wAy1mi",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 197.43928820677468,
			"y": -137.80310551807133,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 187.20534199416034,
			"height": 80.72387387054133,
			"seed": 1883469236,
			"groupIds": [
				"LG-PVBUYXkLc-HuUil_59"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "SIfQzUyDC_7DJjdCFMvxx",
					"type": "arrow"
				},
				{
					"id": "kk5oNuUIGCL1xy487_FAA",
					"type": "arrow"
				},
				{
					"id": "aF29JXHq9Qg4y1ASrCrUL",
					"type": "arrow"
				},
				{
					"id": "x2we-gOViaMvsWkV6-klG",
					"type": "arrow"
				},
				{
					"id": "4qq_ua31MODfhaOH4iIsq",
					"type": "arrow"
				}
			],
			"updated": 1648693783907,
			"link": null
		},
		{
			"type": "text",
			"version": 379,
			"versionNonce": 1803533324,
			"isDeleted": false,
			"id": "sZLzvkfq",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 210.54195920385484,
			"y": -109.94116858280067,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 161,
			"height": 25,
			"seed": 2033865740,
			"groupIds": [
				"LG-PVBUYXkLc-HuUil_59"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Counter_decimal",
			"rawText": "Counter_decimal",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Counter_decimal"
		},
		{
			"type": "rectangle",
			"version": 154,
			"versionNonce": 1517476532,
			"isDeleted": false,
			"id": "vA71O47gCW2TsJUToKVzG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 243.71161227756045,
			"y": 88.5437030214251,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 263.636308149858,
			"height": 101.51514226740056,
			"seed": 2053738164,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "fnhiRLNQdG528vLoT-6jz",
					"type": "arrow"
				},
				{
					"id": "fZB-IBy32JKrwJqc0Ix5s",
					"type": "arrow"
				},
				{
					"id": "AXXBR8_LK2bOdtHGDHQk1",
					"type": "arrow"
				},
				{
					"id": "rMpfBVmSDL-rOd7zvjJe8",
					"type": "arrow"
				},
				{
					"id": "r8niG3OUSsvTl3fEpq_yt",
					"type": "arrow"
				}
			],
			"updated": 1648693783907,
			"link": null
		},
		{
			"type": "text",
			"version": 126,
			"versionNonce": 1589926540,
			"isDeleted": false,
			"id": "cRtvrsvO",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 301.52976635248945,
			"y": 126.80127415512538,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 148,
			"height": 25,
			"seed": 898784396,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Counter_binary",
			"rawText": "Counter_binary",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Counter_binary"
		},
		{
			"type": "rectangle",
			"version": 122,
			"versionNonce": 1059083828,
			"isDeleted": false,
			"id": "wDqZgwilpnyljkVlEjyMA",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 598.1310323079424,
			"y": -136.62457830255687,
			"strokeColor": "#364fc7",
			"backgroundColor": "transparent",
			"width": 197.72727272727263,
			"height": 93.18181818181819,
			"seed": 490041228,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "D0vfVJ4HCQH50OBLsCRfn",
					"type": "arrow"
				},
				{
					"id": "bzxWIjgrXLOjOgUhPqZcF",
					"type": "arrow"
				},
				{
					"id": "Sz9ROh9W3QzjAmf95kcZu",
					"type": "arrow"
				}
			],
			"updated": 1648693918453,
			"link": null
		},
		{
			"type": "text",
			"version": 67,
			"versionNonce": 1827744012,
			"isDeleted": false,
			"id": "WcMRE2AO",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 635.4037595806697,
			"y": -102.53366921164778,
			"strokeColor": "#364fc7",
			"backgroundColor": "transparent",
			"width": 130,
			"height": 25,
			"seed": 712246796,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Decode_7seg",
			"rawText": "Decode_7seg",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Decode_7seg"
		},
		{
			"type": "rectangle",
			"version": 162,
			"versionNonce": 1960492468,
			"isDeleted": false,
			"id": "i9N0PDzssVa6xBkxXNgCK",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -336.29811054356423,
			"y": -337.0226098703274,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 1339.9998168945312,
			"height": 656.6666259765625,
			"seed": 1890688436,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "C_xZ8EIX1gBpawVZp6ygt",
					"type": "arrow"
				},
				{
					"id": "YleVOGl-2ByxyqjuJtJTd",
					"type": "arrow"
				},
				{
					"id": "3HcYP7OAmCYFbfdsXSS5u",
					"type": "arrow"
				},
				{
					"id": "4bMgi5yvhSMWcScYIjfTi",
					"type": "arrow"
				}
			],
			"updated": 1648693783907,
			"link": null
		},
		{
			"type": "text",
			"version": 79,
			"versionNonce": 1770006412,
			"isDeleted": false,
			"id": "iyUmYaH9",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -327.96461444981423,
			"y": -381.0226098703274,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 93,
			"height": 25,
			"seed": 810718860,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "top_level",
			"rawText": "top_level",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "top_level"
		},
		{
			"type": "arrow",
			"version": 221,
			"versionNonce": 195094324,
			"isDeleted": false,
			"id": "C_xZ8EIX1gBpawVZp6ygt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -415.985422302304,
			"y": -173.45111154443464,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 61.83623318033557,
			"height": 0,
			"seed": 759651892,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "i9N0PDzssVa6xBkxXNgCK",
				"gap": 17.851078578404213,
				"focus": 0.5018126645841416
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					61.83623318033557,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 157,
			"versionNonce": 682317324,
			"isDeleted": false,
			"id": "XfV3vdog",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -571.4364503559314,
			"y": -187.00657693963302,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 134,
			"height": 25,
			"seed": 1592473780,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "top_level_clk",
			"rawText": "top_level_clk",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "top_level_clk"
		},
		{
			"type": "arrow",
			"version": 435,
			"versionNonce": 182837428,
			"isDeleted": false,
			"id": "YleVOGl-2ByxyqjuJtJTd",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -413.99511476292696,
			"y": -101.42778381222541,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 61.83623318033551,
			"height": 1.4210854715202004e-14,
			"seed": 1968065588,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": {
				"elementId": "ucR4AylY",
				"gap": 13.242745582924272,
				"focus": 0.3344372316158705
			},
			"endBinding": {
				"elementId": "i9N0PDzssVa6xBkxXNgCK",
				"gap": 15.860771039027213,
				"focus": 0.2824522619594474
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					61.83623318033551,
					-1.4210854715202004e-14
				]
			]
		},
		{
			"type": "text",
			"version": 249,
			"versionNonce": 1735177356,
			"isDeleted": false,
			"id": "ucR4AylY",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -589.2378603458512,
			"y": -118.1082492074238,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 162,
			"height": 25,
			"seed": 112378124,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "YleVOGl-2ByxyqjuJtJTd",
					"type": "arrow"
				}
			],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "top_level_reset",
			"rawText": "top_level_reset",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "top_level_reset"
		},
		{
			"type": "text",
			"version": 84,
			"versionNonce": 441428532,
			"isDeleted": false,
			"id": "ZR3v6NpX",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -596.3233709642213,
			"y": -36.066493531154265,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 155,
			"height": 25,
			"seed": 675203124,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "3HcYP7OAmCYFbfdsXSS5u",
					"type": "arrow"
				}
			],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "top_level_mode",
			"rawText": "top_level_mode",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "top_level_mode"
		},
		{
			"type": "arrow",
			"version": 463,
			"versionNonce": 863912716,
			"isDeleted": false,
			"id": "3HcYP7OAmCYFbfdsXSS5u",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -427.2414112604438,
			"y": -20.81649353115427,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 61.83623318033551,
			"height": 3.552713678800501e-15,
			"seed": 2040791564,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": {
				"elementId": "ZR3v6NpX",
				"gap": 14.081959703777557,
				"focus": 0.22000000000000003
			},
			"endBinding": {
				"elementId": "i9N0PDzssVa6xBkxXNgCK",
				"gap": 29.107067536544037,
				"focus": 0.03693562660070664
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					61.83623318033551,
					-3.552713678800501e-15
				]
			]
		},
		{
			"type": "text",
			"version": 53,
			"versionNonce": 2093237172,
			"isDeleted": false,
			"id": "n4r5ARFr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -596.5316534349245,
			"y": 22.891827086521516,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 153,
			"height": 25,
			"seed": 847736756,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "top_level_stop",
			"rawText": "top_level_stop",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "top_level_stop"
		},
		{
			"type": "arrow",
			"version": 404,
			"versionNonce": 1398407564,
			"isDeleted": false,
			"id": "4bMgi5yvhSMWcScYIjfTi",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -425.3664112604438,
			"y": 35.12276351569997,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 61.83623318033551,
			"height": 7.105427357601002e-15,
			"seed": 649068980,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": null,
			"endBinding": {
				"elementId": "i9N0PDzssVa6xBkxXNgCK",
				"gap": 27.232067536544037,
				"focus": -0.13343775567272362
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					61.83623318033551,
					7.105427357601002e-15
				]
			]
		},
		{
			"type": "arrow",
			"version": 59,
			"versionNonce": 678014260,
			"isDeleted": false,
			"id": "DCzecHsga_dG-NiR78Noz",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -174.44025595267277,
			"y": -134.68150592710384,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 15.476205008370528,
			"height": 0,
			"seed": 724827316,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": {
				"elementId": "aiNQCIaOAvc8r1awW3BGc",
				"focus": -0.8232654296603126,
				"gap": 20.181797827545864
			},
			"endBinding": {
				"elementId": "gazq4xTo",
				"focus": -1.3752109973672448,
				"gap": 9.802451758736538
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					15.476205008370528,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 104,
			"versionNonce": 775548940,
			"isDeleted": false,
			"id": "G2ZYcVwxtQFkZMKywAtZh",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 21.946946480046705,
			"y": -100.36578964312508,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 14.035098427220433,
			"height": 0,
			"seed": 1749231284,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": {
				"elementId": "aiNQCIaOAvc8r1awW3BGc",
				"focus": -0.042286976874688756,
				"gap": 4.235689139974795
			},
			"endBinding": {
				"elementId": "weBRkUztjJd1ChRKMqc4P",
				"focus": -1.5327568349594,
				"gap": 6.727679037497241
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					14.035098427220433,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 166,
			"versionNonce": 1587930804,
			"isDeleted": false,
			"id": "La4AbOje",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -148.72762469854223,
			"y": -229.1211796212267,
			"strokeColor": "#5f3dc4",
			"backgroundColor": "transparent",
			"width": 171,
			"height": 60,
			"seed": 347709708,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "frequency\n1. frequency_clk_in\n2. frequency_clk_out",
			"rawText": "frequency\n1. frequency_clk_in\n2. frequency_clk_out",
			"baseline": 54,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "frequency\n1. frequency_clk_in\n2. frequency_clk_out"
		},
		{
			"type": "line",
			"version": 34,
			"versionNonce": 873991820,
			"isDeleted": false,
			"id": "s8pJPBX8OsHHHrnR04xmk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -335.6712355434407,
			"y": -177.6240957242974,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 143.45238821847096,
			"height": 0,
			"seed": 692363572,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					143.45238821847096,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 35,
			"versionNonce": 1142878260,
			"isDeleted": false,
			"id": "b7bwX4uOO4zndjU3Kza8P",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -192.21884732496972,
			"y": -179.11219822848267,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 47.91667393275668,
			"seed": 1223313844,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					47.91667393275668
				]
			]
		},
		{
			"type": "line",
			"version": 38,
			"versionNonce": 1169054988,
			"isDeleted": false,
			"id": "f6c8Ap9l8KAa7XcFNo4wZ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -192.21884732496972,
			"y": -132.98123858144027,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 22.321428571428612,
			"height": 0,
			"seed": 1747446156,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					22.321428571428612,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 27,
			"versionNonce": 2025507252,
			"isDeleted": false,
			"id": "gazq4xTo",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -173.76650270303878,
			"y": -158.4336159007763,
			"strokeColor": "#5f3dc4",
			"backgroundColor": "transparent",
			"width": 5,
			"height": 20,
			"seed": 924946612,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "DCzecHsga_dG-NiR78Noz",
					"type": "arrow"
				}
			],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "1",
			"rawText": "1",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "1"
		},
		{
			"type": "ellipse",
			"version": 71,
			"versionNonce": 477551500,
			"isDeleted": false,
			"id": "Cpc2Tgb0pgyf4eXBJ0ldM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -183.58790947619727,
			"y": -162.74313607725506,
			"strokeColor": "#5f3dc4",
			"backgroundColor": "transparent",
			"width": 26.19046892438618,
			"height": 26.19046892438618,
			"seed": 764262540,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null
		},
		{
			"type": "text",
			"version": 53,
			"versionNonce": 798978868,
			"isDeleted": false,
			"id": "39DpyJaw",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 31.675685230820534,
			"y": -126.90859627812094,
			"strokeColor": "#5f3dc4",
			"backgroundColor": "transparent",
			"width": 12,
			"height": 20,
			"seed": 711261068,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "2",
			"rawText": "2",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "2"
		},
		{
			"type": "ellipse",
			"version": 79,
			"versionNonce": 850102796,
			"isDeleted": false,
			"id": "weBRkUztjJd1ChRKMqc4P",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 27.664333449958747,
			"y": -130.64875180024828,
			"strokeColor": "#5f3dc4",
			"backgroundColor": "transparent",
			"width": 23.913043478260818,
			"height": 23.913043478260818,
			"seed": 1814857868,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "G2ZYcVwxtQFkZMKywAtZh",
					"type": "arrow"
				}
			],
			"updated": 1648693783907,
			"link": null
		},
		{
			"type": "arrow",
			"version": 32,
			"versionNonce": 1194519732,
			"isDeleted": false,
			"id": "SIfQzUyDC_7DJjdCFMvxx",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 168.49029754039344,
			"y": -128.8225313183051,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 21.153846153846132,
			"height": 0,
			"seed": 1184217268,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783907,
			"link": null,
			"startBinding": {
				"elementId": "qDrrmG39",
				"focus": 1.5243482490168985,
				"gap": 5.555609783001586
			},
			"endBinding": {
				"elementId": "TIsPj8kmeYSgJq3wAy1mi",
				"focus": 0.7774989289000529,
				"gap": 7.795144512535103
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					21.153846153846132,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 72,
			"versionNonce": 726034572,
			"isDeleted": false,
			"id": "kk5oNuUIGCL1xy487_FAA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 164.4231164141588,
			"y": -111.728516832628,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 28.426155485208938,
			"height": 0,
			"seed": 1689783860,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783908,
			"link": null,
			"startBinding": {
				"elementId": "7S3GJyYN",
				"focus": -1.1397927567607495,
				"gap": 1.3979275676074963
			},
			"endBinding": {
				"elementId": "TIsPj8kmeYSgJq3wAy1mi",
				"focus": 0.3539807386533573,
				"gap": 4.5900163074068985
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					28.426155485208938,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 40,
			"versionNonce": 1629173300,
			"isDeleted": false,
			"id": "aF29JXHq9Qg4y1ASrCrUL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 168.062941896363,
			"y": -93.77980670893008,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 22.008541791866975,
			"height": 0,
			"seed": 1829821620,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783908,
			"link": null,
			"startBinding": {
				"elementId": "7TaVJHrJ",
				"focus": -1.1901605662380559,
				"gap": 1.901605662380561
			},
			"endBinding": {
				"elementId": "TIsPj8kmeYSgJq3wAy1mi",
				"focus": -0.0907132350893466,
				"gap": 7.367804518544716
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					22.008541791866975,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 64,
			"versionNonce": 973174540,
			"isDeleted": false,
			"id": "x2we-gOViaMvsWkV6-klG",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 156.30575926542002,
			"y": -75.40373311618168,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 34.193080066840366,
			"height": 0,
			"seed": 1716245772,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783908,
			"link": null,
			"startBinding": {
				"elementId": "7S3GJyYN",
				"focus": 2.492685614883882,
				"gap": 14.926856148838823
			},
			"endBinding": {
				"elementId": "TIsPj8kmeYSgJq3wAy1mi",
				"focus": -0.5459954883225973,
				"gap": 6.94044887451426
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					34.193080066840366,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 42,
			"versionNonce": 1262152844,
			"isDeleted": false,
			"id": "MYUAB2QDJBq6afmb9GnEN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 42.06733585455635,
			"y": -102.03795846770595,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 54.29291696259469,
			"height": 0,
			"seed": 25545268,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693797374,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					54.29291696259469,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 106,
			"versionNonce": 1268838964,
			"isDeleted": false,
			"id": "iK4cItKt0OtyYE4lup3xP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 94.59258221464157,
			"y": -103.0480625603906,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 1.282803405174775,
			"height": 23.473409285373748,
			"seed": 804133044,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693797375,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					1.282803405174775,
					-23.473409285373748
				]
			]
		},
		{
			"type": "line",
			"version": 63,
			"versionNonce": 1891990284,
			"isDeleted": false,
			"id": "DVaXmc4BL5b-IwqI-uAOE",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 94.59258221464157,
			"y": -129.56320482779114,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 79.04037937973487,
			"height": 0,
			"seed": 1151427980,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693797375,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					79.04037937973487,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 38,
			"versionNonce": 746595340,
			"isDeleted": false,
			"id": "QgAJcEEFIgZUUJHdcwKIv",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -338.06512562144553,
			"y": -103.70208395202451,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 126.04164123535156,
			"height": 0,
			"seed": 1818860940,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					126.04164123535156,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 38,
			"versionNonce": 1980560052,
			"isDeleted": false,
			"id": "xCwuC55O_p7gHctemhFuT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -217.2317668567971,
			"y": -105.78540456970029,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0.9756898094908024,
			"height": 64.49558842403611,
			"seed": 1903868044,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0.9756898094908024,
					64.49558842403611
				]
			]
		},
		{
			"type": "line",
			"version": 56,
			"versionNonce": 1937228428,
			"isDeleted": false,
			"id": "Ro3Rh1Cx7YF33u_s-KvQk",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -214.83141338363134,
			"y": -42.589574989747604,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 276.04156494140625,
			"height": 0,
			"seed": 861504948,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					276.04156494140625,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 141,
			"versionNonce": 1925479476,
			"isDeleted": false,
			"id": "Kn7I1anCJLdwva_2uU2oL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 60.124314796968065,
			"y": -41.69399099922934,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0.7969478080491541,
			"height": 73.64736256441408,
			"seed": 1908073100,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0.7969478080491541,
					-73.64736256441408
				]
			]
		},
		{
			"type": "line",
			"version": 75,
			"versionNonce": 1727500556,
			"isDeleted": false,
			"id": "teh0mOPHTIV1lPMRyzm-K",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 172.76821134493287,
			"y": -111.41041001926783,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 113.09522356305806,
			"height": 0,
			"seed": 25952948,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-113.09522356305806,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 34,
			"versionNonce": 1389286836,
			"isDeleted": false,
			"id": "F9-6Ie9cHhW3WsrP3pbF2",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -336.1229625023847,
			"y": -16.96156007551116,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 425.69442749023443,
			"height": 0,
			"seed": 1980568972,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					425.69442749023443,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 69,
			"versionNonce": 645500812,
			"isDeleted": false,
			"id": "R1w6tAfC0n2x8te6PrFIr",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -351.4007233259524,
			"y": 36.51065366960603,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 490.9721883138021,
			"height": 0,
			"seed": 867083148,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					490.9721883138021,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 59,
			"versionNonce": 1129447220,
			"isDeleted": false,
			"id": "uwBFft9VGSCrurd--MdHW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 137.27980756893393,
			"y": 36.59274133088678,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 1.5151515151515298,
			"height": 111.36362711588542,
			"seed": 245260,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					1.5151515151515298,
					-111.36362711588542
				]
			]
		},
		{
			"type": "line",
			"version": 36,
			"versionNonce": 1379384844,
			"isDeleted": false,
			"id": "tUk_frbb75ofHekLZDbA8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 167.07779969724834,
			"y": -75.2759332074655,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 29.54545454545456,
			"height": 0.7575757575757507,
			"seed": 1969429684,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-29.54545454545456,
					0.7575757575757507
				]
			]
		},
		{
			"type": "line",
			"version": 55,
			"versionNonce": 1555298484,
			"isDeleted": false,
			"id": "qNatn-a83NmV92jBch5s-",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 167.58283787196424,
			"y": -93.96280343562601,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 77.32719254462106,
			"height": 1.3034190011416626,
			"seed": 509973772,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-77.32719254462106,
					1.3034190011416626
				]
			]
		},
		{
			"type": "line",
			"version": 43,
			"versionNonce": 1932270732,
			"isDeleted": false,
			"id": "qdGU3vmHylZ1nm-OTWkrQ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 90.31011059923698,
			"y": -16.43755245166531,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 1.2626324277935623,
			"height": 79.79797825668797,
			"seed": 1401393164,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					1.2626324277935623,
					-79.79797825668797
				]
			]
		},
		{
			"type": "arrow",
			"version": 38,
			"versionNonce": 983220788,
			"isDeleted": false,
			"id": "4qq_ua31MODfhaOH4iIsq",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 394.08660179885567,
			"y": -104.18466365234325,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 27.8985595703125,
			"height": 0,
			"seed": 1991724468,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783908,
			"link": null,
			"startBinding": {
				"elementId": "bmF4yfCl",
				"focus": 2.3341654468452666,
				"gap": 13.34165446845266
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					27.8985595703125,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 39,
			"versionNonce": 523448844,
			"isDeleted": false,
			"id": "D0vfVJ4HCQH50OBLsCRfn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 587.795188891278,
			"y": -95.98072026946403,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.5280128767051337,
			"height": 0,
			"seed": 272536588,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693901465,
			"link": null,
			"startBinding": {
				"elementId": "4kfMZWEr",
				"focus": 2.32643717628616,
				"gap": 15.728679859998465
			},
			"endBinding": {
				"elementId": "wDqZgwilpnyljkVlEjyMA",
				"focus": 0.12764402270434885,
				"gap": 10.863856293369452
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					-0.5280128767051337,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 113,
			"versionNonce": 1198836276,
			"isDeleted": false,
			"id": "QAhUGPuY6Ij5oEtzo_0Hg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 848.9555263424332,
			"y": -93.80682049449803,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 240.09623790933836,
			"height": 0,
			"seed": 1852194956,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693913379,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					240.09623790933836,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 120,
			"versionNonce": 878668172,
			"isDeleted": false,
			"id": "qDrrmG39",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 165.85711831605147,
			"y": -155.56867542050318,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 5.297633579799124,
			"height": 21.190534319196495,
			"seed": 184380556,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "SIfQzUyDC_7DJjdCFMvxx",
					"type": "arrow"
				}
			],
			"updated": 1648693783908,
			"link": null,
			"fontSize": 16.952427455357196,
			"fontFamily": 1,
			"text": "1",
			"rawText": "1",
			"baseline": 14.190534319196495,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "1"
		},
		{
			"type": "text",
			"version": 14,
			"versionNonce": 1389282612,
			"isDeleted": false,
			"id": "NNmqHOy4",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 163.77379224880815,
			"y": -128.48534390369238,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 12,
			"height": 20,
			"seed": 1078222772,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "2",
			"rawText": "2",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "2"
		},
		{
			"type": "text",
			"version": 31,
			"versionNonce": 441604108,
			"isDeleted": false,
			"id": "7S3GJyYN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 165.26187295472332,
			"y": -110.3305892650205,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 12,
			"height": 20,
			"seed": 1573847732,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "kk5oNuUIGCL1xy487_FAA",
					"type": "arrow"
				},
				{
					"id": "x2we-gOViaMvsWkV6-klG",
					"type": "arrow"
				}
			],
			"updated": 1648693783908,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "3",
			"rawText": "3",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "3"
		},
		{
			"type": "text",
			"version": 25,
			"versionNonce": 749207220,
			"isDeleted": false,
			"id": "7TaVJHrJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 164.07140403033716,
			"y": -91.87820104654952,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 11,
			"height": 20,
			"seed": 528789004,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "aF29JXHq9Qg4y1ASrCrUL",
					"type": "arrow"
				}
			],
			"updated": 1648693783908,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "4",
			"rawText": "4",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4"
		},
		{
			"type": "text",
			"version": 275,
			"versionNonce": 1357084300,
			"isDeleted": false,
			"id": "OA2pgm4n",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 191.20513050248275,
			"y": -281.1626478487401,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 207,
			"height": 120,
			"seed": 238165300,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": null,
			"updated": 1648693783908,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "Counter_decimal\n1. Couter_decimal_clk\n2. Counter_decimal_reset\n3. Counter_decimal_mode\n4. Counter_decimal_stop\n5. Counter_decimal_out",
			"rawText": "Counter_decimal\n1. Couter_decimal_clk\n2. Counter_decimal_reset\n3. Counter_decimal_mode\n4. Counter_decimal_stop\n5. Counter_decimal_out",
			"baseline": 114,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Counter_decimal\n1. Couter_decimal_clk\n2. Counter_decimal_reset\n3. Counter_decimal_mode\n4. Counter_decimal_stop\n5. Counter_decimal_out"
		},
		{
			"type": "text",
			"version": 14,
			"versionNonce": 975046708,
			"isDeleted": false,
			"id": "bmF4yfCl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 407.19835744968884,
			"y": -137.5263181207959,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"width": 11,
			"height": 20,
			"seed": 738280332,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "4qq_ua31MODfhaOH4iIsq",
					"type": "arrow"
				}
			],
			"updated": 1648693783908,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "5",
			"rawText": "5",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "5"
		},
		{
			"type": "arrow",
			"version": 75,
			"versionNonce": 80528652,
			"isDeleted": false,
			"id": "fnhiRLNQdG528vLoT-6jz",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 213.68351576813708,
			"y": 102.76417215420126,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 27.853284035672317,
			"height": 0.3190278902649908,
			"seed": 2078488628,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783908,
			"link": null,
			"startBinding": {
				"elementId": "VwhcbHAX",
				"focus": -1.5173624338685698,
				"gap": 5.328758720586606
			},
			"endBinding": {
				"elementId": "vA71O47gCW2TsJUToKVzG",
				"focus": 0.663574937695284,
				"gap": 2.1748124737510466
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					27.853284035672317,
					0.3190278902649908
				]
			]
		},
		{
			"type": "arrow",
			"version": 57,
			"versionNonce": 253157812,
			"isDeleted": false,
			"id": "fZB-IBy32JKrwJqc0Ix5s",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 210.2467518077449,
			"y": 130.23584520949274,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 27.35043256710736,
			"height": 0,
			"seed": 1424857396,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783908,
			"link": null,
			"startBinding": {
				"elementId": "VwhcbHAX",
				"focus": 1.2142914334704868,
				"gap": 2.1429143347048694
			},
			"endBinding": {
				"elementId": "vA71O47gCW2TsJUToKVzG",
				"focus": 0.1786024969901227,
				"gap": 6.114427902708201
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					27.35043256710736,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 40,
			"versionNonce": 998785932,
			"isDeleted": false,
			"id": "AXXBR8_LK2bOdtHGDHQk1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 212.8464660380133,
			"y": 155.68099364490612,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 23.931618714943937,
			"height": 0,
			"seed": 388351924,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783908,
			"link": null,
			"startBinding": {
				"elementId": "wtwVkTFU",
				"focus": -1.20496006979524,
				"gap": 2.0496006979523997
			},
			"endBinding": {
				"elementId": "vA71O47gCW2TsJUToKVzG",
				"focus": -0.32270495068873567,
				"gap": 6.933527524603221
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					23.931618714943937,
					0
				]
			]
		},
		{
			"type": "arrow",
			"version": 49,
			"versionNonce": 1387825972,
			"isDeleted": false,
			"id": "rMpfBVmSDL-rOd7zvjJe8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 211.2735610068967,
			"y": 173.92905918593962,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 27.5640869140625,
			"height": 0,
			"seed": 1912311476,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1648693783908,
			"link": null,
			"startBinding": {
				"elementId": "wtwVkTFU",
				"focus": 0.6198464843081097,
				"gap": 1
			},
			"endBinding": {
				"elementId": "vA71O47gCW2TsJUToKVzG",
				"focus": -0.6822191105165641,
				"gap": 4.873964356601249
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					27.5640869140625,
					0
				]
			]
		},
		{
			"type": "ellipse",
			"version": 78,
			"versionNonce": 995095052,
			"isDeleted": false,
			"id": "nGRTgmhISE74NGqOljIZE",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 72.32441889684542,
			"y": -103.1695029377117,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 1.6129032258064484,
			"height": 1.2096774193548328,
			"seed": 1787023628,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null
		},
		{
			"type": "line",
			"version": 100,
			"versionNonce": 861708428,
			"isDeleted": false,
			"id": "yupuKmaFYMcwE3oUCmluU",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 73.47883240430988,
			"y": -101.57989742170871,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 205.45897184511023,
			"seed": 724679436,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693811749,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					205.45897184511023
				]
			]
		},
		{
			"type": "line",
			"version": 27,
			"versionNonce": 916749876,
			"isDeleted": false,
			"id": "3ytrWYBOArJMBNl1tV4hg",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 73.75660340040363,
			"y": 102.97566491553084,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 140.5555623372396,
			"height": 0,
			"seed": 2144481460,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693811749,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					140.5555623372396,
					0
				]
			]
		},
		{
			"type": "freedraw",
			"version": 65,
			"versionNonce": 852381236,
			"isDeleted": false,
			"id": "ZjUOoi9P_goNB-CdFBJpE",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 71.47277729065223,
			"y": -103.2443321001211,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 1.8000000000000114,
			"height": 2.6000000000000085,
			"seed": 2128304908,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.0666674804687517
				],
				[
					-0.06666503906249943,
					0.1333349609375034
				],
				[
					-0.1333349609375034,
					0.5333349609375091
				],
				[
					-0.2666650390625023,
					0.8666674804687489
				],
				[
					-0.2666650390625023,
					1.3333349609375063
				],
				[
					-0.2666650390625023,
					1.666667480468746
				],
				[
					-0.2666650390625023,
					2
				],
				[
					-0.19999999999998863,
					2.2666674804687545
				],
				[
					0,
					2.4666674804687574
				],
				[
					0.20000000000000284,
					2.6000000000000085
				],
				[
					0.4000000000000057,
					2.6000000000000085
				],
				[
					0.5999999999999943,
					2.6000000000000085
				],
				[
					0.8666650390624966,
					2.533334960937509
				],
				[
					1.1333349609375034,
					2.1333349609375034
				],
				[
					1.2666650390625023,
					1.9333349609375006
				],
				[
					1.333334960937492,
					1.733334960937512
				],
				[
					1.4000000000000057,
					1.6000000000000085
				],
				[
					1.4000000000000057,
					1.4000000000000057
				],
				[
					1.4000000000000057,
					1.1333349609375034
				],
				[
					1.4000000000000057,
					0.8666674804687489
				],
				[
					1.2666650390625023,
					0.666667480468746
				],
				[
					1.1333349609375034,
					0.5333349609375091
				],
				[
					0.9333349609375006,
					0.5333349609375091
				],
				[
					0.666665039062508,
					0.4666674804687574
				],
				[
					0.4666650390625051,
					0.4000000000000057
				],
				[
					0.4000000000000057,
					0.4000000000000057
				],
				[
					0.33333496093749204,
					0.4000000000000057
				],
				[
					0.20000000000000284,
					0.6000000000000085
				],
				[
					0.20000000000000284,
					0.9333349609375006
				],
				[
					0.1333349609375034,
					1.3333349609375063
				],
				[
					0.06666503906249943,
					1.666667480468746
				],
				[
					0.06666503906249943,
					1.7999999999999972
				],
				[
					0.06666503906249943,
					1.9333349609375006
				],
				[
					0.06666503906249943,
					2
				],
				[
					0.06666503906249943,
					2.1333349609375034
				],
				[
					0.1333349609375034,
					2.200000000000003
				],
				[
					0.20000000000000284,
					2.2666674804687545
				],
				[
					0.2666650390625023,
					2.200000000000003
				],
				[
					0.33333496093749204,
					2.200000000000003
				],
				[
					0.5999999999999943,
					2.200000000000003
				],
				[
					0.7333349609374977,
					2.200000000000003
				],
				[
					0.9333349609375006,
					2.200000000000003
				],
				[
					1.0666650390624994,
					2.200000000000003
				],
				[
					1.333334960937492,
					2.1333349609375034
				],
				[
					1.4000000000000057,
					2.0666674804687517
				],
				[
					1.4000000000000057,
					1.9333349609375006
				],
				[
					1.4666650390625051,
					1.7999999999999972
				],
				[
					1.4666650390625051,
					1.733334960937512
				],
				[
					1.533334960937509,
					1.533334960937509
				],
				[
					1.4000000000000057,
					1.4666674804687574
				],
				[
					1.333334960937492,
					1.4000000000000057
				],
				[
					1.4000000000000057,
					1.3333349609375063
				],
				[
					1.4000000000000057,
					1.3333349609375063
				]
			],
			"lastCommittedPoint": [
				1.4000000000000057,
				1.3333349609375063
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 117,
			"versionNonce": 1655430924,
			"isDeleted": false,
			"id": "4ck5YMMks6yZe6cPuxeEC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 72.73944232971473,
			"y": -102.3109971391836,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 3.7333349609374977,
			"height": 5.866667480468763,
			"seed": 2064448564,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.13333007812499886,
					-0.0666674804687517
				],
				[
					-0.20000000000000284,
					-0.0666674804687517
				],
				[
					-0.2666650390625023,
					-0.0666674804687517
				],
				[
					-0.3333300781250017,
					-0.0666674804687517
				],
				[
					-0.5333300781250045,
					-0.0666674804687517
				],
				[
					-0.5999999999999943,
					0
				],
				[
					-0.5999999999999943,
					0.13333251953125114
				],
				[
					-0.5999999999999943,
					0.4000000000000057
				],
				[
					-0.5999999999999943,
					0.666665039062508
				],
				[
					-0.4666650390624909,
					0.8000000000000114
				],
				[
					-0.3333300781250017,
					1.0666650390624994
				],
				[
					-0.20000000000000284,
					1.2000000000000028
				],
				[
					0,
					1.4666650390625051
				],
				[
					0.2666699218750068,
					1.666665039062508
				],
				[
					0.46666992187499545,
					1.666665039062508
				],
				[
					0.5333349609375091,
					1.666665039062508
				],
				[
					0.7999999999999972,
					1.6000000000000085
				],
				[
					1.0666699218749898,
					0.8000000000000114
				],
				[
					1.2000000000000028,
					-0.19999999999998863
				],
				[
					1.4000000000000057,
					-1.0666674804687517
				],
				[
					1.4000000000000057,
					-1.5999999999999943
				],
				[
					1.4000000000000057,
					-2
				],
				[
					1.333334960937492,
					-2.333334960937492
				],
				[
					1.1333349609375034,
					-2.533334960937495
				],
				[
					0.7333349609374977,
					-2.9333349609375006
				],
				[
					0.5333349609375091,
					-3.0666674804687517
				],
				[
					0.33333496093749204,
					-3.0666674804687517
				],
				[
					0.20000000000000284,
					-3.0666674804687517
				],
				[
					-0.13333007812499886,
					-3
				],
				[
					-0.4666650390624909,
					-2.7333349609374977
				],
				[
					-0.9333300781250102,
					-2.1999999999999886
				],
				[
					-1.2000000000000028,
					-1.9333349609375006
				],
				[
					-1.3333300781250017,
					-1.5333349609374949
				],
				[
					-1.3333300781250017,
					-1.2666674804687545
				],
				[
					-1.3333300781250017,
					-1.0666674804687517
				],
				[
					-1.3333300781250017,
					-0.5999999999999943
				],
				[
					-1.3333300781250017,
					-0.19999999999998863
				],
				[
					-1.2666650390625023,
					0.20000000000000284
				],
				[
					-1.0666650390624994,
					0.7333325195312455
				],
				[
					-1,
					0.9333325195312483
				],
				[
					-0.8666650390624966,
					1.2000000000000028
				],
				[
					-0.7999999999999972,
					1.2000000000000028
				],
				[
					-0.7333300781249932,
					1.2000000000000028
				],
				[
					-0.666665039062508,
					1.2000000000000028
				],
				[
					-0.5333300781250045,
					1.2000000000000028
				],
				[
					-0.4000000000000057,
					1.0666650390624994
				],
				[
					-0.3333300781250017,
					0.7333325195312455
				],
				[
					-0.3333300781250017,
					0.4000000000000057
				],
				[
					-0.2666650390625023,
					0.13333251953125114
				],
				[
					-0.2666650390625023,
					-0.1333349609375034
				],
				[
					-0.2666650390625023,
					-0.26666748046875455
				],
				[
					-0.2666650390625023,
					-0.4666674804687432
				],
				[
					-0.2666650390625023,
					-0.7333349609374977
				],
				[
					-0.4666650390624909,
					-1.0666674804687517
				],
				[
					-0.5999999999999943,
					-1.2666674804687545
				],
				[
					-0.666665039062508,
					-1.4666674804687432
				],
				[
					-0.7333300781249932,
					-1.5333349609374949
				],
				[
					-0.7999999999999972,
					-1.5333349609374949
				],
				[
					-0.9333300781250102,
					-1.5333349609374949
				],
				[
					-1.2000000000000028,
					-1.4666674804687432
				],
				[
					-1.2666650390625023,
					-1.3999999999999915
				],
				[
					-1.5333300781250045,
					-0.7333349609374977
				],
				[
					-1.5999999999999943,
					-0.3999999999999915
				],
				[
					-1.666665039062508,
					-0.0666674804687517
				],
				[
					-1.666665039062508,
					0.06666503906249943
				],
				[
					-1.666665039062508,
					0.4000000000000057
				],
				[
					-1.666665039062508,
					0.8666650390624966
				],
				[
					-1.5333300781250045,
					1.2666650390625023
				],
				[
					-1.2666650390625023,
					1.666665039062508
				],
				[
					-1.1333300781249989,
					2
				],
				[
					-1,
					2.133332519531251
				],
				[
					-0.7999999999999972,
					2.333332519531254
				],
				[
					-0.666665039062508,
					2.4000000000000057
				],
				[
					-0.4000000000000057,
					2.533332519531257
				],
				[
					0.20000000000000284,
					2.666665039062508
				],
				[
					0.6666699218749983,
					2.8000000000000114
				],
				[
					1.2666699218750068,
					2.8000000000000114
				],
				[
					1.533334960937509,
					2.8000000000000114
				],
				[
					1.7333349609374977,
					2.666665039062508
				],
				[
					1.8666699218750011,
					2.533332519531257
				],
				[
					2.0666699218749898,
					2.333332519531254
				],
				[
					2.0666699218749898,
					2.0666650390624994
				],
				[
					2.0666699218749898,
					1.5333325195312568
				],
				[
					2.0666699218749898,
					0.7333325195312455
				],
				[
					2.0666699218749898,
					0
				],
				[
					2.0666699218749898,
					-0.33333496093749204
				],
				[
					2.0666699218749898,
					-0.4666674804687432
				],
				[
					1.8666699218750011,
					-0.8666674804687489
				],
				[
					1.7999999999999972,
					-1.1333349609375034
				],
				[
					1.533334960937509,
					-1.333334960937492
				],
				[
					1.4666699218749955,
					-1.3999999999999915
				],
				[
					1.1333349609375034,
					-1.5333349609374949
				],
				[
					1,
					-1.5333349609374949
				],
				[
					0.8666699218750011,
					-1.5333349609374949
				],
				[
					0.5333349609375091,
					-1.5333349609374949
				],
				[
					0.1333349609375034,
					-1.5333349609374949
				],
				[
					-0.20000000000000284,
					-1.7333349609374977
				],
				[
					-0.666665039062508,
					-1.7333349609374977
				],
				[
					-1,
					-1.7999999999999972
				],
				[
					-1.0666650390624994,
					-1.7999999999999972
				],
				[
					-1.1333300781249989,
					-1.7333349609374977
				],
				[
					-1.1333300781249989,
					-1.5999999999999943
				],
				[
					-1.1333300781249989,
					-1.5333349609374949
				],
				[
					-1.0666650390624994,
					-1.333334960937492
				],
				[
					-1.0666650390624994,
					-1.333334960937492
				]
			],
			"lastCommittedPoint": [
				-1.0666650390624994,
				-1.333334960937492
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 76,
			"versionNonce": 1439590324,
			"isDeleted": false,
			"id": "FiG0g3TA7dohgRMhuxAZN",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -22.794052334389022,
			"y": -42.91999101958662,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 3.2608695652173765,
			"height": 4.891304347826093,
			"seed": 1113530764,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.3623232634171245
				],
				[
					-0.18115499745243824,
					-0.543478260869577
				],
				[
					-0.18115499745243824,
					-0.9058015242866873
				],
				[
					-0.18115499745243824,
					-1.0869565217391397
				],
				[
					-0.3623099949048765,
					-1.268118153447702
				],
				[
					-0.3623099949048765,
					-1.0869565217391397
				],
				[
					-0.3623099949048765,
					-0.724639892578125
				],
				[
					-0.3623099949048765,
					-0.18116163170856225
				],
				[
					-0.5434782608695627,
					0.18115499745243824
				],
				[
					-0.5434782608695627,
					0.3623166291610005
				],
				[
					-0.5434782608695627,
					0.9057948900305632
				],
				[
					-0.5434782608695627,
					1.268111519191578
				],
				[
					-0.5434782608695627,
					1.8115897800611407
				],
				[
					0,
					2.5362296726392657
				],
				[
					0.3623232634171245,
					2.898546301800266
				],
				[
					0.5434782608695627,
					3.0797079335088284
				],
				[
					1.0869565217391255,
					3.0797079335088284
				],
				[
					1.44927978515625,
					2.5362296726392657
				],
				[
					1.44927978515625,
					2.3550680409307034
				],
				[
					1.2681247877038118,
					0.9057948900305632
				],
				[
					1.0869565217391255,
					0.5434782608695627
				],
				[
					0.9058015242866873,
					0.3623166291610005
				],
				[
					0.5434782608695627,
					0
				],
				[
					0.18116826596468627,
					-0.18116163170856225
				],
				[
					0,
					-0.18116163170856225
				],
				[
					-0.724633258322001,
					-0.3623232634171245
				],
				[
					-0.9057882557744392,
					-0.3623232634171245
				],
				[
					-1.0869565217391255,
					-0.18116163170856225
				],
				[
					-1.2681115191915637,
					0.18115499745243824
				],
				[
					-1.449266516644002,
					0.3623166291610005
				],
				[
					-1.6304347826086882,
					0.724633258322001
				],
				[
					-1.6304347826086882,
					1.0869565217391255
				],
				[
					-1.8115897800611265,
					1.268111519191578
				],
				[
					-1.8115897800611265,
					1.8115897800611407
				],
				[
					-1.2681115191915637,
					2.717391304347828
				],
				[
					-1.0869565217391255,
					2.898546301800266
				],
				[
					-1.0869565217391255,
					3.0797079335088284
				],
				[
					-0.9057882557744392,
					3.2608695652173907
				],
				[
					-0.724633258322001,
					3.442024562669829
				],
				[
					-0.5434782608695627,
					3.623186194378391
				],
				[
					-0.18115499745243824,
					3.623186194378391
				],
				[
					0,
					3.623186194378391
				],
				[
					0.3623232634171245,
					3.623186194378391
				],
				[
					0.724646526834249,
					3.2608695652173907
				],
				[
					1.0869565217391255,
					2.717391304347828
				],
				[
					1.0869565217391255,
					2.3550680409307034
				],
				[
					1.0869565217391255,
					1.6304347826086882
				],
				[
					1.2681247877038118,
					1.0869565217391255
				],
				[
					1.2681247877038118,
					0.9057948900305632
				],
				[
					1.2681247877038118,
					0.724633258322001
				],
				[
					1.2681247877038118,
					0.5434782608695627
				],
				[
					1.2681247877038118,
					0.18115499745243824
				],
				[
					1.2681247877038118,
					0
				],
				[
					1.2681247877038118,
					-0.18116163170856225
				],
				[
					1.0869565217391255,
					-0.3623232634171245
				],
				[
					0.9058015242866873,
					-0.3623232634171245
				],
				[
					0.724646526834249,
					-0.3623232634171245
				],
				[
					0.18116826596468627,
					-0.3623232634171245
				],
				[
					0,
					-0.3623232634171245
				],
				[
					-0.3623099949048765,
					-0.3623232634171245
				],
				[
					-0.5434782608695627,
					-0.3623232634171245
				],
				[
					-0.724633258322001,
					-0.3623232634171245
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				-0.724633258322001,
				-0.3623232634171245
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 74,
			"versionNonce": 41164172,
			"isDeleted": false,
			"id": "ydv1sBlgeYxLVhB1yhyp7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -1.5984001604759612,
			"y": -17.919991019586618,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 5.615950874660314,
			"height": 3.9855094577955157,
			"seed": 964419596,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0.18116826596465785,
					0.18115499745243824
				],
				[
					0.18116826596465785,
					0.3623166291610005
				],
				[
					0.18116826596465785,
					0.9057948900305632
				],
				[
					0.18116826596465785,
					1.0869565217391255
				],
				[
					0.18116826596465785,
					1.4492731509001402
				],
				[
					0.18116826596465785,
					1.8115897800611407
				],
				[
					0.18116826596465785,
					1.992751411769703
				],
				[
					0.3623099949048765,
					2.173913043478265
				],
				[
					0.5434782608695627,
					2.3550680409307034
				],
				[
					1.0869565217391255,
					2.3550680409307034
				],
				[
					1.6304347826086882,
					2.3550680409307034
				],
				[
					1.8116030485733745,
					2.3550680409307034
				],
				[
					1.9927447775135931,
					2.3550680409307034
				],
				[
					2.173913043478251,
					2.3550680409307034
				],
				[
					2.536223038383156,
					1.992751411769703
				],
				[
					2.7173913043478137,
					1.6304347826086882
				],
				[
					2.8985595703125,
					1.268111519191578
				],
				[
					2.8985595703125,
					1.0869565217391255
				],
				[
					2.8985595703125,
					0.9057948900305632
				],
				[
					3.2608695652173765,
					0.5434782608695627
				],
				[
					3.2608695652173765,
					0.18115499745243824
				],
				[
					3.2608695652173765,
					-0.18116163170856225
				],
				[
					3.2608695652173765,
					-0.5434782608695627
				],
				[
					3.0797012992527186,
					-0.724639892578125
				],
				[
					2.7173913043478137,
					-0.9058015242866873
				],
				[
					2.3550813094429373,
					-0.9058015242866873
				],
				[
					1.8116030485733745,
					-1.0869565217391397
				],
				[
					0.9057882557744392,
					-1.44927978515625
				],
				[
					-0.18116826596468627,
					-1.44927978515625
				],
				[
					-1.0869565217391255,
					-1.44927978515625
				],
				[
					-1.4492665166440304,
					-1.44927978515625
				],
				[
					-1.9927447775135931,
					-1.44927978515625
				],
				[
					-2.1739130434782794,
					-1.44927978515625
				],
				[
					-2.3550813094429373,
					-1.44927978515625
				],
				[
					-2.3550813094429373,
					-1.2681181534476877
				],
				[
					-2.3550813094429373,
					0.18115499745243824
				],
				[
					-2.3550813094429373,
					0.5434782608695627
				],
				[
					-2.1739130434782794,
					1.268111519191578
				],
				[
					-1.8116030485733745,
					1.6304347826086882
				],
				[
					-1.6304347826087167,
					1.992751411769703
				],
				[
					-1.4492665166440304,
					2.173913043478265
				],
				[
					-1.2681247877038118,
					2.3550680409307034
				],
				[
					-0.9057882557744676,
					2.3550680409307034
				],
				[
					-0.3623099949049049,
					2.5362296726392657
				],
				[
					-0.18116826596468627,
					2.5362296726392657
				],
				[
					0.3623099949048765,
					2.5362296726392657
				],
				[
					0.5434782608695627,
					2.5362296726392657
				],
				[
					0.9057882557744392,
					2.5362296726392657
				],
				[
					1.2681247877037833,
					2.5362296726392657
				],
				[
					1.449266516644002,
					2.5362296726392657
				],
				[
					1.8116030485733745,
					2.173913043478265
				],
				[
					1.8116030485733745,
					1.6304347826086882
				],
				[
					1.8116030485733745,
					1.0869565217391255
				],
				[
					1.8116030485733745,
					0.3623166291610005
				],
				[
					1.8116030485733745,
					0.18115499745243824
				],
				[
					1.6304347826086882,
					0
				],
				[
					1.6304347826086882,
					-0.18116163170856225
				],
				[
					1.0869565217391255,
					-0.5434782608695627
				],
				[
					0.9057882557744392,
					-0.724639892578125
				],
				[
					0.7246465268342206,
					-0.724639892578125
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				0.7246465268342206,
				-0.724639892578125
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 57,
			"versionNonce": 613372212,
			"isDeleted": false,
			"id": "lZxDsmTBIo4rw4VHP2csQ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 30.829127225733288,
			"y": 35.34087854563077,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 4.710136081861435,
			"height": 5.253627611243218,
			"seed": 1142444468,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.18115499745243824
				],
				[
					0,
					0.3623099949049049
				],
				[
					-0.18114172894021863,
					0.3623099949049049
				],
				[
					-0.7246199898098098,
					0.3623099949049049
				],
				[
					-0.9057882557744676,
					0.3623099949049049
				],
				[
					-1.4492665166440304,
					0.5434782608695627
				],
				[
					-1.6304347826087167,
					0.724633258322001
				],
				[
					-1.6304347826087167,
					1.2681115191915922
				],
				[
					-1.8115765115489353,
					1.9927447775135931
				],
				[
					-1.8115765115489353,
					2.173913043478251
				],
				[
					-1.8115765115489353,
					2.3550680409307176
				],
				[
					-1.8115765115489353,
					2.536223038383156
				],
				[
					-1.6304347826087167,
					3.0797012992527186
				],
				[
					-1.4492665166440304,
					3.260869565217405
				],
				[
					-0.7246199898098098,
					3.442024562669843
				],
				[
					-0.3623099949049049,
					3.442024562669843
				],
				[
					0.18116826596465785,
					3.442024562669843
				],
				[
					0.3623365319293441,
					3.442024562669843
				],
				[
					0.9058147927989069,
					3.442024562669843
				],
				[
					1.0869565217391255,
					3.442024562669843
				],
				[
					1.2681247877037833,
					3.442024562669843
				],
				[
					1.811603048573346,
					2.8985463018002804
				],
				[
					1.9927713145380324,
					2.717391304347842
				],
				[
					2.355081309442909,
					2.3550680409307176
				],
				[
					2.536249575407595,
					1.811589780061155
				],
				[
					2.8985595703125,
					1.0869565217391255
				],
				[
					2.8985595703125,
					0.9057882557744676
				],
				[
					2.7173913043478137,
					0.3623099949049049
				],
				[
					2.355081309442909,
					-0.18116826596465785
				],
				[
					1.811603048573346,
					-0.9058015242866873
				],
				[
					1.4492930536684696,
					-1.44927978515625
				],
				[
					1.2681247877037833,
					-1.8116030485733745
				],
				[
					1.0869565217391255,
					-1.8116030485733745
				],
				[
					0.7246465268342206,
					-1.8116030485733745
				],
				[
					0.18116826596465785,
					-1.6304347826086882
				],
				[
					0,
					-1.44927978515625
				],
				[
					-0.18114172894021863,
					-1.2681247877038118
				],
				[
					-0.3623099949049049,
					-1.0869565217391255
				],
				[
					-0.3623099949049049,
					-0.724646526834249
				],
				[
					-0.3623099949049049,
					-0.3623232634171245
				],
				[
					-0.3623099949049049,
					0.18115499745243824
				],
				[
					-0.3623099949049049,
					0.3623099949049049
				],
				[
					-0.18114172894021863,
					0.724633258322001
				],
				[
					0,
					1.2681115191915922
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				0,
				1.2681115191915922
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 17,
			"versionNonce": 732990476,
			"isDeleted": false,
			"id": "wlZwHlW_WwADH66nxhmMh",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -24.263380058857337,
			"y": -43.52453081241183,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 1.0101133404355949,
			"height": 1.0100994688091873,
			"seed": 148403636,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.2525375828598442,
					0.25252371123342954
				],
				[
					0,
					0.25252371123342954
				],
				[
					0.5050381747159065,
					0.7575757575757578
				],
				[
					0.7575757575757507,
					1.0100994688091873
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				0.7575757575757507,
				1.0100994688091873
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 16,
			"versionNonce": 242682548,
			"isDeleted": false,
			"id": "LH4Ugf0kFbtls1GE9OOJ0",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -23.505804301281586,
			"y": -42.514431343602645,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0.7575757575757507,
			"height": 0.7575757575757578,
			"seed": 673577908,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0.25251908735796746,
					0.2525283351089058
				],
				[
					0.5050381747159065,
					0.5050520463423283
				],
				[
					0.7575757575757507,
					0.7575757575757578
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				0.7575757575757507,
				0.7575757575757578
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 12,
			"versionNonce": 5974668,
			"isDeleted": false,
			"id": "wx7KhzOa6cGLMpc6cSN3z",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -21.233077028554305,
			"y": -40.74675149334223,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0.0001,
			"height": 0.0001,
			"seed": 2010719500,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0.0001,
					0.0001
				]
			],
			"lastCommittedPoint": [
				0.0001,
				0.0001
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 14,
			"versionNonce": 640390196,
			"isDeleted": false,
			"id": "zAr6dZ1LRvdjafMIGJzGJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -20.980557941196366,
			"y": -42.26190300849374,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 0.7575757575757578,
			"seed": 1861102388,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-0.7575757575757578
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				0,
				-0.7575757575757578
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 16,
			"versionNonce": 1395521804,
			"isDeleted": false,
			"id": "mBZbL2zSwyhfEwjK-BdcB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -21.738133698772117,
			"y": -44.53463028122101,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 1.2626324277935623,
			"height": 0.7575757575757578,
			"seed": 1015561996,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.25251908735793904,
					0
				],
				[
					-1.0100948449337182,
					-0.5050520463423354
				],
				[
					-1.2626324277935623,
					-0.7575757575757578
				],
				[
					0,
					0
				]
			],
			"lastCommittedPoint": [
				-1.2626324277935623,
				-0.7575757575757578
			],
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 45,
			"versionNonce": 965930420,
			"isDeleted": false,
			"id": "vakPMWhv7dAAtRtCy4EUJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -20.980557941196366,
			"y": -42.00937929726032,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0.7461689822005049,
			"height": 170.06261024337562,
			"seed": 1168716852,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0.7461689822005049,
					170.06261024337562
				]
			]
		},
		{
			"type": "line",
			"version": 103,
			"versionNonce": 262338444,
			"isDeleted": false,
			"id": "HUhgA-m_p9pZKp1zf_Ier",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -0.020955816433087193,
			"y": -17.261903008493746,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0.40984786714343846,
			"height": 169.85965440063808,
			"seed": 844237108,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0.40984786714343846,
					169.85965440063808
				]
			]
		},
		{
			"type": "line",
			"version": 65,
			"versionNonce": 1775441716,
			"isDeleted": false,
			"id": "jfSArYSb-1DZhZKD05new",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 30.677340249268042,
			"y": 35.87819495011752,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 136.3500188427888,
			"seed": 824584076,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					136.3500188427888
				]
			]
		},
		{
			"type": "line",
			"version": 54,
			"versionNonce": 569648652,
			"isDeleted": false,
			"id": "tbnP3ksRmBsSERLmQIjye",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": -21.156227835255265,
			"y": 129.1421735977155,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 234.05798870584243,
			"height": 0,
			"seed": 644228108,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					234.05798870584243,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 64,
			"versionNonce": 1129451700,
			"isDeleted": false,
			"id": "o0Mb9_SW6KqoQIs3qr38M",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 1.669885658290923,
			"y": 154.50451012964487,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 214.49274477751362,
			"height": 0,
			"seed": 1510287796,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					214.49274477751362,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 48,
			"versionNonce": 1595638924,
			"isDeleted": false,
			"id": "1b-JHu2QSpssGh4R2aZGF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 215.80029390387517,
			"y": 174.43203751585412,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 184.93994975905585,
			"height": 0,
			"seed": 583375500,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					-184.93994975905585,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 13,
			"versionNonce": 1437981236,
			"isDeleted": false,
			"id": "kQbACk2W",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 217.55840041725708,
			"y": 74.75960196429193,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 5,
			"height": 20,
			"seed": 14671924,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "1",
			"rawText": "1",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "1"
		},
		{
			"type": "text",
			"version": 19,
			"versionNonce": 1825772300,
			"isDeleted": false,
			"id": "VwhcbHAX",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 212.1236178085614,
			"y": 108.09293087478787,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 12,
			"height": 20,
			"seed": 82076468,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "fnhiRLNQdG528vLoT-6jz",
					"type": "arrow"
				},
				{
					"id": "fZB-IBy32JKrwJqc0Ix5s",
					"type": "arrow"
				}
			],
			"updated": 1648693783909,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "2",
			"rawText": "2",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "2"
		},
		{
			"type": "text",
			"version": 13,
			"versionNonce": 1273263028,
			"isDeleted": false,
			"id": "HPwCLfa3",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 210.31201475998802,
			"y": 133.09293087478787,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 12,
			"height": 20,
			"seed": 2112103988,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "3",
			"rawText": "3",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "3"
		},
		{
			"type": "text",
			"version": 27,
			"versionNonce": 697618828,
			"isDeleted": false,
			"id": "wtwVkTFU",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 212.1236443455859,
			"y": 157.73059434285852,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 11,
			"height": 20,
			"seed": 361940364,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "AXXBR8_LK2bOdtHGDHQk1",
					"type": "arrow"
				},
				{
					"id": "rMpfBVmSDL-rOd7zvjJe8",
					"type": "arrow"
				}
			],
			"updated": 1648693783909,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "4",
			"rawText": "4",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4"
		},
		{
			"type": "text",
			"version": 447,
			"versionNonce": 67362100,
			"isDeleted": false,
			"id": "m8SgHJZt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 277.9092731231104,
			"y": -39.843796645066874,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 197,
			"height": 120,
			"seed": 514771508,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693783909,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "Counter_binary\n1. Couter_binary_clk\n2. Counter_binary_reset\n3. Counter_binary_mode\n4. Counter_binary_stop\n5. Counter_binary_out",
			"rawText": "Counter_binary\n1. Couter_binary_clk\n2. Counter_binary_reset\n3. Counter_binary_mode\n4. Counter_binary_stop\n5. Counter_binary_out",
			"baseline": 114,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Counter_binary\n1. Couter_binary_clk\n2. Counter_binary_reset\n3. Counter_binary_mode\n4. Counter_binary_stop\n5. Counter_binary_out"
		},
		{
			"type": "text",
			"version": 20,
			"versionNonce": 345686028,
			"isDeleted": false,
			"id": "Tj3znCTP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 535.2885399473236,
			"y": 105.56837085865334,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 11,
			"height": 20,
			"seed": 626279052,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "5",
			"rawText": "5",
			"baseline": 14,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "5"
		},
		{
			"type": "arrow",
			"version": 61,
			"versionNonce": 662312628,
			"isDeleted": false,
			"id": "r8niG3OUSsvTl3fEpq_yt",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 517.600480546465,
			"y": 144.43594846656717,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 590.4000994690291,
			"height": 0,
			"seed": 1497290764,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1648693783909,
			"link": null,
			"startBinding": {
				"elementId": "vA71O47gCW2TsJUToKVzG",
				"focus": 0.10116075684387188,
				"gap": 10.252560119046507
			},
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					590.4000994690291,
					0
				]
			]
		},
		{
			"type": "text",
			"version": 114,
			"versionNonce": 1287979956,
			"isDeleted": false,
			"id": "vFUFk6xR",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"angle": 0,
			"x": 618.0210390433112,
			"y": -221.9724583383084,
			"strokeColor": "#364fc7",
			"backgroundColor": "transparent",
			"width": 168,
			"height": 60,
			"seed": 1212348212,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1648693886507,
			"link": null,
			"fontSize": 16,
			"fontFamily": 1,
			"text": "Decode_7seg\n1. Decode_7seg_in\n2. Decode_7seg_out",
			"rawText": "Decode_7seg\n1. Decode_7seg_in\n2. Decode_7seg_out",
			"baseline": 54,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Decode_7seg\n1. Decode_7seg_in\n2. Decode_7seg_out"
		},
		{
			"id": "1LuJDfMgY10YbJvDKVGfn",
			"type": "line",
			"x": 445.86016428629307,
			"y": -96.37043866684525,
			"width": 120.02881339639418,
			"height": 0.7087190731064084,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 124213044,
			"version": 65,
			"versionNonce": 54871860,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693822927,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					120.02881339639418,
					0.7087190731064084
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "ZYcvuuQzYH04MqKcj1uj7",
			"type": "line",
			"x": 444.8744672377144,
			"y": -95.87911089808668,
			"width": 2.173913043478251,
			"height": 9.782608695652158,
			"angle": 0,
			"strokeColor": "#2b8a3e",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1424656652,
			"version": 54,
			"versionNonce": 102444300,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					2.173913043478251,
					-9.782608695652158
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "3wjOncGRVeEXmGQw9hhon",
			"type": "line",
			"x": 424.2222933246709,
			"y": -103.12548328684346,
			"width": 20.652173913043498,
			"height": 0,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 711513740,
			"version": 30,
			"versionNonce": 872216076,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693822927,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					20.652173913043498,
					0
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "C3qWHEg7zfWKhAVe-FgXW",
			"type": "freedraw",
			"x": 947.2371738837638,
			"y": -102.2237356444299,
			"width": 5.092592592592609,
			"height": 18.209884078414348,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1061587252,
			"version": 16,
			"versionNonce": 1797601420,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.15431722005212123,
					1.2345716688368071
				],
				[
					-0.15431722005212123,
					1.5432174117476904
				],
				[
					-0.30863444010412877,
					2.3148148148148096
				],
				[
					-0.6172801830150547,
					3.395069263599538
				],
				[
					-1.5432061089410354,
					6.018518518518519
				],
				[
					-2.4691320348669024,
					9.259259259259252
				],
				[
					-4.166666666666629,
					14.197534631799769
				],
				[
					-4.938264069733805,
					17.129629629629633
				],
				[
					-5.092592592592609,
					18.209884078414348
				],
				[
					-5.092592592592609,
					18.209884078414348
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-5.092592592592609,
				18.209884078414348
			]
		},
		{
			"id": "fD3xCf1T2JgqRrT5Wt0L5",
			"type": "freedraw",
			"x": 954.6445812911712,
			"y": -102.2237356444299,
			"width": 2.314814814814781,
			"height": 13.117291485821752,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 2074284596,
			"version": 20,
			"versionNonce": 793863732,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.15431722005212123,
					0.1543285228587905
				],
				[
					-0.30863444010412877,
					0.3086457429108833
				],
				[
					-0.4629629629629335,
					1.0802544487847143
				],
				[
					-0.6172801830150547,
					2.4691433376736143
				],
				[
					-1.0802431459779882,
					5.401238335503464
				],
				[
					-1.3888888888889142,
					7.870370370370367
				],
				[
					-1.697523328993043,
					10.49383092809606
				],
				[
					-2.006169071903969,
					11.882719816984945
				],
				[
					-2.006169071903969,
					12.5
				],
				[
					-2.006169071903969,
					12.65432852285879
				],
				[
					-2.1604862919559764,
					12.808645742910883
				],
				[
					-2.1604862919559764,
					12.962962962962962
				],
				[
					-2.314814814814781,
					13.117291485821752
				],
				[
					-2.314814814814781,
					13.117291485821752
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-2.314814814814781,
				13.117291485821752
			]
		},
		{
			"id": "etK_kmQM6972hessCPYq_",
			"type": "freedraw",
			"x": 951.4038405504305,
			"y": -127.84101582744493,
			"width": 2.314814814814781,
			"height": 10.49383092809606,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 713465396,
			"version": 26,
			"versionNonce": 897992460,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0.15432852285880472,
					0
				],
				[
					0.4629629629630472,
					0
				],
				[
					1.234571668836793,
					0
				],
				[
					1.3888888888889142,
					0
				],
				[
					1.5432174117477189,
					0
				],
				[
					2.0061803747106524,
					0.15432287145543455
				],
				[
					2.314814814814781,
					0.6172858344183965
				],
				[
					2.314814814814781,
					1.234571668836793
				],
				[
					2.314814814814781,
					2.160497594762731
				],
				[
					2.314814814814781,
					2.932100649233206
				],
				[
					2.314814814814781,
					3.858020923755774
				],
				[
					2.314814814814781,
					4.938275372540502
				],
				[
					1.8518518518518476,
					6.481481481481481
				],
				[
					1.69753463179984,
					7.716053150318274
				],
				[
					1.5432174117477189,
					8.79629629629629
				],
				[
					1.234571668836793,
					9.567905002170136
				],
				[
					1.234571668836793,
					10.185185185185176
				],
				[
					1.234571668836793,
					10.339502405237255
				],
				[
					1.0802544487847854,
					10.49383092809606
				],
				[
					1.0802544487847854,
					10.49383092809606
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				1.0802544487847854,
				10.49383092809606
			]
		},
		{
			"id": "kvbhfNfkirEpymZh1LW9Q",
			"type": "freedraw",
			"x": 948.3174283325485,
			"y": -121.66818008887434,
			"width": 7.2530788845486995,
			"height": 0.6172914858217524,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1292134068,
			"version": 20,
			"versionNonce": 36601780,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0.30863444010424246,
					0.1543285228587905
				],
				[
					0.4629629629630472,
					0.3086457429108833
				],
				[
					1.0802431459779882,
					0.3086457429108833
				],
				[
					1.8518518518518476,
					0.3086457429108833
				],
				[
					3.086412217881957,
					0.3086457429108833
				],
				[
					4.475301106770871,
					0.3086457429108833
				],
				[
					5.555555555555543,
					0.3086457429108833
				],
				[
					6.01851851851859,
					0.3086457429108833
				],
				[
					6.172835738570598,
					0.3086457429108833
				],
				[
					6.635798701533645,
					0.3086457429108833
				],
				[
					6.944444444444457,
					0.4629629629629619
				],
				[
					7.098761664496578,
					0.4629629629629619
				],
				[
					7.2530788845486995,
					0.6172914858217524
				],
				[
					7.2530788845486995,
					0.6172914858217524
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				7.2530788845486995,
				0.6172914858217524
			]
		},
		{
			"id": "B8w7AdlgM38mZr6kktsmF",
			"type": "freedraw",
			"x": 946.0776198742046,
			"y": 138.3283461690693,
			"width": 4.333349609374977,
			"height": 19.66667480468749,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1038840500,
			"version": 17,
			"versionNonce": 1572963724,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.33332519531251137
				],
				[
					0,
					1.6666748046874886
				],
				[
					-1.6666748046875455,
					9
				],
				[
					-3,
					15.333325195312511
				],
				[
					-4.333349609374977,
					18.33332519531251
				],
				[
					-4.333349609374977,
					19.33332519531251
				],
				[
					-4.333349609374977,
					19.66667480468749
				],
				[
					-4,
					17.33332519531251
				],
				[
					-2.6666748046875455,
					14
				],
				[
					-1.6666748046875455,
					11.666674804687489
				],
				[
					-1.6666748046875455,
					11.666674804687489
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-1.6666748046875455,
				11.666674804687489
			]
		},
		{
			"id": "LSBLb67iWAQhb8lzpJDVk",
			"type": "freedraw",
			"x": 953.7578224011461,
			"y": 138.7105554551072,
			"width": 1.9927580460258696,
			"height": 15.217391304347814,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1768540556,
			"version": 13,
			"versionNonce": 628003468,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.3623232634172382,
					2.1739130434782794
				],
				[
					-0.3623232634172382,
					3.260869565217405
				],
				[
					-0.3623232634172382,
					5.434782608695656
				],
				[
					-0.9058015242867441,
					9.782608695652186
				],
				[
					-0.9058015242867441,
					12.862323263417125
				],
				[
					-1.44927978515625,
					14.673913043478251
				],
				[
					-1.630434782608745,
					15.217391304347814
				],
				[
					-1.9927580460258696,
					15.036236306895375
				],
				[
					-1.9927580460258696,
					14.855081309442937
				],
				[
					-1.9927580460258696,
					14.855081309442937
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-1.9927580460258696,
				14.855081309442937
			]
		},
		{
			"id": "wRzasENA",
			"type": "text",
			"x": 945.4244802221378,
			"y": 116.31200915330692,
			"width": 11,
			"height": 20,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 887107724,
			"version": 17,
			"versionNonce": 476228660,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"text": "9",
			"rawText": "9",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "9"
		},
		{
			"id": "xFKf_c-wro1JFd6ecfHUD",
			"type": "freedraw",
			"x": 511.37229401703655,
			"y": -107.52360583640476,
			"width": 1.2820512820512704,
			"height": 18.58974358974359,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1306472844,
			"version": 14,
			"versionNonce": 1368678668,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.2136739095052178
				],
				[
					0,
					1.2820512820512988
				],
				[
					0,
					3.846153846153854
				],
				[
					0,
					8.54700724283856
				],
				[
					-0.6410256410256352,
					13.247860639523253
				],
				[
					-0.8546956380208144,
					16.452988844651458
				],
				[
					-0.8546956380208144,
					18.16238794571315
				],
				[
					-0.8546956380208144,
					18.376065767728377
				],
				[
					-1.0683812850560344,
					18.58974358974359
				],
				[
					-1.2820512820512704,
					18.376065767728377
				],
				[
					-1.2820512820512704,
					18.376065767728377
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				-1.2820512820512704,
				18.376065767728377
			]
		},
		{
			"id": "s7NQLs3KRqUUWc83EFpPG",
			"type": "freedraw",
			"x": 516.2868291482465,
			"y": -106.24155455435346,
			"width": 0,
			"height": 19.658117049779634,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 1042613812,
			"version": 14,
			"versionNonce": 153552308,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.4273478190104072
				],
				[
					0,
					0.8546995505308388
				],
				[
					0,
					2.1367508325821234
				],
				[
					0,
					6.410256410256409
				],
				[
					0,
					11.538461538461533
				],
				[
					0,
					18.162387945713135
				],
				[
					0,
					19.44443922776442
				],
				[
					0,
					19.658117049779634
				],
				[
					0,
					19.44443922776442
				],
				[
					0,
					18.376065767728363
				],
				[
					0,
					18.376065767728363
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				0,
				18.376065767728363
			]
		},
		{
			"id": "y9dvXDsW",
			"type": "text",
			"x": 516.0731435012112,
			"y": -137.47232378512268,
			"width": 11,
			"height": 20,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 89058828,
			"version": 19,
			"versionNonce": 1157390220,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693783910,
			"link": null,
			"text": "4",
			"rawText": "4",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "4"
		},
		{
			"id": "8EKT2Uhh",
			"type": "text",
			"x": 81.71043390269449,
			"y": -155.35672810106246,
			"width": 21,
			"height": 20,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1392130444,
			"version": 14,
			"versionNonce": 1089230220,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693836322,
			"link": null,
			"text": "s0",
			"rawText": "s0",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "s0"
		},
		{
			"id": "yKq1oSdW",
			"type": "text",
			"x": 130.95175527423243,
			"y": 80.10182499463716,
			"width": 21,
			"height": 20,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1385213708,
			"version": 6,
			"versionNonce": 1363205172,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693839372,
			"link": null,
			"text": "s0",
			"rawText": "s0",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "s0"
		},
		{
			"id": "d98peW5d",
			"type": "text",
			"x": 466.7804953208069,
			"y": -127.84291345376337,
			"width": 14,
			"height": 20,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1325285556,
			"version": 8,
			"versionNonce": 910777868,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693843086,
			"link": null,
			"text": "s1",
			"rawText": "s1",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "s1"
		},
		{
			"id": "XCnDRS3t",
			"type": "text",
			"x": 1147.4184183041277,
			"y": -107.05866462831551,
			"width": 162,
			"height": 20,
			"angle": 0,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 604617868,
			"version": 46,
			"versionNonce": 1300088372,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693864590,
			"link": null,
			"text": "top_level_out_7seg",
			"rawText": "top_level_out_7seg",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "top_level_out_7seg"
		},
		{
			"id": "bIbm7jw8",
			"type": "text",
			"x": 1154.5612754469848,
			"y": 133.8937017918855,
			"width": 150,
			"height": 20,
			"angle": 0,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 745706508,
			"version": 43,
			"versionNonce": 1252521100,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693877417,
			"link": null,
			"text": "top_level_out_led",
			"rawText": "top_level_out_led",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "top_level_out_led"
		},
		{
			"id": "4kfMZWEr",
			"type": "text",
			"x": 574.3425081404794,
			"y": -129.24509203232563,
			"width": 5,
			"height": 20,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 281362828,
			"version": 25,
			"versionNonce": 1197214900,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "D0vfVJ4HCQH50OBLsCRfn",
					"type": "arrow"
				},
				{
					"id": "Sz9ROh9W3QzjAmf95kcZu",
					"type": "arrow"
				}
			],
			"updated": 1648693918453,
			"link": null,
			"text": "1",
			"rawText": "1",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "1"
		},
		{
			"id": "so12LroP",
			"type": "text",
			"x": 816.0091239445159,
			"y": -124.3839978754246,
			"width": 12,
			"height": 20,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 456623412,
			"version": 6,
			"versionNonce": 811825972,
			"isDeleted": false,
			"boundElements": [
				{
					"id": "bzxWIjgrXLOjOgUhPqZcF",
					"type": "arrow"
				}
			],
			"updated": 1648693911497,
			"link": null,
			"text": "2",
			"rawText": "2",
			"fontSize": 16,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 14,
			"containerId": null,
			"originalText": "2"
		},
		{
			"id": "bzxWIjgrXLOjOgUhPqZcF",
			"type": "arrow",
			"x": 797.4295654799357,
			"y": -93.87938278244246,
			"width": 41.9192042495265,
			"height": 0,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 2032821044,
			"version": 31,
			"versionNonce": 1082545036,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693911497,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					41.9192042495265,
					0
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "wDqZgwilpnyljkVlEjyMA",
				"focus": -0.08254214493412984,
				"gap": 1.5712604447207354
			},
			"endBinding": {
				"elementId": "so12LroP",
				"focus": -2.0504615092982137,
				"gap": 11.339645784946356
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		},
		{
			"id": "Sz9ROh9W3QzjAmf95kcZu",
			"type": "arrow",
			"x": 572.870314781641,
			"y": -97.19832499114538,
			"width": 23.38707708543336,
			"height": 0,
			"angle": 0,
			"strokeColor": "#0b7285",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 90,
			"groupIds": [],
			"strokeSharpness": "round",
			"seed": 138765236,
			"version": 13,
			"versionNonce": 1975470220,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1648693918453,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					23.38707708543336,
					0
				]
			],
			"lastCommittedPoint": null,
			"startBinding": {
				"elementId": "4kfMZWEr",
				"focus": 2.2046767041180244,
				"gap": 12.046767041180246
			},
			"endBinding": {
				"elementId": "wDqZgwilpnyljkVlEjyMA",
				"focus": 0.15377797770629015,
				"gap": 1.8736404408679732
			},
			"startArrowhead": null,
			"endArrowhead": "arrow"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#0b7285",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 0.5,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 90,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 16,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%