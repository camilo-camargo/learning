Determine which square on the viewport corresponds to this pixel. 
The next formulas change the scale of the coordination.

$$ Vx = Cx * (Vw/Cw)$$
$$ Vy = Cy * (Vy/Cy)$$
The viewport is embeeded in 3D space, for that reason, the Vz is the distance.
$$ Vz = d $$ 

