Methodological Algorith to draw a picture, with not appropiate terms.
```pseudocode
Place the eye and the frame as desired
For each square on the canvas
	Determine which square on the grid corresponds to this quare on the canvas
	Determine the color seen through that grid square
	Paint the square with that color
```

Raytracing Algorithm
```pseudocode
Place the camera and the viewport as desired
For each pixel on the canvas
	Determine which pixel on the viewport corresponds to this pixel
	Determine the color seen through that square
	Paint the pixel with that color
```



# Assumptions
1. Fixed viewing position, called camera position.
2. camera orientation. 