In the real world, light comes from a light source, bounces off several objects, and  then reches our eyes.

# Photon Tracing or Photon mapping
The photon tracing is a tecnique that simulated photons leaving light sources.

# Rays of light
Ray originatend from the camera, going through a point in the viewport. 

# The Ray Equation
Ray passes through O and its direction (from O to V). 
$$ P = O + t(V-O) $$
Where t is any real  number. 
$$ (V-O) = D $$
D, is the direction of the ray. 
$$ P = O + tD $$
## Subdivisions of the Parameter Space
t < 0 , behind the camera
0 <= t <= 1, Between the camera and the projection plane, viewport
t > 1, In front of the projection plane/ viewport

# The Sphere Equation

A sphere is the set of points that lie at a radius. 
C, is the center of the sphere
r, the radius

$$ distance(P,C) = r $$
# Ray Meets Sphere
Is the intersect of a point P at ray formula and sphere formula, and because P points to the same position. 
$$ <O+tD-C, O+tD-C>$$
if t satisfaced the equation, then t is sustitute at Ray Equation.
Treating the equation bit more.
$$ CO = O-C $$
$$ <CO+tD,CO+tD> = r*r $$
$$ <CO + tD, CO> <CO+tD, tD> = r*r $$
$$ <CO, CO> + <tD, CO> + <CO, tD> + <tD, tD> = r*r $$
$$ <tD, tD> + 2<CO,tD> + <CO, CO> = r*r$$
This is not more and nothing less than a good old quadratic equation. 

The ray is tangent to the sphere. 

