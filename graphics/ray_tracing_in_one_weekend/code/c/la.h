#ifndef _LINEAR_ALGEBRA_
#define _LINEAR_ALGEBRA_   
#include <stdlib.h> 
#include <math.h>

typedef struct {
	float x;
	float y;
	float z;
}Vec3; 

Vec3* vec3_new(float x, float y, float z){
	Vec3 *v = (Vec3 *)malloc(sizeof(Vec3)); 
	v->x = x;
	v->y = y;
	v->z = z; 
	return v;
}   

Vec3* vec3_plus(Vec3 *v, Vec3 *u){
	return vec3_new(v->x+u->x,v->y+u->y,v->z+u->z);
} 

Vec3* vec3_subs(Vec3 *v, Vec3 *u){ 
	return vec3_new(v->x-u->x,v->y-u->y,v->z-u->z);
} 

float vec3_dot(Vec3 *v, Vec3 *u){ 
	return (
			v->x * u->x +
			v->y * u->y +
			v->z * u->z);	
}  

float vec3_length_squared(Vec3 *v){
	return (v->x*v->x + v->y*v->y + v->z*v->z);
}

float vec3_length(Vec3 *v){
	return (float) sqrt(vec3_length_squared(v));	
}

Vec3* vec3_unit(Vec3 *v){
	
}



#endif
