#include <stdio.h>   
#include <stdlib.h>  
#include <math.h> 

#include "image.h" 
#include "la.h"

//Point
typedef struct {
	int a;
	int b;
}Point; 
 
Point * point_new(){
	return (Point *) malloc(sizeof(Point));
} 

void ppm_init(Image *img){
	printf("P3\n%d %d\n255\n", img->width, img->height);	
} 

void ppm_write(float red, float green, float blue){
	printf("%d %d %d\n", (int)floor(255.999*red), (int)floor(255.999*green), (int)floor(255.999*blue));	
} 

int main(){
	Vec3 *p = vec3_new(1,1,1);
	Vec3 *p1 = vec3_new(1,1,2);  
	Vec3 *pr = vec3_plus(p, p1); 
	printf("%f %f %f \n", pr->x, pr->y, pr->z); 
	printf("dot product: %f\n", vec3_dot(p,p1));
	printf("distance: %f", vec3_length(p));
}


int main2(int argc, char **argv){
	Image *img = image_new(1920, 1080);
	ppm_init(img); 
	for(int y = img->height-1; y >= 0; --y){
		for(int x = 0; x < img->width; ++x){  
			float r = (float)x/(img->width-1);
			float g = (float)y/(img->height-1);
			float b = (float) 0.60;
			ppm_write(r,g,b);
		}	
	}
}
