#ifndef _IMAGE_H
#define _IMAGE_H  
#include <stdlib.h>

typedef struct {
	int width;
	int height;
}Image;

//Constructor 
Image *image_new(int width, int height){
	Image *img = (Image *) malloc(sizeof(Image)); 
 	img->width = width;
	img->height = height;
	return (Image*)img;
} 

//Destructor
void image_del(Image *img){
	free(img);
}

#endif

