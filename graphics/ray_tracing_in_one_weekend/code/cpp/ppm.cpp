#include <iostream> 

int main(int argc, char **argv){
	const int img_width = 256*5; 
	const int img_height = 256*5; 
	std::cout << "P3\n" << img_width << ' ' << img_height << "\n255\n";
	for(int y = img_height-1; y >= 0 ; --y ){
		std::cerr << "\rScanlines remaining:" << y << std::flush;
		for(int x = img_width-1; x >= 0; --x){
			auto r = double(x) / (img_width-1); 
			auto g = double(y) / (img_height-1); 
			auto b = 0.25; 

			int ir = static_cast<int>(255.999 * r);
			int ig = static_cast<int>(255.999 * g); 
			int ib = static_cast<int>(255.999 * b);  
		}
	} 
	std::cerr << "\nDone\n";
}
