#ifndef __PLANE__
#define __PLANE__ 
#include "../Ray.h"
#include "../../utilities/ShadeRec.h"
#include "../../utilities/RGBColor.h"
#include "../../utilities/Constants.h"

class Plane{
	public: 
		Plane(void); 
		Plane(const Point3D p, const Normal& n);
		virtual bool
		hit(const Ray& ray, double &tmin, ShadeRec& sr) 
		const = 0; 
	protected:  
		Normal normal;
		Point3D point; //point through which plane passes
		RGBColor color;
};  


//[TODO] create cpp 
bool Plane::hit(const Ray& ray, double& tmin, ShadeRec& sr)const{
	double t = (point - ray.o) * normal/(ray.d * normal); 
	if(t > KEpsilon){
		tmin = t;	 
		sr.normal = normal;
		sr.local_hit_point = ray.o + t * ray.d;
		return (true);
	}else{
		return (false);
	}
}
#endif 

