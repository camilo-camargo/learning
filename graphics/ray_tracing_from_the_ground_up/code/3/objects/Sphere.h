#ifndef __SPHERE__
#define __SPHERE__ 
#include "../Ray.h"
#include "../../utilities/ShadeRec.h"
#include "../../utilities/RGBColor.h"
#include "../../utilities/Constants.h" 
#include <math.h>

class Sphere{
	public: 
		Sphere(void); 
		Sphere(const Point3D p, const Normal& n);
		virtual bool
		hit(const Ray& ray, double &tmin, ShadeRec& sr) 
		const = 0;   

	protected:  
		Normal normal;
		Point3D center; //point through which plane passes
		double radius;
		RGBColor color;
};  

//[TODO] create of sphere.cpp  
bool Sphere::hit(const Ray& ray, double& tmin, ShadeRec& sr)const{
	double t;
	Vector3D temp = ray.o - center;
	double a = ray.d * ray.d;
	double b = 2.0 * temp*ray.d;
	double c = temp * temp -radius * radius;
	double disc = b * b - 4.0 * a * c; 
	if(disc < 0.0){
		return(false);	 
	}else{
		double e = sqrt(disc);
		double denom= 2.0 * a; 
		t = (-b -e)/denom;  //smaller root
		if(t > KEpsilon){
			tmin = t;
			sr.normal = (temp+t * ray.d)/radius;
			sr.local_hit_point = ray.o + t * ray.d; 
			return(true);
		}
		t = (-b + e) / denom; //larger root
		if(t > KEpsilon){
			tmin=t;
			sr.normal = (temp + t * ray.d)/radius; 
			sr.local_hit_point = ray.o + t * ray.d; 
			return (true);
		}
	} 
	return (false);
}  
#endif 

