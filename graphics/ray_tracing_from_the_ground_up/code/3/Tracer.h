#ifndef __TRACER__
#define __TRACER__  
#include "util.h" 

class SingleSphere{
	public:
		RGBColor	trace_ray(const Ray& ray) const;
		World* world_ptr;
};

class Tracer{
	public: 
		Tracer(void); 
		Tracer(World* w_ptr); 
		virtual RGBColor trace_ray(const Ray& ray) const; 
	protected:
		World* world_ptr;

}; 

Tracer::Tracer(void):
	world_ptr(NULL){} 

Tracer::Tracer(World* w_ptr)
	:world_ptr(w_ptr){} 

RGBColor
Tracer::trace_ray(const Ray& ray) const{
	return (RGBColor(0,0,0));
}  

RGBColor
SingleSphere::trace_ray(const Ray& ray) const{
	ShadeRec sr(*world_ptr);
	double t;
	if(world_ptr->sphere.hit(ray, t, sr))
		return (RGBColor(1,0,0));
	else
		return (RGBColor(0,0,0));
}



#endif
