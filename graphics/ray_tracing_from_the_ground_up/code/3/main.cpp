#include "util.h"
#include <iostream>

using namespace std;

int main(int argc, char **argv){  
	World w;
	w.build();	 
	w.render_scene(); 
	return(0);
}
