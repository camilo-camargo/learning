#include "util.h"
#include "World.h" 
#include "ViewPlane.h"


void World::build(void){
	vp.set_hres(200);
	vp.set_vres(200);
	vp.set_pixel_size(1.0);
	vp.set_gamma(1.0); 

	background_color = RGBColor(0,0,0);
	tracer_ptr = new SingleSphere(this);

	sphere.set_center(0.0); 
	sphere.set_radius(85.0);
} 

void World::render_scene(void) const{
	RGBColor pixel_color;
	Ray ray;
	double zw = 100.0; // hard wired in 
	double x, y;
	open_window(vp.hres, vp.vres); 
	ray.d = Vector3D(0,0,-1); 

	for(int r = 0; r < vp.vres; r++){
		for(int c = 0; c <= vp.hres; c++){
			x = vp.s * (c - 0.5	* (vp.hres -1.0)); 
			y = vp.s * (r - 0.5 * (vp.vres -1.0)); 

			ray.o = Point3D(x,y,zw);
			pixel_color = tracer_ptr -> tracer_ray(ray); 
			display_pixel(r,c,pixel_color);
		}
	}

}

