#ifndef __WORLD__
#define __WORLD__
#include "ViewPlane.h" 
#include "Tracer.h"
#include "objects/Sphere.h" 
#include "../utilities/RGBColor.h" 

class World {
	public:
		ViewPlane vp;
		RGBColor background_color;
		Sphere sphere;
		Tracer* tracer_ptr; 

		World(void);
		void build(void);
		void render_scene(void) const;
		void open_window(const int hres, const int vres) const; 
		void display_pixel(const int row, const int column, const RGBColor & pixel_color)const;

};

#endif
