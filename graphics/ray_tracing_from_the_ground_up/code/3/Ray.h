#ifndef __RAY__
#define __RAY__

#include "../utilities/Point3D.h"
#include "../utilities/Vector3D.h" 


class Ray{
	public:
		Point3D  o; //origin 
		Vector3D d; // direction 
		void ray(void); 
		void ray(const Point3D& origin, const Vector3D& dir); //constructor 
		Ray(const Ray& ray); //Copy Constructor 
		Ray& operator=(const Ray& rhs); //assignment operator
		~Ray(void); //Destructor
};
#endif 
