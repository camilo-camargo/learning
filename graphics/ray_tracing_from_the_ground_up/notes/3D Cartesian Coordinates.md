The 3D cartesian coordinates is form by a order pair of 3 real numbers, x,y and z. The 3D space point is determined  by the distance from the origin. 
Each coordinate axis defines a coordinate plane, they are the following (x,y), (x,z), (y,z). 

In computer graphics the 3D axis coordinates the field axis are world coordinates.  Where the y axis points up and the (x,z) plane is horizontal and it use right-handedness coordinate system.

[TODO]  create a char of 3D coordinates

## Cylindrical Coordinates
The cylindrical coordinates uses a straight-line distance r, an angle theta, and y coordinate.
$$ r \in [0,\infty) $$
The Azimuth angle,  can be express how as a Cartesian coordinates.
$$ x = r \sin \phi$$
$$ y = y$$
$$z = r \cos\phi $$

[TODO] create a char of cylindrical coodinates 

## Spherical Coordinates
The spherical coordinates defines a distance r and two angles, theta and phi.
$$ r \in [0, \infty) $$ $$ \theta = angle between $$
$$ \theta \in [0, \pi] $$
The theta is know as polar angle.
