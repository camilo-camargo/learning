A  vector is a directed line segment, defined by the length, and the direction.
## Normalization
The normalization generate a unit vector, which has length one.
hat is for the following
$$ \hat{u} $$

## Magnitude

The magnitude of a vector is the length of its line segment, and is denoted by 
$$ ||u|| $$
## Operations
### Addition
### Subtraction
### Scalar Product
### Division

[TODO] Write the above basic operations
### Dot Product
$$ u \cdot v = ||u||v||\cos\theta $$
Where the angle is between vector u and vector v.
![[Pasted image 20220505182632.png]]
![[Pasted image 20220505182639.png]]
![[Pasted image 20220505182648.png]]




