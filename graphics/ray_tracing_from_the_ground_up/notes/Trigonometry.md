The trigonometry is defined from a right-angled triangle. 
This right-angled triangle is the average of one side to another.
[TODO] created right angle. a  is opposite, b is adjacent, and c is the hypotenuse.

The basic trigonometric functions are defined as
$$ \sin \theta = \frac{a}{c} $$
$$ \cos \theta = \frac{b}{c}$$
$$ \tan \theta = \frac{\sin\theta}{\cos\theta}$$

## Pythagoras' theorem
$$ c^2 = a^2 + b^2 $$
When we set c how 1, then we have the following relations
$$ \sin^2 \theta + \cos^2 = 1$$
$$ \sin(\theta\pm \phi ) = \sin\theta \cos\phi \pm \cos\theta \sin\phi$$
$$\cos(\theta\pm\phi)=\cos\theta\cos\phi \mp\sin\theta\sin\phi $$

