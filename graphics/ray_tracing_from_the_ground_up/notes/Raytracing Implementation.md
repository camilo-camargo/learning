## Ray Tracer Algorithm
```
Define a object with a material
Define some light sources
define a window 
for each pixel
	shot a ray towards the objects
	compute the nearest hit point
	if the ray hits an object
		compute the color by material and the light
	else
		set the pixel color to black
```

The tracer shoots a ray from the camera, in the real world the ray comes from the light source.

View Plane: is called all pixels on the plane.
Shading: is the process to compute color, where a ray hits an object.
Ray object intersection calculation: Is the process to calculate where a ray hits an object.

For a real and more realistic image representation, you need increase the number of pixels.


## The World
The world objects save all of the things at the world space. So the world contains a view plane and all objects.

Absolute coordinates: is the coordinates system of the world.

## Rays
A ray is a infinite straight line comes from the orginin. Where d is the direction of the ary.
$$ p = o + td$$
The t is a real number. 

The above units are express as world units where the ray is intersected with the objects.

### Primary Ray
The primary ray started at the origin.
### Secondary Ray
The secondary ray started ath the object surfaces, and are reflected.
### Shadow ray
The shadow ray started at the object surface, and  are used or shading.
### Light ray
The light start at the light source.


## Ray Object Intersections
The t has and the before equation has the following range
$$ t \in [\epsilon, +\infty)$$
Where 
$$ \epsilon = 10^{-6}$$
## Rays and implicit Surfaces
The hit points is find with the same solution of the ray equation and implicit surface equation.
$$ f(o+td) = 0$$
The hits points can be
* Even number of hit points if the ray **start outside** of the sourface
* Odd number of hit points if the ray hits surface tangentially
* Odd number of hit points if the ray start inside the surface

## Planes
The planes are easiet to intersect because has a form the linear equation.\
[TODO]
## Spheres

[TODO]

## Representing Colors
