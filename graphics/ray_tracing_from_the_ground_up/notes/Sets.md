A set is an unordered collection of objects of the same type, each object is called element.

$$ \mathbb{R} \rightarrow \ real \ number$$
$$ \mathbb{R}^+ \rightarrow \ the \ set \ no \ negative \ number, \ with \ zero$$
$$\mathbb{R}^2 \rightarrow (x,y)  $$
$$\mathbb{R}^3 \rightarrow (x,y,z) $$

## Definition of Set

### Predicate
$$ \mathbb{R} = \{ x:x \in  \mathbb{R} \ and x \ge  0 \} $$
### Belongs to
$$ \in \rightarrow \ belongs \ to$$
$$ :\ \rightarrow  \ such \ that $$

## Subset
The subset is a set that belongs to some larget set.

$$ A \subseteq B $$
## Ordered Pairs and The cartesian Product of Sets
The ordered pairs of elements is a sequence of two elements in a definite order.

## Intervals
An interval is a subset of the real line that contains at least two numbers.
The notation for closed interval is, []. 
The notation for half-closed (] and [), or two opens (). \

### Empty interval
$$ \varnothing $$

### Intersection
$$ \cap $$ 


