The raytracing is a computer graphics technique that creates images by shooting rays. When a ray light hit an objects, the ray tracer determine the color of the pixel by how much light is reflected, is possible that the ray can bounce off of them and hit other objects.

Each ray can be traced independently with that ray tracing can use as many processors as are available, "Embarrasingly parallelizable".

## Eficiency
Programming data types.
float, shading calculations. 
doubles, ray object intersection calculations. 

### Data Storage
The geometry objects, lights, and sample points need to  be stored in linear data structure. (STL vector)
### Pass by Reference
Pass all the compound objects into fuctions with references.

### Don't return by reference
Don't use a reference to a complex class, this cause memory leak. 

### Avoid Floating-Point Divides 
This take many more  machines  cycles  on Intel chips that floating multiplication.

### Use Inlining Judiciously
The judicious use of inlining can help your ray tracer run faster. Only inline small functions, don't inline consturctors, destructors, or virtual functions. This is  only a suggestion to your compiler. 


## Coding Styles
#### Identifiers
Class names, with Upper case.
Member function and data member  names are lower, separated by underscores.
Pointer names, end with underscore concatenated with ptr. 
The names of data members usually contains the data-member name after and underscore.

### Concrete Data Types
Memory that allocate memory dynamically must have their own copy constructor, assigment operator, and destructor.

### Function Signatures
Save you a lot of typing over a lifetime. 

### Pure Virtual and Virtual Functions
Declare as pure virtual a inheritance function to another objects.


## Debugging 
### Ray Trace Single-Pixel Images
Trace a single pixel image for view what is the value at that point.

### Keep Track of Pixel Coordinates
Maintain two global variables that store the current pixeld being rendered. 

### Use count
If't often easiest to debug code in loops with count statements.



## Recomendation
*  Use 2 hit fuction for each object.
	* The first returns the ray parameter at the nearest hit point. 
	* The second is for shadow rays.


