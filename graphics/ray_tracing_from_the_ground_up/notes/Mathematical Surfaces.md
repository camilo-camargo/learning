The are two ways for define the object. The implicit equations are easy to trace, on the other, the parametric equations are more difficult to ray trace.

## Implict Equations
The implicit equation is defined by the following equation
$$ f(x,y,z) = 0 $$
Implicit equation divides 3D space into two regions, less than zero and greater than zero. It can be open and closed, where infiniti means open and closed means is finite surface. 
### Plane
A plane is a flat of a infinity surface, we can express this how a dot product.
$$ (p-a) \cdot n = 0 $$ This equations if we sustitute for each components, we have the following equation
$$ Ax + By + Cz + D = 0 $$
$$ n_{x}x+n_{y}+n_{z} + (-a \cdot n)  = 0 $$

### Spheres 
The spherical shell is defined by a distance and a point. 
$$ c = (c_{x}, c_{y}, c_{z}) $$
$$ p = (x,y,z) $$
The implicit equation
$$ (x-c_{x})^2 + (y-c_{y}) + (z-c_{z})^2 - r^2 = 0 $$ 
## Parametric Equations 
A point on parametric equation is expressed in the form
$$ p(u,v) = [f(u,v), g(u,v), h(u,v)]$$
where f(u,v) are explicit function of the two parameters. 
$$ p(u,v) = [x(u,v), y(u,v), z(u,v)] $$
#### Circular cylinders
$$ x^2 + z^2 = r^2$$ $$ \phi = atan(x,z) $$
If phi is less than zero, then 
$$ \phi = \phi + 2\pi $$
#### spheres
$$ \theta = \cos^{-1}(y/r ) $$
#### Tangent plane
The tangent plane is perpendicular to the normal and just touches the surface at the given point. 
