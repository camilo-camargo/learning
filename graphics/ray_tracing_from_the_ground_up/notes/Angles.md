The angle is mesured how  the slope or a line in regard to the x-axis. If the angle is positive  this is called in **counterclockwise direction**, on the  other hand, is called **clockwise**. 
The angle is denoted by $$ \theta $$
and the value of theta is restricted to the range
$$ (\mathbb{R}, \mathbb{R})$$
## Degree and Radians
An angle can be specified in degrees or radians. 
A radian is the angle that represent the middle arc of the circle.
$$ 1 \ radian = \frac{180\degree}{\pi}$$
The trigonometry functions always represent arguments in radians.
