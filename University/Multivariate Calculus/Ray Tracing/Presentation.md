## Introduction to Python
### Classes
### Operator Overriding
### Dispatch Overriding Functions

## Linear Algebra
### Dot Product
### Cross Product


## PPM File Format 


## Ray Tracing Explanation
### Forward Ray Tracing 
### Backward Ray Tracing
### Refraction
### Reflection 

## Color Theory
### RGB
### CMYK 


## Camera
### Antialiasing
### Gama Correction

## Shading

## Light


## Materials
### Diffuse
### Metal 
## Dielectrics


## Lerp
### My first gradient




## Ray Tracing Implementation
### Basic Camera
### Ray

## Practice
### My First Circle
### My First Sphere



## Next Steps
### Efficiency
### 








