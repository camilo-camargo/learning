from flask import Flask, render_template, request, redirect
from model.user import User
from controller.connection import Connection
from multiprocessing import Process 

import os
import signal


# Mysql Connection
conn = Connection("Learn_Client", "root", "4041") 
conn.init()

# Flask initialization
app = Flask(__name__, template_folder='view') 

# Attempts for send
attemps = 0 

#close the server
def shutdown_server():
    sig = getattr(signal, "SIGKILL", signal.SIGTERM)
    os.kill(os.getpid(),sig)
    
# Pages 

@app.route("/Welcome", methods=['POST', 'GET'])
def welcome():  
    global form_data
    return render_template('welcome.html', username = form_data['username']) 

@app.route("/", methods=['POST', 'GET'])
def index(): 
    global attemps 
    global form_data
    index = "".join(open('view/index.html', "r").readlines()) 

    if(request.method == 'POST'):
        form_data = request.form
        if(form_data['form'] == 'New'):
            user = User(form_data['username'], form_data['password'])
            conn.user_create(user.name(), user.password())
            return redirect('/')

        if(form_data['form'] == 'Send'):  
            if(not (form_data['password'] == form_data['confirm_password'])):
                return redirect('/')


            attemps = attemps + 1
            user = User(form_data['username'], form_data['password'])
            print(user.name, user.password())
            print(conn.user_check(user.name(), user.password()))
            if(conn.user_check(user.name(), user.password())): 
                return redirect('/Welcome')

            if(attemps == 3):  
                attemps = 0
                shutdown_server()

            return redirect('/')

        if(form_data['form'] == 'Exit'):
            shutdown_server()

    if(request.method == 'GET'):
        return index


if __name__ == "__main__":
     app.run()
