import mysql.connector 
from mysql.connector import Error 

class Connection:
    """ Mysql Host Connection class """

    def __init__(self, database, user, password): 
        self.__database = database
        self.__user = user
        self.__password = password 

    def init(self):
        """ Try mysql connection """
        try:
            self.__conn = mysql.connector.connect(
                    host="localhost", 
                    database=self.__database,
                    user = self.__user,
                    password = self.__password)
            if(self.__conn.is_connected()):
                return True  
        except:
            return False  

    def user_check(self, username, password):
        cursor = self.__conn.cursor()
        cursor.execute(f"SELECT username, password FROM user WHERE username LIKE \"{username}\" AND password={password}")
        record = cursor.fetchall()
        if(record == []):
            return False
        return True

    def user_create(self, username, password):
        cursor = self.__conn.cursor() 
        if(not self.user_check(username, password)):
            cursor.execute(f"INSERT INTO user (id,username,password) VALUES (null,\"{username}\", {password})") 
            self.__conn.commit()
            return True
        return True
