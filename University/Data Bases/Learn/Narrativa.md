
## Narrativa
### 1. En su ejercicio  identifiqué
<mark style="background: #FFF3A3A6;">Amarilo</mark> => Sustantivos
<mark style="background: #FF5582A6;">Rojo</mark> => Verbos
<mark style="background: #BBFABBA6;">Verdes</mark> => Adjetivos 

Un estudiante necesita una aplicación informática, la cual le permita llevar su organización respecto a la los archivos que utiliza en su tiempo de estudio, como también sus distintos horarios con su respectiva técnica de aprendizaje. La aplicación posee un usuario, el cual administrara cada entorno de estudio.

La información de esta aplicación informática está organizada de la siguiente manera.


* Cada <mark style="background: #FFF3A3A6;">usuario</mark> debe <mark style="background: #FF5582A6;">tener</mark> uno o mas <mark style="background: #FFF3A3A6;">entorno</mark> en los cuales está trabajando. Un entorno está conformado por un <mark style="background: #BBFABBA6;">Identificador</mark> único, una<mark style="background: #BBFABBA6;"> temática de estudio</mark> , <mark style="background: #BBFABBA6;">aplicaciones</mark> que estructuran el entorno y la<mark style="background: #BBFABBA6;"> ruta de la carpeta</mark> donde están todos los archivos de aprendizaje.
* Los <mark style="background: #FFF3A3A6;">archivos de aprendizaje</mark> están compuestos por un único <mark style="background: #BBFABBA6;">identificador </mark> para cada archivo, <mark style="background: #BBFABBA6;">ruta absoluta</mark> , <mark style="background: #BBFABBA6;">peso</mark> , <mark style="background: #BBFABBA6;">formato</mark> y <mark style="background: #BBFABBA6;">creador</mark> . Estos archivos están <mark style="background: #FF5582A6;">alojados</mark> en uno o más <mark style="background: #FFF3A3A6;">entornos</mark> .
* Los Usuarios de la aplicación están definidos por un <mark style="background: #BBFABBA6;">Identificador</mark> numérico, el cual es único, <mark style="background: #BBFABBA6;">nombre</mark> , <mark style="background: #BBFABBA6;">apellido</mark> , <mark style="background: #BBFABBA6;">email</mark> , <mark style="background: #BBFABBA6;">contraseña</mark> .
* Cada <mark style="background: #FFF3A3A6;">usuario</mark> , el cual es el <mark style="background: #FF5582A6;">creador</mark> de los <mark style="background: #FFF3A3A6;">archivos de aprendizaje</mark> , <mark style="background: #FF5582A6;">posee</mark> uno o más <mark style="background: #FFF3A3A6;">calendarios</mark> que contiene un nombre, año y su respectivo tipo de calendario. Cada calendario <mark style="background: #FF5582A6;">dispone</mark> los  respectivos <mark style="background: #FFF3A3A6;"> eventos</mark> de estudio de cada usuario. 
* Cada <mark style="background: #FFF3A3A6;">entorno</mark> <mark style="background: #FF5582A6;">cuenta con</mark> una<mark style="background: #FFF3A3A6;"> técnica de estudio</mark> . Las técnicas de estudio están determinadas por<mark style="background: #BBFABBA6;"> intervalos y cantidad de intervalos de estudio</mark> , <mark style="background: #BBFABBA6;">cantidad e intervalos de retroalimentación</mark> , <mark style="background: #BBFABBA6;">tiempo de descanso</mark> , además <mark style="background: #FF5582A6;">poseen</mark> <mark style="background: #FFF3A3A6;">reglas</mark> .
* Cada regla de estudio, las cuales contienen un tipo restricciones.
* Un evento se determina por fecha y  descripción.












