import names
from random import random
from random import randrange
from datetime import timedelta
import mysql.connector 
import math 
from datetime import datetime  
import lorem


#stackoverflow

def random_date(start, end):
    """
    This function will return a random datetime between two datetime 
    objects.
    """
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)

c_mysql = mysql.connector.connect(
        user='root',
        password = '4041',
        host = '127.0.0.1',
        database= 'learn')


email_domain = "@gmail.com"
mycursor = c_mysql.cursor()  


def environment_and_files(file, steps):  
    print("Environment... \r") 

    format = ["jpg", "png", "pdf"]
    files = ["notes", "book", "summary", "textbooks", "courses", "multimedia", "data", "supplementary materials", "Open Courseware", "Learning Modules", "Open Textbooks", "Streaming Videos", "Open Access Journals", "Online Tutorials", "Digital Learning Objects"]

    apps = ["Anki", "Obsidian", "Firefox", "Gimp"]
    subject = []
    with open(file, "r") as subject_file: 
        for i in subject_file:
            subject.append(''.join(i.split("\n")).strip())
    mycursor.execute("SET FOREIGN_KEY_CHECKS=0")
    mycursor.execute("TRUNCATE TABLE environment")
    mycursor.execute("") 

    mycursor.execute("TRUNCATE TABLE file")
    mycursor.execute("") 



    for i in range(steps):
        app = apps[math.floor(random()*apps.__len__())]
        sub = subject[math.floor(random()*subject.__len__())]  
        path = ''.join(sub.lower().split())
        mycursor.execute('INSERT INTO environment VALUES (null,"{0}","{1}","./{2}/")'.format(sub, app, path)) 
        path_file = ''.join(files[math.floor(random()*files.__len__())].split())
        format_file = format[math.floor(random()*format.__len__())]
        mycursor.execute('INSERT INTO file VALUES (null, "{0}", {1}, "{2}")'.format(path_file + "_"+ datetime.now().date().__str__(),math.floor(random()*100), format_file))
        c_mysql.commit() 

def email(first_name, last_name):
    return (first_name + "" + last_name + email_domain) 

def user_data(steps): 
    print("\rUser data...")
    global mycursor
    mycursor.execute("SET FOREIGN_KEY_CHECKS=0") 
    c_mysql.commit()
    mycursor.execute("TRUNCATE TABLE user")
    c_mysql.commit()

    for i in range(steps): 
        first_name = names.get_first_name()
        last_name = names.get_last_name()  
        data = '(null,"{0}", "{1}", "{2}", {3})'.format(first_name, last_name, email(first_name, last_name), math.floor((random()*9999))) 
        data_all ="INSERT INTO user values {0}".format(data) 
        mycursor.execute(data_all)
        c_mysql.commit() 


def user_environment_and_files_relationship(steps):
    print("\rUser data...")
    global mycursor
    mycursor.execute("SET FOREIGN_KEY_CHECKS=0") 
    c_mysql.commit()
    mycursor.execute("TRUNCATE TABLE user_environment")
    mycursor.execute("TRUNCATE TABLE environment_file")
    mycursor.execute("TRUNCATE TABLE user_file")
    c_mysql.commit()

    for i in range(steps):
        mycursor.execute("INSERT INTO user_environment VALUES ({0}, {1})".format(math.floor(random()*100),math.floor(random()*100)))
        mycursor.execute("INSERT INTO environment_file VALUES ({0}, {1})".format(math.floor(random()*100),math.floor(random()*100)))
        mycursor.execute("INSERT INTO user_file VALUES ({0}, {1})".format(math.floor(random()*100),math.floor(random()*100)))
        c_mysql.commit()  

def ran(max):
    return math.floor(random()*max)

def calendar(steps): 
    print("\rCalendar data...")
    global mycursor
    mycursor.execute("SET FOREIGN_KEY_CHECKS=0") 
    c_mysql.commit()
    mycursor.execute("TRUNCATE TABLE calendar")
    mycursor.execute("TRUNCATE TABLE user_calendar")
    c_mysql.commit()


    calendars = ["University", "Work", "Job"] 
    cal_type = ["lunisolar", "solar", "lunar", "seasonal"]
    for i in range(steps):
        mycursor.execute('INSERT INTO calendar VALUES (null, "{0}", "{1}", "{2}")'.format(calendars[ran(calendars.__len__())], 2000+ran(40), cal_type[ran(cal_type.__len__())])) 
        for x in range(math.floor(steps/2)):
            try:
                mycursor.execute('INSERT INTO user_calendar VALUES ({0}, {1})'.format(ran(steps)+ran(10), ran(math.floor(steps/2)))) 
            except:
                print("WARNING: repait")
        c_mysql.commit()

def event(users): 
    print("\rEvent data...")
    global mycursor
    mycursor.execute("SET FOREIGN_KEY_CHECKS=0") 
    c_mysql.commit()
    mycursor.execute("TRUNCATE TABLE event")
    mycursor.execute("TRUNCATE TABLE calendar_event")
    c_mysql.commit()


    d1 = datetime.strptime('1/1/2019 1:30 PM', '%m/%d/%Y %I:%M %p')
    d2 = datetime.strptime('1/1/2025 4:50 AM', '%m/%d/%Y %I:%M %p') 
    events = ["Read", "Study Time", "Research", "Programming", "Walking", "Play Ping Pong"]
    for i in range(users):
        mycursor.execute('INSERT INTO event VALUES (null,"{0}","{1}","{2}")'.format(events[ran(events.__len__())],random_date(d1, d2).date(),lorem.sentence()))
        for x in range(users):
            try:
                mycursor.execute('INSERT INTO calendar_event VALUES ({0}, {1})'.format(ran(users), ran(users)))
            except:
                print("WARNING: repait")
        c_mysql.commit() 

def technique():
    print("\rTechnique data...")
    global mycursor
    mycursor.execute("SET FOREIGN_KEY_CHECKS=0") 
    c_mysql.commit()
    mycursor.execute("TRUNCATE TABLE techique")
    mycursor.execute("TRUNCATE TABLE intervals") 
    mycursor.execute('INSERT  INTO intervals VALUES (null, "00:25:00")');
    mycursor.execute('INSERT  INTO intervals VALUES (null, "00:35:00")');

    mycursor.execute('INSERT  INTO techique VALUES (null, null,{0}, "{1}")'.format(ran(2),"00:05:00"))
    c_mysql.commit()

    

if __name__ == "__main__":  
    size = 30
    user_data(size) 
    environment_and_files("subject.txt", size) 
    user_environment_and_files_relationship(size); 
    calendar(2*size) 
    event(size) 
    technique()
    
    c_mysql.close()


