
Camilo Andrés Camargo Castañeda

## Narrativa
### 1. En su ejercicio  identifiqué
<mark style="background: #FFF3A3A6;">Amarilo</mark> => Sustantivos
<mark style="background: #FF5582A6;">Rojo</mark> => Verbos
<mark style="background: #BBFABBA6;">Verdes</mark> => Adjetivos 

Un estudiante necesita una aplicación informática, la cual le permita llevar su organización respecto a la los archivos que utiliza en su tiempo de estudio, como también sus distintos horarios con su respectiva técnica de aprendizaje. La aplicación posee un usuario, el cual administrara cada entorno de estudio.

La información de esta aplicación informática está organizada de la siguiente manera.


* Cada <mark style="background: #FFF3A3A6;">usuario</mark> debe <mark style="background: #FF5582A6;">tener</mark> uno o mas <mark style="background: #FFF3A3A6;">entorno</mark> en los cuales está trabajando. Un entorno está conformado por un <mark style="background: #BBFABBA6;">Identificador</mark> único, una<mark style="background: #BBFABBA6;"> temática de estudio</mark> , <mark style="background: #BBFABBA6;">aplicaciones</mark> que estructuran el entorno y la<mark style="background: #BBFABBA6;"> ruta de la carpeta</mark> donde están todos los archivos de aprendizaje.
* Los <mark style="background: #FFF3A3A6;">archivos de aprendizaje</mark> están compuestos por un único <mark style="background: #BBFABBA6;">identificador </mark> para cada archivo, <mark style="background: #BBFABBA6;">ruta absoluta</mark> , <mark style="background: #BBFABBA6;">peso</mark> , <mark style="background: #BBFABBA6;">formato</mark> y <mark style="background: #BBFABBA6;">creador</mark> . Estos archivos están <mark style="background: #FF5582A6;">alojados</mark> en uno o más <mark style="background: #FFF3A3A6;">entornos</mark> .
* Los Usuarios de la aplicación están definidos por un <mark style="background: #BBFABBA6;">Identificador</mark> numérico, el cual es único, <mark style="background: #BBFABBA6;">nombre</mark> , <mark style="background: #BBFABBA6;">apellido</mark> , <mark style="background: #BBFABBA6;">email</mark> , <mark style="background: #BBFABBA6;">contraseña</mark> .
* Cada <mark style="background: #FFF3A3A6;">usuario</mark> , el cual es el <mark style="background: #FF5582A6;">creador</mark> de los <mark style="background: #FFF3A3A6;">archivos de aprendizaje</mark> , <mark style="background: #FF5582A6;">posee</mark> uno o más <mark style="background: #FFF3A3A6;">calendarios</mark> que contiene un nombre, año y su respectivo tipo de calendario. Cada calendario <mark style="background: #FF5582A6;">dispone</mark> los  respectivos <mark style="background: #FFF3A3A6;"> eventos</mark> de estudio de cada usuario. 
* Cada <mark style="background: #FFF3A3A6;">entorno</mark> <mark style="background: #FF5582A6;">cuenta con</mark> una<mark style="background: #FFF3A3A6;"> técnica de estudio</mark> . Las técnicas de estudio están determinadas por<mark style="background: #BBFABBA6;"> intervalos y cantidad de intervalos de estudio</mark> , <mark style="background: #BBFABBA6;">cantidad e intervalos de retroalimentación</mark> , <mark style="background: #BBFABBA6;">tiempo de descanso</mark> , además <mark style="background: #FF5582A6;">poseen</mark> <mark style="background: #FFF3A3A6;">reglas</mark> .
* Cada regla de estudio, las cuales contienen un tipo restricciones.
* Un evento se determina por fecha y  descripción.


### 2. Clasifique los Adjetivos según el Sustantivo a que hace referencia.
| ENTORNO             | ARCHIVOS | USUARIOS   | CALENDARIO |
| ------------------- | ----------------------- | ---------- | ---------- |
| Identificador       | Identificador           | Nombre     |  Nombre          |
| Tematica_de_estudo  | Ruta absoluta           | Apellido   |     Año       |
| Aplicaciones        | peso                    | Email      |     Tipo       |
| Ruta _de_la_carpeta | formato                 | Contraseña |            |
|                     | creador                 |            |            |

| EVENTO      | TECNICA_DE_ESTUDIO                          | REGLAS |
| ----------- | ------------------------------------------- | ------ |
| fecha       | Intervalo_de_estudio                        | Tipo   |
| Descripcion | Cantidad_de_intervalos_de_estudio           |        |
|             | Intervalos_de_retroalimentacion             |        |
|             | Cantidad_de_intervalos_de_retroalimentacion |        |
|             | Intervalo de descanso                       |        |
|             |                                             |        |



### 3.Identifique los Sustantivos que son enlazados por Verbos.
| Sustantivo | Verbo    | Sustantivo |
| ---------- | -------- | ---------- |
| USUARIO    | Tiene    | ENTORNO    |
| ARCHIVOS   | Alojados | ENTORNO    |
| USUARIO    | Creador  | ARCHIVOS   |
| USUARIO    | Posee    | CALENDARIO |
| CALENDARIO | Dispone  | EVENTOS    |
| ENTORNO    | Cuento   | TECNICA    |
| TECNICA    | Posee    | REGLAS     |


### 4. Modele los sustantivos como Entidades.
![[Pasted image 20220330180126.png]]

### 5. Modele los adjetivos como Atributos de las Entidades.
![[Pasted image 20220330182120.png]]

### 6. Seleccione la PK de cada Entidad.
![[Pasted image 20220330185720.png]]

### 7. Sin duplicar Entidades, modele los verbos como Relaciones entre Entidades.
![[Pasted image 20220401163239.png]]

### 8. Identifique y modele las cardinalidades entre las relaciones.
![[Pasted image 20220401163101.png]]
### 9. Identifique y modele las modalidades entre las relaciones.
![[Pasted image 20220401162937.png]]
###  10. Resuelva las relaciones N:M.
![[manytomany.png]]
### 11. Identifique y modele posibles generalizaciones/especializaciones.
![[Generalization.png]]

### 12. Normalizacion (1FN, 2FN, 3FN)

Este modelo después del último paso de generalización ha quedado normalizada, ya que
1FN: Se encuentra atomizada junto con cero duplicidad.
2FN: Las llaves dependen funcionalmente de la PK.
3FN: No existe dependencia transitiva.
Modelo final 

![[Generalization.png]]
