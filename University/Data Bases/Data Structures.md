## Creating and Using 
## Create Databases
```mysql
CREATE DATABASE learn;
USE learn
```
If the database already exists, you can create an exception.
```mysql
CREATE DATABASE IF NO EXISTS learn;
```

### Case-sensitive
Some operating systems aren't case-sensitive, for that reason, MySQL is has some restriction with that system, for example, Windows.
> We can change the parameter lower_case_table_names


* Options:
	* 0 => specified
	* 1  => lower_case
	* 2 => compared in lower_case, save as specified.

Names with reserved words or with symbols as mention surround by backticks.

## Create Tables 
Syntax.
```mysql 
CREATE TABLE table_name(
		name type [NOT NULL | NULL] [DEFAULT value]
);
```
The name of the table has limitation of 64 characters.
