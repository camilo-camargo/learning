library ieee;
use ieee.std_logic_1164.all;

entity counter_binary is   
	generic( 
		counter_binary_size : integer := 10;
	);
	port(
		counter_binary_clk: in std_logic; 
		counter_binary_reset: in std_logic;
		counter_binary_mode: in std_logic;
		counter_binary_stop: in std_logic; 
		counter_binary_out: out std_logic_vector(counter_binary_size downto 0)
		 );
end counter_binary; 

architecture rtl of counter_binary is 
	signal temp: std_logic_vector(counter_binary_size downto 0);
begin  

	process(counter_binary_clk, counter_binary_reset, counter_binary_mode, counter_binary_stop)
	begin 
	   if (counter_binary_reset = '1' and counter_binary_mode ='1') or (counter_binary_mode = '1' and temp ="0000000000")then
	      temp <= (others => '1');
	   elsif (counter_binary_reset = '1' and counter_binary_mode ='0') or (counter_binary_mode = '0' and temp ="1111111111")then
	      temp <= (others => '0');
	   elsif counter_binary_clk' event and counter_binary_clk = '0' and counter_binary_stop = '0'then
	      temp (9 downto 1) <= temp(8 downto 0);
	      temp(0) <= not temp(9);
	    elsif counter_binary_clk' event and counter_binary_clk = '0' and counter_binary_stop = '0' then
	      temp (8 downto 0) <= temp(9 downto 1);
	      temp(9) <= temp(0);
	   end if; 

	   counter_binary_out <= temp; 
	end process;
end rtl;
