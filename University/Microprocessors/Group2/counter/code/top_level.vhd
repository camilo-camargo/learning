library ieee; 
use ieee.std_logic_1164.all;


entity top_level is
	port(
		top_level_clk:  in std_logic;
		top_level_reset: in std_logic;
		top_level_mode:  in std_logic;
		top_level_stop: in std_logic;
		top_level_pulse: out std_logic;
		top_level_out_7seg: out std_logic_vector(6 downto 0);
		top_level_out_led: out std_logic_vector(9 downto 0)
		 );
end top_level; 

architecture rtl of top_level is
	signal s0 : std_logic;
	signal s1 : std_logic_vector(3 downto 0); 

	component counter is 
   port(        
     counter_clk: in std_logic;
     counter_reset: in std_logic;
	  counter_mode: in std_logic;
     counter_stop: in std_logic; 
     counter_pulse: out std_logic;
     counter_out: out std_logic_vector(3 downto 0)
   );
	end component; 

component dec_7seg is
	port( 
		dec_7seg_in: in std_logic_vector(3 downto 0);
		dec_7seg_out: out std_logic_vector(6 downto 0)
	 );
end component;

component frequency is
	port(
			 frequency_clk50mhz: in std_logic;
			 frequency_clk: out std_logic
		 );
end component;

component counter_binary is 
	port(
		counter_binary_clk: in std_logic; 
		counter_binary_reset: in std_logic;
		counter_binary_mode: in std_logic;
		counter_binary_stop: in std_logic; 
		counter_binary_out: out std_logic_vector(9 downto 0)
		 );
end component; 


begin
	u0: frequency
	 port map(
	 	frequency_clk50mhz  => top_level_clk,
		frequency_clk => s0
	 );	
	u1: counter
	port map( 
	  counter_clk => s0,
     counter_reset => top_level_reset,
	  counter_mode  => top_level_mode,
     counter_stop => top_level_stop,
     counter_pulse =>  top_level_pulse,
     counter_out => s1
	); 
	u2: dec_7seg
	port map( 
		dec_7seg_in => s1,
		dec_7seg_out => top_level_out_7seg
  	);
	u3:  counter_binary
	port map(
		counter_binary_clk => s0,
		counter_binary_reset => top_level_reset,
		counter_binary_mode => top_level_mode,
		counter_binary_stop => top_level_stop,
		counter_binary_out => top_level_out_led
		
       );

end rtl;
