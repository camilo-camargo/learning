library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


	
entity counter is 
   port(        
     counter_clk: in std_logic;
     counter_reset: in std_logic;
	  counter_mode: in std_logic;
     counter_stop: in std_logic; 
     counter_pulse: out std_logic;
     counter_out: out std_logic_vector(3 downto 0)
   );
end entity; 

architecture rtl of counter is 
	constant diez: integer := 10;
	signal r_next, r_reg: std_logic_vector(3 downto 0) := "0000"; 
begin
   --flipflop
   process(counter_clk, counter_reset, counter_mode) 
   begin  
		-- 1001 = 9 decimal
		if((counter_reset='1' and counter_mode='1') or (counter_mode='1' and r_reg="1001")) then
			r_reg <= (others => '0'); 
		elsif((counter_reset ='1' and counter_mode='0') or (counter_mode='0' and r_reg="0000")) then
			r_reg <= "1001";
		elsif(counter_clk'event and counter_clk ='1') then 
			r_reg <= r_next;
		end if;
   end process;  

	r_next <= r_reg - 1 when counter_mode = '0' and counter_stop = '0' else 
		  r_reg + 1 when counter_mode = '1' and counter_stop = '1';   

	counter_out <= r_reg;
end rtl; 
