library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity counter is 
	generic(
		bin: integer := 5
	);
   port(
     counter_clk: in std_logic; 
     counter_reset: in std_logic;
	  counter_mode: in std_logic  ;
     counter_stop: in std_logic ;
	  counter_start: in std_logic_vector(bin-1 downto 0) ;  
	  counter_end: in std_logic_vector(bin-1 downto 0) ;  
     counter_out: out std_logic_vector(bin-1 downto 0)
   );
end entity;

architecture rtl of counter is
	signal r_next,r_temp, r_reg: std_logic_vector(bin-1 downto 0);
begin    
   process(counter_clk, counter_reset, counter_mode)
   begin 
		if(counter_clk'event and counter_clk ='1') then
			if(counter_reset='0' and counter_mode='1')or (counter_mode='1' and r_reg=counter_end) then 
					r_reg <= counter_start;  
			elsif(counter_mode='0') then 
				if (counter_end < counter_start) then  
					if(counter_reset='0') then
						r_reg <= counter_start;  
					elsif(r_reg = counter_end)  then
						r_reg <= counter_start; 
					else
						r_reg <= r_next;
					end if; 
				elsif(counter_end > counter_start) then
					if(counter_reset='0') then 
						r_reg <= counter_end; 
					elsif(r_reg = counter_start) then 
						r_reg <= counter_end;	
					else
						r_reg <= r_next;
					end if; 
				end if;
			else
				r_reg <= r_next;
			end if;  
		end if;
		
   end process;

	r_next <= r_reg - 1 when counter_mode = '0' and counter_stop = '0' else
		  r_reg + 1 when counter_mode = '1' and counter_stop = '0';

	counter_out <= r_reg;
end rtl;
