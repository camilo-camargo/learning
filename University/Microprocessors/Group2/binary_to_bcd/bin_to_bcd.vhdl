----------------------------------------------------------------------------------
-- CONVERSOR DE BINARIO ( 10 BITS ) A BCD (4 DISPLAY)
---------------------------------------------------------------------------------
Library ieee;
Use ieee.std_logic_1164.all;
Use ieee.numeric_std.all;
Use ieee.std_logic_arith.all;
Use ieee.std_logic_unsigned.all;

entity bin_to_bcd is
	GENERIC
	(
		NBITS  : integer :=  5; -- Cantidad de bits del numero binario.
		NSALIDA: integer := 8  -- Cantidad de bits de salida en formato BCD.
	);
	PORT
	(
		bin_to_bcd_in: in  STD_LOGIC_VECTOR(NBITS-1   downto 0);
		bin_to_bcd_out: out STD_LOGIC_VECTOR(NSALIDA-1 downto 0)
	);
end bin_to_bcd;

architecture rtl of bin_to_bcd is
begin
	proceso_bcd: process(bin_to_bcd_in)
		variable z: STD_LOGIC_VECTOR(NBITS+NSALIDA-1 downto 0);
	begin
		-- Inicialización de datos en cero.
		z := (others => '0');
		-- Se realizan los primeros tres corrimientos.
		z(NBITS+2 downto 3) := bin_to_bcd_in;
		-- Ciclo para las iteraciones restantes.
		for i in 0 to NBITS-4 loop
			-- Unidades (4 bits).
			if z(NBITS+3 downto NBITS) > 4 then
				z(NBITS+3 downto NBITS) := z(NBITS+3 downto NBITS) + 3;
			end if;
			-- Decenas (4 bits).
			if z(NBITS+7 downto NBITS+4) > 4 then
				z(NBITS+7 downto NBITS+4) := z(NBITS+7 downto NBITS+4) + 3;
			end if;
			-- Corrimiento a la izquierda.
			z(NBITS+NSALIDA-1 downto 1) := z(NBITS+NSALIDA-2 downto 0);
		end loop;
		-- Pasando datos de variable Z, correspondiente a BCD.
		bin_to_bcd_out <= z(NBITS+NSALIDA-1 downto NBITS);
	end process;
end rtl;
