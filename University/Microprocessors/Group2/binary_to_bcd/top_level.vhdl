library ieee;
use ieee.std_logic_1164.all;

entity top_level is 
	generic(
		bin: integer := 5;
		bcd: integer := 8;
		seg: integer := 7
	);
	port(
		top_level_clk: in std_logic;  
		top_level_reset: in std_logic:='0'; 
		top_level_mode: in std_logic:='0'; 
		top_level_stop: in std_logic:='0'; 
  		 top_level_start: in std_logic_vector(0 to  bin-1):="00011";
  		top_level_end: in std_logic_vector(0 to  bin-1) := "00001";
		top_level_out_7seg_1: out std_logic_vector(0 to seg-1);
		top_level_out_7seg_2: out std_logic_vector(0 to seg-1)
		 );
end top_level;

architecture rtl of top_level is 
	component counter is 
   port(
     counter_clk: in std_logic;
     counter_reset: in std_logic;
	  counter_mode: in std_logic;
     counter_stop: in std_logic;
	  counter_start: in std_logic_vector(bin-1 downto 0); 
	  counter_end: in std_logic_vector(bin-1 downto 0);  
     counter_out: out std_logic_vector(bin-1 downto 0)
   );
	end component; 
	component frequency is
	port(
			 frequency_clk50mhz: in std_logic;
			 frequency_clk: out std_logic
		 );
	end component;
	component dec_7seg is
	port( 
		dec_7seg_in: in std_logic_vector(3 downto 0);
		dec_7seg_out: out std_logic_vector(6 downto 0)
	 );
	end component;

	component bin_to_bcd is
	port
	(
		bin_to_bcd_in: in  STD_LOGIC_VECTOR(bin-1   downto 0);
		bin_to_bcd_out: out STD_LOGIC_VECTOR(bcd-1 downto 0)
	);
	end component;



	signal s0 : std_logic;
	signal s1 : std_logic_vector(bin-1 downto 0):=(others => '0');
	signal s2 : std_logic_vector(bcd-1 downto 0):= (others => '0');
	signal s3 : std_logic_vector(seg-1 downto 0):= (others =>'0');
	signal s4 : std_logic_vector(seg-1 downto 0):= (others => '0');
begin   
	f0: frequency
	port map(
			 frequency_clk50mhz => top_level_clk,
			 frequency_clk => s0
			  ); 
	c0: counter
   port map(
     counter_clk => s0, 
     counter_reset => top_level_reset,
	  counter_mode =>  top_level_mode,
     counter_stop =>    top_level_stop,
	  counter_start => top_level_start,
	  counter_end => top_level_end,
     counter_out => s1
   );  
	b0: bin_to_bcd
	port map(
		bin_to_bcd_in => s1, 
		bin_to_bcd_out => s2
	);
	d0: dec_7seg 
	port map( 
		dec_7seg_in => s2(3 downto 0),
		dec_7seg_out => top_level_out_7seg_1
	 );
	d1: dec_7seg 
	port map( 
		dec_7seg_in => s2(7 downto 4),
		dec_7seg_out => top_level_out_7seg_2
	 ); 
end rtl;

