------------------------------------------------------ 
-- Title: 
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity top_level is  
	port( 
		top_clk: in std_logic;  
		top_money_u: in std_logic; 
		top_money_d: in std_logic;
		top_reset: in std_logic;
  		top_product: out std_logic; 
		top_exchange: out std_logic
	); 
end entity; 

architecture rtl of top_level is   
	-- components 
	component frequency is
	port(
		 frequency_clk50mhz: in std_logic;
		 frequency_clk: out std_logic
	);
	end component;  

	component money is  
	port( 
		money_clk: in std_logic; 
		money_reset: in std_logic;
		money_d: in std_logic;
		money_u: in std_logic;
		money_product: out std_logic;
		money_exchange: out std_logic
	); 
	end component; 
	-- signals 
	signal sig_clk: std_logic;
begin 
	-- code 
	f0: frequency
	port map(
		 frequency_clk50mhz =>  top_clk,
		 frequency_clk => sig_clk
	); 
	m0: money 
	port map(
		money_clk => sig_clk,
		money_reset => top_reset,
		money_d => top_money_d,
		money_u => top_money_u,
		money_product => top_product,
		money_exchange => top_exchange 
	);

end rtl;



