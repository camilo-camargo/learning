------------------------------------------------------ 
-- Title: Counter with finite machine state
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- description:  
library ieee;
use ieee.std_logic_1164.all; 

entity money is  
	--generic(
	--			
	--);
	port( 
		money_clk: in std_logic; 
		money_reset: in std_logic;
		money_d: in std_logic;
		money_u: in std_logic;
		money_product: out std_logic;
		money_exchange: out std_logic
	); 
end entity; 

architecture rtl of money is   
	type state is ( s0,  s1,  s2,  s3,  s4); 
	-- signals 
	signal cur_state, next_state: state := s0;  
	signal cur_sel, cur_coins: std_logic_vector(1 downto 0); 
	
	function money_sel (
		money_sel_d: std_logic; 
		money_sel_u: std_logic;
		money_sel_0: state := s0;
		money_sel_1: state := s0;
		money_sel_2: state := s0
		)return std_logic is variable ret_state: state;
	begin 
	end function;
begin 
	-- code  
	process(money_clk,money_reset) begin 
		if(money_reset = '1') then   
			cur_state <= s0; 	 
		elsif(money_clk'event and money_clk='1') then
			cur_state <= next_state;
		end if; 
	end process;

	process(cur_state, money_d, money_u) begin  
		case cur_state is 
			when s0 =>     
				cur_coins <= "00";
				cur_sel <= "00";
				if(money_d = '0' and money_u = '1') then
					next_state <= s1; -- one coins
				elsif(money_d = '1' and money_u = '0') then
					next_state <= s2; -- two coins
				else
					next_state <= s0; -- zero coins  
				end if;  
				-- here we will implement the money's inputs with push buttons
			when s1 =>  
				cur_coins <= "01"; 
				cur_sel <= "00";
				if(money_d = '0' and money_u = '1') then
					next_state <= s2; -- one coins  
				elsif(money_d = '1' and money_u = '0') then 
					next_state <= s3; -- two coins   
				else
					next_state <= s1; -- zero coins 
				end if; 
			when s2 =>    
				cur_coins <= "10";
				cur_sel <= "00";
				if(money_d = '0' and money_u = '1') then
					next_state <= s3; -- one coins  
				elsif(money_d = '1' and money_u = '0') then 
					next_state <= s4; -- two coins   
				else
					next_state <= s2; -- zero coins 
				end if;  
			when s3 =>  
				cur_coins <= "00"; 
				cur_sel <= "10"; 
				next_state <= s0;
			when s4 => 
				cur_coins <= "10"; 
				cur_sel <= "11"; 
				next_state <= s0;
		end case;	
	end process; 

	money_product <= cur_sel(0);
	money_exchange <= cur_sel(1);  
	
end rtl;



