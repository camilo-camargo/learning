------------------------------------------------------ 
-- Title: 
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity top_level  is  
	---generic(
	---);
	port( 
		top_clk: in std_logic;
		top_reset: in std_logic;  
		top_mode: in std_logic_vector(5 downto 0);
		top_leds: out std_logic_vector(3 downto 0)

		
	); 
end entity; 

architecture rtl of top_level  is   
	-- components 

	component frequency
	port(
		 frequency_clk50mhz: in std_logic;
		 frequency_clk: out std_logic
	 );
	end component; 

	component glass is  
	port(  
		glass_clk: in std_logic; 
		glass_reset: in std_logic;
		glass_sensor_st_0:  in std_logic;
		glass_sensor_st_1: in std_logic;
		glass_sensor_st_2: in std_logic; 
		glass_sensor_sup :  in std_logic;
		glass_sensor_sdw :  in std_logic;
		glass_sensor_fg: in std_logic;
		glass_leds: out std_logic_vector(3 downto 0)
	); 
	end component; 



	-- signals 
	signal sig_clk: std_logic; 
begin  
	-- code 
	f0: frequency
	port map(
		frequency_clk50mhz => top_clk,
		frequency_clk =>  sig_clk
	); 

	g0: glass
	port map(  
		glass_clk => sig_clk, 
		glass_reset => top_reset,
		glass_sensor_st_0 => top_mode(0),
		glass_sensor_st_1 => top_mode(1),
		glass_sensor_st_2 => top_mode(2),
		glass_sensor_sup  => top_mode(3),
		glass_sensor_sdw  => top_mode(4),
		glass_sensor_fg => top_mode(5),
		glass_leds => top_leds
	); 
	


end rtl;



