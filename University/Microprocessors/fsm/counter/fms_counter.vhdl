------------------------------------------------------ 
-- Title: Counter with finite machine state
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all; 


entity fms_counter is  
	generic( 
		fms_counter_out_size: integer := 4
	);
	port( 
		fms_counter_clk: in std_logic;
		fms_counter_mode :in std_logic;
		fms_counter_reset :in std_logic;
		fms_counter_stop :in std_logic;
		fms_counter_out : out std_logic_vector(fms_counter_out_size-1 downto 0)
	); 
end entity; 

architecture rtl of fms_counter is   
	type state is ( s0,  s1,  s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9);

	signal cur_state, next_state: state; 
	signal cur_count: std_logic_vector(fms_counter_out_size-1 downto 0):= (others => '0');
begin 
	-- change the state when the clock is been set
	process(fms_counter_clk, fms_counter_reset, fms_counter_stop) begin 
		if(fms_counter_reset = '1') then
			cur_state <= s0;
		elsif (fms_counter_clk'event and fms_counter_clk = '1' and fms_counter_stop = '0') then
			cur_state <= next_state;
		end if;
	end process; 

	-- finite machine state code  
	-- the mode is ascendant at 1, descendent at 0
	process(cur_state) begin
		case cur_state is  
			----------------------------------------------------------
			-- START
			----------------------------------------------------------
			when s0 =>  
				cur_count <= "0000"; 
				if(fms_counter_mode = '1') then 
						next_state <= s1; -- increment
				else 
					next_state <= s9; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s1 =>  
				cur_count <= "0001";
				if(fms_counter_mode = '1') then 
						next_state <= s2; -- increment
				else 
					next_state <= s0; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s2 =>  
				cur_count <= "0010"; 
				if(fms_counter_mode = '1') then 
						next_state <= s3; -- increment
				else 
					next_state <= s1; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s3 =>  
				cur_count <= "0011"; 
				if(fms_counter_mode = '1') then 
						next_state <= s4; -- increment
				else 
					next_state <= s2; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s4 =>  
				cur_count <= "0100"; 
				if(fms_counter_mode = '1') then 
						next_state <= s5; -- increment
				else 
					next_state <= s3; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s5 =>  
				cur_count <= "0101"; 
				if(fms_counter_mode = '1') then 
						next_state <= s6; -- increment
				else 
					next_state <= s4; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s6 =>  
				cur_count <= "0110"; 
				if(fms_counter_mode = '1') then 
						next_state <= s7; -- increment
				else 
					next_state <= s5; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s7 =>  
				cur_count <= "0111"; 
				if(fms_counter_mode = '1') then 
						next_state <= s8; -- increment
				else 
					next_state <= s6; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s8 =>  
				cur_count <= "1000"; 
				if(fms_counter_mode = '1') then 
						next_state <= s9; -- increment
				else 
					next_state <= s7; -- descendent
				end if; 
			----------------------------------------------------------
			----------------------------------------------------------
			when s9 =>  
				cur_count <= "1001"; 
				if(fms_counter_mode = '1') then 
						next_state <= s0; -- increment
				else 
					next_state <= s8; -- descendent
				end if; 
			----------------------------------------------------------
			-- END
			----------------------------------------------------------

	end case; 
	end process; 
	fms_counter_out <= cur_count;

end rtl;

