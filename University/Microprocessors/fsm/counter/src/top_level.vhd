------------------------------------------------------ 
-- Title: 
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity top_level  is  
	generic(
		bin_to_bcd_binary_size  : integer :=  4;
		bin_to_bcd_out_size: integer := 7; 
		fms_counter_out_size: integer := 4
	);
	port( 
		top_clk: in std_logic;
		top_mode: in std_logic;
		top_reset: in std_logic; 
		top_stop: in std_logic;
		top_out_bcd: out std_logic_vector(bin_to_bcd_out_size-1 downto 0)
	); 
end entity; 

architecture rtl of top_level  is   
	-- components 
	component dec_7seg is
	port( 
		dec_7seg_in: in std_logic_vector(3 downto 0);
		dec_7seg_out: out std_logic_vector(6 downto 0)
	 );
	end component;


	component frequency
	port(
		 frequency_clk50mhz: in std_logic;
		 frequency_clk: out std_logic
	 );
	end component; 

	component fms_counter is  
	port( 
		fms_counter_clk: in std_logic;
		fms_counter_mode :in std_logic;
		fms_counter_reset :in std_logic;
		fms_counter_stop :in std_logic;
		fms_counter_out : out std_logic_vector(fms_counter_out_size-1 downto 0)
	); 
	end component; 
	-- signals 
	signal sig_clk: std_logic; 
	signal sig_counter_out: std_logic_vector(fms_counter_out_size-1 downto 0);
begin  
	-- code 
	f0: frequency
	port map(
		frequency_clk50mhz => top_clk,
		frequency_clk =>  sig_clk
	);    

	c0: fms_counter
	port map( 
		fms_counter_clk => sig_clk,
		fms_counter_mode => top_mode,
		fms_counter_reset => top_reset,
		fms_counter_stop => top_stop,
		fms_counter_out => sig_counter_out
	);   
	d0: dec_7seg
	port map( 
		dec_7seg_in => sig_counter_out,
		dec_7seg_out => top_out_bcd
	 );

end rtl;



