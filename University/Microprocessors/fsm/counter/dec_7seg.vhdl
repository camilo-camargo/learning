library ieee; 
use ieee.std_logic_1164.all;

entity dec_7seg is
	port( 
		dec_7seg_in: in std_logic_vector(3 downto 0);
		dec_7seg_out: out std_logic_vector(6 downto 0)
	 );
end dec_7seg;

architecture rtl of dec_7seg is 
begin
					--   gfedcba
	dec_7seg_out <= not "0111111" when dec_7seg_in = "0000" else -- 0
						 not "0000110" when dec_7seg_in = "0001" else -- 1
						 not "1011011" when dec_7seg_in = "0010" else -- 2
						 not "1001111" when dec_7seg_in = "0011" else -- 3
						 not "1100110" when dec_7seg_in = "0100" else -- 4
						 not "1101101" when dec_7seg_in = "0101" else -- 5
						 not "1111101" when dec_7seg_in = "0110" else -- 6
						 not "0000111" when dec_7seg_in = "0111" else -- 7
						 not "1111111" when dec_7seg_in = "1000" else -- 8
						 not "1101111" when dec_7seg_in = "1001" else -- 9
						 not "1110111" when dec_7seg_in = "1010" else -- a 10
						 not "1111100" when dec_7seg_in = "1011" else -- b 11
						 not "1011110" when dec_7seg_in = "1100" else -- d 12
						 not "1111001" when dec_7seg_in = "1100" else -- e 13
						 not "0111001" when dec_7seg_in = "1101" else -- c 14
						 not "1110001"; -- f 15
end rtl;


