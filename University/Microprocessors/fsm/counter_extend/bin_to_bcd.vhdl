----------------------------------------------------------------------------------
-- CONVERSOR DE BINARIO ( 10 BITS ) A BCD (4 DISPLAY)
---------------------------------------------------------------------------------
Library ieee;
Use ieee.std_logic_1164.all;
Use ieee.numeric_std.all;
Use ieee.std_logic_arith.all;
Use ieee.std_logic_unsigned.all;

entity bin_to_bcd is
	GENERIC
	(
		bin_to_bcd_bits_size  : integer :=  8; -- Cantidad de bits del numero binario.
		bin_to_bcd_out_size: integer := 8  -- Cantidad de bits de salida en formato BCD.
	);
	PORT
	(
		bin_to_bcd_in: in  STD_LOGIC_VECTOR(bin_to_bcd_bits_size-1   downto 0);
		bin_to_bcd_out: out STD_LOGIC_VECTOR(bin_to_bcd_out_size-1 downto 0)
	);
end bin_to_bcd;

architecture rtl of bin_to_bcd is
begin
	proceso_bcd: process(bin_to_bcd_in)
		variable z: STD_LOGIC_VECTOR(bin_to_bcd_bits_size+bin_to_bcd_out_size-1 downto 0);
	begin
		-- Inicialización de datos en cero.
		z := (others => '0');
		-- Se realizan los primeros tres corrimientos.
		z(bin_to_bcd_bits_size+2 downto 3) := bin_to_bcd_in;
		-- Ciclo para las iteraciones restantes.
		for i in 0 to bin_to_bcd_bits_size-4 loop
			-- Unidades (4 bits).
			if z(bin_to_bcd_bits_size+3 downto bin_to_bcd_bits_size) > 4 then
				z(bin_to_bcd_bits_size+3 downto bin_to_bcd_bits_size) := z(bin_to_bcd_bits_size+3 downto bin_to_bcd_bits_size) + 3;
			end if;
			-- Decenas (4 bits).
			if z(bin_to_bcd_bits_size+7 downto bin_to_bcd_bits_size+4) > 4 then
				z(bin_to_bcd_bits_size+7 downto bin_to_bcd_bits_size+4) := z(bin_to_bcd_bits_size+7 downto bin_to_bcd_bits_size+4) + 3;
			end if;
			-- Corrimiento a la izquierda.
			z(bin_to_bcd_bits_size+bin_to_bcd_out_size-1 downto 1) := z(bin_to_bcd_bits_size+bin_to_bcd_out_size-2 downto 0);
		end loop;
		-- Pasando datos de variable Z, correspondiente a BCD.
		bin_to_bcd_out <= z(bin_to_bcd_bits_size+bin_to_bcd_out_size-1 downto bin_to_bcd_bits_size);
	end process;
end rtl;
