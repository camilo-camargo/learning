------------------------------------------------------ 
-- Title: Counter with finite machine state
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all; 


entity fms_counter is  
	--generic(
	--);
	port( 
		fms_counter_clk: in std_logic;
		fms_counter_mode :in std_logic;
		fms_counter_reset :in std_logic;
		fms_counter_stop :in std_logic;
		fms_counter_mode_count :in std_logic_vector(3 downto 0); -- 0001: even, 0010: odd, 0100: 3 factor, 1000: 5 factor  
		fms_counter_out : out std_logic_vector(7 downto 0)
	); 
end entity; 

architecture rtl of fms_counter is   
	type state is ( s0,  s1,  s2,  s3,  s4,  s5,  s6,  s7,  s8,  s9,
						s10, s11, s12, s13, s14, s15, s16, s17, s18, s19, 
						s20, s21, s22, s23, s24, s25, s26, s27, s28, s29, s30);

	signal cur_state, next_state: state; 
	signal cur_count: std_logic_vector(7 downto 0):= (others => '0');
begin 
	-- change the state when the clock is been set
	process(fms_counter_clk, fms_counter_reset, fms_counter_stop) begin 
		if(fms_counter_reset = '1') then
			cur_state <= s0;
		elsif (fms_counter_clk'event and fms_counter_clk = '1' and fms_counter_stop = '0') then
			cur_state <= next_state;
		end if;
	end process; 

	-- finite machine state code  
	-- the mode is ascendant at 1, descendent at 0
	process(cur_state) begin
		case cur_state is  
			----------------------------------------------------------
			-- START
			----------------------------------------------------------
			when s0 =>  
				cur_count <= (others => '0'); 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s2;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s1; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s3;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s1; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s30;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s29;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s30;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s30;
					else 
						next_state <= s30; -- descendent
					end if;
				end if;  
		----------------------------------------------------------
			when s1 =>  
				cur_count <= "00000001"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s2;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s3; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s3;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s2; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s0;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s29;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  

		----------------------------------------------------------
			when s2 =>  
				cur_count <= "00000010"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s4;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s3; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s3;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s0;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  



			----------------------------------------------------------
			when s3 =>  
				cur_count <= "00000011"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s4;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  

			----------------------------------------------------------
			----------------------------------------------------------
			----------------------------------------------------------
			----------------------------------------------------------
			----------------------------------------------------------
			when s4 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  

----------------------------------------------------------
			when s5 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s6 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s7 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  



----------------------------------------------------------
			when s8 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s9 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s10 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  



----------------------------------------------------------
			when s11 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s12 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  

----------------------------------------------------------
			when s13 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  



----------------------------------------------------------
			when s14 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  

----------------------------------------------------------
			when s15 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  



----------------------------------------------------------
			when s16 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s17 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  

----------------------------------------------------------
			when s18 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s19 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s20 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s21 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s22 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  



----------------------------------------------------------
			when s23 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s24 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s25 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s26 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s27 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s28 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  


----------------------------------------------------------
			when s29 =>  
				cur_count <= "00000100"; 
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s6;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s5; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s6;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s5;
					else 
						next_state <= s3; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s2;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s1;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s0;
					else 
						next_state <= s0; -- descendent
					end if;
				end if;  




			----------------------------------------------------------
			-- END
			----------------------------------------------------------
			when s30 => 
				cur_count <= "00110000";
				if(fms_counter_mode = '1') then 
					if(fms_counter_mode_count = "0001") then -- even
						next_state <= s0;
					elsif(fms_counter_mode_count  = "0010") then  -- odd
						next_state <= s1; 
					elsif(fms_counter_mode_count  = "0100") then   -- 3 factor
						next_state <= s0;
					elsif(fms_counter_mode_count  = "1000") then   -- 5 factor
						next_state <= s0;
					else 
						next_state <= s0; -- increment
					end if;
				else 
					if(fms_counter_mode_count = "0001") then  -- even reverse
						next_state <= s28;   
					elsif(fms_counter_mode_count  = "0010") then -- odd reverse
						next_state <= s29;
					elsif(fms_counter_mode_count  = "0100") then  -- 3 factor reverse
						next_state <= s27;
					elsif(fms_counter_mode_count  = "1000") then  -- 5 factor reverse
						next_state <= s25;
					else 
						next_state <= s28; -- descendent
					end if;
				end if;  



		end case; 
	end process; 
	fms_counter_out <= cur_count;

end rtl;

