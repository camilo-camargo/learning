------------------------------------------------------ 
-- Title: 
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity top_level is  
	generic(
		bin_to_bcd_bits_size  : integer :=  8; -- Cantidad de bits del numero binario.
		bin_to_bcd_out_size: integer := 8  -- Cantidad de bits de salida en formato BCD.
	);
	port( 
		top_clk: in std_logic;
		top_mode: in std_logic;
		top_reset: in std_logic;
		top_stop: in std_logic;
		top_mode_count: in std_logic_vector(3 downto 0); 
		top_dec7_0: out std_logic_vector(6 downto 0); 
		top_dec7_1: out std_logic_vector(6 downto 0) 
	); 
end entity; 

architecture rtl of top_level is   
	-- component  
	component frequency
	port(
		frequency_clk50mhz: in std_logic;
		frequency_clk: out std_logic
	 ); 
	end component;

	component bin_to_bcd is
	port
	(
		bin_to_bcd_in: in  STD_LOGIC_VECTOR(bin_to_bcd_bits_size-1   downto 0);
		bin_to_bcd_out: out STD_LOGIC_VECTOR(bin_to_bcd_out_size-1 downto 0)
	);
	end component; 

	component dec_7seg is
	port( 
		dec_7seg_in: in std_logic_vector(3 downto 0);
		dec_7seg_out: out std_logic_vector(6 downto 0)
	 );
	end component;

	
	component fms_counter is  
	port( 
		fms_counter_clk: in std_logic;
		fms_counter_mode :in std_logic;
		fms_counter_reset :in std_logic;
		fms_counter_stop :in std_logic;
		fms_counter_mode_count :in std_logic_vector(3 downto 0); -- 0001: even, 0010: odd, 0100: 3 factor, 1000: 5 factor  
		fms_counter_out : out std_logic_vector(7 downto 0)
	); 
	end component; 
	-- signals 
	signal sig_clk: std_logic; 
	signal sig_bcd: std_logic_vector(7 downto 0); 
	signal sig_dec: std_logic_vector(7 downto 0); 
begin 
	-- code   

	f0:  frequency
	port map( 
		frequency_clk50mhz => top_clk,
		frequency_clk => sig_clk
	 );

	fms0: fms_counter
	port map( 
		fms_counter_clk => sig_clk,
		fms_counter_mode  => top_mode,
		fms_counter_reset  => top_reset,
		fms_counter_stop  => top_stop,
		fms_counter_mode_count  => top_mode_count,
		fms_counter_out  =>  sig_bcd
	);  

	bcd0: bin_to_bcd
	port map(
		bin_to_bcd_in => sig_bcd,
		bin_to_bcd_out => sig_dec
	); 

	d0: dec_7seg
	port map( 
		dec_7seg_in => sig_dec(3 downto 0),
		dec_7seg_out => top_dec7_0
	 );

	d1: dec_7seg
	port map( 
		dec_7seg_in => sig_dec(7 downto 4),
		dec_7seg_out =>  top_dec7_1
	 );


end rtl;



