------------------------------------------------------ 
-- Title:  stoplight with wait  for people cross the street
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity stoplight is  
	--generic(
	--
	--);
	port( 
		stoplight_clk: in std_logic; 
		stoplight_reset: in std_logic;
		stoplight_test: in std_logic;  
		stoplight_person: in std_logic;
		stoplight_leds: out std_logic_vector(5 downto 0) -- r/r/y/y/g/g
	); 
end entity; 

architecture rtl of stoplight is   
	-- constants
	constant max_time: integer  := 2700; --45s 
	constant rg_time:  integer  := 1800; --30s
	constant ry_time:  integer  := 300;  -- 5s
	constant gr_time:  integer  := 2700; --45s
	constant yr_time:  integer  := 300;  -- 5s 
	constant person_time: integer := 6; -- 10s
	constant test_time:integer  := 2;   -- 1s 
	-- signals 

	type state is (rg, ry, gr, yr, yy, wa);  
	signal r1, y1, g1, r2, y2, g2: std_logic;
	signal cur_state, next_state: state;  
	signal time_state: integer range 0 to max_time; 
	signal wait_state_time: integer range 0 to max_time; 
	signal flag_wait: std_logic := '0';
	signal next_wait_state : state; 

begin

process(cur_state, stoplight_test)
	variable temp_count: integer range 0 to max_time;
	begin
	case cur_state is
		when rg => 
			r1 <=  '1'; -- red 1
			y1 <=  '0'; -- yellow 1
			g1 <=  '0'; -- green 1

			r2 <=  '0'; -- red 2
			y2 <=  '0'; -- yellow 2
			g2 <=  '1'; -- green 2 

			next_state <= ry; -- next state red yellow  
			-- Test
			if(stoplight_test='0') then time_state <= rg_time;
			else time_state <= test_time; 
			end if;   

			if(stoplight_person = '1') then   
				next_wait_state <= next_state;
				next_state <= wa;				 
			end if; 

		when ry => 
			r1 <=  '1'; -- red 1
			y1 <=  '0'; -- yellow 1
			g1 <=  '0'; -- green 1 

			r2 <=  '0'; -- red 2
			y2 <=  '1'; -- yellow 2
			g2 <=  '0'; -- green 2 

			next_state <= gr; -- next state red yellow  
			-- Test
			if(stoplight_test='0') then time_state <= ry_time;
			else time_state <= test_time; 
			end if;    

			if(stoplight_person = '1') then   
				next_wait_state <= next_state;
				next_state <= wa;				 
			end if; 

		when gr => 
			r1 <=  '0'; -- red 1
			y1 <=  '0'; -- yellow 1
			g1 <=  '1'; -- green 1

			r2 <=  '1'; -- red 2
			y2 <=  '0'; -- yellow 2
			g2 <=  '0'; -- green 2 


			next_state <= yr; -- next state red yellow  
			-- Test
			if(stoplight_test='0') then time_state <= gr_time;
			else time_state <= test_time; 
			end if; 

			if(stoplight_person = '1') then   
				next_wait_state <= next_state;
				next_state <= wa;				 
			end if; 

		when yr => 
			r1 <=  '0'; -- red 1
			y1 <=  '1'; -- yellow 1
			g1 <=  '0'; -- green 1 

			r2 <=  '1'; -- red 2
			g2 <=  '0'; -- green 2 
			y2 <=  '0'; -- yellow 2

			next_state <= rg; -- next state red yellow  
			-- Test
			if(stoplight_test='0') then time_state <= gr_time;
			else time_state <= test_time; 
			end if;  

			if(stoplight_person = '1') then   
				next_wait_state <= next_state;
				next_state <= wa;				 
			end if; 

		when yy => 
			r1 <=  '0'; -- red 1
			y1 <=  '1'; -- yellow 1
			g1 <=  '0'; -- green 1 

			y2 <=  '1'; -- yellow 2
			r2 <=  '0'; -- red 2
			g2 <=  '0'; -- green 2 

			next_state <= ry; -- next state red yellow    

		when wa => --wait state
			if(flag_wait = '0') then 
				wait_state_time <= time_state;
				temp_count := time_state + person_time;  
				flag_wait <= '1';

			else
				temp_count := temp_count - 1;
				next_state <= wa;
				if(temp_count = wait_state_time) then
					next_state <= next_wait_state; 
					flag_wait <= '0';
				end if;
			end if;
	end case; 
	end process;  

	-- code
	process(stoplight_clk, stoplight_reset) 
		variable count: integer range 0 to max_time;  
	begin 
		if(stoplight_reset = '1') then
			cur_state <= yy; count := 0;
		elsif (stoplight_clk'event and stoplight_clk='1') then
			count := count + 1; 
			if(count=time_state) then --reset the count
				cur_state <= next_state; 
				count := 0;
			end if;
		end if;
	end process; 


	stoplight_leds  <= r1 & y1 & g1 & r2 & y2 & g2; 
end rtl;
