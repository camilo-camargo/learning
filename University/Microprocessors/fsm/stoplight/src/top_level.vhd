------------------------------------------------------ 
-- Title: 
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  

library ieee;
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity top_level is  
	--generic(
	--
	--);
	port(  
		top_clk: in std_logic;
		top_reset: in std_logic;
		top_test: in std_logic;
		top_person: in std_logic; 
		top_leds:  out std_logic_vector(5 downto 0)

	); 
end entity; 

architecture rtl of top_level is  
	-- components  
	component stoplight is  
	port(
		stoplight_clk: in std_logic; 
		stoplight_reset: in std_logic;
		stoplight_test: in std_logic ;  
		stoplight_person: in std_logic ;
		stoplight_leds: out std_logic_vector(5 downto 0) -- r/r/y/y/g/g
	); 
	end component; 

begin 
	-- code 
	sl0: stoplight
	port map( 
		stoplight_clk => top_clk, 
		stoplight_reset => top_reset,
		stoplight_test => top_test,
		stoplight_person => top_person,
		stoplight_leds =>  top_leds
	); 
end rtl;



