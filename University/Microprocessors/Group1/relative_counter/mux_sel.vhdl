library ieee;
use ieee.std_logic_1164.all; 

entity mux_sel is
	port(
		mux_sel_in: in std_logic_vector(3 downto 0);
		mux_sel_out: out std_logic_vector(1 downto 0)
		 );
end entity; 

architecture rtl of  mux_sel is
begin  
	mux_sel_out <= "00" when (mux_sel_in = "0001") else
						 "01" when (mux_sel_in = "0010") else
						 "10" when (mux_sel_in = "0100") else
						 "11";
end rtl;


