library ieee;
use ieee.std_logic_1164.all;

entity eduard is
	port(
	clk:in std_logic;
	reset:in std_logic;
	Salida_Q: out std_logic_vector(6 downto 0)
 );
end eduard;


architecture rtl of eduard is

signal r_reg: std_logic_vector(6 downto 0);
signal r_next: std_logic_vector (6 downto 0);

begin
--registro--
	process (clk, reset)
	begin
	 if (reset='1')then
	  r_reg <= (others=>'0');
	elsif clk'event and clk = '1' then
	  r_reg <= r_next;
  end if;
end process;
--logica del eestado sigiente--
r_next <=
	not "0000110" when r_reg= "1110111" else--a-1--
	not"1111100" when r_reg= "0000110" else--b-2--
	not"1011011" when r_reg= "1111100" else--c-3--
	not"0111001" when r_reg= "1011011" else--d-4--
	not"1001111" when r_reg= "0111001" else--e-5--
	not"1011110" when r_reg= "1001111" else--f--
	not"1100110" when r_reg= "1011110" else
	not"1111001" when r_reg= "1100110" else
	not"1101101" when r_reg= "1111001" else
	not"1110001" when r_reg= "1101101" else
	not"1110111";--r_reg = "1110111"
--logica de salida
Salida_Q <= r_reg;
end rtl;
