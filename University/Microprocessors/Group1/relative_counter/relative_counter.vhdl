library ieee;
use ieee.std_logic_1164.all;

entity relative_counter is 
  port (
   clk: in std_logic;
	reset: in std_logic;
	output: out std_logic_vector(0 to 6)
  );

end relative_counter;

architecture rtl of relative_counter is

signal r_reg: std_logic_vector(6 downto 0);
signal r_next: std_logic_vector(6 downto 0);

begin
    process(clk, reset)
	 begin
	     if (reset = '1')then 
		     r_reg <= (others => '0');
		  elsif clk' event and clk ='1' then 
		     r_reg <= r_next;
		end if;
	end process; 
						 -- gfedcba
	r_next <=  "1111111" when r_reg = "0000000" else
				  "1111110" when r_reg = "1111111" else
				  "1111100" when r_reg = "1111110" else
				  "1111000" when r_reg = "1111100" else
				  "1110000" when r_reg = "1111000" else
				  "1100000" when r_reg = "1110000" else
				  "1000000" when r_reg = "1100000" else
				  not "0000000";

	output <= r_reg;
	end rtl;
