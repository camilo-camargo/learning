library ieee;
use ieee.std_logic_1164.all;

entity frequency is
	port(
			 clk50mhz: in std_logic;
			 clk: out std_logic
		 );
end entity;


architecture rtl of frequency is 
	constant max_count: INTEGER := 2;
	signal count: INTEGER range 0 to max_count;
	signal clk_state: STD_LOGIC := '0';
begin 
	gen_clock: process(clk50mhz,clk_state, count)
	begin
		if clk50mhz' event and clk50mhz='1' then
			if count < max_count then
				count <= count + 1;
			else
				clk_state <= not clk_state;
				count <= 0;
			end if;
		end if;
	end process;

	persecond: process (clk_state)
	begin
		clk <= clk_state;
	end process;
end rtl;
