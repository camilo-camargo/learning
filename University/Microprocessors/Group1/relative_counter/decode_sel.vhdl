library ieee;
use ieee.std_logic_1164.all;


entity decode_sel is 
	port(
		dec_sel_in: in std_logic_vector(1 downto 0);
		dec_sel_out: out std_logic_vector(3 downto 0)
		 );
end decode_sel; 

architecture rtl of decode_sel is
begin 
	dec_sel_out <= "0001" when (dec_sel_in = "00") else
						"0010" when (dec_sel_in = "01") else
						"0100" when (dec_sel_in = "10") else
						"1000";
end rtl;
