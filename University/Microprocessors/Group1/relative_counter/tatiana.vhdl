library ieee;
use ieee.std_logic_1164.all;

entity tatiana is 
port (
   clk: in std_logic;
	reset: in std_logic;
	salida: out std_logic_vector(6 downto 0)
);

end tatiana;

architecture rtl of tatiana is
	signal r_reg: std_logic_vector(6 downto 0);
	signal r_next: std_logic_vector(6 downto 0);

begin
    process(clk, reset)
	 begin
	     if (reset = '1')then 
		     r_reg <= (others => '0');
		  elsif clk' event and clk ='1' then 
		     r_reg <= r_next;
		end if;
	end process;
	--logica del estado siguiente--
	r_next <=
	         not "0111111" when r_reg= "0000110" else --1 0
			   not "1111001" when r_reg= "0111111" else --E
				not "0000111" when r_reg= "1111001" else --7
				not "0111001" when r_reg= "0000111" else --C
				not "1100110" when r_reg= "0111001" else --4
				not "1111100" when r_reg= "1100110" else --b
				not "1110111" when r_reg= "1111100" else --A
				not "1111111" when r_reg= "1110111" else --8
				not "1101101" when r_reg= "1111111" else --5
				"0000110";
	salida <= r_reg;
	end rtl;
				
				
				
				
				
				
				
				
