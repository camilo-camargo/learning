library ieee;
use ieee.std_logic_1164.all;


entity mux_dpy is
	port(
		mux_dpy_in_0, mux_dpy_in_1, mux_dpy_in_2, mux_dpy_in_3: in std_logic_vector(6 downto 0);
		mux_dpy_sel: in std_logic_vector(1 downto 0);
		mux_dpy_out: out std_logic_vector(6 downto 0)
		 );
end entity; 

architecture rtl of  mux_dpy is
begin
	mux_dpy_out  <= mux_dpy_in_0 when (mux_dpy_sel = "00") else
						 mux_dpy_in_1 when  (mux_dpy_sel = "01") else
						 mux_dpy_in_2 when  (mux_dpy_sel = "10") else 
						 mux_dpy_in_3;
end rtl;
