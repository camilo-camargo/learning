library IEEE;
use ieee.std_logic_1164.all;

entity leonardo is 
port(
	clk: in std_logic;
	reset: in std_logic;
	output : out std_logic_vector(6 downto 0)
);
end leonardo;

architecture rtl of leonardo is 
	signal r_reg: std_logic_vector(6 downto 0);
	signal r_next: std_logic_vector(6 downto 0);

	begin 
	--registro
	process (clk, reset)
	begin
	if (reset ='1') then 
		r_reg<= (others => '0');
	elsif clk'event and clk ='1' then
		r_reg <= r_next;
	end if;
	end process;

--logica del estado siguiente
r_next<=
                --gfedcba
              "1111100" when r_reg="0000000" else 
		   	  "1011011" when r_reg="1111100" else   --estado presente, estado siguiente
              "1111111" when r_reg="1011011" else 
				  "1000111" when r_reg="1111111" else 
				  "0000110" when r_reg="1000111" else 
				  "1001111" when r_reg="0000110" else 
				  "1100110" when r_reg="1001111" else 
				  "1101101" when r_reg="1100110" else 	  
			     "1100111" when r_reg="1101101" else 
		  	  	  "0000000";
				 
	--logica salida
	output <=r_reg;
	end rtl;
			
			
