library ieee;
use ieee.std_logic_1164.all;

entity top_level is 
	port( 
			clkTop: in std_logic; 
			selTop: in std_logic_vector(1 downto 0);
			resetTop: in std_logic; 
			ledTop: out std_logic_vector(3 downto 0 );
			outTop: out std_logic_vector(6 downto 0)
		 );
end top_level;

architecture rtl of top_level is
	signal s0: std_logic; 
	signal s1, s2, s3, s4: std_logic_vector(6 downto 0); 
	component frequency is
		port(
			 clk50mhz: in std_logic;
			 clk: out std_logic
		 );
	end component;	 

	component relative_counter is 
		port(
			clk: in std_logic;
			reset: in std_logic;
			output: out std_logic_vector(0 to 6)
		);
	end component;   
	component decode_sel is 
	port(
		dec_sel_in: in std_logic_vector(1 downto 0);
		dec_sel_out: out std_logic_vector(3 downto 0)
		 );
	 end component decode_sel; 
	component leonardo is 
	port(
		clk: in std_logic;
		reset: in std_logic;
		output : out std_logic_vector(6 downto 0)
	);
	end component leonardo;
	component eduard is
	port(
		clk:in std_logic;
		reset:in std_logic;
		Salida_Q: out std_logic_vector(0 to 6)
 );
	end component eduard;
	component tatiana is 
	port (
		clk: in std_logic;
		reset: in std_logic;
		salida: out std_logic_vector(6 downto 0)
	); 
	end component tatiana;


	component mux_dpy is
	port(
		mux_dpy_in_0, mux_dpy_in_1, mux_dpy_in_2, mux_dpy_in_3: in std_logic_vector(6 downto 0);
		mux_dpy_sel: in std_logic_vector(1 downto 0);
		mux_dpy_out: out std_logic_vector(6 downto 0)
		 );
	end component; 
begin  
	u0: frequency
  	port map( 
		clk50mhz => clkTop,
		clk => s0
	); 

	u1: relative_counter -- camilo
	port map(
		clk => s0,
		reset => resetTop,
		output => s1
	  );   
	u2: leonardo
	port map(
		clk => s0,
		reset => resetTop,
		output => s2
	  );   

	u3: eduard
	port map(
		clk => s0,
		reset => resetTop,
		Salida_Q => s3
	  );   
	u4: tatiana
	port map(
		clk => s0,
		reset => resetTop,
		salida => s4
	  );   


	u10: mux_dpy
	port map(
		mux_dpy_in_0  => s1,
		mux_dpy_in_1 => s2,
		mux_dpy_in_2 => s3,
		mux_dpy_in_3 => s4,
		mux_dpy_sel => selTop,
		mux_dpy_out => outTop
  	); 
	u11: decode_sel 
	port map(
		dec_sel_in => selTop,
		dec_sel_out =>  ledTop
	);   

end rtl;
