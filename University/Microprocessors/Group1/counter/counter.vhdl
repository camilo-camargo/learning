library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity counter is  
	port( 
		counter_clk: in std_logic;
		counter_reset: in std_logic;
		counter_mode: in std_logic;
  		counter_stop: in std_logic; 
		counter_pulse: out std_logic;
		counter_output: out std_logic_vector(3 downto 0)
	 );
end counter; 


architecture rtl of counter is 
	signal counter_r_reg: std_logic_vector(3 downto 0) := "0000";
	signal counter_r_next: std_logic_vector(3 downto 0) := "0000";
begin 
	process (counter_clk, counter_reset)
	begin 
		if (counter_reset = '1') then
			counter_r_reg <= (others => '0');
		elsif (counter_clk'event and counter_clk='1') then
			counter_pulse <= '0';
			counter_r_reg <= counter_r_next;
		end if;
	end process;   

-- 0 >= ascendent 
-- 1 >= decendent
	process(counter_r_reg,counter_mode, counter_stop)
	begin 
		if (counter_r_reg = "0000" and counter_mode='0') then 
			counter_r_next <= "1001";
			counter_pulse <= '1';
		elsif (counter_r_reg = "1001" and counter_mode='1') then
			counter_r_next <= "0000"; 
			counter_pulse <= '1';
		elsif (counter_mode = '1' and counter_stop='0') then
			counter_r_next <= (counter_r_reg + 1);  
			counter_pulse <= '0';
		elsif (counter_mode = '0' and counter_stop = '0') then
			counter_r_next <= (counter_r_reg - 1); 
			counter_pulse <= '0';
		end if;
	end process; 

	counter_output <= counter_r_next;
end rtl;
