library ieee;
use ieee.std_logic_1164.all;

entity counter_led is  
   port(
   	counter_led_clk: in std_logic;
      counter_led_reset: in std_logic;
   	counter_led_mode: in std_logic;
   	counter_led_led:  out std_logic_vector(9 downto 0)
       );
end counter_led; 

architecture rtl of counter_led is 
   signal counter_led_dec: std_logic_vector(9 downto 0) := "0000000000";
   signal counter_led_asc: std_logic_vector(0 to 9) := "1111111111";
begin 
   process(counter_led_clk, counter_led_reset, counter_led_mode)
   begin

      if counter_led_reset = '1' or (counter_led_asc="1111111111" and counter_led_mode ='1') then
	 counter_led_asc <= (others => '0');
      elsif counter_led_reset = '1' or (counter_led_dec="0000000000" and counter_led_mode ='0') then
	 counter_led_dec <= (others => '1');
      end if; 

       if counter_led_clk' event and counter_led_clk= '0'  and counter_led_mode ='0' then 
	  counter_led_dec (9 downto 1) <= counter_led_dec (8 downto 0);
	  counter_led_dec (0) <= not counter_led_dec(9);  
	  counter_led_led <= counter_led_dec;
       elsif counter_led_clk'event and counter_led_clk = '1' and counter_led_mode = '1' then
	  counter_led_asc(1 to 9) <= counter_led_asc (0 to 8);
	  counter_led_asc (0) <= not counter_led_asc(9); 
	  counter_led_led <= counter_led_asc;
       end if;
    end process; 
end rtl;
