library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity  top_level is
	port(
		top_clk: in std_logic;   
		top_reset: in std_logic; 
		top_mode: in std_logic;  
		top_stop: in std_logic;
		top_output: out std_logic_vector(6 downto 0);  
		top_led : out std_logic;
		top_out_led: out std_logic_vector(9 downto 0)
	);
end top_level; 

architecture rtl of top_level is 
	signal s0 :std_logic := '0';
	signal s1 :std_logic_vector(3 downto 0) := "0000";  
	component counter
		port( 
			counter_clk: in std_logic;
			counter_reset: in std_logic;
			counter_mode: in std_logic;
			counter_stop: in std_logic;  
			counter_pulse: out std_logic;
			counter_output: out std_logic_vector(3 downto 0)
		 );
	end component counter; 
	component frequency is
	port(
			 frequency_clk50mhz: in std_logic;
			 frequency_clk: out std_logic
		 );
	end component frequency; 

	component dec_7seg is 
		port( 
			dec_7seg_in: in std_logic_vector(3 downto 0);
			dec_7seg_out: out std_logic_vector(6 downto 0)
		 );
      end component dec_7seg;
      component counter_led is 
      port(
   	counter_led_clk: in std_logic;
      	counter_led_reset: in std_logic;
   	counter_led_mode: in std_logic;
   	counter_led_led:  out std_logic_vector(9 downto 0)
       );
      end  component counter_led; 


begin   
	u0: frequency
	port map(
		frequency_clk50mhz => top_clk, 
 		frequency_clk => s0
	);
	u1: counter
	port map(
			counter_clk => s0,
			counter_reset => top_reset, 
			counter_mode =>  top_mode,
			counter_stop => top_stop,
			counter_pulse => top_led,
			counter_output => s1
   ); 
	u2: dec_7seg
	port map(
			dec_7seg_in => s1,
			dec_7seg_out => top_output
	);
	u3: counter_led
	port map(
		counter_led_clk => s0,
		counter_led_reset => top_reset,
		counter_led_mode => top_mode, 
		counter_led_led => top_out_led
		); 

	top_led <= s0;
end rtl;
