Library ieee;
use ieee.std_logic_1164.all;

entity tatiana is
port (
	input: in std_logic_vector (0 to 3);
	output: out std_logic_vector(0 to 6)
);
end tatiana;

architecture flujo of tatiana is
begin

with input select
     output <= "0100001" when "0000", --d
	            "0000010" when "0001", --2
					"0000110" when "0010", --6
					"0111111" when "0011", -- "-"
					"0101011" when "0100", --n
					"0000001" when "0101", --1
					"0000010" when "0110", --2
					"0111111" when "0111", -- "-"
					"0001000" when "1000", --A
					"0000010" when "1001", --2
					"1111111" when "1010", --0
					"1111111" when "1011", --0
					"0000010" when "1100", --2
					"0111111" when "1101", -- "-"
					"0000110" when "1110", --E
					"0001110" when others;--F
end flujo;
