library ieee;
use ieee.std_logic_1164.all;

entity multiplexer is
	port(
		a,b,c,d: in std_logic_vector(0 to 6);
		sel: in std_logic_vector(0 to 1);
		output: out std_logic_vector(0 to 6)
		 );
end multiplexer; 

architecture rtl of multiplexer is  
	signal s1: std_logic_vector(0 to 1);
begin  
	s1 <= sel;
	output <= a when (sel= "00") else
			    b when (sel = "01") else
			    c when (sel = "10") else
			    d;
end rtl; 
