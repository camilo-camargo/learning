library ieee;
use ieee.std_logic_1164.all;

entity camilo is 
    port(
        input: in std_logic_vector(3 downto 0);
        output: out std_logic_vector(6 downto 0)
    );
end camilo;

architecture rtl of camilo is
    begin
        -- d 11 - 
        with input select
        --             gfedcba
        output <= not "1011110" when "0000", -- d
                  not "0000110" when "0001", -- 1
                  not "0000110" when "0010", -- 1
                  not "1000000" when "0011", -- -
                  not "1010100" when "0100", -- |=|
                  not "0111111" when "0101", -- 0
                  not "1100111" when "0110", -- 4  
                  not "1000000" when "0111", -- -
                  not "1110111" when "1000", -- A
                  not "1011011" when "1001", -- 2
                  not "0111111" when "1010", -- 0
                  not "0111111" when "1011", -- 0
                  not "1011011" when "1100", -- 2
                  not "1000000" when "1101", -- -
                  not "1111001" when "1110", -- E
                  not "1110001" when others; -- F
end rtl;
