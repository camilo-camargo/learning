library IEEE;
use IEEE.std_logic_1164.all;

--Descripcion  en caja negra


entity leonardo is
	port(
		input: in std_logic_vector(0 to 3); --Entradas simples
		output: out std_logic_vector(0 to 6)  -- Salidas simples
	);
end leonardo;

--Descripcion del circuito

architecture flujo of leonardo  is
begin
with input select
output <= "0100001" when "0000", --d
          "0100100" when "0001",  --2
		  	 "0000010" when "0010",--6
          "0111111" when "0011",--- _
		    "1001000" when "0100",--n
          "1000000" when "0101",--0
		    "0000000" when "0110",--8.
		    "0111111" when "0111",
          "0100100" when "1001",--2
		    "1000000" when "1010",
          "1000000" when "1011",--0
		    "0100100" when "1100",--2
		    "0111111" when "1101",
          "1000111" when "1110",
		    "0000011" when others;--b 
end flujo;
