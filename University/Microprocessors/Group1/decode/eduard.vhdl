library IEEE;
use IEEE.std_logic_1164.all;

--Descripcion en caja negra

entity eduard is
	port(
		input : in std_Logic_vector(0 to 3);            -- Entradas simples
		output: out std_logic_vector(0 to 6)        -- Salidas simples
	);
end eduard;
                          -- Descripcion del circuito
architecture flujo of eduard is
begin
 with input select
		output <= "0100001" when "0000", -- D
					 "0110000" when "0001",--3
					 "1000000" when "0010", --0
					 "0111111" when "0011",--[-]
					 "0101011" when "0100", -- n
					 "1000000" when "0101",-- 0
					 "0110000" when "0110",-- 3
					 "0111111" when "0111",--[-]
					 "0001000" when "1000",-- A
					 "0100100" when "1001",--2
					 "1000000" when "1010",--0
					 "1000000" when "1011",--0
					 "0110000" when "1100",--3
					 "0111111" when "1101",--[-]
					 "1111001" when "1110",--[e]
					 "1000111" when others;--[L]
					 
						

end flujo;
