library ieee;
use ieee.std_logic_1164.all;

entity top_level is
	port(
		T_input: in std_logic_vector(0 to 3);
		T_sel: in std_logic_vector(0 to 1);
		T_output: out std_logic_vector(0 to 6)
		 );
end top_level; 

architecture rtl of top_level is
	signal s1, s2, s3, s4 : std_logic_vector(0 to 6);
	component camilo is 
    port(
        input: in std_logic_vector(3 downto 0);
        output: out std_logic_vector(6 downto 0)
    );
	end component camilo;

	component leonardo is
		port(
			input: in std_logic_vector(0 to 3); --Entradas simples
			output: out std_logic_vector(0 to 6)  -- Salidas simples
		);
	end component leonardo;

	component tatiana is
	port (
		input: in std_logic_vector (0 to 3);
		output: out std_logic_vector(0 to 6)
	);
	end component tatiana;
	
	component eduard is
	port(
		input : in std_Logic_vector(0 to 3);          
		output: out std_logic_vector(0 to 6)     
	);
	end component eduard; 

	component multiplexer is
	port(
		a,b,c,d: in std_logic_vector(0 to 6);
		sel: in std_logic_vector(0 to 1);
		output: out std_logic_vector(0 to 6)
		 );
	end component multiplexer; 
begin 

	u0: camilo
	port map(
		input => T_input,
		output => s1
	);

	u1: leonardo
	port map(
		input => T_input,
		output => s2
	);

	u2: tatiana
	port map(
		input => T_input,
		output => s3
	);

	u3: eduard
	port map(
		input => T_input,
		output => s4
	); 

	u4: multiplexer 
	port map(
		a => s1,
		b => s2,
		c => s3,
		d => s4,
		sel => T_sel,
		output => T_output
	);
end architecture;
