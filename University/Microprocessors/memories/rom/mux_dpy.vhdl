------------------------------------------------------ 
-- Title: Multiplexer
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date:
-- Description:

library ieee;
use ieee.std_logic_1164.all;


entity mux_dpy is 
	generic(
		mux_dpy_size: integer := 5;
		mux_dpy_sel_size: integer := 2
	);
	port(
		mux_dpy_in_0, mux_dpy_in_1, mux_dpy_in_2: in std_logic_vector(mux_dpy_size-1 downto 0);
		mux_dpy_sel: in std_logic_vector(mux_dpy_sel_size-1 downto 0);
		mux_dpy_out_start: out std_logic_vector(mux_dpy_size-1 downto 0); 
		mux_dpy_out_end: out std_logic_vector(mux_dpy_size-1 downto 0)
		 );
end entity; 

architecture rtl of  mux_dpy is
begin 
	process(mux_dpy_sel)
	begin 
		if(mux_dpy_sel = "00") then 
			mux_dpy_out_start <= mux_dpy_in_0;
			mux_dpy_out_end <= mux_dpy_in_1; 
		elsif(mux_dpy_sel = "01") then
			mux_dpy_out_start <= mux_dpy_in_1;
			mux_dpy_out_end <= mux_dpy_in_2;  
		elsif(mux_dpy_sel = "10") then
			mux_dpy_out_start <= mux_dpy_in_2;
			mux_dpy_out_end <= mux_dpy_in_0;   
		else
			mux_dpy_out_start <= mux_dpy_in_0;
			mux_dpy_out_end <= mux_dpy_in_1; 
		end if;
	end process;
end rtl;
