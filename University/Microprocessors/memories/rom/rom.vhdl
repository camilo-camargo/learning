------------------------------------------------------ 
-- Title: ROM
------------------------------------------------------ 
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 2022-04-27
-- Description: A ROM memory non-volatile

------------------------------------------------------ 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all; 

entity rom is  
	generic(
		rom_address_size: integer := 8; 
		rom_address_out_size: integer := 7; 
		rom_in_address_size: integer := 5; 
		rom_size :integer := 25
	);
	port( 
		rom_clk: in std_logic;  
		rom_addr: in std_logic_vector(rom_in_address_size-1 downto 0);  
		rom_enable: in std_logic;
		rom_data: out std_logic_vector(rom_address_out_size-1 downto 0)
	); 
end entity;  

architecture rtl of rom is
	-- types
	type rom_memory is array(0 to rom_size-1) of std_logic_vector(rom_address_size-1 downto 0); 
	-- signals  
	signal rom_information: rom_memory := ( 
	-- Nicolas

	-- Camilo

	-- Brayan 

	); 
	signal rom_data_tmp: std_logic_vector(rom_address_out_size downto 0);
	begin 
	-- code    
	-- Process that search an specific non-delete information data at ROM space. 
	process(rom_clk, rom_enable)
	begin 
		if rom_clk' event and rom_clk='1' and rom_enable = '1' then 
			rom_data_tmp <= rom_information(conv_integer(rom_addr)); 
			rom_data <= rom_data_tmp(rom_address_out_size-1 downto 0);
		end if;
	end process; 
	--  end comment
end rtl;
