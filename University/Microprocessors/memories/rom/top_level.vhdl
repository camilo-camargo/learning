------------------------------------------------------ 
-- Title:  Top Level Rom
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date:  
-- Description: 
library ieee;
use ieee.std_logic_1164.all;
entity top_level is   
	generic(
		rom_in_address_size: integer := 5; 
		rom_address_size: integer := 8; 
		rom_address_out_size: integer := 7; 
		mux_dpy_size: integer := 5;
		mux_dpy_sel_size: integer := 2;
		counter_bin: integer := 5; 
		top_out_size: integer := 7
	); 
	port(
		top_clk: in std_logic; 
		top_sel: in std_logic_vector(mux_dpy_sel_size-1 downto 0); 
		top_out: out std_logic_vector(top_out_size-1 downto 0)  
	);
end entity; 

architecture rtl of top_level is   
	-- components 
	component counter is 
   port(
     counter_clk: in std_logic; 
     counter_reset: in std_logic;
	  counter_mode: in std_logic  ;
     counter_stop: in std_logic ;
	  counter_start: in std_logic_vector(counter_bin-1 downto 0) ;  
	  counter_end: in std_logic_vector(counter_bin-1 downto 0) ;  
     counter_out: out std_logic_vector(counter_bin-1 downto 0)
   );
	end component;
	component frequency is 
	port(
		frequency_clk50mhz: in std_logic;
		frequency_clk: out std_logic
		 );
	end component; 
	component rom is  
	port( 
		rom_clk: in std_logic;  
		rom_addr: in std_logic_vector(rom_in_address_size-1 downto 0);  
		rom_enable: in std_logic;
		rom_data: out std_logic_vector(rom_address_out_size-1 downto 0)
	); 
	end component;   

	component mux_dpy is 
	port(
		mux_dpy_in_0, mux_dpy_in_1, mux_dpy_in_2: in std_logic_vector(mux_dpy_size-1 downto 0);
		mux_dpy_sel: in std_logic_vector(mux_dpy_sel_size-1 downto 0);
		mux_dpy_out_start: out std_logic_vector(mux_dpy_size-1 downto 0);
		mux_dpy_out_end: out std_logic_vector(mux_dpy_size-1 downto 0)
		 );
	end component; 
	-- signals 
	signal sig_clk : std_logic; 
	signal sig_count: std_logic_vector(counter_bin-1 downto 0); 
	signal sig_count_start,sig_count_end: std_logic_vector(counter_bin-1 downto 0);
begin  
	-- code  
	f0:  frequency
	port map(
		frequency_clk50mhz => top_clk, 
		frequency_clk => sig_clk
	 );
	mux0: mux_dpy
	port map(
		mux_dpy_in_0 =>  "00000",
		mux_dpy_in_1 =>  "10000", 
		mux_dpy_in_2 =>  "11111", 
		mux_dpy_sel =>  top_sel, 
		mux_dpy_out_start => sig_count_start,
		mux_dpy_out_end => sig_count_end
	); 


	c0: counter
	port map(
     counter_clk => sig_clk, 
     counter_reset => '0', 
	  counter_mode => '1', 
     counter_stop => '0', 
	  counter_start => sig_count_start,
	  counter_end => sig_count_end, 
     counter_out =>  sig_count
   ); 

	r0: rom
	port map( 
		rom_clk => sig_clk, 
		rom_addr => sig_count, 
		rom_enable => '1' , 
		rom_data => top_out
	);  
end rtl;
