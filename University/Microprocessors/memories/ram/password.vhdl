------------------------------------------------------ 
-- Title:  Password saver and checker
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all; 


entity password is  
	generic(
		password_data_size: integer := 16;
		password_users_size:  integer := 2
	);
	port( 
		password_clk: in std_logic;
		password_w_or_r: in std_logic := '1'; --write=1, read=0
		password_data: in std_logic_vector(password_data_size-1 downto 0); 
		password_ram_data: in std_logic_vector(password_data_size-1 downto 0); 
		password_ram_w_or_r: out std_logic;
		password_address: out std_logic_vector(password_users_size-1 downto 0); 
		password_led_check: out std_logic := '0';
		password_led_save: out std_logic
	);  
end entity; 

architecture rtl of password is   
	-- signals 
	signal sig_address_out: std_logic_vector(password_users_size-1 downto 0) := "00";  
	signal sig_password_save: std_logic := '0';
begin 
	-- code 
	process(password_clk) 
	begin  
		if(password_clk'event and password_clk = '1') then   
			if(password_w_or_r = '1' and sig_password_save = '0') then  
				password_ram_w_or_r <= '1';
				sig_address_out <= sig_address_out + 1; 
				--flag
				if(sig_address_out = 3) then 
					sig_password_save <= '1';
				end if;  
			elsif(password_w_or_r = '0' and sig_password_save='1') then
				password_ram_w_or_r <= '0';
				sig_address_out <= sig_address_out + 1;

				if(password_ram_data = password_data) then
					password_led_check  <= '1'; 
				else
					password_led_check <= '0';
				end if;
			end if;
		end if;
	end process; 

	password_address <= sig_address_out; 
	password_led_save <= sig_password_save;

end rtl;



