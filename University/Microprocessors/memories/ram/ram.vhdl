------------------------------------------------------ 
-- Title: RAM 
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description:  
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all; 

entity ram is  
	generic(
	   ram_address_size: integer := 2; -- The address size is the number of the each item in ram
		ram_data_size: integer  := 16;   -- The size of each item in ram
		ram_size: integer := 4          -- the size of all item possible in ram
	);
	port( 
		ram_clk: in std_logic;  
		ram_w_or_r: in std_logic;
		ram_address: in std_logic_vector(ram_address_size-1 downto 0);
		ram_data_in: in std_logic_vector(ram_data_size-1 downto 0);
		ram_data_out: out std_logic_vector(ram_data_size-1 downto 0)
	); 
end entity; 

architecture rtl of ram is    
	--type
	type ram_memory is array(ram_size downto 0) of std_logic_vector(ram_data_size-1 downto 0);
	signal ram_data: ram_memory;
	-- signals
begin  
	-- code
	process(ram_clk)
	begin
		if(ram_clk'event and ram_clk = '1') then 
			if(ram_w_or_r = '1') then -- if it is write
				ram_data(conv_integer(ram_address)) <= ram_data_in;
			else -- if it is read 
				ram_data_out <= ram_data(conv_integer(ram_address)); 
			end if; 
		end if;
		end process;
end rtl;



