------------------------------------------------------ 
-- Title:  Top Level RAM Security
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date: 
-- Description: 
library ieee;
use ieee.std_logic_1164.all;

entity top_level is  
	generic(
		password_data_size: integer := 16 ;
		password_users_size:  integer := 2;
	   ram_address_size: integer := 2; -- The address size is the number of the each item in ram
		ram_data_size: integer  := 16;   -- The size of each item in ram
		ram_size: integer := 4          -- the size of all item possible in ram
	);
	port( 
		top_clk : in std_logic;  
		top_data_in: in std_logic_vector(password_data_size-1 downto 0);
		top_w_or_r : in std_logic;
		top_led_allow : out std_logic;
		top_led_save : out std_logic

	); 
end entity; 

architecture rtl of top_level is   
	--components 
	component frequency is 
	port(
			 frequency_clk50mhz: in std_logic;
			 frequency_clk: out std_logic
		 );
	end component;
	component password is
	port( 
		password_clk: in std_logic;
		password_w_or_r: in std_logic := '1'; --write=1, read=0
		password_data: in std_logic_vector(password_data_size-1 downto 0); 
		password_ram_data: in std_logic_vector(password_data_size-1 downto 0); 
		password_ram_w_or_r: out std_logic;
		password_address: out std_logic_vector(password_users_size-1 downto 0); 
		password_led_check: out std_logic := '0';
		password_led_save: out std_logic
	);   
	end component;

	component ram is  
	port( 
		ram_clk: in std_logic;  
		ram_w_or_r: in std_logic;
		ram_address: in std_logic_vector(ram_address_size-1 downto 0);
		ram_data_in: in std_logic_vector(ram_data_size-1 downto 0);
		ram_data_out: out std_logic_vector(ram_data_size-1 downto 0)

	); 
	end component; 


	-- signals 
	signal sig_clk: std_logic; 
	signal sig_w_or_r: std_logic;
	signal sig_user: std_logic_vector(password_users_size-1 downto 0);
	signal sig_ram_data: std_logic_vector(password_data_size-1 downto 0);
begin 
	-- code  

	f0: frequency
	port map(
		 frequency_clk50mhz => top_clk,
		 frequency_clk => sig_clk
	 );
	r0: ram
	port map( 
		ram_clk => sig_clk,
		ram_w_or_r => sig_w_or_r, 
		ram_address =>  sig_user, 
		ram_data_in => top_data_in, 
		ram_data_out => sig_ram_data 
	); 

	p0: password
	port map( 
		password_clk => sig_clk,
		password_w_or_r => top_w_or_r,
		password_data => top_data_in,
		password_ram_data => sig_ram_data,
		password_ram_w_or_r => sig_w_or_r,
		password_address => sig_user,
		password_led_check => top_led_allow, 
		password_led_save => top_led_save
	); 
end rtl;



