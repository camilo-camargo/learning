#include <stdio.h>  

// table of number to binary 7_seg
// n =   gfedcba
// 1 = 0b0 000 0110  "C"
// 2 = 0b01011011 
// 3 = 0b01001111 
// 4 = 0b01100110  
// 5 = 0b01101101 
// 6 = 0b01111101 
// 7 = 0b01000111 
// 8 = 0b01111111 
// 9 = 0b01100111 
// A = 0b01110111 
// b = 0b01111100
// C = 0b01111001  
// d = 0b01011110 
// E = 0b01111001 
// F = 0b01110001

int main(int argc, char **argv){  
	//decode "msg"
	if(argc != 2){
		printf("Usage: %s msg\n", argv[0]);	
	}

	char *msg = argv[1];   
	while(msg != NULL){
		printf("%c", *msg++);	
	}
	return 0;
}
