#include <stdio.h> 
#include <stdlib.h>

void print_bin(unsigned char value)
{
    for (int i = sizeof(char) * 7; i >= 0; i--)
        printf("%d", (value & (1 << i)) >> i );
}

int main(int argc, char **argv){  
	// mux mux_name size  
	if(argc != 3){
		printf("USAGE: mux mux_name size\n");	  
		exit(1);
	} 

	int size = atoi(argv[2]); 
	char* mux =  argv[1]; 

	printf("mux_dpy_out <= ");
	for(int i= 0; i<size; i++){
		printf("%s_in_%d when (%s_sel == \"", mux, i, mux);	
		print_bin(i); 
		if(i != size-1){
			printf("\") else\n\t\t");
		}else{
			printf("\");\n");
		}
	} 
}
