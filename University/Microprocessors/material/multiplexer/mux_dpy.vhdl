------------------------------------------------------ 
-- Title: Multiplexer
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date:
-- Description:

library ieee;
use ieee.std_logic_1164.all;


entity mux_dpy is 
	generic(
		mux_dpy_size: integer := 7;
		mux_dpy_sel_size: integer := 2;
	);
	port(
		mux_dpy_in_0, mux_dpy_in_1, mux_dpy_in_2, mux_dpy_in_3: in std_logic_vector(mux_dpy_size-1 downto 0);
		mux_dpy_sel: in std_logic_vector(mux_dpy_sel_size-1 downto 0);
		mux_dpy_out: out std_logic_vector(mux_dpy_size-1 downto 0)
		 );
end entity; 

architecture rtl of  mux_dpy is
begin
	mux_dpy_out <= mux_dpy_in_0 when (mux_dpy_sel == "00000000") else
						mux_dpy_in_1 when (mux_dpy_sel == "00000001") else
						mux_dpy_in_2 when (mux_dpy_sel == "00000010");
end rtl;
