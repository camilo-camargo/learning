------------------------------------------------------ 
-- Title: Counter Binary
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda 
-- Date:
-- Description: This counter increment or decrement depending of the mode. 

library ieee;
use ieee.std_logic_1164.all; 


entity counter_binary is   
	generic( 
		counter_binary_size : integer := 3 -- size of the counter
	); 
	port(
		counter_binary_clk: in std_logic;           -- clock signal
		counter_binary_reset: in std_logic := '0';  -- Reset option (0 = off) (1 = enable)
		counter_binary_mode: in std_logic  := '1';  -- Mode option  (0 = descendent)  (1 = ascendent)
		counter_binary_stop: in std_logic  := '0';  -- Stop option  (0 = off) (1 = enable)
		counter_binary_out: out std_logic_vector(counter_binary_size-1 downto 0) --  
	);
end counter_binary; 

architecture rtl of counter_binary is 
	signal temp: std_logic_vector(counter_binary_size-1 downto 0)  := (others => '0');  
	signal zeros: std_logic_vector(counter_binary_size-1 downto 0) := (others => '0'); 
	signal ones: std_logic_vector(counter_binary_size-1 downto 0)  := (others => '1');

begin     

	--temp <=  zeros when counter_binary_mode = '1' and temp = ones else
	--			ones  when counter_binary_mode = '0' and temp = ones;

	process(counter_binary_clk, counter_binary_reset, counter_binary_mode, counter_binary_stop)
	begin 

	   if (counter_binary_reset = '1' and counter_binary_mode ='1') or (counter_binary_mode = '1' and temp=ones) then
	      temp <= zeros;
	   elsif (counter_binary_reset = '1' and counter_binary_mode ='0') or (counter_binary_mode = '0' and temp=zeros) then
	      temp <= ones; 
		end if; 

	   if counter_binary_clk' event and counter_binary_clk = '1' and counter_binary_stop = '0'  and counter_binary_mode = '0'then
	      temp (counter_binary_size-1 downto 1) <= temp(counter_binary_size-2 downto 0);
	      temp(0) <= not temp(counter_binary_size-1);
	    elsif counter_binary_clk' event and counter_binary_clk = '1' and counter_binary_stop = '0' and counter_binary_mode = '1' then
	      temp (counter_binary_size-2 downto 0) <= temp(counter_binary_size-1 downto 1);
	      temp(counter_binary_size-1) <= not temp(0);
	   end if; 

	   counter_binary_out <= temp; 
	end process; 

end rtl;
