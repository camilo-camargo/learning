------------------------------------------------------ 
-- Title: Counter Binary
------------------------------------------------------
-- Developer: Camilo Andres Camargo Castaneda  & Jorge Nicolas Diaz Lizarazo
-- Date: 2022-04-22
-- Description: This counter increment or decrement depending of the mode. 

library ieee;
use ieee.std_logic_1164.all;   
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity counter_binary is   
	generic( 
		counter_binary_size : integer := 16; -- size of the counter  
		counter_binary_shift_size: integer := 4
	); 
	port(
		counter_binary_clk: in std_logic;           -- clock signal
		counter_binary_shift: in std_logic_vector(counter_binary_shift_size-1 downto 0);
		counter_binary_out: out std_logic_vector(counter_binary_size-1 downto 0) --  
	);
end counter_binary; 

architecture rtl of counter_binary is 
	signal temp: std_logic_vector(counter_binary_size-1 downto 0)  := (others => '0');  
	signal temp1: unsigned(counter_binary_size-1 downto 0)  := (others => '0');  
	signal zeros: std_logic_vector(counter_binary_size-1 downto 0) := (others => '0'); 
	signal ones: std_logic_vector(counter_binary_size-1 downto 0)  := (others => '1'); 
	signal tmp: std_logic := '0';

begin      

	process(counter_binary_shift)
	begin 
		temp <= zeros;
		temp(counter_binary_shift_size-1 downto 0) <=  counter_binary_shift;
	end process;

	process(counter_binary_clk)
	begin       
		if(counter_binary_clk'event and counter_binary_shift = "0000" and counter_binary_clk = '0') then  
			temp <= ones;
		else 
			tmp <= temp(counter_binary_size-1);
			temp1 <= shift_left(unsigned(temp), 1);
			temp <= std_logic_vector(temp1); 
			temp1 <= (others => '0');
			temp(0) <= tmp;  
		end if; 

		counter_binary_out <= temp;
	end process; 

end rtl;
