# El principio y el pecado 

Hombre es libre, el cual puede decidir entre lo **bueno** y lo **malo**.  (Génesis 3)
Pecado → Es la negación de lo que es Dios
Mediante el pecado, el hombre rechaza el don de la felicidad sobre natural. (Génesis 3,5)
Pecado Original


----
El hombre --- Hombre y Mujer --- es herido por el pecado. 
Hombre → **fatiga** para conseguir su alimento.
Mujer → **Dolores** con la que la mujer dará a luz.

Además, en marca el hecho de la muerte.  "Eres polvo y en polvo te convertirás"(Génesis 3, 19)

----

Imagen y Semejanza de dios ha sido ofuscada. 
La grandeza y dignidad del hombre se realizan en alianza con Dios. 

----

# Él te dominará
El hombre es la «única criatura sobre la tierra que Dios ha amado por sí misma». 
Comunión: «no puede encontrar su propia plenitud si no es en la entrega sincera de sí mismo a los demás»

Aunque en la sagrada escritura hace referencia a una dominación contra la mujer, «Hacia tu marido irá tu apetencia y él te dominará» (_Gén_ 3, 16), generando una ruptura, una amenaza con la anterior mencionada comunión. 
La igualdad dará como resultado la auténtica «communio personarum».



El hombre y la mujer han sido creados por Dios con igual dignidad en cuanto personas humanas y al mismo tiempo, con una recíproca complementariedad en cuanto varón y mujer. 

