> El cristiano que une su propia muerte a la de Jesús ve la muerte como una ida hacia Él y la entrada en la vida eterna. Cuando la Iglesia dice por última vez las palabras de perdón de la absolución de Cristo sobre el cristiano moribundo, lo sella por última vez con una unción fortificante y le da a Cristo en el viático como alimento para el viaje. Le habla entonces con una dulce seguridad:

>  Alma cristiana, al salir de este mundo, marcha en el nombre de Dios Padre Todopoderoso, que te creó, en el nombre de Jesucristo, Hijo de Dios vivo, que murió por ti, en el nombre del Espíritu Santo, que sobre ti descendió. Entra en el lugar de la paz y que tu morada esté junto a Dios en Sión, la ciudad santa, con Santa María Virgen, Madre de Dios, con san José y todos los ángeles y santos [...] Te entrego a Dios, y, como criatura suya, te pongo en sus manos, pues es tu Hacedor, que te formó del polvo de la tierra. Y al dejar esta vida, salgan a tu encuentro la Virgen María y todos los ángeles y santos [...] Que puedas contemplar cara a cara a tu Redentor» (_Rito de la Unción de Enfermos y de su cuidado pastoral, Orden de recomendación de moribundo_s, 146-147).


 1. ¿Qué tipo de felicidad es la que realmente perfecciona la vida?

El tipo de felicidad que realmente perfecciona la vida no es materialidad, ni mucha menos dependencia. El tipo creencia a un Dios superior, creador, cuidador, y de amor, la verdadera felicidad.

2. ¿Todo hombre desea la felicidad última?                

La felicidad última considera como la ida hacia Dios, puede que esté sometida a debate por personas no creyentes, por tal motivo, la respuesta es no. La respuesta a esta interrogante puede afirmarse si y solo si la persona es creyente, de otro modo, la respuesta al ser afirmativa está sujeta a una felicidad equívoca o temporal.

3. ¿Puede el hombre alcanzar la suprema felicidad?

El hombre puede alcanzarlo tomando diferentes caminos, unos más rápidos que otros, pero todos llegando al final, la suprema felicidad. El hombre si puede llegar a la suprema felicidad, poniendo su espíritu, y alma en él en cruento con Dios, no saturando su deseo instantáneo y mucho menos vulnerando la dignidad de otra persona. Cumpliendo lo anterior mencionado, el hombre será feliz.

Cumpliendo lo anterior mencionado, el hombre sera feliz. 

4. ¿Puede un hombre ser más feliz que otro?

Si, ya que cada ser humano es educado socialmente diferente, partiendo de que si uno es ateo según la felicidad cristiana, es imposible conseguir dicho gozo. Existen además personas las cuales pierden su rumbo por felicidades instantáneas, las cuales, como punto final, desemboca en una amargura. Por lo mencionado anteriormente, la facilidad se lleva siempre y cuando estemos de mano con el creador, y aportando a un bien común.

5. ¿Expuesto el pensamiento de santo Tomás, puede alguien ser feliz en esta vida?
Todo el mundo puede ser feliz, si ponemos la razionalidad y la fe al la par.

6. ¿Existe en esta vida la perfecta y verdadera felicidad, si así lo fuera cómo se puede lograr?

La verdadera felicidad se logra partir de tener lo necesario y servir a los demás, además de poner en práctica la religión. Y no los deseos placenteros de placer.

 7. ¿Una persona que no le interesa conocer las cosas acerca de Dios, o mejor aún que se considere ateo puede alcanzar la suprema felicidad?

Como he mencionado en las respuestas 2 y 5, dicha felicidad es otorgada a las personas creyentes, las cuales no tienen una felicidad instantánea, sino divina, la cual trasciende fronteras, universos, y cualquier cosa viviente.

8. ¿En nuestro contexto actual el **hedonismo** y el **consumismo** se podrían considerar como la felicidad última del hombre?  


 Según el texto ¿Cómo entiende y  describe usted su felicidad?
 
 9. Haga una resumen del Catecismo de la Iglesia Católica sobre: el juicio particular, el cielo, el purgatorio, el infierno, el juicio final, la esperanza de los cielos nuevos y la tierra nueva.

En resumen, el catecismo explica la muerte como una vía para ir con el creador, con ello supone dos paraderos. El purgatorio, el paradero para personas pecadoras, las cuales se les establece el pecado de las tinieblas, el cual es vivir en llamas. El cielo, el cual da lugar a un nuevo ciclo de vida que es otorgado por Jesucristo.


Link del Catecismo de la Iglesia Católica: [http://www.vatican.va/archive/catechism_sp/p123a12_sp.html](http://www.vatican.va/archive/catechism_sp/p123a12_sp.html)