---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
La Biblia ^quEjZxtC

Antiguo Testamento ^x0olDWKX

Nuevo Testamento ^2dnxos8K

Pentateuco ^O2pOB4yU

Libros Historicos ^xG3Z5sAW

Libros Profeticos ^jQsy744p

Josue
Jueces
1 y 2 Samuel
1 y 2 Reyes
1 y 2 Cronicas
Esdras
Nehemias
Tobias
Judit
Ester
1 y 2 Macabeos ^so9iCqxc

Genesis
Exodo
Levitico
Numeros
Deuteronomio 
Pentateuco ^fuwNN2L1

 ^hU9c976U

 ^4pMAptlN

Isaias
Jeremias
Ezequiel
Daniel
Oseas
Joel
Amos
Abdias
Jonas
Miqueas
Nahum
Habacuc
Sofonias
Ageo
Zacarias
Malaquias
Lamentaciones
Baruc
 ^ZUo2lsjP

Libros Poeticos y Sapienciales ^BggMsbSS

Salmos
Cantor de los cantares
Proverbios
Job
Eclesiastes
Eclesiastico
Sabiduria ^yYlsmfT8

Evangelios ^sKNnM9hU

Cartas de San Pablo ^8onqwlg9

Cartas Catolicas ^3dhPcRta

Mateo
Marcos
Lucas
Juan ^LH6tyKkS

Romanos
1 y 2 Corintios
Galatas
Efesios
Filipenses
Colosenses
1 y 2 Tesalonicenses
1 y 2 Timoteo
Tito
Filemon
Hebreos ^KC7eNCU7

Santiago
1 y 2 Pedro
1, 2 y 3 Juan
Judas ^FyYNcgU4

Hechos de los Apostoles ^52Mdgees

Apocalispis ^kReZmivj

Camilo Andres Camargo Castaneda ^591iH8FI

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "rectangle",
			"version": 244,
			"versionNonce": 1063336426,
			"isDeleted": false,
			"id": "-oMbikaTibvVfFTDVaV0j",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 53.47066406341469,
			"y": -169.17639271487076,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 180,
			"height": 35,
			"seed": 336863594,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"type": "text",
					"id": "quEjZxtC"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 196,
			"versionNonce": 1305948598,
			"isDeleted": false,
			"id": "quEjZxtC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 58.47066406341469,
			"y": -164.17639271487076,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 170,
			"height": 25,
			"seed": 1051428394,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "La Biblia",
			"rawText": "La Biblia",
			"baseline": 17,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "-oMbikaTibvVfFTDVaV0j",
			"originalText": "La Biblia"
		},
		{
			"type": "rectangle",
			"version": 434,
			"versionNonce": 62287018,
			"isDeleted": false,
			"id": "5-qNFP38e9-ZlBQcTn6rA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 593.8221740722656,
			"y": 62.09139903317612,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 180,
			"height": 60,
			"seed": 404235062,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "2dnxos8K",
					"type": "text"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 481,
			"versionNonce": 179538678,
			"isDeleted": false,
			"id": "2dnxos8K",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 598.8221740722656,
			"y": 67.09139903317612,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 170,
			"height": 50,
			"seed": 794252074,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Nuevo \nTestamento",
			"rawText": "Nuevo Testamento",
			"baseline": 42,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "5-qNFP38e9-ZlBQcTn6rA",
			"originalText": "Nuevo Testamento"
		},
		{
			"type": "line",
			"version": 393,
			"versionNonce": 1415006058,
			"isDeleted": false,
			"id": "n_4nZuumApoebSvY_kgZd",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -209.20080580560878,
			"y": -6.304225936209718,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 893.3094188145228,
			"height": 0,
			"seed": 1994489462,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					893.3094188145228,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 117,
			"versionNonce": 892517430,
			"isDeleted": false,
			"id": "fy1ryHXroewsVBDaJOVjI",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 142.2545938438751,
			"y": -118.13089372185425,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 112.96775817871094,
			"seed": 13913066,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					112.96775817871094
				]
			]
		},
		{
			"type": "line",
			"version": 65,
			"versionNonce": 2039401002,
			"isDeleted": false,
			"id": "IWBt4KbrRT-9ryggkNns7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -212.10487360437324,
			"y": -7.548776274819403,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 61.49867007606909,
			"seed": 251146026,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					61.49867007606909
				]
			]
		},
		{
			"type": "line",
			"version": 124,
			"versionNonce": 1127859574,
			"isDeleted": false,
			"id": "P82QGgqk9WoUbAU9kJN0w",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 683.5665664550655,
			"y": -6.878128294923215,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 60.47769546508789,
			"seed": 1744922154,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					60.47769546508789
				]
			]
		},
		{
			"type": "rectangle",
			"version": 424,
			"versionNonce": 2051536106,
			"isDeleted": false,
			"id": "ewdJzGmhL2Jx4fY57KKrx",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -298.16250495940284,
			"y": 57.07060130368393,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 180,
			"height": 60,
			"seed": 1318005174,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "x0olDWKX",
					"type": "text"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 416,
			"versionNonce": 638428854,
			"isDeleted": false,
			"id": "x0olDWKX",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -293.16250495940284,
			"y": 62.07060130368393,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 170,
			"height": 50,
			"seed": 77198506,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Antiguo \nTestamento",
			"rawText": "Antiguo Testamento",
			"baseline": 42,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "ewdJzGmhL2Jx4fY57KKrx",
			"originalText": "Antiguo Testamento"
		},
		{
			"type": "rectangle",
			"version": 844,
			"versionNonce": 1208862634,
			"isDeleted": false,
			"id": "bqL66CWlOKxpZQWdEZDnD",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -585.0801406252301,
			"y": 288.7851422834188,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 129,
			"height": 35,
			"seed": 1787883510,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "O2pOB4yU",
					"type": "text"
				},
				{
					"id": "qAffX0YpqVRgfHaQvlaYM",
					"type": "arrow"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 797,
			"versionNonce": 1795900010,
			"isDeleted": false,
			"id": "O2pOB4yU",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -580.0801406252301,
			"y": 293.7851422834188,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 119,
			"height": 25,
			"seed": 1999549034,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Pentateuco",
			"rawText": "Pentateuco",
			"baseline": 17,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "bqL66CWlOKxpZQWdEZDnD",
			"originalText": "Pentateuco"
		},
		{
			"type": "rectangle",
			"version": 752,
			"versionNonce": 269480246,
			"isDeleted": false,
			"id": "96oyZNGwd42sMkr9QPAHn",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -216.53877586460408,
			"y": 277.4262326764852,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 177,
			"height": 60,
			"seed": 457006838,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "jQsy744p",
					"type": "text"
				},
				{
					"id": "7Kavd9TAv7l14Dne67puL",
					"type": "arrow"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 785,
			"versionNonce": 380131958,
			"isDeleted": false,
			"id": "jQsy744p",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -211.53877586460408,
			"y": 282.4262326764852,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 167,
			"height": 50,
			"seed": 1225044842,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Libros \nProfeticos",
			"rawText": "Libros Profeticos",
			"baseline": 42,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "96oyZNGwd42sMkr9QPAHn",
			"originalText": "Libros Profeticos"
		},
		{
			"type": "line",
			"version": 485,
			"versionNonce": 1249456106,
			"isDeleted": false,
			"id": "BbT3PDYBbOJumQDxZBZPa",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -128.6950423025183,
			"y": 310.42128203767805,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 2.539669942150198,
			"height": 3.7960313844814664,
			"seed": 751180906,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					2.539669942150198,
					-3.7960313844814664
				]
			]
		},
		{
			"type": "rectangle",
			"version": 750,
			"versionNonce": 417438646,
			"isDeleted": false,
			"id": "GqhsyHkTw4fggt9oWk4Wo",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -414.07541426312105,
			"y": 276.2851422834188,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 165,
			"height": 60,
			"seed": 581406838,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "xG3Z5sAW",
					"type": "text"
				},
				{
					"id": "dIUgo3EQm9biLVMjc--wF",
					"type": "arrow"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 753,
			"versionNonce": 484488438,
			"isDeleted": false,
			"id": "xG3Z5sAW",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -409.07541426312105,
			"y": 281.2851422834188,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 155,
			"height": 50,
			"seed": 1773165034,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Libros \nHistoricos",
			"rawText": "Libros Historicos",
			"baseline": 42,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "GqhsyHkTw4fggt9oWk4Wo",
			"originalText": "Libros Historicos"
		},
		{
			"type": "line",
			"version": 457,
			"versionNonce": 644463978,
			"isDeleted": false,
			"id": "wNliwtyUgV9Iaj748Xjzd",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -336.47810466571315,
			"y": 313.04880010115096,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 2.539669942150198,
			"height": 3.7960313844814664,
			"seed": 188902506,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					2.539669942150198,
					-3.7960313844814664
				]
			]
		},
		{
			"type": "rectangle",
			"version": 1364,
			"versionNonce": 472557110,
			"isDeleted": false,
			"id": "XrdBQolF0M9CbVpSDFBoM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -418.89471367847506,
			"y": 415.4472472750788,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 168,
			"height": 335,
			"seed": 1681575722,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "so9iCqxc",
					"type": "text"
				},
				{
					"id": "dIUgo3EQm9biLVMjc--wF",
					"type": "arrow"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 1489,
			"versionNonce": 396652406,
			"isDeleted": false,
			"id": "so9iCqxc",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -413.89471367847506,
			"y": 445.4472472750788,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 158,
			"height": 275,
			"seed": 754749558,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Josue\nJueces\n1 y 2 Samuel\n1 y 2 Reyes\n1 y 2 Cronicas\nEsdras\nNehemias\nTobias\nJudit\nEster\n1 y 2 Macabeos",
			"rawText": "Josue\nJueces\n1 y 2 Samuel\n1 y 2 Reyes\n1 y 2 Cronicas\nEsdras\nNehemias\nTobias\nJudit\nEster\n1 y 2 Macabeos",
			"baseline": 268,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "XrdBQolF0M9CbVpSDFBoM",
			"originalText": "Josue\nJueces\n1 y 2 Samuel\n1 y 2 Reyes\n1 y 2 Cronicas\nEsdras\nNehemias\nTobias\nJudit\nEster\n1 y 2 Macabeos"
		},
		{
			"type": "freedraw",
			"version": 540,
			"versionNonce": 1991568106,
			"isDeleted": false,
			"id": "RArKoK5e3gZNP3mhnZqtX",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -301.3156518051776,
			"y": 550.9934947941058,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.8834248204385062,
			"height": 2.0613196588330993,
			"seed": 1541300278,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.14723747007309385,
					-0.29447001795608685
				],
				[
					0.14723747007309385,
					-0.5889449581022745
				],
				[
					0.7361873503654124,
					-1.7668447186869685
				],
				[
					0.8834248204385062,
					-2.0613196588330993
				],
				[
					0.8834248204385062,
					-2.0613196588330993
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 514,
			"versionNonce": 731058358,
			"isDeleted": false,
			"id": "uZLrrHw5l9RQ4ZCrQiXhY",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -308.2576187473119,
			"y": 448.9140536369548,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 1.7668447186870253,
			"height": 1.6196121708039186,
			"seed": 961916406,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.14723747007309385,
					0.14723747007306542
				],
				[
					0.8834248204385631,
					-0.44171241021928154
				],
				[
					1.4723747007308816,
					-1.177899760584694
				],
				[
					1.7668447186870253,
					-1.4723747007308532
				],
				[
					1.7668447186870253,
					-1.4723747007308532
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "rectangle",
			"version": 1208,
			"versionNonce": 1466695082,
			"isDeleted": false,
			"id": "06cc0vhUqb-OPgzhjhhiF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -600.9478935377883,
			"y": 415.4472472750788,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 151,
			"height": 160,
			"seed": 1105298922,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "fuwNN2L1",
					"type": "text"
				},
				{
					"id": "qAffX0YpqVRgfHaQvlaYM",
					"type": "arrow"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 1156,
			"versionNonce": 489863274,
			"isDeleted": false,
			"id": "fuwNN2L1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -595.9478935377883,
			"y": 420.4472472750788,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 141,
			"height": 150,
			"seed": 1800512950,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Genesis\nExodo\nLevitico\nNumeros\nDeuteronomio \nPentateuco",
			"rawText": "Genesis\nExodo\nLevitico\nNumeros\nDeuteronomio \nPentateuco",
			"baseline": 142,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "06cc0vhUqb-OPgzhjhhiF",
			"originalText": "Genesis\nExodo\nLevitico\nNumeros\nDeuteronomio \nPentateuco"
		},
		{
			"type": "freedraw",
			"version": 492,
			"versionNonce": 247651126,
			"isDeleted": false,
			"id": "LWBccmkbQqKvYuUqYL6Q1",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -527.9980713472889,
			"y": 503.8284930308678,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.8834248204385062,
			"height": 2.0613196588330993,
			"seed": 2025815798,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.14723747007309385,
					-0.29447001795608685
				],
				[
					0.14723747007309385,
					-0.5889449581022745
				],
				[
					0.7361873503654124,
					-1.7668447186869685
				],
				[
					0.8834248204385062,
					-2.0613196588330993
				],
				[
					0.8834248204385062,
					-2.0613196588330993
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 491,
			"versionNonce": 1473518378,
			"isDeleted": false,
			"id": "7fKTgJDczg1VSjwM68Xpz",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -509.29893725895727,
			"y": 473.7748460818382,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 1.4723648563507936,
			"height": 2.6502695391255315,
			"seed": 338989930,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.14723254788304985,
					-0.2944749401461877
				],
				[
					0.5889498802923185,
					-1.177899760584694
				],
				[
					1.4723648563507936,
					-2.6502695391255315
				],
				[
					1.4723648563507936,
					-2.6502695391255315
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 494,
			"versionNonce": 1399719030,
			"isDeleted": false,
			"id": "6whta-Cx2WY7C0j_x9WEk",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -534.1820352459783,
			"y": 448.2855459385039,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 2.0613245810232,
			"height": 3.5336894373739938,
			"seed": 1704996918,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.14723747007309385,
					-0.14723254788304985
				],
				[
					-0.14723747007309385,
					-0.44170748802923754
				],
				[
					0,
					-1.6196072486139315
				],
				[
					0.5889498802923754,
					-2.355794598979344
				],
				[
					1.3251372306577878,
					-3.239214497227806
				],
				[
					1.9140871109501063,
					-3.5336894373739938
				],
				[
					1.9140871109501063,
					-3.5336894373739938
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 492,
			"versionNonce": 1706763754,
			"isDeleted": false,
			"id": "u8Xyz3CSS8oU6qGI__SVn",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -521.077920098234,
			"y": 425.5851340496429,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 1.7668447186870253,
			"height": 1.6196121708039186,
			"seed": 1742902826,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.14723747007309385,
					0.14723747007306542
				],
				[
					0.8834248204385631,
					-0.44171241021928154
				],
				[
					1.4723747007308816,
					-1.177899760584694
				],
				[
					1.7668447186870253,
					-1.4723747007308532
				],
				[
					1.7668447186870253,
					-1.4723747007308532
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 598,
			"versionNonce": 1130497462,
			"isDeleted": false,
			"id": "tqkdZT1MAlk3K4SyzmG4Z",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -328.1127020051506,
			"y": 627.6937101955673,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.8834248204385062,
			"height": 2.0613196588330993,
			"seed": 219243638,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.14723747007309385,
					-0.29447001795608685
				],
				[
					0.14723747007309385,
					-0.5889449581022745
				],
				[
					0.7361873503654124,
					-1.7668447186869685
				],
				[
					0.8834248204385062,
					-2.0613196588330993
				],
				[
					0.8834248204385062,
					-2.0613196588330993
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 279,
			"versionNonce": 536008874,
			"isDeleted": false,
			"id": "hU9c976U",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -162.8652204620197,
			"y": 396.4238484174666,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11,
			"height": 25,
			"seed": 2047102378,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "",
			"rawText": "",
			"baseline": 17,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": ""
		},
		{
			"type": "text",
			"version": 279,
			"versionNonce": 801365750,
			"isDeleted": false,
			"id": "4pMAptlN",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -148.42077601757524,
			"y": 396.4238484174666,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 11,
			"height": 25,
			"seed": 1405255274,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724553,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "",
			"rawText": "",
			"baseline": 17,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": ""
		},
		{
			"type": "rectangle",
			"version": 1417,
			"versionNonce": 103166826,
			"isDeleted": false,
			"id": "ZxIwj_zroddo-8tlPrtnZ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -209.11883552346183,
			"y": 415.4472472750788,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 168,
			"height": 486,
			"seed": 472462134,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "ZUo2lsjP",
					"type": "text"
				},
				{
					"id": "7Kavd9TAv7l14Dne67puL",
					"type": "arrow"
				}
			],
			"updated": 1651873724553,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 1771,
			"versionNonce": 1788884522,
			"isDeleted": false,
			"id": "ZUo2lsjP",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -204.11883552346183,
			"y": 420.9472472750788,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 158,
			"height": 475,
			"seed": 2090812202,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Isaias\nJeremias\nEzequiel\nDaniel\nOseas\nJoel\nAmos\nAbdias\nJonas\nMiqueas\nNahum\nHabacuc\nSofonias\nAgeo\nZacarias\nMalaquias\nLamentaciones\nBaruc\n",
			"rawText": "Isaias\nJeremias\nEzequiel\nDaniel\nOseas\nJoel\nAmos\nAbdias\nJonas\nMiqueas\nNahum\nHabacuc\nSofonias\nAgeo\nZacarias\nMalaquias\nLamentaciones\nBaruc\n",
			"baseline": 467,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "ZxIwj_zroddo-8tlPrtnZ",
			"originalText": "Isaias\nJeremias\nEzequiel\nDaniel\nOseas\nJoel\nAmos\nAbdias\nJonas\nMiqueas\nNahum\nHabacuc\nSofonias\nAgeo\nZacarias\nMalaquias\nLamentaciones\nBaruc\n"
		},
		{
			"type": "freedraw",
			"version": 634,
			"versionNonce": 927275382,
			"isDeleted": false,
			"id": "8RS4AYWTSE7QcZqkEGmUE",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -118.33682385013736,
			"y": 627.6937101955673,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.8834248204385062,
			"height": 2.0613196588330993,
			"seed": 437518506,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.14723747007309385,
					-0.29447001795608685
				],
				[
					0.14723747007309385,
					-0.5889449581022745
				],
				[
					0.7361873503654124,
					-1.7668447186869685
				],
				[
					0.8834248204385062,
					-2.0613196588330993
				],
				[
					0.8834248204385062,
					-2.0613196588330993
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "arrow",
			"version": 794,
			"versionNonce": 1184658154,
			"isDeleted": false,
			"id": "qAffX0YpqVRgfHaQvlaYM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -520.3934442557307,
			"y": 335.1711770509501,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 64.27607022412872,
			"seed": 669208822,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873743213,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "bqL66CWlOKxpZQWdEZDnD",
				"gap": 11.386034767531328,
				"focus": -0.0028945173565794245
			},
			"endBinding": {
				"elementId": "06cc0vhUqb-OPgzhjhhiF",
				"gap": 15.999999999999943,
				"focus": 0.06694634810672304
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					64.27607022412872
				]
			]
		},
		{
			"type": "arrow",
			"version": 802,
			"versionNonce": 1713047594,
			"isDeleted": false,
			"id": "dIUgo3EQm9biLVMjc--wF",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -336.85835225326343,
			"y": 344.29988413359814,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 57.17455412212172,
			"seed": 805703082,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873743209,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "GqhsyHkTw4fggt9oWk4Wo",
				"gap": 8.014741850179348,
				"focus": 0.06403561200172589
			},
			"endBinding": {
				"elementId": "XrdBQolF0M9CbVpSDFBoM",
				"gap": 13.97280901935892,
				"focus": -0.023376649699861623
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					57.17455412212172
				]
			]
		},
		{
			"type": "arrow",
			"version": 784,
			"versionNonce": 1361216938,
			"isDeleted": false,
			"id": "7Kavd9TAv7l14Dne67puL",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -127.85895939244926,
			"y": 345.4409905885478,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 54.006256686531,
			"seed": 201604790,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873743217,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "96oyZNGwd42sMkr9QPAHn",
				"gap": 8.014757912062578,
				"focus": -0.002031824544122168
			},
			"endBinding": {
				"elementId": "ZxIwj_zroddo-8tlPrtnZ",
				"gap": 16,
				"focus": -0.03262052224985047
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0,
					54.006256686531
				]
			]
		},
		{
			"type": "line",
			"version": 212,
			"versionNonce": 1587131382,
			"isDeleted": false,
			"id": "nAmSeLg5MKP0EbXJOrvxC",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -549.5515588926168,
			"y": 227.29230899485725,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 435.89588165283203,
			"height": 0,
			"seed": 1179653046,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					435.89588165283203,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 149,
			"versionNonce": 939798122,
			"isDeleted": false,
			"id": "ofDIfFDT_Ziqt5axRmq2V",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -232.3289000994854,
			"y": 115.46564120921272,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 112.96775817871094,
			"seed": 95326582,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					112.96775817871094
				]
			]
		},
		{
			"type": "line",
			"version": 235,
			"versionNonce": 1550566710,
			"isDeleted": false,
			"id": "xNBeBR3AiLj_9yo8pPGud",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -552.7262435905482,
			"y": 226.72847593155825,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 29.04588179154831,
			"seed": 1441824682,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					29.04588179154831
				]
			]
		},
		{
			"type": "line",
			"version": 217,
			"versionNonce": 1158074666,
			"isDeleted": false,
			"id": "VxfOCf7DyDoV7TIlHopj7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -323.75080553664816,
			"y": 224.88829853878718,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 31.535533558238626,
			"seed": 1121632234,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					31.535533558238626
				]
			]
		},
		{
			"type": "line",
			"version": 207,
			"versionNonce": 275361398,
			"isDeleted": false,
			"id": "E2W6OLUxdDtGoErYejFVb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -114.12454589532223,
			"y": 227.715443485749,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 35.3314830638744,
			"seed": 1755960374,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					35.3314830638744
				]
			]
		},
		{
			"type": "rectangle",
			"version": 831,
			"versionNonce": 1193745386,
			"isDeleted": false,
			"id": "rpc4hMJZPcUP5gy1m0TGM",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 42.88925899716526,
			"y": 279.24938230855463,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 177,
			"height": 60,
			"seed": 495685558,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "BggMsbSS",
					"type": "text"
				},
				{
					"id": "7Kavd9TAv7l14Dne67puL",
					"type": "arrow"
				},
				{
					"id": "ZKd7YhD5vcs8EJC-AqhIi",
					"type": "arrow"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 954,
			"versionNonce": 477105834,
			"isDeleted": false,
			"id": "BggMsbSS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 47.88925899716526,
			"y": 284.24938230855463,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 167,
			"height": 50,
			"seed": 2122247850,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Libros Poeticos \ny Sapienciales",
			"rawText": "Libros Poeticos y Sapienciales",
			"baseline": 42,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "rpc4hMJZPcUP5gy1m0TGM",
			"originalText": "Libros Poeticos y Sapienciales"
		},
		{
			"type": "freedraw",
			"version": 88,
			"versionNonce": 761071862,
			"isDeleted": false,
			"id": "qRNzoDZMCwZQmyz0bC8qu",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 142.3687150156838,
			"y": 289.85637451543096,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 1.352403428819457,
			"height": 3.211952492042826,
			"seed": 1895948854,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.1690560800058165,
					0
				],
				[
					0.6762017144097285,
					-1.6905014603226505
				],
				[
					1.0143025716146212,
					-2.5357507776330976
				],
				[
					1.352403428819457,
					-3.0429020634404083
				],
				[
					1.352403428819457,
					-3.211952492042826
				],
				[
					1.352403428819457,
					-3.211952492042826
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 197,
			"versionNonce": 697084266,
			"isDeleted": false,
			"id": "-qZ8Cbto5vPQZCHNLwmN_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": -116.3373535861245,
			"y": 227.22853269698504,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 246.06019453568894,
			"height": 0,
			"seed": 1344018230,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					246.06019453568894,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 268,
			"versionNonce": 33323574,
			"isDeleted": false,
			"id": "LIz_BHGV5TlT_mzwzg7wo",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 128.84466604253507,
			"y": 226.41635410770695,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 35.3314830638744,
			"seed": 95329462,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					35.3314830638744
				]
			]
		},
		{
			"type": "arrow",
			"version": 1066,
			"versionNonce": 730374954,
			"isDeleted": false,
			"id": "ZKd7YhD5vcs8EJC-AqhIi",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 133.74299731846753,
			"y": 340.24938230855463,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0.5558934793274659,
			"height": 65.42900769950194,
			"seed": 975526122,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873743224,
			"link": null,
			"locked": false,
			"startBinding": {
				"elementId": "rpc4hMJZPcUP5gy1m0TGM",
				"gap": 1,
				"focus": -0.023552036116673256
			},
			"endBinding": {
				"elementId": "shfBNRyyxkH9yR6mPbApb",
				"gap": 9.298811479048254,
				"focus": 0.11025702414895538
			},
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": "arrow",
			"points": [
				[
					0,
					0
				],
				[
					0.5558934793274659,
					65.42900769950194
				]
			]
		},
		{
			"type": "rectangle",
			"version": 1560,
			"versionNonce": 203146102,
			"isDeleted": false,
			"id": "shfBNRyyxkH9yR6mPbApb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 41.9667342368237,
			"y": 414.9772014871048,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 168,
			"height": 225,
			"seed": 659382454,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "yYlsmfT8",
					"type": "text"
				},
				{
					"id": "7Kavd9TAv7l14Dne67puL",
					"type": "arrow"
				},
				{
					"id": "ZKd7YhD5vcs8EJC-AqhIi",
					"type": "arrow"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 2052,
			"versionNonce": 1040180406,
			"isDeleted": false,
			"id": "yYlsmfT8",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 46.9667342368237,
			"y": 427.4772014871048,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 158,
			"height": 200,
			"seed": 1195290026,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Salmos\nCantor de los \ncantares\nProverbios\nJob\nEclesiastes\nEclesiastico\nSabiduria",
			"rawText": "Salmos\nCantor de los cantares\nProverbios\nJob\nEclesiastes\nEclesiastico\nSabiduria",
			"baseline": 193,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "shfBNRyyxkH9yR6mPbApb",
			"originalText": "Salmos\nCantor de los cantares\nProverbios\nJob\nEclesiastes\nEclesiastico\nSabiduria"
		},
		{
			"type": "freedraw",
			"version": 87,
			"versionNonce": 233204138,
			"isDeleted": false,
			"id": "8YJ-LJM0xohBLZJz0zMkW",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 129.21599537749816,
			"y": 582.0161583668456,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 3.8727823893229356,
			"height": 3.319535688920496,
			"seed": 262562218,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.27662797407671746,
					-0.2766372218276274
				],
				[
					1.1065026485558747,
					-1.1065118963068699
				],
				[
					1.6597585967092812,
					-1.9364050662878753
				],
				[
					2.489642518939405,
					-2.766279740767118
				],
				[
					3.596154415246218,
					-3.0428984670928685
				],
				[
					3.8727823893229356,
					-3.319535688920496
				],
				[
					3.8727823893229356,
					-3.319535688920496
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 86,
			"versionNonce": 2035146230,
			"isDeleted": false,
			"id": "wh-M4vgnEWwLQ5VgH7Lo9",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 167.06157969867382,
			"y": 559.7686844640773,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 1.239699375482303,
			"height": 0.9016022858795623,
			"seed": 2023929334,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					-0.11270405333721101,
					-0.11270028573494528
				],
				[
					0.1126965181327364,
					-0.22540057146989056
				],
				[
					0.22540057146989056,
					-0.33810085720483585
				],
				[
					0.6762017144097285,
					-0.6762017144096717
				],
				[
					0.7888982325424649,
					-0.788902000144617
				],
				[
					1.0142988040123555,
					-0.9016022858795623
				],
				[
					1.126995322145092,
					-0.9016022858795623
				],
				[
					1.126995322145092,
					-0.9016022858795623
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "rectangle",
			"version": 448,
			"versionNonce": 1904190570,
			"isDeleted": false,
			"id": "OxdbPl38OyOaNS6qUUtUb",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 347.29109995937324,
			"y": 234.30531217601413,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 180,
			"height": 35,
			"seed": 532035562,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "sKNnM9hU",
					"type": "text"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 503,
			"versionNonce": 587902774,
			"isDeleted": false,
			"id": "sKNnM9hU",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 352.29109995937324,
			"y": 239.30531217601413,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 170,
			"height": 25,
			"seed": 131666870,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Evangelios",
			"rawText": "Evangelios",
			"baseline": 17,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "OxdbPl38OyOaNS6qUUtUb",
			"originalText": "Evangelios"
		},
		{
			"type": "rectangle",
			"version": 543,
			"versionNonce": 1805879082,
			"isDeleted": false,
			"id": "zr5ldIgHPiRcx9VO8cZU_",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 568.475030492646,
			"y": 234.30531217601413,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 225,
			"height": 36,
			"seed": 296218154,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "8onqwlg9",
					"type": "text"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 631,
			"versionNonce": 932927606,
			"isDeleted": false,
			"id": "8onqwlg9",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 573.475030492646,
			"y": 239.80531217601413,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 215,
			"height": 25,
			"seed": 800383350,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Cartas de San Pablo",
			"rawText": "Cartas de San Pablo",
			"baseline": 17,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "zr5ldIgHPiRcx9VO8cZU_",
			"originalText": "Cartas de San Pablo"
		},
		{
			"type": "rectangle",
			"version": 494,
			"versionNonce": 616370666,
			"isDeleted": false,
			"id": "F0L-EqmmhD_5VrsgBbiGB",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 836.3359161127632,
			"y": 222.29385041614574,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 180,
			"height": 60,
			"seed": 1841311018,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "3dhPcRta",
					"type": "text"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 570,
			"versionNonce": 1589959094,
			"isDeleted": false,
			"id": "3dhPcRta",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 841.3359161127632,
			"y": 227.29385041614574,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 170,
			"height": 50,
			"seed": 1870926454,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Cartas \nCatolicas",
			"rawText": "Cartas Catolicas",
			"baseline": 42,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "F0L-EqmmhD_5VrsgBbiGB",
			"originalText": "Cartas Catolicas"
		},
		{
			"type": "line",
			"version": 55,
			"versionNonce": 2144814250,
			"isDeleted": false,
			"id": "c69SHQbM35P0aFQ_jaIBl",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 683.1434785403665,
			"y": 119.4016629848661,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 60.58144309303978,
			"seed": 1950282870,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					60.58144309303978
				]
			]
		},
		{
			"type": "line",
			"version": 93,
			"versionNonce": 2141991670,
			"isDeleted": false,
			"id": "SVwIJ5GI44LQ0WgqGmB8x",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 417.76947395066367,
			"y": 184.04578046406655,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 513.2362874348958,
			"height": 0,
			"seed": 2108704182,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					513.2362874348958,
					0
				]
			]
		},
		{
			"type": "line",
			"version": 51,
			"versionNonce": 1184702314,
			"isDeleted": false,
			"id": "ES3z24TLMlN6YwUyCof8v",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 418.0592746854106,
			"y": 182.01717532083737,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 31.443345811631957,
			"seed": 349796586,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					31.443345811631957
				]
			]
		},
		{
			"type": "line",
			"version": 36,
			"versionNonce": 1423336502,
			"isDeleted": false,
			"id": "teOv2IcEyHbXwK5q7m_a5",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 930.7323877306965,
			"y": 184.20696002332897,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 21.631068354067565,
			"seed": 1679857130,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					21.631068354067565
				]
			]
		},
		{
			"type": "line",
			"version": 49,
			"versionNonce": 1431937578,
			"isDeleted": false,
			"id": "8cJuBzFrp8QrPgyI5bHQ1",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 682.2405194941636,
			"y": 184.03471003129587,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 19.414578290052788,
			"seed": 782843318,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					19.414578290052788
				]
			]
		},
		{
			"type": "rectangle",
			"version": 1767,
			"versionNonce": 2649462,
			"isDeleted": false,
			"id": "hcfpgHURS9HB4HpVvAd96",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 351.03019785655454,
			"y": 317.6495790899754,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 168,
			"height": 120.01983642578125,
			"seed": 921834666,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "LH6tyKkS",
					"type": "text"
				},
				{
					"id": "7Kavd9TAv7l14Dne67puL",
					"type": "arrow"
				},
				{
					"id": "ZKd7YhD5vcs8EJC-AqhIi",
					"type": "arrow"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 2212,
			"versionNonce": 1190644970,
			"isDeleted": false,
			"id": "LH6tyKkS",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 356.03019785655454,
			"y": 327.65949730286604,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 158,
			"height": 100,
			"seed": 2019975926,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Mateo\nMarcos\nLucas\nJuan",
			"rawText": "Mateo\nMarcos\nLucas\nJuan",
			"baseline": 92,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "hcfpgHURS9HB4HpVvAd96",
			"originalText": "Mateo\nMarcos\nLucas\nJuan"
		},
		{
			"type": "line",
			"version": 47,
			"versionNonce": 518475446,
			"isDeleted": false,
			"id": "irppCUlviBUhwp2ZXMjkI",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 436.758619947159,
			"y": 270.94345966821885,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 41.07918875558039,
			"seed": 877698742,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					41.07918875558039
				]
			]
		},
		{
			"type": "rectangle",
			"version": 1761,
			"versionNonce": 553820074,
			"isDeleted": false,
			"id": "UEC3SGSEImM4M5KhabLya",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 564.022499779762,
			"y": 315.8526714107593,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 221,
			"height": 310,
			"seed": 1936784758,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "KC7eNCU7",
					"type": "text"
				},
				{
					"id": "7Kavd9TAv7l14Dne67puL",
					"type": "arrow"
				},
				{
					"id": "ZKd7YhD5vcs8EJC-AqhIi",
					"type": "arrow"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 2443,
			"versionNonce": 1572341750,
			"isDeleted": false,
			"id": "KC7eNCU7",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 569.022499779762,
			"y": 333.3526714107593,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 211,
			"height": 275,
			"seed": 1983921386,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Romanos\n1 y 2 Corintios\nGalatas\nEfesios\nFilipenses\nColosenses\n1 y 2 Tesalonicenses\n1 y 2 Timoteo\nTito\nFilemon\nHebreos",
			"rawText": "Romanos\n1 y 2 Corintios\nGalatas\nEfesios\nFilipenses\nColosenses\n1 y 2 Tesalonicenses\n1 y 2 Timoteo\nTito\nFilemon\nHebreos",
			"baseline": 268,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "UEC3SGSEImM4M5KhabLya",
			"originalText": "Romanos\n1 y 2 Corintios\nGalatas\nEfesios\nFilipenses\nColosenses\n1 y 2 Tesalonicenses\n1 y 2 Timoteo\nTito\nFilemon\nHebreos"
		},
		{
			"type": "freedraw",
			"version": 54,
			"versionNonce": 2141526634,
			"isDeleted": false,
			"id": "YirteFVttTj04NNseBUJx",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 693.1496742463798,
			"y": 569.837806656611,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 3.7705529254416206,
			"height": 3.175208050271749,
			"seed": 2080037750,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.19845050314199852,
					-0.19845713739812254
				],
				[
					0.39690100628399705,
					-0.19845713739812254
				],
				[
					0.9922525157099926,
					-0.5953581436821196
				],
				[
					1.3891468877378657,
					-1.38916015625
				],
				[
					1.786047894021749,
					-1.5876040251358745
				],
				[
					2.182948900305746,
					-1.786061162533997
				],
				[
					2.3813994034477446,
					-1.786061162533997
				],
				[
					2.579849906589743,
					-1.9845050314198716
				],
				[
					2.97675091287374,
					-2.3814060377038686
				],
				[
					3.572102422299622,
					-2.9767641813858745
				],
				[
					3.7705529254416206,
					-3.175208050271749
				],
				[
					3.7705529254416206,
					-3.175208050271749
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "line",
			"version": 70,
			"versionNonce": 211786038,
			"isDeleted": false,
			"id": "19IClRxSh5o09_1QVPLXd",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 663.8860772157631,
			"y": 269.51467263747423,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 44.798295762803775,
			"seed": 1409117418,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					44.798295762803775
				]
			]
		},
		{
			"type": "line",
			"version": 55,
			"versionNonce": 1101675818,
			"isDeleted": false,
			"id": "ILFilunmkPiaRlp-cehxJ",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 928.2180458502544,
			"y": 283.66825541347504,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 30.749335038034587,
			"seed": 2070028150,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					30.749335038034587
				]
			]
		},
		{
			"type": "rectangle",
			"version": 1850,
			"versionNonce": 855835254,
			"isDeleted": false,
			"id": "MZ5aEQ5w_7vjoMOhY3whT",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 848.3045117845809,
			"y": 314.54008433228944,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 168,
			"height": 120.01983642578125,
			"seed": 1532952246,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [
				{
					"id": "OuUZC5JyvahWlneXYx0qa",
					"type": "text"
				},
				{
					"id": "7Kavd9TAv7l14Dne67puL",
					"type": "arrow"
				},
				{
					"id": "ZKd7YhD5vcs8EJC-AqhIi",
					"type": "arrow"
				},
				{
					"type": "text",
					"id": "OuUZC5JyvahWlneXYx0qa"
				},
				{
					"type": "text",
					"id": "FyYNcgU4"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"type": "text",
			"version": 2375,
			"versionNonce": 880609258,
			"isDeleted": false,
			"id": "FyYNcgU4",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": 853.304511784581,
			"y": 325.05000254518006,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 158,
			"height": 100,
			"seed": 1293807530,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Santiago\n1 y 2 Pedro\n1, 2 y 3 Juan\nJudas",
			"rawText": "Santiago\n1 y 2 Pedro\n1, 2 y 3 Juan\nJudas",
			"baseline": 92,
			"textAlign": "center",
			"verticalAlign": "middle",
			"containerId": "MZ5aEQ5w_7vjoMOhY3whT",
			"originalText": "Santiago\n1 y 2 Pedro\n1, 2 y 3 Juan\nJudas"
		},
		{
			"type": "line",
			"version": 89,
			"versionNonce": 598164406,
			"isDeleted": false,
			"id": "Bjf-6elaDVWyIzMh4MGSA",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"angle": 0,
			"x": 812.18395795554,
			"y": 179.3804019877398,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 0,
			"height": 540.0609425136022,
			"seed": 443918326,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"boundElements": null,
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"startBinding": null,
			"endBinding": null,
			"lastCommittedPoint": null,
			"startArrowhead": null,
			"endArrowhead": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					540.0609425136022
				]
			]
		},
		{
			"id": "jk-2sA8MVWWErrE8fkSGn",
			"type": "line",
			"x": 543.6240127127945,
			"y": 179.1275344253096,
			"width": 0,
			"height": 544.2992401123047,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"seed": 1763890614,
			"version": 70,
			"versionNonce": 1076394666,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0,
					544.2992401123047
				]
			],
			"lastCommittedPoint": null,
			"startBinding": null,
			"endBinding": null,
			"startArrowhead": null,
			"endArrowhead": null
		},
		{
			"id": "HaaqoEcXigzOywe7OL4DU",
			"type": "rectangle",
			"x": 401.21649068465206,
			"y": 728.9007355351807,
			"width": 279,
			"height": 36,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"seed": 569594294,
			"version": 218,
			"versionNonce": 280059126,
			"isDeleted": false,
			"boundElements": [
				{
					"type": "text",
					"id": "52Mdgees"
				}
			],
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"id": "52Mdgees",
			"type": "text",
			"x": 406.21649068465206,
			"y": 733.9007355351807,
			"width": 269,
			"height": 26,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"seed": 845431478,
			"version": 167,
			"versionNonce": 324506986,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"text": "Hechos de los Apostoles",
			"rawText": "Hechos de los Apostoles",
			"fontSize": 20.112092829932855,
			"fontFamily": 1,
			"textAlign": "center",
			"verticalAlign": "middle",
			"baseline": 18,
			"containerId": "HaaqoEcXigzOywe7OL4DU",
			"originalText": "Hechos de los Apostoles"
		},
		{
			"id": "YkTsefAAo_ncFESdwCIlV",
			"type": "freedraw",
			"x": 593.6328783431353,
			"y": 739.8050160644258,
			"width": 2.1829621688178804,
			"height": 4.3659044348675025,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "round",
			"seed": 1472406378,
			"version": 13,
			"versionNonce": 530992694,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"points": [
				[
					0,
					0
				],
				[
					0.39690100628388336,
					0
				],
				[
					0.39690100628388336,
					-0.19845050314199852
				],
				[
					0.7938020125678804,
					-1.1907030188518775
				],
				[
					1.38916015625,
					-2.1829555345617564
				],
				[
					1.7860611625338834,
					-3.3736585534137475
				],
				[
					1.9845050314198716,
					-4.167453931725504
				],
				[
					2.1829621688178804,
					-4.3659044348675025
				],
				[
					2.1829621688178804,
					-4.3659044348675025
				]
			],
			"pressures": [],
			"simulatePressure": true,
			"lastCommittedPoint": [
				2.1829621688178804,
				-4.3659044348675025
			]
		},
		{
			"id": "JEO_HZVoFvbEY-wHDUwjx",
			"type": "rectangle",
			"x": 709.6734728784338,
			"y": 725.9984419555519,
			"width": 206.53703689575195,
			"height": 35.94429016113281,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"seed": 1023082154,
			"version": 75,
			"versionNonce": 1330993194,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1651873724554,
			"link": null,
			"locked": false
		},
		{
			"id": "kReZmivj",
			"type": "text",
			"x": 759.9419913263098,
			"y": 731.4705870361183,
			"width": 106,
			"height": 25,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [
				"STZMHjwsEHwTlZvNw5nIs"
			],
			"strokeSharpness": "sharp",
			"seed": 704923690,
			"version": 91,
			"versionNonce": 2104170358,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1651873724554,
			"link": null,
			"locked": false,
			"text": "Apocalispis",
			"rawText": "Apocalispis",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 17,
			"containerId": null,
			"originalText": "Apocalispis"
		},
		{
			"id": "591iH8FI",
			"type": "text",
			"x": 569.5406714592216,
			"y": 887.5087218706781,
			"width": 348,
			"height": 25,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 0.5,
			"strokeStyle": "solid",
			"roughness": 0,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 1248626422,
			"version": 94,
			"versionNonce": 453562218,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1651873740725,
			"link": null,
			"locked": false,
			"text": "Camilo Andres Camargo Castaneda",
			"rawText": "Camilo Andres Camargo Castaneda",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 17,
			"containerId": null,
			"originalText": "Camilo Andres Camargo Castaneda"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 0.5,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 0,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%