

# Camilo Andres Camargo Castaneda
*1. Detecta tres momentos significativos en tu vida en los que crees que Dios ha sido importante. Si es posible uno en la infancia, otro en la adolescencia y el otro en la juventud. *

**Infancia**
Dios ha sido muy importante, ya que mi familia cuando era muy niño no poseíamos muchos recursos para poder cuidar a mi tío que estaba muy grave en el hospital, pero gracias a las oraciones y la encomendaron al señor él se pudo mejorar, aunque teniendo como consecuencias una actitud de niño joven, porque él se cayó de un camión y se fracture el cráneo.
**Adolescencia**
Mi familia es muy religiosa y orar todos los días el rosario, un día mi mamá tuvo un paro cardiaco el cual fue tratado, ella estuvo muy mal, pero gracias a Dios ella está con vida y ella está aún más unida a Dios, comunicándose y pidiendo por todos sus familiares.

**Juventud**
Ahora me encuentro en un constante apego a Dios, aunque sé que a veces no dialogo con el muy seguido, siento que lo necesito no solo en momentos malos sino en todos, ya que he sufrido frustraciones contantes, adicciones no malas para el cuerpo pero si para la mente, y el me ayuda a afrontar todas mis debilidades ser una mejor persona. 

*2. Describe el contexto que consideras relevante. *


Considero que todos los contextos son necesarios tener la compañía de Dios con nosotros, pero sin duda Dios a cuidado mucho a mi madre, la mujer que me dio la vida y siempre lucha por nosotros. Por ello en este contexto de Petición a Dios es la fundamental como la de arrepentimiento, ya que todos somos pecadores y él no nos juzga por muy pecadores, si no nos impulsa a sobrellevar todo con calma y con su compañía.

*3. Una vez hecha la descripción, analiza si hay constantes y cuáles serian las variables. *

Las contantes siempre son las peticiones a Dios por nuestra familia como arrepentimiento, las variables son la dedicación que dedicamos a su encuentro, ya que como todos, tenemos momentos en los cuales solicitamos con agrado y a veces con un poco de tristeza, aunque en general las variables siempre son de tiempo como la oración que dedicamos día a día o la cantidades de oraciones por día.

*4.  ¿Cuál es la imagen de Dios que emerge de esas experiencias? ¿Qué te ha quedado para tu vivencia personal?*

Por experiencia propia creo en Dios y que él es nuestro acompañante, así que las experiencias en mi vida son retadoras como en la de todos, pero en especial el nos ayuda a afrontar todo sentimiento malo, adicción, y acontecimientos que la vida nos preparara cada día como la delincuencia que se presenta en el país, pero confió en él y su deseo para nosotros. Para mí como creyente en él su imagen es de salvador, acompañante y él sufre por nosotros. 


*5.  ¿En qué medida está imagen de Dios ha estado actuando y actúa en tu vida personal ordinaria? 
*
Como persona ordinaria, esta imagen se salvaron y luchador por nosotras actúa frequentemente, ya que como comentaba anteriormente, necesitamos creen en el, ya que él sufre por nosotros y dedica su amor para sus hijos. Con lo anterior y experiencias que he tenido con hasta hoy el salvador, el padre, el hijo y espíritu santo.


*Dinámica 2*

*1. Si un ateo te preguntara por qué crees en Dios, que respuesta le darías*

Mi respuesta es que tenemos un Dios y tu creencia de que no existe es muy vaga, por lo que tus argumentos son muy básicos, ya que este mundo tuvo que ser creado por alguien, o te preguntaría quien fue primero el huevo o la gallina, seguro no sabes, por eso tenemos un creador. 

Y sobre nuestra religión, con lo anterior dicho te lo argumentaría con la biblia, porque es el primer libro de este mundo y podemos creer que si no lo vimos no existe, aunque es muy vaga tu respuesta, porque tú no puedes ver átomo ni puedes ver cosas que conforman nuestro mundo. 

*2. Cuál es la imagen de dios que más ha prevalecido en tu vida?
*
La imagen de Dios es como salvaron, ya que gracias su crucifixión por nosotros demuestra que se sacrificó por nosotros los pecadores.

*3. Dios, Significa algo en tu vida, si tú no fueras creyente. Tu vida actual seria distinta o sería igual.*

MI vida sería algo diferentes, ya que la sociedad y mi familia en generar no soportaría y estaría sometido a cambios, aunque también cabe la posibilidad de seguir como voy, aunque sin tener a mi lado a quien contarle, y pedirle por mi familia, probablemente sería algo diferente. 

*4. Dibuja un corazón grande y escribe dentro de él los sentimientos que experimenta al descubrir la presencia de Dios en tu vida.*

![[Heart]]










