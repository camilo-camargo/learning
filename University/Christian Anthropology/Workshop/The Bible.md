The classification of the bible is the old and New Testament. 

## The Old Testament
This books was written before Jesus.

## The New Testament
This books was written after Jesus.

## Genre 
The second is a division of a genres of literatures. 

## Old Testament

### The Pentateuch, or the Books of the Law
Genesis, Exodus, Leviticus, Numbers, and Deuteronomy
### Historical Books
Joshua, Judges, Ruth, 1 Samuel, 2 Samuel, 1 Kings, 2 Kings, 1 Chronicles, 2 Chronicles, Ezra, Nehemiah, and Esther
### Wisdom Literature
Job, Psalms, Proverbs, Ecclesiastes, and the Song of Solomon
### The Prophets
Isaiah, Jeremiah, Lamentations, Ezekiel, Daniel, Hosea, Joel, Amos, Obadiah, Jonah, Micah, Nahum, Habakkuk, Zephaniah, Haggai, Zechariah, and Malachi 
### The Gospels
Matthew, Make, Luke and John

## New Testament

### Epistles
Romans, 1 Corinthians, 2 Corinthians, Galatians, Ephesians, Philippians, Colossians, 1 Thessalonians, 2 Thessalonians, 1 Timothy, 2 Timothy, Titus, Philemon, Hebrews, James, 1 Peter, 2 Peter, 1 John, 2 John, 3 John, and Jude. 
### Prophetics/Apocalytic Literature
Revelation



