---

excalidraw-plugin: parsed
tags: [excalidraw]

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
challenging
partly
complexity
nonlinearity
stringent
nonlinear
search algorithms 
optimality 
robust
uncertainty
metaheuristic
bat algorithm
cuckoo search
firefly algorithm
harmony search
particle swarm optimization
paradigm
energy consumption
performance 
efficiency
profit
growing demand
time-consuming
seek
suboptimal
fluid dynamics
maximum-likelihood
multicriteria
software packages
despite
linked
modelling components
aim
feasible
multiphysics solver
black box
estimate
predict
nature-inspired
metaheuristic 
ant algorithms 
mimic
foraging
pheromone
priori
bee algorithms
sources
concentrations
bee colony
scouts
emple
oyeed
bees 
honey bee 
combinatorial
developed
harmonics
magical
randomly 
annealling
ideal
prescribed
genetic
research articles 
differential 
further
carries
notation
mutation
particle swarm
 ^ZZniOeP6

desafiante
parcialmente
complejidad
no linealidad
riguroso
no lineal
algoritmos de busqueda
optimización
robusto
incertidumbre
metaheurística
algoritmo de murciélago
búsqueda de cuco
algoritmo de luciérnaga
búsqueda de armonía
optimización de Enjambre de partículas
paradigma
consumo de energía
rendimiento
eficiencia
ganancia
creciente demanda
pérdida de tiempo
buscar
subóptimo
dinámica de fluidos
máxima verosimilitud
multicriterio
paquetes de programas
A pesar de
vinculado
componentes de modelado
apuntar
factible
solucionador de multifísica
caja negra
estimar
predecir
inspirado en la naturaleza
metaheurística
algoritmos de hormigas
imitar
forrajeo
feromona
a priori
algoritmos de abejas
fuentes
concentraciones
colonia de abejas
exploradores
emple
oído
abejas
miel de abeja
combinacional
desarrollado
Armónicos
mágico
al azar
recocido
ideal
prescrito
genético
artículos de investigación
diferencial
más
lleva
notación
mutación
enjambre de particulas
 ^lwfd5lax

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"id": "ZZniOeP6",
			"type": "text",
			"x": -371.0264994303384,
			"y": -214.0858592987056,
			"width": 266,
			"height": 1775,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 594149022,
			"version": 49,
			"versionNonce": 1502172254,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1647644311098,
			"link": null,
			"text": "challenging\npartly\ncomplexity\nnonlinearity\nstringent\nnonlinear\nsearch algorithms \noptimality \nrobust\nuncertainty\nmetaheuristic\nbat algorithm\ncuckoo search\nfirefly algorithm\nharmony search\nparticle swarm optimization\nparadigm\nenergy consumption\nperformance \nefficiency\nprofit\ngrowing demand\ntime-consuming\nseek\nsuboptimal\nfluid dynamics\nmaximum-likelihood\nmulticriteria\nsoftware packages\ndespite\nlinked\nmodelling components\naim\nfeasible\nmultiphysics solver\nblack box\nestimate\npredict\nnature-inspired\nmetaheuristic \nant algorithms \nmimic\nforaging\npheromone\npriori\nbee algorithms\nsources\nconcentrations\nbee colony\nscouts\nemple\noyeed\nbees \nhoney bee \ncombinatorial\ndeveloped\nharmonics\nmagical\nrandomly \nannealling\nideal\nprescribed\ngenetic\nresearch articles \ndifferential \nfurther\ncarries\nnotation\nmutation\nparticle swarm\n",
			"rawText": "challenging\npartly\ncomplexity\nnonlinearity\nstringent\nnonlinear\nsearch algorithms \noptimality \nrobust\nuncertainty\nmetaheuristic\nbat algorithm\ncuckoo search\nfirefly algorithm\nharmony search\nparticle swarm optimization\nparadigm\nenergy consumption\nperformance \nefficiency\nprofit\ngrowing demand\ntime-consuming\nseek\nsuboptimal\nfluid dynamics\nmaximum-likelihood\nmulticriteria\nsoftware packages\ndespite\nlinked\nmodelling components\naim\nfeasible\nmultiphysics solver\nblack box\nestimate\npredict\nnature-inspired\nmetaheuristic \nant algorithms \nmimic\nforaging\npheromone\npriori\nbee algorithms\nsources\nconcentrations\nbee colony\nscouts\nemple\noyeed\nbees \nhoney bee \ncombinatorial\ndeveloped\nharmonics\nmagical\nrandomly \nannealling\nideal\nprescribed\ngenetic\nresearch articles \ndifferential \nfurther\ncarries\nnotation\nmutation\nparticle swarm\n",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 1768,
			"containerId": null,
			"originalText": "challenging\npartly\ncomplexity\nnonlinearity\nstringent\nnonlinear\nsearch algorithms \noptimality \nrobust\nuncertainty\nmetaheuristic\nbat algorithm\ncuckoo search\nfirefly algorithm\nharmony search\nparticle swarm optimization\nparadigm\nenergy consumption\nperformance \nefficiency\nprofit\ngrowing demand\ntime-consuming\nseek\nsuboptimal\nfluid dynamics\nmaximum-likelihood\nmulticriteria\nsoftware packages\ndespite\nlinked\nmodelling components\naim\nfeasible\nmultiphysics solver\nblack box\nestimate\npredict\nnature-inspired\nmetaheuristic \nant algorithms \nmimic\nforaging\npheromone\npriori\nbee algorithms\nsources\nconcentrations\nbee colony\nscouts\nemple\noyeed\nbees \nhoney bee \ncombinatorial\ndeveloped\nharmonics\nmagical\nrandomly \nannealling\nideal\nprescribed\ngenetic\nresearch articles \ndifferential \nfurther\ncarries\nnotation\nmutation\nparticle swarm\n"
		},
		{
			"id": "lwfd5lax",
			"type": "text",
			"x": 5.83750406900981,
			"y": -214.0858592987055,
			"width": 383,
			"height": 1775,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"fillStyle": "hachure",
			"strokeWidth": 1,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 691848094,
			"version": 130,
			"versionNonce": 1571498114,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1647644311098,
			"link": null,
			"text": "desafiante\nparcialmente\ncomplejidad\nno linealidad\nriguroso\nno lineal\nalgoritmos de busqueda\noptimización\nrobusto\nincertidumbre\nmetaheurística\nalgoritmo de murciélago\nbúsqueda de cuco\nalgoritmo de luciérnaga\nbúsqueda de armonía\noptimización de Enjambre de partículas\nparadigma\nconsumo de energía\nrendimiento\neficiencia\nganancia\ncreciente demanda\npérdida de tiempo\nbuscar\nsubóptimo\ndinámica de fluidos\nmáxima verosimilitud\nmulticriterio\npaquetes de programas\nA pesar de\nvinculado\ncomponentes de modelado\napuntar\nfactible\nsolucionador de multifísica\ncaja negra\nestimar\npredecir\ninspirado en la naturaleza\nmetaheurística\nalgoritmos de hormigas\nimitar\nforrajeo\nferomona\na priori\nalgoritmos de abejas\nfuentes\nconcentraciones\ncolonia de abejas\nexploradores\nemple\noído\nabejas\nmiel de abeja\ncombinacional\ndesarrollado\nArmónicos\nmágico\nal azar\nrecocido\nideal\nprescrito\ngenético\nartículos de investigación\ndiferencial\nmás\nlleva\nnotación\nmutación\nenjambre de particulas\n",
			"rawText": "desafiante\nparcialmente\ncomplejidad\nno linealidad\nriguroso\nno lineal\nalgoritmos de busqueda\noptimización\nrobusto\nincertidumbre\nmetaheurística\nalgoritmo de murciélago\nbúsqueda de cuco\nalgoritmo de luciérnaga\nbúsqueda de armonía\noptimización de Enjambre de partículas\nparadigma\nconsumo de energía\nrendimiento\neficiencia\nganancia\ncreciente demanda\npérdida de tiempo\nbuscar\nsubóptimo\ndinámica de fluidos\nmáxima verosimilitud\nmulticriterio\npaquetes de programas\nA pesar de\nvinculado\ncomponentes de modelado\napuntar\nfactible\nsolucionador de multifísica\ncaja negra\nestimar\npredecir\ninspirado en la naturaleza\nmetaheurística\nalgoritmos de hormigas\nimitar\nforrajeo\nferomona\na priori\nalgoritmos de abejas\nfuentes\nconcentraciones\ncolonia de abejas\nexploradores\nemple\noído\nabejas\nmiel de abeja\ncombinacional\ndesarrollado\nArmónicos\nmágico\nal azar\nrecocido\nideal\nprescrito\ngenético\nartículos de investigación\ndiferencial\nmás\nlleva\nnotación\nmutación\nenjambre de particulas\n",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 1768,
			"containerId": null,
			"originalText": "desafiante\nparcialmente\ncomplejidad\nno linealidad\nriguroso\nno lineal\nalgoritmos de busqueda\noptimización\nrobusto\nincertidumbre\nmetaheurística\nalgoritmo de murciélago\nbúsqueda de cuco\nalgoritmo de luciérnaga\nbúsqueda de armonía\noptimización de Enjambre de partículas\nparadigma\nconsumo de energía\nrendimiento\neficiencia\nganancia\ncreciente demanda\npérdida de tiempo\nbuscar\nsubóptimo\ndinámica de fluidos\nmáxima verosimilitud\nmulticriterio\npaquetes de programas\nA pesar de\nvinculado\ncomponentes de modelado\napuntar\nfactible\nsolucionador de multifísica\ncaja negra\nestimar\npredecir\ninspirado en la naturaleza\nmetaheurística\nalgoritmos de hormigas\nimitar\nforrajeo\nferomona\na priori\nalgoritmos de abejas\nfuentes\nconcentraciones\ncolonia de abejas\nexploradores\nemple\noído\nabejas\nmiel de abeja\ncombinacional\ndesarrollado\nArmónicos\nmágico\nal azar\nrecocido\nideal\nprescrito\ngenético\nartículos de investigación\ndiferencial\nmás\nlleva\nnotación\nmutación\nenjambre de particulas\n"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%