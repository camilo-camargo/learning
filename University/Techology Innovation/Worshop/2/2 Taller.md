---

excalidraw-plugin: parsed

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
1) La tipologia del entorno es importante ya que este determina 
como una empresa se comporta y actua en un entorno especifico. ^0FLuT5gP

2) Clasificacion de tipologia del entorno ^WO5EXbRb

Estabilidad: La tipologia de entorno de estabilidad es categorizada
en dos enfoques estables y dinamicos.
    Estables: El enfoque estable es aquel el cual es predictible o certidumbres 
los cuales conocen los cambios que produce el ambiente externo. 
    Dinamicos: El enfoque dinamico es el opuesto del estable, ya que esta
sujeto a variedad de cambios por parte del entorno externo. Lo cual esta 
dimension es impredectible. ^d6mwL63w

Complejidad: Se divide en
    Simple: En el  cual la compresion de los conocimientos
es facil de entender.
    Compleja: Es aquella la cual aborda la complejidad
Simple pero es generalizada en una, aunque tambien se
puede dividir y llegar a muchas complejidades sencillas. ^t5OV7HFb

Solucion ^tWLavaOt

Hostilidad: Esta se puede ver de dos puntos favorables y hostiles. 
Los cuales necesitan una pronta reaccion, la cual puede estar dividida 
segun su nivel de agresividad de la reaccion.  ^NGdkDVzJ

Diversidad: Es la cual se determina apartir de sectores integrados
o diversificados, relacionada con la mayor o menor amplitud de
los segmentos de clientes, tales como, gama de producto, 
zonas, servicios, etc ... ^NdVuecZl

3) Modelos de entorno ^ir7baDpU

Entornos estables: estables, simples e integrados.
Entornos Reactivos-Adaptativos: estables, favorables y diversos.
Entornos inestables turbulentos: dinamicos, complejos, hostiles y diversos. ^D6mJUSth

4)  ^tOd0clFy

Niveles de turbulencia segun Ansoff  ^d9TQIsgl

Estable: Cuando el entorno es estable, sencillo,
predictible.
Reactivo: Cuando el entorno es estable, favorable a
la gestion empresarial.
Anticipador: Cuando el entorno es cambiante, desfavorable,
y esta sometido a continuas alteraciones del ambiente.
Explorador: Cuando no es posible la extrapolacion de organizaciones
complejas.
Nivel Creativo: Cuando la unica forma de reaccionar generan 
respuestas rapidas y flexibles. ^PTRoDcgI

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "text",
			"version": 564,
			"versionNonce": 1216882512,
			"isDeleted": false,
			"id": "0FLuT5gP",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -994.0017057718928,
			"y": -698.0560864071178,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 646,
			"height": 50,
			"seed": 496406591,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477221,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "1) La tipologia del entorno es importante ya que este determina \ncomo una empresa se comporta y actua en un entorno especifico.",
			"rawText": "1) La tipologia del entorno es importante ya que este determina \ncomo una empresa se comporta y actua en un entorno especifico.",
			"baseline": 43,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "1) La tipologia del entorno es importante ya que este determina \ncomo una empresa se comporta y actua en un entorno especifico."
		},
		{
			"type": "text",
			"version": 213,
			"versionNonce": 15513008,
			"isDeleted": false,
			"id": "WO5EXbRb",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -994.2839725247472,
			"y": -623.8993313974215,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 392,
			"height": 25,
			"seed": 420009777,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477221,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "2) Clasificacion de tipologia del entorno",
			"rawText": "2) Clasificacion de tipologia del entorno",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "2) Clasificacion de tipologia del entorno"
		},
		{
			"type": "text",
			"version": 1415,
			"versionNonce": 2126540624,
			"isDeleted": false,
			"id": "d6mwL63w",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -984.8632388466616,
			"y": -572.6372919662647,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 798,
			"height": 175,
			"seed": 2046308447,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943480688,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Estabilidad: La tipologia de entorno de estabilidad es categorizada\nen dos enfoques estables y dinamicos.\n    Estables: El enfoque estable es aquel el cual es predictible o certidumbres \nlos cuales conocen los cambios que produce el ambiente externo. \n    Dinamicos: El enfoque dinamico es el opuesto del estable, ya que esta\nsujeto a variedad de cambios por parte del entorno externo. Lo cual esta \ndimension es impredectible.",
			"rawText": "Estabilidad: La tipologia de entorno de estabilidad es categorizada\nen dos enfoques estables y dinamicos.\n    Estables: El enfoque estable es aquel el cual es predictible o certidumbres \nlos cuales conocen los cambios que produce el ambiente externo. \n    Dinamicos: El enfoque dinamico es el opuesto del estable, ya que esta\nsujeto a variedad de cambios por parte del entorno externo. Lo cual esta \ndimension es impredectible.",
			"baseline": 168,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Estabilidad: La tipologia de entorno de estabilidad es categorizada\nen dos enfoques estables y dinamicos.\n    Estables: El enfoque estable es aquel el cual es predictible o certidumbres \nlos cuales conocen los cambios que produce el ambiente externo. \n    Dinamicos: El enfoque dinamico es el opuesto del estable, ya que esta\nsujeto a variedad de cambios por parte del entorno externo. Lo cual esta \ndimension es impredectible."
		},
		{
			"type": "freedraw",
			"version": 269,
			"versionNonce": 120225712,
			"isDeleted": false,
			"id": "aGO4FCcirydqpreQzm5AJ",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 20,
			"angle": 0,
			"x": -987.6439875097624,
			"y": -565.5926013627509,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 120.625,
			"height": 19.375,
			"seed": 475206911,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477221,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.625,
					0.625
				],
				[
					-1.25,
					1.25
				],
				[
					-1.25,
					1.875
				],
				[
					-1.25,
					2.5
				],
				[
					-0.625,
					2.5
				],
				[
					0.625,
					1.25
				],
				[
					2.5,
					-1.25
				],
				[
					5,
					-3.75
				],
				[
					7.5,
					-5.625
				],
				[
					9.375,
					-6.25
				],
				[
					10,
					-5
				],
				[
					10,
					-3.75
				],
				[
					8.125,
					-1.875
				],
				[
					5,
					1.25
				],
				[
					1.875,
					3.75
				],
				[
					0,
					6.25
				],
				[
					-0.625,
					7.5
				],
				[
					0,
					7.5
				],
				[
					1.875,
					6.25
				],
				[
					5.625,
					3.125
				],
				[
					9.375,
					0.625
				],
				[
					15,
					-2.5
				],
				[
					16.875,
					-3.75
				],
				[
					18.125,
					-3.75
				],
				[
					16.25,
					-1.25
				],
				[
					13.75,
					0.625
				],
				[
					10,
					3.75
				],
				[
					6.25,
					6.875
				],
				[
					4.375,
					7.5
				],
				[
					3.75,
					8.125
				],
				[
					5.625,
					7.5
				],
				[
					8.125,
					5.625
				],
				[
					12.5,
					3.125
				],
				[
					16.25,
					0.625
				],
				[
					18.125,
					-0.625
				],
				[
					18.75,
					0
				],
				[
					17.5,
					1.875
				],
				[
					15,
					4.375
				],
				[
					11.25,
					6.875
				],
				[
					8.75,
					9.375
				],
				[
					7.5,
					10.625
				],
				[
					8.125,
					11.25
				],
				[
					10.625,
					10
				],
				[
					15.625,
					6.875
				],
				[
					20.625,
					3.125
				],
				[
					25,
					0.625
				],
				[
					26.875,
					0
				],
				[
					26.875,
					1.25
				],
				[
					25,
					4.375
				],
				[
					21.875,
					7.5
				],
				[
					19.375,
					10
				],
				[
					17.5,
					11.25
				],
				[
					16.875,
					11.25
				],
				[
					17.5,
					10.625
				],
				[
					20,
					8.75
				],
				[
					23.75,
					6.25
				],
				[
					26.875,
					4.375
				],
				[
					31.875,
					1.25
				],
				[
					35,
					0
				],
				[
					35.625,
					0
				],
				[
					34.375,
					1.25
				],
				[
					32.5,
					2.5
				],
				[
					30.625,
					3.75
				],
				[
					29.375,
					5
				],
				[
					31.25,
					4.375
				],
				[
					33.75,
					2.5
				],
				[
					36.875,
					0.625
				],
				[
					39.375,
					-0.625
				],
				[
					40.625,
					-0.625
				],
				[
					40.625,
					0
				],
				[
					40,
					1.25
				],
				[
					38.125,
					3.125
				],
				[
					32.5,
					9.375
				],
				[
					34.375,
					9.375
				],
				[
					37.5,
					7.5
				],
				[
					41.25,
					4.375
				],
				[
					45,
					1.25
				],
				[
					47.5,
					-1.25
				],
				[
					49.375,
					-1.25
				],
				[
					49.375,
					-0.625
				],
				[
					48.125,
					0.625
				],
				[
					45,
					3.75
				],
				[
					43.125,
					5.625
				],
				[
					41.875,
					6.875
				],
				[
					40.625,
					7.5
				],
				[
					40.625,
					8.125
				],
				[
					41.875,
					7.5
				],
				[
					44.375,
					6.25
				],
				[
					47.5,
					5
				],
				[
					51.875,
					2.5
				],
				[
					57.5,
					-0.625
				],
				[
					60.625,
					-1.875
				],
				[
					61.875,
					-1.875
				],
				[
					61.875,
					-0.625
				],
				[
					59.375,
					2.5
				],
				[
					55,
					6.25
				],
				[
					50.625,
					9.375
				],
				[
					46.875,
					12.5
				],
				[
					45.625,
					13.125
				],
				[
					46.875,
					12.5
				],
				[
					51.25,
					10
				],
				[
					56.875,
					6.25
				],
				[
					62.5,
					2.5
				],
				[
					67.5,
					-0.625
				],
				[
					70.625,
					-2.5
				],
				[
					71.875,
					-3.125
				],
				[
					70.625,
					-2.5
				],
				[
					68.125,
					-0.625
				],
				[
					64.375,
					1.875
				],
				[
					61.25,
					4.375
				],
				[
					58.75,
					6.875
				],
				[
					58.125,
					7.5
				],
				[
					58.75,
					7.5
				],
				[
					62.5,
					5.625
				],
				[
					68.125,
					2.5
				],
				[
					72.5,
					0
				],
				[
					79.375,
					-4.375
				],
				[
					81.25,
					-5.625
				],
				[
					81.875,
					-5
				],
				[
					80,
					-1.875
				],
				[
					77.5,
					0.625
				],
				[
					73.125,
					4.375
				],
				[
					70,
					7.5
				],
				[
					68.75,
					9.375
				],
				[
					68.125,
					10
				],
				[
					70.625,
					8.75
				],
				[
					74.375,
					6.25
				],
				[
					78.75,
					3.125
				],
				[
					81.875,
					0.625
				],
				[
					85,
					-1.25
				],
				[
					86.25,
					-2.5
				],
				[
					85.625,
					-1.875
				],
				[
					84.375,
					0
				],
				[
					81.25,
					3.125
				],
				[
					78.125,
					6.25
				],
				[
					76.25,
					8.125
				],
				[
					76.875,
					8.125
				],
				[
					80,
					6.25
				],
				[
					84.375,
					3.125
				],
				[
					88.125,
					0.625
				],
				[
					93.125,
					-3.125
				],
				[
					96.25,
					-4.375
				],
				[
					96.875,
					-3.75
				],
				[
					96.25,
					-1.25
				],
				[
					93.125,
					2.5
				],
				[
					90.625,
					5.625
				],
				[
					88.75,
					7.5
				],
				[
					88.125,
					8.75
				],
				[
					87.5,
					9.375
				],
				[
					88.75,
					8.75
				],
				[
					90.625,
					7.5
				],
				[
					94.375,
					5
				],
				[
					98.125,
					1.875
				],
				[
					100,
					0
				],
				[
					101.875,
					-1.25
				],
				[
					101.25,
					0
				],
				[
					99.375,
					2.5
				],
				[
					96.875,
					5
				],
				[
					95,
					8.125
				],
				[
					93.75,
					9.375
				],
				[
					94.375,
					10
				],
				[
					96.25,
					8.75
				],
				[
					99.375,
					5.625
				],
				[
					103.125,
					2.5
				],
				[
					105.625,
					0
				],
				[
					107.5,
					-0.625
				],
				[
					108.125,
					0
				],
				[
					106.875,
					1.875
				],
				[
					105,
					5
				],
				[
					103.125,
					7.5
				],
				[
					101.875,
					9.375
				],
				[
					101.875,
					10
				],
				[
					102.5,
					10
				],
				[
					104.375,
					8.75
				],
				[
					107.5,
					6.25
				],
				[
					110,
					4.375
				],
				[
					111.875,
					3.125
				],
				[
					113.125,
					2.5
				],
				[
					113.75,
					3.75
				],
				[
					113.125,
					5
				],
				[
					111.875,
					6.875
				],
				[
					111.25,
					8.75
				],
				[
					111.25,
					9.375
				],
				[
					113.125,
					8.125
				],
				[
					115.625,
					6.25
				],
				[
					117.5,
					4.375
				],
				[
					118.75,
					3.75
				],
				[
					119.375,
					3.75
				],
				[
					118.75,
					4.375
				],
				[
					117.5,
					6.25
				],
				[
					116.875,
					6.875
				],
				[
					116.25,
					6.875
				],
				[
					115.625,
					5.625
				],
				[
					115.625,
					5.625
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 744,
			"versionNonce": 914527056,
			"isDeleted": false,
			"id": "t5OV7HFb",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -991.456722905315,
			"y": -380.3509333730184,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 563,
			"height": 150,
			"seed": 371922335,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477221,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Complejidad: Se divide en\n    Simple: En el  cual la compresion de los conocimientos\nes facil de entender.\n    Compleja: Es aquella la cual aborda la complejidad\nSimple pero es generalizada en una, aunque tambien se\npuede dividir y llegar a muchas complejidades sencillas.",
			"rawText": "Complejidad: Se divide en\n    Simple: En el  cual la compresion de los conocimientos\nes facil de entender.\n    Compleja: Es aquella la cual aborda la complejidad\nSimple pero es generalizada en una, aunque tambien se\npuede dividir y llegar a muchas complejidades sencillas.",
			"baseline": 143,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Complejidad: Se divide en\n    Simple: En el  cual la compresion de los conocimientos\nes facil de entender.\n    Compleja: Es aquella la cual aborda la complejidad\nSimple pero es generalizada en una, aunque tambien se\npuede dividir y llegar a muchas complejidades sencillas."
		},
		{
			"type": "freedraw",
			"version": 192,
			"versionNonce": 1461651888,
			"isDeleted": false,
			"id": "P0_6Q_CujD06WLih7kNnP",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -992.6687674397281,
			"y": -370.1097896483219,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 107.8947368421052,
			"height": 17.894736842105317,
			"seed": 99735505,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477221,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0.5263157894736423,
					0.526315789473756
				],
				[
					1.0526315789473415,
					0.526315789473756
				],
				[
					1.0526315789473415,
					1.052631578947512
				],
				[
					2.105263157894683,
					1.052631578947512
				],
				[
					4.210526315789423,
					0
				],
				[
					7.368421052631561,
					-2.1052631578945693
				],
				[
					11.57894736842104,
					-4.736842105263122
				],
				[
					15.263157894736764,
					-6.842105263157805
				],
				[
					18.421052631578902,
					-8.421052631578846
				],
				[
					19.999999999999943,
					-8.421052631578846
				],
				[
					20.526315789473642,
					-7.368421052631447
				],
				[
					19.473684210526244,
					-5.789473684210407
				],
				[
					16.842105263157862,
					-3.6842105263157237
				],
				[
					14.210526315789423,
					-1.5789473684210407
				],
				[
					12.105263157894683,
					0
				],
				[
					11.052631578947285,
					1.5789473684211544
				],
				[
					11.052631578947285,
					2.1052631578947967
				],
				[
					12.105263157894683,
					2.1052631578947967
				],
				[
					15.263157894736764,
					1.052631578947512
				],
				[
					19.473684210526244,
					-1.5789473684210407
				],
				[
					24.736842105263122,
					-4.210526315789366
				],
				[
					29.473684210526244,
					-5.263157894736764
				],
				[
					31.57894736842104,
					-5.789473684210407
				],
				[
					31.052631578947285,
					-4.210526315789366
				],
				[
					28.421052631578902,
					-2.1052631578945693
				],
				[
					24.736842105263122,
					1.052631578947512
				],
				[
					21.57894736842104,
					4.210526315789593
				],
				[
					18.9473684210526,
					6.842105263157919
				],
				[
					18.421052631578902,
					7.894736842105317
				],
				[
					19.999999999999943,
					7.368421052631675
				],
				[
					23.15789473684208,
					5.263157894736878
				],
				[
					30.526315789473642,
					1.052631578947512
				],
				[
					36.31578947368416,
					-1.5789473684210407
				],
				[
					39.473684210526244,
					-2.6315789473683253
				],
				[
					41.052631578947285,
					-2.1052631578945693
				],
				[
					39.473684210526244,
					-0.5263157894735286
				],
				[
					35.263157894736764,
					3.157894736842195
				],
				[
					30.526315789473642,
					6.315789473684276
				],
				[
					27.36842105263156,
					7.894736842105317
				],
				[
					24.736842105263122,
					9.473684210526471
				],
				[
					25.78947368421052,
					8.947368421052715
				],
				[
					28.9473684210526,
					7.368421052631675
				],
				[
					35.78947368421046,
					3.157894736842195
				],
				[
					41.578947368420984,
					0
				],
				[
					45.263157894736764,
					-2.1052631578945693
				],
				[
					48.947368421052545,
					-3.6842105263157237
				],
				[
					51.052631578947285,
					-4.210526315789366
				],
				[
					51.578947368420984,
					-4.210526315789366
				],
				[
					51.052631578947285,
					-3.6842105263157237
				],
				[
					48.4210526315789,
					-1.5789473684210407
				],
				[
					46.31578947368416,
					0.526315789473756
				],
				[
					45.263157894736764,
					2.1052631578947967
				],
				[
					45.263157894736764,
					3.157894736842195
				],
				[
					47.8947368421052,
					2.1052631578947967
				],
				[
					51.052631578947285,
					0.526315789473756
				],
				[
					55.78947368421046,
					-1.0526315789472847
				],
				[
					59.473684210526244,
					-3.1578947368420813
				],
				[
					62.10526315789468,
					-3.6842105263157237
				],
				[
					62.63157894736838,
					-3.1578947368420813
				],
				[
					61.578947368420984,
					-1.0526315789472847
				],
				[
					58.947368421052545,
					1.5789473684211544
				],
				[
					55.78947368421046,
					3.6842105263158373
				],
				[
					54.21052631578942,
					5.263157894736878
				],
				[
					53.157894736842024,
					5.789473684210634
				],
				[
					54.21052631578942,
					5.789473684210634
				],
				[
					56.31578947368416,
					4.210526315789593
				],
				[
					59.99999999999994,
					2.1052631578947967
				],
				[
					63.684210526315724,
					0.526315789473756
				],
				[
					66.8421052631578,
					-1.0526315789472847
				],
				[
					68.94736842105254,
					-1.5789473684210407
				],
				[
					69.47368421052624,
					-1.5789473684210407
				],
				[
					69.47368421052624,
					-1.0526315789472847
				],
				[
					67.3684210526315,
					0.526315789473756
				],
				[
					64.73684210526307,
					3.157894736842195
				],
				[
					62.10526315789468,
					5.263157894736878
				],
				[
					61.052631578947285,
					6.842105263157919
				],
				[
					61.578947368420984,
					6.842105263157919
				],
				[
					64.21052631578942,
					5.789473684210634
				],
				[
					68.4210526315789,
					3.157894736842195
				],
				[
					73.68421052631572,
					0.526315789473756
				],
				[
					79.47368421052624,
					-1.5789473684210407
				],
				[
					83.15789473684202,
					-2.1052631578945693
				],
				[
					84.73684210526307,
					-2.1052631578945693
				],
				[
					84.73684210526307,
					-1.5789473684210407
				],
				[
					82.63157894736833,
					0
				],
				[
					79.47368421052624,
					2.6315789473685527
				],
				[
					76.31578947368416,
					4.210526315789593
				],
				[
					73.68421052631572,
					6.842105263157919
				],
				[
					74.21052631578942,
					7.894736842105317
				],
				[
					76.31578947368416,
					6.842105263157919
				],
				[
					81.57894736842098,
					4.210526315789593
				],
				[
					85.78947368421046,
					1.5789473684211544
				],
				[
					88.94736842105254,
					0
				],
				[
					91.57894736842098,
					-1.0526315789472847
				],
				[
					93.15789473684202,
					-1.5789473684210407
				],
				[
					92.63157894736833,
					-0.5263157894735286
				],
				[
					91.05263157894728,
					1.052631578947512
				],
				[
					89.99999999999994,
					2.6315789473685527
				],
				[
					88.42105263157885,
					4.736842105263236
				],
				[
					87.8947368421052,
					5.263157894736878
				],
				[
					88.94736842105254,
					5.263157894736878
				],
				[
					91.05263157894728,
					3.6842105263158373
				],
				[
					95.26315789473676,
					1.5789473684211544
				],
				[
					97.3684210526315,
					0
				],
				[
					99.47368421052624,
					-1.0526315789472847
				],
				[
					100.52631578947359,
					-1.5789473684210407
				],
				[
					101.05263157894728,
					-1.5789473684210407
				],
				[
					101.05263157894728,
					-1.0526315789472847
				],
				[
					101.05263157894728,
					0
				],
				[
					101.05263157894728,
					0.526315789473756
				],
				[
					101.05263157894728,
					2.1052631578947967
				],
				[
					100.52631578947359,
					3.157894736842195
				],
				[
					101.05263157894728,
					3.157894736842195
				],
				[
					102.63157894736833,
					2.6315789473685527
				],
				[
					104.73684210526307,
					1.5789473684211544
				],
				[
					106.8421052631578,
					0
				],
				[
					107.8947368421052,
					-0.5263157894735286
				],
				[
					107.8947368421052,
					0
				],
				[
					106.8421052631578,
					1.5789473684211544
				],
				[
					105.78947368421046,
					3.157894736842195
				],
				[
					104.73684210526307,
					4.736842105263236
				],
				[
					104.73684210526307,
					5.789473684210634
				],
				[
					104.21052631578937,
					5.263157894736878
				],
				[
					105.78947368421046,
					2.6315789473685527
				],
				[
					105.78947368421046,
					2.6315789473685527
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 71,
			"versionNonce": 1970514256,
			"isDeleted": false,
			"id": "tWLavaOt",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -996.8792937555174,
			"y": -746.4377248709936,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 75,
			"height": 25,
			"seed": 1821706687,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477221,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Solucion",
			"rawText": "Solucion",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Solucion"
		},
		{
			"type": "text",
			"version": 817,
			"versionNonce": 200430512,
			"isDeleted": false,
			"id": "NGdkDVzJ",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -997.531072833927,
			"y": -198.89894591645418,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 716,
			"height": 75,
			"seed": 164540991,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477222,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Hostilidad: Esta se puede ver de dos puntos favorables y hostiles. \nLos cuales necesitan una pronta reaccion, la cual puede estar dividida \nsegun su nivel de agresividad de la reaccion. ",
			"rawText": "Hostilidad: Esta se puede ver de dos puntos favorables y hostiles. \nLos cuales necesitan una pronta reaccion, la cual puede estar dividida \nsegun su nivel de agresividad de la reaccion. ",
			"baseline": 68,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Hostilidad: Esta se puede ver de dos puntos favorables y hostiles. \nLos cuales necesitan una pronta reaccion, la cual puede estar dividida \nsegun su nivel de agresividad de la reaccion. "
		},
		{
			"type": "freedraw",
			"version": 254,
			"versionNonce": 567410512,
			"isDeleted": false,
			"id": "S74dOOUiln1qnvBO8IhCB",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -994.3744232175586,
			"y": -189.0762157374262,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 106.47058823529409,
			"height": 15.294117647058783,
			"seed": 1163646257,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477222,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.5882352941176805,
					-0.5882352941176805
				],
				[
					-1.1764705882353041,
					-0.5882352941176805
				],
				[
					-1.7647058823529846,
					-1.176470588235361
				],
				[
					-2.3529411764706083,
					-1.176470588235361
				],
				[
					-4.117647058823536,
					-0.5882352941176805
				],
				[
					-5.882352941176521,
					0.5882352941175668
				],
				[
					-7.6470588235294485,
					2.3529411764704946
				],
				[
					-8.235294117647072,
					3.5294117647058556
				],
				[
					-7.6470588235294485,
					2.941176470588289
				],
				[
					-5.882352941176521,
					1.7647058823529278
				],
				[
					-3.5294117647059124,
					-1.176470588235361
				],
				[
					-1.1764705882353041,
					-2.941176470588289
				],
				[
					0,
					-4.705882352941217
				],
				[
					0.5882352941176237,
					-5.294117647058783
				],
				[
					0.5882352941176237,
					-4.705882352941217
				],
				[
					-0.5882352941176805,
					-3.5294117647058556
				],
				[
					-1.7647058823529846,
					-1.7647058823529278
				],
				[
					-4.117647058823536,
					1.7647058823529278
				],
				[
					-4.705882352941217,
					3.5294117647058556
				],
				[
					-4.705882352941217,
					4.117647058823536
				],
				[
					-2.941176470588289,
					3.5294117647058556
				],
				[
					0.5882352941176237,
					0.5882352941175668
				],
				[
					2.941176470588232,
					-1.176470588235361
				],
				[
					5.882352941176464,
					-3.5294117647058556
				],
				[
					7.058823529411711,
					-4.117647058823536
				],
				[
					7.058823529411711,
					-2.941176470588289
				],
				[
					4.70588235294116,
					-1.176470588235361
				],
				[
					1.7647058823529278,
					1.7647058823529278
				],
				[
					-1.1764705882353041,
					4.117647058823536
				],
				[
					-2.941176470588289,
					5.294117647058783
				],
				[
					-0.5882352941176805,
					4.117647058823536
				],
				[
					2.3529411764705515,
					1.176470588235361
				],
				[
					5.882352941176464,
					-1.176470588235361
				],
				[
					8.235294117647015,
					-2.3529411764706083
				],
				[
					9.41176470588232,
					-1.7647058823529278
				],
				[
					8.235294117647015,
					0
				],
				[
					5.882352941176464,
					2.3529411764704946
				],
				[
					2.941176470588232,
					4.705882352941217
				],
				[
					1.7647058823529278,
					5.882352941176464
				],
				[
					2.3529411764705515,
					5.882352941176464
				],
				[
					4.70588235294116,
					4.117647058823536
				],
				[
					8.235294117647015,
					1.176470588235361
				],
				[
					12.352941176470551,
					-0.5882352941176805
				],
				[
					14.70588235294116,
					-0.5882352941176805
				],
				[
					15.294117647058783,
					0.5882352941175668
				],
				[
					13.529411764705856,
					2.3529411764704946
				],
				[
					11.764705882352928,
					4.117647058823536
				],
				[
					9.999999999999943,
					4.705882352941217
				],
				[
					9.41176470588232,
					5.294117647058783
				],
				[
					9.41176470588232,
					4.117647058823536
				],
				[
					10.588235294117624,
					2.941176470588289
				],
				[
					14.70588235294116,
					0
				],
				[
					18.235294117647015,
					-1.7647058823529278
				],
				[
					19.999999999999943,
					-1.7647058823529278
				],
				[
					19.999999999999943,
					0
				],
				[
					17.64705882352939,
					2.941176470588289
				],
				[
					15.294117647058783,
					4.705882352941217
				],
				[
					12.352941176470551,
					7.058823529411711
				],
				[
					11.176470588235247,
					8.235294117647072
				],
				[
					12.352941176470551,
					6.470588235294144
				],
				[
					15.294117647058783,
					4.705882352941217
				],
				[
					18.235294117647015,
					2.941176470588289
				],
				[
					21.764705882352928,
					1.176470588235361
				],
				[
					24.70588235294116,
					0.5882352941175668
				],
				[
					25.294117647058783,
					1.176470588235361
				],
				[
					24.11764705882348,
					2.941176470588289
				],
				[
					21.764705882352928,
					5.294117647058783
				],
				[
					19.41176470588232,
					7.058823529411711
				],
				[
					17.64705882352939,
					8.235294117647072
				],
				[
					19.999999999999943,
					6.470588235294144
				],
				[
					24.70588235294116,
					3.5294117647058556
				],
				[
					29.999999999999943,
					0.5882352941175668
				],
				[
					34.70588235294116,
					-1.176470588235361
				],
				[
					37.64705882352939,
					-1.7647058823529278
				],
				[
					38.82352941176464,
					0
				],
				[
					37.05882352941171,
					1.7647058823529278
				],
				[
					34.70588235294116,
					3.5294117647058556
				],
				[
					31.764705882352928,
					4.705882352941217
				],
				[
					29.999999999999943,
					4.705882352941217
				],
				[
					29.999999999999943,
					4.117647058823536
				],
				[
					32.35294117647055,
					1.7647058823529278
				],
				[
					36.47058823529409,
					0
				],
				[
					39.99999999999994,
					-1.7647058823529278
				],
				[
					41.76470588235287,
					-2.3529411764706083
				],
				[
					42.941176470588175,
					-2.3529411764706083
				],
				[
					41.76470588235287,
					-1.176470588235361
				],
				[
					38.82352941176464,
					1.176470588235361
				],
				[
					34.70588235294116,
					3.5294117647058556
				],
				[
					31.764705882352928,
					5.882352941176464
				],
				[
					30.588235294117624,
					6.470588235294144
				],
				[
					32.35294117647055,
					5.882352941176464
				],
				[
					36.47058823529409,
					4.117647058823536
				],
				[
					41.76470588235287,
					1.7647058823529278
				],
				[
					47.05882352941171,
					-0.5882352941176805
				],
				[
					50.588235294117624,
					-1.176470588235361
				],
				[
					51.76470588235287,
					-1.7647058823529278
				],
				[
					50.588235294117624,
					-0.5882352941176805
				],
				[
					47.05882352941171,
					1.176470588235361
				],
				[
					42.35294117647055,
					3.5294117647058556
				],
				[
					38.82352941176464,
					5.294117647058783
				],
				[
					38.235294117647015,
					6.470588235294144
				],
				[
					39.41176470588232,
					5.882352941176464
				],
				[
					43.529411764705856,
					4.705882352941217
				],
				[
					48.82352941176464,
					1.7647058823529278
				],
				[
					54.11764705882348,
					-0.5882352941176805
				],
				[
					57.05882352941171,
					-2.3529411764706083
				],
				[
					57.64705882352939,
					-2.3529411764706083
				],
				[
					56.47058823529409,
					-1.176470588235361
				],
				[
					52.35294117647055,
					0.5882352941175668
				],
				[
					48.235294117647015,
					2.941176470588289
				],
				[
					44.11764705882348,
					4.705882352941217
				],
				[
					43.529411764705856,
					5.882352941176464
				],
				[
					45.88235294117641,
					4.705882352941217
				],
				[
					52.35294117647055,
					2.3529411764704946
				],
				[
					59.41176470588232,
					-0.5882352941176805
				],
				[
					64.7058823529411,
					-2.941176470588289
				],
				[
					70.58823529411757,
					-5.294117647058783
				],
				[
					71.76470588235287,
					-5.294117647058783
				],
				[
					70.58823529411757,
					-3.5294117647058556
				],
				[
					65.8823529411764,
					-0.5882352941176805
				],
				[
					61.76470588235287,
					1.176470588235361
				],
				[
					57.64705882352939,
					3.5294117647058556
				],
				[
					56.47058823529409,
					4.117647058823536
				],
				[
					57.64705882352939,
					3.5294117647058556
				],
				[
					60.588235294117624,
					2.3529411764704946
				],
				[
					65.8823529411764,
					-0.5882352941176805
				],
				[
					68.82352941176464,
					-2.3529411764706083
				],
				[
					71.76470588235287,
					-4.117647058823536
				],
				[
					72.35294117647055,
					-4.705882352941217
				],
				[
					71.76470588235287,
					-4.117647058823536
				],
				[
					69.41176470588232,
					-1.7647058823529278
				],
				[
					62.941176470588175,
					2.3529411764704946
				],
				[
					57.64705882352939,
					4.705882352941217
				],
				[
					52.941176470588175,
					7.647058823529392
				],
				[
					53.529411764705856,
					8.823529411764639
				],
				[
					57.05882352941171,
					7.647058823529392
				],
				[
					64.11764705882348,
					5.294117647058783
				],
				[
					70.58823529411757,
					1.7647058823529278
				],
				[
					77.05882352941171,
					-1.176470588235361
				],
				[
					81.17647058823525,
					-2.941176470588289
				],
				[
					82.35294117647055,
					-3.5294117647058556
				],
				[
					81.17647058823525,
					-2.941176470588289
				],
				[
					77.64705882352933,
					-1.176470588235361
				],
				[
					72.94117647058818,
					0.5882352941175668
				],
				[
					69.41176470588232,
					2.941176470588289
				],
				[
					67.05882352941171,
					4.117647058823536
				],
				[
					67.05882352941171,
					5.294117647058783
				],
				[
					68.23529411764702,
					4.705882352941217
				],
				[
					72.35294117647055,
					3.5294117647058556
				],
				[
					77.05882352941171,
					1.176470588235361
				],
				[
					81.17647058823525,
					-1.176470588235361
				],
				[
					84.11764705882348,
					-2.941176470588289
				],
				[
					85.29411764705878,
					-3.5294117647058556
				],
				[
					82.94117647058818,
					-2.3529411764706083
				],
				[
					79.41176470588232,
					0
				],
				[
					75.29411764705878,
					2.941176470588289
				],
				[
					72.94117647058818,
					4.705882352941217
				],
				[
					72.94117647058818,
					5.882352941176464
				],
				[
					74.7058823529411,
					5.294117647058783
				],
				[
					78.82352941176464,
					3.5294117647058556
				],
				[
					83.5294117647058,
					0.5882352941175668
				],
				[
					87.05882352941171,
					-1.7647058823529278
				],
				[
					89.41176470588232,
					-2.941176470588289
				],
				[
					88.82352941176464,
					-2.3529411764706083
				],
				[
					85.8823529411764,
					-0.5882352941176805
				],
				[
					82.35294117647055,
					2.3529411764704946
				],
				[
					79.99999999999994,
					4.705882352941217
				],
				[
					79.41176470588232,
					5.882352941176464
				],
				[
					79.99999999999994,
					6.470588235294144
				],
				[
					82.94117647058818,
					5.294117647058783
				],
				[
					87.05882352941171,
					2.941176470588289
				],
				[
					90.58823529411757,
					0.5882352941175668
				],
				[
					91.76470588235287,
					0
				],
				[
					91.17647058823525,
					0.5882352941175668
				],
				[
					88.82352941176464,
					2.3529411764704946
				],
				[
					85.8823529411764,
					4.705882352941217
				],
				[
					83.5294117647058,
					7.058823529411711
				],
				[
					83.5294117647058,
					8.823529411764639
				],
				[
					94.7058823529411,
					1.176470588235361
				],
				[
					94.11764705882348,
					1.7647058823529278
				],
				[
					92.94117647058818,
					2.941176470588289
				],
				[
					89.99999999999994,
					5.882352941176464
				],
				[
					87.64705882352933,
					8.823529411764639
				],
				[
					88.23529411764702,
					10
				],
				[
					90.58823529411757,
					10
				],
				[
					93.5294117647058,
					8.823529411764639
				],
				[
					96.47058823529403,
					6.470588235294144
				],
				[
					98.23529411764702,
					4.705882352941217
				],
				[
					96.47058823529403,
					4.117647058823536
				],
				[
					94.7058823529411,
					4.705882352941217
				],
				[
					94.7058823529411,
					4.705882352941217
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 514,
			"versionNonce": 466378160,
			"isDeleted": false,
			"id": "NdVuecZl",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -998.106674738857,
			"y": -98.91510301647634,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 671,
			"height": 100,
			"seed": 1274771089,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477222,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Diversidad: Es la cual se determina apartir de sectores integrados\no diversificados, relacionada con la mayor o menor amplitud de\nlos segmentos de clientes, tales como, gama de producto, \nzonas, servicios, etc ...",
			"rawText": "Diversidad: Es la cual se determina apartir de sectores integrados\no diversificados, relacionada con la mayor o menor amplitud de\nlos segmentos de clientes, tales como, gama de producto, \nzonas, servicios, etc ...",
			"baseline": 93,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Diversidad: Es la cual se determina apartir de sectores integrados\no diversificados, relacionada con la mayor o menor amplitud de\nlos segmentos de clientes, tales como, gama de producto, \nzonas, servicios, etc ..."
		},
		{
			"type": "freedraw",
			"version": 205,
			"versionNonce": 469079376,
			"isDeleted": false,
			"id": "b6XZlhBGh3ZuHRgAwniP-",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -996.0951804859835,
			"y": -92.38636738429238,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 106.24999999999994,
			"height": 19.583333333333258,
			"seed": 478787327,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477222,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.41666666666662877,
					0
				],
				[
					-0.8333333333333712,
					-0.41666666666674246
				],
				[
					-1.6666666666666288,
					-0.8333333333333712
				],
				[
					-2.5,
					-0.41666666666674246
				],
				[
					-4.166666666666629,
					0.8333333333332575
				],
				[
					-5,
					2.9166666666666288
				],
				[
					-4.583333333333371,
					4.166666666666629
				],
				[
					-1.6666666666666288,
					4.166666666666629
				],
				[
					1.25,
					2.0833333333332575
				],
				[
					3.3333333333333712,
					0.41666666666662877
				],
				[
					5.416666666666629,
					-2.0833333333333712
				],
				[
					5.833333333333371,
					-3.3333333333333712
				],
				[
					5.833333333333371,
					-3.75
				],
				[
					4.583333333333371,
					-2.5
				],
				[
					2.0833333333333712,
					0
				],
				[
					-1.25,
					4.166666666666629
				],
				[
					-4.166666666666629,
					8.333333333333258
				],
				[
					-4.166666666666629,
					9.583333333333258
				],
				[
					-1.6666666666666288,
					9.166666666666629
				],
				[
					4.166666666666629,
					4.166666666666629
				],
				[
					8.333333333333371,
					0.41666666666662877
				],
				[
					13.333333333333314,
					-3.3333333333333712
				],
				[
					15,
					-5
				],
				[
					14.166666666666629,
					-2.9166666666667425
				],
				[
					10.416666666666629,
					1.25
				],
				[
					5.416666666666629,
					7.916666666666629
				],
				[
					3.3333333333333712,
					10.833333333333258
				],
				[
					1.6666666666666288,
					12.916666666666629
				],
				[
					1.25,
					13.75
				],
				[
					2.5,
					12.5
				],
				[
					5.833333333333371,
					9.166666666666629
				],
				[
					10.416666666666629,
					5.416666666666629
				],
				[
					13.75,
					2.9166666666666288
				],
				[
					15.416666666666629,
					2.9166666666666288
				],
				[
					14.583333333333314,
					4.5833333333332575
				],
				[
					12.083333333333371,
					7.5
				],
				[
					9.166666666666629,
					10.416666666666629
				],
				[
					7.083333333333371,
					12.916666666666629
				],
				[
					5.833333333333371,
					14.166666666666629
				],
				[
					6.666666666666629,
					14.583333333333258
				],
				[
					11.25,
					12.5
				],
				[
					17.5,
					8.75
				],
				[
					22.91666666666663,
					5
				],
				[
					26.25,
					3.3333333333332575
				],
				[
					27.083333333333314,
					4.166666666666629
				],
				[
					25.833333333333314,
					6.666666666666629
				],
				[
					23.75,
					9.583333333333258
				],
				[
					21.66666666666663,
					11.25
				],
				[
					20.833333333333314,
					12.083333333333258
				],
				[
					22.083333333333314,
					11.25
				],
				[
					25.41666666666663,
					8.333333333333258
				],
				[
					29.583333333333314,
					5.8333333333332575
				],
				[
					32.91666666666663,
					4.166666666666629
				],
				[
					34.583333333333314,
					4.166666666666629
				],
				[
					34.583333333333314,
					5.416666666666629
				],
				[
					33.75,
					7.5
				],
				[
					32.083333333333314,
					9.166666666666629
				],
				[
					30.833333333333314,
					10
				],
				[
					30.41666666666663,
					10
				],
				[
					31.66666666666663,
					8.333333333333258
				],
				[
					34.583333333333314,
					5.8333333333332575
				],
				[
					37.083333333333314,
					3.75
				],
				[
					38.75,
					3.3333333333332575
				],
				[
					39.583333333333314,
					4.166666666666629
				],
				[
					39.583333333333314,
					6.666666666666629
				],
				[
					38.333333333333314,
					9.166666666666629
				],
				[
					37.5,
					10.833333333333258
				],
				[
					36.66666666666663,
					11.666666666666629
				],
				[
					36.25,
					11.666666666666629
				],
				[
					37.5,
					10
				],
				[
					40.833333333333314,
					7.5
				],
				[
					44.583333333333314,
					5
				],
				[
					47.083333333333314,
					3.75
				],
				[
					48.333333333333314,
					4.166666666666629
				],
				[
					47.91666666666663,
					6.25
				],
				[
					46.66666666666663,
					8.75
				],
				[
					45.41666666666663,
					10.833333333333258
				],
				[
					45,
					12.083333333333258
				],
				[
					47.5,
					10.416666666666629
				],
				[
					50.833333333333314,
					7.5
				],
				[
					53.75,
					5
				],
				[
					57.5,
					2.9166666666666288
				],
				[
					59.16666666666663,
					3.3333333333332575
				],
				[
					59.16666666666663,
					5.416666666666629
				],
				[
					57.91666666666663,
					7.916666666666629
				],
				[
					57.083333333333314,
					10.416666666666629
				],
				[
					56.66666666666663,
					11.666666666666629
				],
				[
					57.5,
					11.25
				],
				[
					59.583333333333314,
					9.166666666666629
				],
				[
					62.91666666666663,
					6.666666666666629
				],
				[
					65.83333333333331,
					4.166666666666629
				],
				[
					67.91666666666663,
					3.3333333333332575
				],
				[
					69.99999999999994,
					3.3333333333332575
				],
				[
					71.24999999999994,
					4.166666666666629
				],
				[
					72.49999999999994,
					5
				],
				[
					72.91666666666663,
					5.8333333333332575
				],
				[
					73.33333333333331,
					6.25
				],
				[
					73.74999999999994,
					5.416666666666629
				],
				[
					74.58333333333331,
					4.166666666666629
				],
				[
					76.24999999999994,
					2.0833333333332575
				],
				[
					77.91666666666663,
					0.8333333333332575
				],
				[
					78.74999999999994,
					0.41666666666662877
				],
				[
					78.74999999999994,
					0.8333333333332575
				],
				[
					78.74999999999994,
					2.5
				],
				[
					78.33333333333331,
					3.75
				],
				[
					78.33333333333331,
					4.5833333333332575
				],
				[
					79.16666666666663,
					5
				],
				[
					80.41666666666663,
					4.5833333333332575
				],
				[
					82.08333333333331,
					3.3333333333332575
				],
				[
					84.16666666666663,
					2.5
				],
				[
					84.99999999999994,
					1.6666666666666288
				],
				[
					85.83333333333331,
					1.6666666666666288
				],
				[
					84.99999999999994,
					2.9166666666666288
				],
				[
					84.58333333333331,
					4.5833333333332575
				],
				[
					83.33333333333331,
					7.0833333333332575
				],
				[
					82.49999999999994,
					8.75
				],
				[
					82.08333333333331,
					10
				],
				[
					82.08333333333331,
					10.416666666666629
				],
				[
					83.33333333333331,
					9.583333333333258
				],
				[
					85.83333333333331,
					7.5
				],
				[
					87.49999999999994,
					6.25
				],
				[
					89.16666666666663,
					5
				],
				[
					89.99999999999994,
					4.5833333333332575
				],
				[
					90.41666666666663,
					4.5833333333332575
				],
				[
					90.41666666666663,
					5
				],
				[
					89.99999999999994,
					5.8333333333332575
				],
				[
					89.16666666666663,
					7.916666666666629
				],
				[
					88.74999999999994,
					9.583333333333258
				],
				[
					88.33333333333331,
					10.833333333333258
				],
				[
					88.74999999999994,
					11.25
				],
				[
					89.99999999999994,
					10.416666666666629
				],
				[
					91.66666666666663,
					8.75
				],
				[
					95.41666666666663,
					5.416666666666629
				],
				[
					97.08333333333331,
					3.75
				],
				[
					97.91666666666663,
					2.9166666666666288
				],
				[
					97.49999999999994,
					3.75
				],
				[
					97.08333333333331,
					5
				],
				[
					97.08333333333331,
					6.666666666666629
				],
				[
					96.66666666666663,
					7.5
				],
				[
					96.66666666666663,
					7.916666666666629
				],
				[
					97.49999999999994,
					7.5
				],
				[
					98.74999999999994,
					5.8333333333332575
				],
				[
					100.41666666666663,
					4.166666666666629
				],
				[
					101.24999999999994,
					2.9166666666666288
				],
				[
					100.83333333333326,
					2.9166666666666288
				],
				[
					99.16666666666663,
					5
				],
				[
					97.91666666666663,
					7.0833333333332575
				],
				[
					97.08333333333331,
					8.75
				],
				[
					97.49999999999994,
					9.166666666666629
				],
				[
					97.91666666666663,
					8.75
				],
				[
					97.91666666666663,
					8.75
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 110,
			"versionNonce": 2010458032,
			"isDeleted": false,
			"id": "ir7baDpU",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -1028.4562915970946,
			"y": 41.7525215045963,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 220,
			"height": 25,
			"seed": 324041841,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477222,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "3) Modelos de entorno",
			"rawText": "3) Modelos de entorno",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "3) Modelos de entorno"
		},
		{
			"type": "text",
			"version": 386,
			"versionNonce": 2033609552,
			"isDeleted": false,
			"id": "D6mJUSth",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -990.6097307505333,
			"y": 82.1758019278767,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 723,
			"height": 75,
			"seed": 1827439455,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477222,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Entornos estables: estables, simples e integrados.\nEntornos Reactivos-Adaptativos: estables, favorables y diversos.\nEntornos inestables turbulentos: dinamicos, complejos, hostiles y diversos.",
			"rawText": "Entornos estables: estables, simples e integrados.\nEntornos Reactivos-Adaptativos: estables, favorables y diversos.\nEntornos inestables turbulentos: dinamicos, complejos, hostiles y diversos.",
			"baseline": 68,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Entornos estables: estables, simples e integrados.\nEntornos Reactivos-Adaptativos: estables, favorables y diversos.\nEntornos inestables turbulentos: dinamicos, complejos, hostiles y diversos."
		},
		{
			"type": "freedraw",
			"version": 149,
			"versionNonce": 741650864,
			"isDeleted": false,
			"id": "KD6kDnWvReON4i-L-eitm",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -988.8319529727555,
			"y": 93.39802415009865,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 182.77777777777771,
			"height": 5.555555555555657,
			"seed": 144123409,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477222,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					-1.6666666666667425
				],
				[
					0.5555555555555429,
					-2.2222222222222854
				],
				[
					0,
					-2.2222222222222854
				],
				[
					0,
					-1.6666666666667425
				],
				[
					0.5555555555555429,
					-1.6666666666667425
				],
				[
					1.1111111111110858,
					-1.6666666666667425
				],
				[
					2.2222222222221717,
					-2.2222222222222854
				],
				[
					4.444444444444457,
					-2.7777777777778283
				],
				[
					6.111111111111086,
					-2.7777777777778283
				],
				[
					8.333333333333314,
					-2.7777777777778283
				],
				[
					10.555555555555543,
					-2.7777777777778283
				],
				[
					13.333333333333314,
					-2.7777777777778283
				],
				[
					16.111111111111086,
					-2.7777777777778283
				],
				[
					19.4444444444444,
					-2.7777777777778283
				],
				[
					22.22222222222217,
					-3.333333333333485
				],
				[
					24.4444444444444,
					-2.7777777777778283
				],
				[
					26.111111111111086,
					-2.7777777777778283
				],
				[
					27.777777777777715,
					-2.2222222222222854
				],
				[
					30,
					-1.6666666666667425
				],
				[
					32.22222222222217,
					-1.6666666666667425
				],
				[
					33.88888888888886,
					-1.6666666666667425
				],
				[
					35.55555555555554,
					-1.6666666666667425
				],
				[
					37.22222222222217,
					-1.6666666666667425
				],
				[
					38.88888888888886,
					-1.6666666666667425
				],
				[
					40.55555555555554,
					-1.6666666666667425
				],
				[
					41.66666666666663,
					-2.2222222222222854
				],
				[
					42.777777777777715,
					-2.2222222222222854
				],
				[
					43.333333333333314,
					-1.6666666666667425
				],
				[
					43.88888888888886,
					-1.6666666666667425
				],
				[
					44.99999999999994,
					-1.6666666666667425
				],
				[
					46.111111111111086,
					-1.6666666666667425
				],
				[
					47.777777777777715,
					-1.6666666666667425
				],
				[
					49.99999999999994,
					-1.6666666666667425
				],
				[
					51.66666666666663,
					-1.1111111111111995
				],
				[
					52.777777777777715,
					-1.1111111111111995
				],
				[
					54.99999999999994,
					-0.5555555555556566
				],
				[
					57.22222222222217,
					-0.5555555555556566
				],
				[
					59.99999999999994,
					-0.5555555555556566
				],
				[
					62.22222222222217,
					-0.5555555555556566
				],
				[
					64.4444444444444,
					-0.5555555555556566
				],
				[
					66.66666666666663,
					-0.5555555555556566
				],
				[
					68.33333333333326,
					-0.5555555555556566
				],
				[
					69.99999999999994,
					-0.5555555555556566
				],
				[
					71.66666666666663,
					-0.5555555555556566
				],
				[
					73.33333333333326,
					-0.5555555555556566
				],
				[
					74.99999999999994,
					-0.5555555555556566
				],
				[
					76.11111111111109,
					-0.5555555555556566
				],
				[
					77.22222222222217,
					0
				],
				[
					78.33333333333326,
					0
				],
				[
					79.4444444444444,
					0
				],
				[
					81.11111111111103,
					0
				],
				[
					82.77777777777771,
					0
				],
				[
					84.99999999999994,
					0
				],
				[
					86.66666666666663,
					0
				],
				[
					88.33333333333326,
					0
				],
				[
					89.99999999999994,
					0
				],
				[
					92.77777777777771,
					0
				],
				[
					96.66666666666663,
					-0.5555555555556566
				],
				[
					99.99999999999994,
					-0.5555555555556566
				],
				[
					103.33333333333326,
					-0.5555555555556566
				],
				[
					107.22222222222217,
					-0.5555555555556566
				],
				[
					111.11111111111103,
					-0.5555555555556566
				],
				[
					115.55555555555549,
					-0.5555555555556566
				],
				[
					118.8888888888888,
					0
				],
				[
					124.4444444444444,
					0
				],
				[
					127.77777777777771,
					0.5555555555554292
				],
				[
					129.99999999999994,
					0.5555555555554292
				],
				[
					133.33333333333326,
					0.5555555555554292
				],
				[
					134.99999999999994,
					0.5555555555554292
				],
				[
					138.33333333333326,
					1.1111111111110858
				],
				[
					143.33333333333326,
					1.1111111111110858
				],
				[
					145.5555555555555,
					1.1111111111110858
				],
				[
					148.33333333333326,
					1.666666666666515
				],
				[
					151.11111111111103,
					1.666666666666515
				],
				[
					152.77777777777771,
					1.666666666666515
				],
				[
					154.99999999999994,
					1.666666666666515
				],
				[
					157.77777777777771,
					1.666666666666515
				],
				[
					159.99999999999994,
					2.2222222222221717
				],
				[
					161.66666666666657,
					2.2222222222221717
				],
				[
					163.33333333333326,
					2.2222222222221717
				],
				[
					164.9999999999999,
					2.2222222222221717
				],
				[
					167.22222222222211,
					1.666666666666515
				],
				[
					169.44444444444434,
					1.666666666666515
				],
				[
					171.11111111111103,
					1.666666666666515
				],
				[
					172.22222222222211,
					1.666666666666515
				],
				[
					173.33333333333326,
					1.1111111111110858
				],
				[
					174.44444444444434,
					1.1111111111110858
				],
				[
					175.5555555555555,
					1.1111111111110858
				],
				[
					176.66666666666657,
					1.1111111111110858
				],
				[
					178.33333333333326,
					1.1111111111110858
				],
				[
					179.44444444444434,
					1.666666666666515
				],
				[
					180.5555555555555,
					1.666666666666515
				],
				[
					181.11111111111103,
					1.666666666666515
				],
				[
					181.66666666666657,
					1.666666666666515
				],
				[
					182.22222222222211,
					1.666666666666515
				],
				[
					182.77777777777771,
					1.666666666666515
				],
				[
					180.5555555555555,
					0.5555555555554292
				],
				[
					180.5555555555555,
					0.5555555555554292
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 122,
			"versionNonce": 1676443984,
			"isDeleted": false,
			"id": "HHTAEk-Ec4L_E4oUn4Rw6",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -986.0541751949779,
			"y": 115.62024637232082,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 188.33333333333331,
			"height": 7.777777777777601,
			"seed": 1281539967,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477223,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0,
					0.5555555555554292
				],
				[
					-0.5555555555555429,
					1.1111111111110858
				],
				[
					0,
					1.1111111111110858
				],
				[
					0.5555555555555998,
					1.1111111111110858
				],
				[
					2.7777777777778283,
					0.5555555555554292
				],
				[
					4.444444444444457,
					0.5555555555554292
				],
				[
					5.5555555555556,
					0.5555555555554292
				],
				[
					7.777777777777828,
					1.1111111111110858
				],
				[
					11.111111111111143,
					1.6666666666667425
				],
				[
					15,
					1.6666666666667425
				],
				[
					22.77777777777783,
					1.6666666666667425
				],
				[
					27.77777777777783,
					1.6666666666667425
				],
				[
					33.888888888888914,
					1.6666666666667425
				],
				[
					41.11111111111114,
					1.6666666666667425
				],
				[
					45.5555555555556,
					1.6666666666667425
				],
				[
					51.11111111111114,
					1.1111111111110858
				],
				[
					56.666666666666686,
					0.5555555555554292
				],
				[
					62.22222222222223,
					0
				],
				[
					66.11111111111114,
					0
				],
				[
					69.44444444444446,
					0
				],
				[
					72.22222222222223,
					0
				],
				[
					75.55555555555554,
					0
				],
				[
					78.88888888888891,
					0
				],
				[
					82.77777777777777,
					0
				],
				[
					85.55555555555554,
					0.5555555555554292
				],
				[
					88.33333333333331,
					0.5555555555554292
				],
				[
					90.55555555555554,
					1.1111111111110858
				],
				[
					92.77777777777777,
					1.1111111111110858
				],
				[
					96.66666666666669,
					1.6666666666667425
				],
				[
					100.55555555555554,
					1.6666666666667425
				],
				[
					104.44444444444446,
					2.2222222222221717
				],
				[
					108.33333333333331,
					2.2222222222221717
				],
				[
					111.11111111111109,
					2.777777777777601
				],
				[
					113.33333333333331,
					2.777777777777601
				],
				[
					115,
					3.3333333333332575
				],
				[
					117.22222222222223,
					3.3333333333332575
				],
				[
					120,
					3.3333333333332575
				],
				[
					122.22222222222223,
					3.888888888888914
				],
				[
					123.88888888888886,
					4.444444444444343
				],
				[
					125.55555555555554,
					5
				],
				[
					127.22222222222223,
					5.555555555555429
				],
				[
					128.88888888888886,
					5.555555555555429
				],
				[
					132.77777777777777,
					5.555555555555429
				],
				[
					134.44444444444446,
					5.555555555555429
				],
				[
					137.77777777777777,
					6.111111111111086
				],
				[
					140.55555555555554,
					6.111111111111086
				],
				[
					142.77777777777777,
					6.111111111111086
				],
				[
					145.55555555555554,
					6.6666666666667425
				],
				[
					148.33333333333331,
					6.6666666666667425
				],
				[
					153.88888888888886,
					6.6666666666667425
				],
				[
					157.22222222222223,
					6.6666666666667425
				],
				[
					159.4444444444444,
					7.222222222222172
				],
				[
					162.22222222222217,
					7.222222222222172
				],
				[
					163.88888888888886,
					7.222222222222172
				],
				[
					165.55555555555554,
					7.777777777777601
				],
				[
					167.77777777777777,
					7.777777777777601
				],
				[
					170,
					7.777777777777601
				],
				[
					171.66666666666663,
					7.777777777777601
				],
				[
					173.33333333333331,
					7.222222222222172
				],
				[
					175.55555555555554,
					7.222222222222172
				],
				[
					177.77777777777777,
					7.222222222222172
				],
				[
					181.1111111111111,
					7.222222222222172
				],
				[
					183.88888888888886,
					7.222222222222172
				],
				[
					186.1111111111111,
					7.222222222222172
				],
				[
					187.22222222222217,
					7.222222222222172
				],
				[
					187.77777777777777,
					7.222222222222172
				],
				[
					186.1111111111111,
					7.222222222222172
				],
				[
					182.22222222222217,
					7.222222222222172
				],
				[
					177.22222222222217,
					7.777777777777601
				],
				[
					172.22222222222217,
					7.777777777777601
				],
				[
					172.22222222222217,
					7.777777777777601
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 143,
			"versionNonce": 1244080,
			"isDeleted": false,
			"id": "aVppwERz9fBvcw_Hv_Lt1",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -983.2763974172001,
			"y": 143.95357970565408,
			"strokeColor": "#e67700",
			"backgroundColor": "transparent",
			"width": 187.22222222222217,
			"height": 4.444444444444571,
			"seed": 801782769,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477223,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-1.6666666666666856,
					-0.5555555555556566
				],
				[
					-2.7777777777778283,
					0
				],
				[
					-3.3333333333333712,
					0
				],
				[
					-3.888888888888914,
					0
				],
				[
					-4.444444444444457,
					0
				],
				[
					-5,
					0.5555555555556566
				],
				[
					-5.555555555555543,
					0.5555555555556566
				],
				[
					-6.111111111111086,
					0.5555555555556566
				],
				[
					-7.2222222222222285,
					0.5555555555556566
				],
				[
					-7.777777777777828,
					1.1111111111110858
				],
				[
					-7.777777777777828,
					1.6666666666667425
				],
				[
					-7.777777777777828,
					2.2222222222221717
				],
				[
					-7.2222222222222285,
					2.7777777777778283
				],
				[
					-5.555555555555543,
					2.7777777777778283
				],
				[
					-3.3333333333333712,
					2.7777777777778283
				],
				[
					0,
					3.333333333333485
				],
				[
					4.444444444444457,
					3.333333333333485
				],
				[
					9.444444444444457,
					2.7777777777778283
				],
				[
					14.444444444444457,
					1.6666666666667425
				],
				[
					19.444444444444457,
					1.1111111111110858
				],
				[
					22.777777777777715,
					0.5555555555556566
				],
				[
					25.555555555555543,
					0.5555555555556566
				],
				[
					27.22222222222217,
					0.5555555555556566
				],
				[
					30,
					0.5555555555556566
				],
				[
					32.777777777777715,
					0
				],
				[
					36.111111111111086,
					-0.5555555555556566
				],
				[
					38.333333333333314,
					-0.5555555555556566
				],
				[
					39.4444444444444,
					-0.5555555555556566
				],
				[
					41.111111111111086,
					0
				],
				[
					42.77777777777777,
					0
				],
				[
					46.111111111111086,
					0
				],
				[
					48.88888888888886,
					0
				],
				[
					51.111111111111086,
					0
				],
				[
					52.777777777777715,
					0
				],
				[
					54.4444444444444,
					0.5555555555556566
				],
				[
					57.22222222222217,
					1.1111111111110858
				],
				[
					60.55555555555554,
					1.1111111111110858
				],
				[
					64.99999999999994,
					1.1111111111110858
				],
				[
					68.88888888888886,
					1.1111111111110858
				],
				[
					73.33333333333331,
					1.6666666666667425
				],
				[
					76.66666666666663,
					1.6666666666667425
				],
				[
					79.4444444444444,
					2.2222222222221717
				],
				[
					82.22222222222217,
					2.2222222222221717
				],
				[
					85.55555555555549,
					2.7777777777778283
				],
				[
					88.88888888888886,
					2.7777777777778283
				],
				[
					92.77777777777771,
					2.7777777777778283
				],
				[
					95.55555555555549,
					3.333333333333485
				],
				[
					97.77777777777771,
					3.333333333333485
				],
				[
					98.88888888888886,
					3.333333333333485
				],
				[
					101.66666666666663,
					3.333333333333485
				],
				[
					105.55555555555549,
					2.7777777777778283
				],
				[
					109.99999999999994,
					2.7777777777778283
				],
				[
					113.33333333333326,
					2.7777777777778283
				],
				[
					115.55555555555549,
					2.7777777777778283
				],
				[
					118.33333333333326,
					2.7777777777778283
				],
				[
					121.66666666666663,
					2.7777777777778283
				],
				[
					123.8888888888888,
					2.7777777777778283
				],
				[
					126.66666666666663,
					2.2222222222221717
				],
				[
					128.8888888888888,
					1.6666666666667425
				],
				[
					131.11111111111103,
					1.6666666666667425
				],
				[
					134.4444444444444,
					1.1111111111110858
				],
				[
					138.8888888888888,
					1.1111111111110858
				],
				[
					142.77777777777771,
					0.5555555555556566
				],
				[
					144.99999999999994,
					0.5555555555556566
				],
				[
					147.22222222222217,
					1.1111111111110858
				],
				[
					148.33333333333326,
					1.1111111111110858
				],
				[
					150.5555555555555,
					1.1111111111110858
				],
				[
					153.33333333333326,
					1.1111111111110858
				],
				[
					155.5555555555555,
					1.1111111111110858
				],
				[
					157.22222222222217,
					1.1111111111110858
				],
				[
					157.77777777777771,
					1.1111111111110858
				],
				[
					158.8888888888888,
					1.6666666666667425
				],
				[
					159.99999999999994,
					2.2222222222221717
				],
				[
					161.66666666666657,
					2.2222222222221717
				],
				[
					163.8888888888888,
					2.7777777777778283
				],
				[
					165.5555555555555,
					2.7777777777778283
				],
				[
					166.66666666666657,
					2.7777777777778283
				],
				[
					167.22222222222217,
					2.7777777777778283
				],
				[
					168.33333333333326,
					2.7777777777778283
				],
				[
					169.44444444444434,
					2.7777777777778283
				],
				[
					171.11111111111103,
					2.7777777777778283
				],
				[
					172.22222222222217,
					3.333333333333485
				],
				[
					173.8888888888888,
					3.333333333333485
				],
				[
					174.99999999999994,
					3.888888888888914
				],
				[
					176.11111111111103,
					3.888888888888914
				],
				[
					177.22222222222217,
					3.888888888888914
				],
				[
					178.33333333333326,
					3.888888888888914
				],
				[
					178.8888888888888,
					3.888888888888914
				],
				[
					179.44444444444434,
					3.888888888888914
				],
				[
					178.33333333333326,
					3.888888888888914
				],
				[
					176.66666666666657,
					3.888888888888914
				],
				[
					176.11111111111103,
					3.888888888888914
				],
				[
					176.11111111111103,
					3.888888888888914
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "text",
			"version": 19,
			"versionNonce": 2070927184,
			"isDeleted": false,
			"id": "tOd0clFy",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -1021.6428571428567,
			"y": 220.20170002627287,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 30,
			"height": 25,
			"seed": 905738143,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477224,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "4) ",
			"rawText": "4) ",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "4) "
		},
		{
			"type": "text",
			"version": 164,
			"versionNonce": 362284464,
			"isDeleted": false,
			"id": "d9TQIsgl",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -990.2142857142853,
			"y": 218.77312859770234,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 360,
			"height": 25,
			"seed": 266311121,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477224,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Niveles de turbulencia segun Ansoff ",
			"rawText": "Niveles de turbulencia segun Ansoff ",
			"baseline": 18,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Niveles de turbulencia segun Ansoff "
		},
		{
			"type": "text",
			"version": 1327,
			"versionNonce": 874016080,
			"isDeleted": false,
			"id": "PTRoDcgI",
			"fillStyle": "hachure",
			"strokeWidth": 4,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -977.6262939958578,
			"y": 262.43772487099375,
			"strokeColor": "#000000",
			"backgroundColor": "transparent",
			"width": 672,
			"height": 250,
			"seed": 1542370225,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477224,
			"link": null,
			"fontSize": 20,
			"fontFamily": 1,
			"text": "Estable: Cuando el entorno es estable, sencillo,\npredictible.\nReactivo: Cuando el entorno es estable, favorable a\nla gestion empresarial.\nAnticipador: Cuando el entorno es cambiante, desfavorable,\ny esta sometido a continuas alteraciones del ambiente.\nExplorador: Cuando no es posible la extrapolacion de organizaciones\ncomplejas.\nNivel Creativo: Cuando la unica forma de reaccionar generan \nrespuestas rapidas y flexibles.",
			"rawText": "Estable: Cuando el entorno es estable, sencillo,\npredictible.\nReactivo: Cuando el entorno es estable, favorable a\nla gestion empresarial.\nAnticipador: Cuando el entorno es cambiante, desfavorable,\ny esta sometido a continuas alteraciones del ambiente.\nExplorador: Cuando no es posible la extrapolacion de organizaciones\ncomplejas.\nNivel Creativo: Cuando la unica forma de reaccionar generan \nrespuestas rapidas y flexibles.",
			"baseline": 243,
			"textAlign": "left",
			"verticalAlign": "top",
			"containerId": null,
			"originalText": "Estable: Cuando el entorno es estable, sencillo,\npredictible.\nReactivo: Cuando el entorno es estable, favorable a\nla gestion empresarial.\nAnticipador: Cuando el entorno es cambiante, desfavorable,\ny esta sometido a continuas alteraciones del ambiente.\nExplorador: Cuando no es posible la extrapolacion de organizaciones\ncomplejas.\nNivel Creativo: Cuando la unica forma de reaccionar generan \nrespuestas rapidas y flexibles."
		},
		{
			"type": "freedraw",
			"version": 59,
			"versionNonce": 166449072,
			"isDeleted": false,
			"id": "Qoy9jKh01JEJwQdw4MGaG",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -982.4089026915101,
			"y": 275.7855509579498,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 77.82608695652175,
			"height": 2.173913043478251,
			"seed": 1249899729,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477224,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					0.43478260869562746,
					0
				],
				[
					0.8695652173913686,
					0
				],
				[
					1.304347826086996,
					-0.43478260869562746
				],
				[
					1.7391304347826235,
					-0.43478260869562746
				],
				[
					3.0434782608696196,
					-0.43478260869562746
				],
				[
					5.652173913043498,
					-0.43478260869562746
				],
				[
					9.130434782608631,
					-0.43478260869562746
				],
				[
					12.608695652173878,
					-0.8695652173912549
				],
				[
					18.260869565217376,
					-1.3043478260868824
				],
				[
					23.04347826086962,
					-1.7391304347826235
				],
				[
					26.95652173913038,
					-2.173913043478251
				],
				[
					30.434782608695627,
					-2.173913043478251
				],
				[
					32.17391304347825,
					-2.173913043478251
				],
				[
					34.78260869565213,
					-2.173913043478251
				],
				[
					38.26086956521738,
					-2.173913043478251
				],
				[
					40.869565217391255,
					-2.173913043478251
				],
				[
					44.78260869565213,
					-1.7391304347826235
				],
				[
					46.52173913043475,
					-1.7391304347826235
				],
				[
					47.82608695652175,
					-1.3043478260868824
				],
				[
					48.695652173913004,
					-0.8695652173912549
				],
				[
					49.56521739130437,
					-0.8695652173912549
				],
				[
					52.17391304347825,
					-0.8695652173912549
				],
				[
					55.6521739130435,
					-0.8695652173912549
				],
				[
					59.13043478260863,
					-1.3043478260868824
				],
				[
					61.73913043478262,
					-1.3043478260868824
				],
				[
					63.47826086956519,
					-1.3043478260868824
				],
				[
					63.913043478260875,
					-0.8695652173912549
				],
				[
					64.78260869565213,
					-0.8695652173912549
				],
				[
					64.78260869565213,
					-0.43478260869562746
				],
				[
					65.65217391304344,
					-0.43478260869562746
				],
				[
					66.08695652173913,
					-0.43478260869562746
				],
				[
					66.52173913043475,
					0
				],
				[
					66.95652173913038,
					0
				],
				[
					68.26086956521738,
					0
				],
				[
					69.56521739130432,
					0
				],
				[
					70.86956521739125,
					0
				],
				[
					71.73913043478257,
					0
				],
				[
					72.17391304347825,
					0
				],
				[
					72.60869565217388,
					0
				],
				[
					73.47826086956519,
					0
				],
				[
					75.21739130434776,
					0
				],
				[
					76.08695652173913,
					-0.43478260869562746
				],
				[
					77.39130434782606,
					-0.43478260869562746
				],
				[
					77.82608695652175,
					-0.43478260869562746
				],
				[
					77.82608695652175,
					0
				],
				[
					77.39130434782606,
					0
				],
				[
					76.95652173913038,
					-0.43478260869562746
				],
				[
					76.08695652173913,
					-0.8695652173912549
				],
				[
					76.08695652173913,
					-0.8695652173912549
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 47,
			"versionNonce": 926827344,
			"isDeleted": false,
			"id": "8Y4wGsFMunl8VvOpLav5i",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -981.1045548654231,
			"y": 379.2638118275149,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 94.78260869565213,
			"height": 3.478260869565247,
			"seed": 356727999,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477224,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					1.3043478260868824,
					0
				],
				[
					3.9130434782608745,
					0
				],
				[
					9.130434782608631,
					-0.8695652173912549
				],
				[
					17.826086956521635,
					-1.7391304347825098
				],
				[
					24.78260869565213,
					-2.173913043478251
				],
				[
					29.565217391304373,
					-2.173913043478251
				],
				[
					35.21739130434776,
					-2.173913043478251
				],
				[
					39.999999999999886,
					-1.7391304347825098
				],
				[
					44.3478260869565,
					-1.7391304347825098
				],
				[
					48.26086956521738,
					-1.7391304347825098
				],
				[
					51.73913043478251,
					-2.173913043478251
				],
				[
					55.652173913043384,
					-2.6086956521738784
				],
				[
					59.56521739130426,
					-3.043478260869506
				],
				[
					63.043478260869506,
					-3.043478260869506
				],
				[
					66.08695652173907,
					-3.043478260869506
				],
				[
					68.695652173913,
					-3.043478260869506
				],
				[
					69.99999999999994,
					-3.043478260869506
				],
				[
					71.73913043478251,
					-3.043478260869506
				],
				[
					72.60869565217388,
					-3.043478260869506
				],
				[
					74.34782608695645,
					-3.478260869565247
				],
				[
					76.08695652173907,
					-3.478260869565247
				],
				[
					78.26086956521732,
					-3.478260869565247
				],
				[
					80.43478260869557,
					-3.478260869565247
				],
				[
					82.1739130434782,
					-3.478260869565247
				],
				[
					83.47826086956513,
					-3.478260869565247
				],
				[
					84.78260869565213,
					-3.043478260869506
				],
				[
					86.5217391304347,
					-3.043478260869506
				],
				[
					88.26086956521732,
					-3.043478260869506
				],
				[
					89.56521739130426,
					-2.6086956521738784
				],
				[
					90.43478260869557,
					-2.6086956521738784
				],
				[
					90.86956521739125,
					-2.6086956521738784
				],
				[
					91.30434782608688,
					-2.173913043478251
				],
				[
					92.60869565217382,
					-2.173913043478251
				],
				[
					93.91304347826076,
					-2.173913043478251
				],
				[
					94.34782608695645,
					-2.173913043478251
				],
				[
					94.78260869565213,
					-2.6086956521738784
				],
				[
					94.78260869565213,
					-2.6086956521738784
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 53,
			"versionNonce": 355883440,
			"isDeleted": false,
			"id": "DIbyVEfMDW0N5CfH-cl5k",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -976.7567287784666,
			"y": 424.4812031318627,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 89.99999999999989,
			"height": 3.478260869565247,
			"seed": 1093350065,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477224,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.43478260869562746,
					-0.4347826086955138
				],
				[
					-0.43478260869562746,
					-0.8695652173912549
				],
				[
					0.43478260869562746,
					-0.8695652173912549
				],
				[
					1.7391304347826235,
					-0.8695652173912549
				],
				[
					3.0434782608696196,
					-0.8695652173912549
				],
				[
					4.782608695652129,
					-0.4347826086955138
				],
				[
					7.391304347826122,
					-0.4347826086955138
				],
				[
					10.869565217391255,
					-0.4347826086955138
				],
				[
					14.78260869565213,
					0
				],
				[
					19.13043478260863,
					0.43478260869574115
				],
				[
					23.478260869565133,
					0.43478260869574115
				],
				[
					27.39130434782612,
					0.43478260869574115
				],
				[
					31.739130434782624,
					0
				],
				[
					36.086956521739125,
					-0.4347826086955138
				],
				[
					40,
					-1.3043478260868824
				],
				[
					43.043478260869506,
					-1.7391304347825098
				],
				[
					48.26086956521738,
					-2.173913043478251
				],
				[
					52.17391304347825,
					-2.173913043478251
				],
				[
					54.78260869565213,
					-2.173913043478251
				],
				[
					58.695652173913004,
					-2.6086956521737648
				],
				[
					60.869565217391255,
					-2.6086956521737648
				],
				[
					62.60869565217388,
					-2.6086956521737648
				],
				[
					63.91304347826082,
					-2.173913043478251
				],
				[
					66.08695652173907,
					-2.173913043478251
				],
				[
					66.95652173913038,
					-2.6086956521737648
				],
				[
					68.695652173913,
					-2.6086956521737648
				],
				[
					71.73913043478257,
					-2.6086956521737648
				],
				[
					73.47826086956513,
					-2.6086956521737648
				],
				[
					76.08695652173907,
					-2.6086956521737648
				],
				[
					79.13043478260863,
					-2.6086956521737648
				],
				[
					80.43478260869563,
					-3.043478260869506
				],
				[
					81.73913043478251,
					-3.043478260869506
				],
				[
					82.60869565217388,
					-3.043478260869506
				],
				[
					83.0434782608695,
					-2.6086956521737648
				],
				[
					83.47826086956513,
					-2.6086956521737648
				],
				[
					83.91304347826082,
					-2.173913043478251
				],
				[
					84.78260869565213,
					-1.7391304347825098
				],
				[
					86.52173913043475,
					-1.3043478260868824
				],
				[
					88.26086956521732,
					-1.3043478260868824
				],
				[
					89.56521739130426,
					-1.3043478260868824
				],
				[
					89.56521739130426,
					-1.7391304347825098
				],
				[
					88.26086956521732,
					-2.173913043478251
				],
				[
					88.26086956521732,
					-2.173913043478251
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 53,
			"versionNonce": 1127831888,
			"isDeleted": false,
			"id": "5rSW4eJoL2-dNJO4bQt3D",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -972.8436853002058,
			"y": 473.61163791447143,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 126.52173913043481,
			"height": 4.782608695652243,
			"seed": 542854367,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477225,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					-0.43478260869574115,
					0.43478260869574115
				],
				[
					-0.43478260869574115,
					0.8695652173912549
				],
				[
					0,
					0.8695652173912549
				],
				[
					1.304347826086996,
					0.8695652173912549
				],
				[
					3.478260869565247,
					0.8695652173912549
				],
				[
					6.0869565217391255,
					0.8695652173912549
				],
				[
					9.565217391304259,
					0.8695652173912549
				],
				[
					14.78260869565213,
					1.304347826086996
				],
				[
					20,
					1.304347826086996
				],
				[
					25.217391304347757,
					1.304347826086996
				],
				[
					30,
					1.7391304347826235
				],
				[
					33.47826086956525,
					1.7391304347826235
				],
				[
					43.47826086956513,
					1.304347826086996
				],
				[
					49.56521739130426,
					1.7391304347826235
				],
				[
					54.3478260869565,
					2.173913043478251
				],
				[
					59.99999999999994,
					2.173913043478251
				],
				[
					64.78260869565213,
					2.608695652173992
				],
				[
					85.65217391304338,
					2.608695652173992
				],
				[
					86.95652173913038,
					2.608695652173992
				],
				[
					89.99999999999994,
					2.173913043478251
				],
				[
					92.60869565217382,
					2.173913043478251
				],
				[
					94.78260869565213,
					2.608695652173992
				],
				[
					96.5217391304347,
					2.608695652173992
				],
				[
					98.26086956521732,
					3.043478260869506
				],
				[
					100.43478260869557,
					3.043478260869506
				],
				[
					103.0434782608695,
					3.478260869565247
				],
				[
					104.78260869565207,
					3.478260869565247
				],
				[
					106.95652173913038,
					3.043478260869506
				],
				[
					108.69565217391295,
					3.478260869565247
				],
				[
					110.43478260869557,
					3.478260869565247
				],
				[
					112.1739130434782,
					3.9130434782608745
				],
				[
					114.34782608695645,
					3.9130434782608745
				],
				[
					116.08695652173907,
					3.9130434782608745
				],
				[
					117.39130434782601,
					4.347826086956502
				],
				[
					118.69565217391295,
					4.347826086956502
				],
				[
					120.43478260869557,
					4.347826086956502
				],
				[
					121.73913043478251,
					4.347826086956502
				],
				[
					123.91304347826076,
					4.347826086956502
				],
				[
					125.21739130434776,
					4.347826086956502
				],
				[
					126.08695652173907,
					4.347826086956502
				],
				[
					125.65217391304338,
					4.782608695652243
				],
				[
					123.91304347826076,
					4.782608695652243
				],
				[
					123.91304347826076,
					4.782608695652243
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		},
		{
			"type": "freedraw",
			"version": 51,
			"versionNonce": 715105200,
			"isDeleted": false,
			"id": "K474OyFZ5rMSCsaR0TfrE",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 30,
			"angle": 0,
			"x": -984.1480331262931,
			"y": 325.35076834925417,
			"strokeColor": "#c92a2a",
			"backgroundColor": "transparent",
			"width": 85.21739130434781,
			"height": 3.4782608695651334,
			"seed": 527676561,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"boundElements": [],
			"updated": 1646943477225,
			"link": null,
			"points": [
				[
					0,
					0
				],
				[
					2.6086956521738784,
					-2.173913043478251
				],
				[
					3.478260869565247,
					-2.6086956521738784
				],
				[
					25.652173913043498,
					-0.43478260869562746
				],
				[
					26.95652173913038,
					0
				],
				[
					29.565217391304373,
					0
				],
				[
					32.60869565217388,
					0.43478260869562746
				],
				[
					35.6521739130435,
					0.43478260869562746
				],
				[
					38.26086956521738,
					0.8695652173912549
				],
				[
					40,
					0.8695652173912549
				],
				[
					41.73913043478262,
					0.8695652173912549
				],
				[
					43.04347826086962,
					0.8695652173912549
				],
				[
					44.78260869565213,
					0.8695652173912549
				],
				[
					46.52173913043475,
					0.8695652173912549
				],
				[
					48.695652173913004,
					0.43478260869562746
				],
				[
					50,
					0.43478260869562746
				],
				[
					51.73913043478262,
					0.43478260869562746
				],
				[
					53.04347826086962,
					0
				],
				[
					55.21739130434787,
					0
				],
				[
					57.391304347826065,
					-0.43478260869562746
				],
				[
					60,
					-0.8695652173912549
				],
				[
					62.17391304347825,
					-0.8695652173912549
				],
				[
					64.3478260869565,
					-1.304347826086996
				],
				[
					65.6521739130435,
					-1.304347826086996
				],
				[
					67.39130434782606,
					-1.304347826086996
				],
				[
					69.13043478260869,
					-1.304347826086996
				],
				[
					71.30434782608694,
					-1.7391304347826235
				],
				[
					73.47826086956519,
					-1.7391304347826235
				],
				[
					75.65217391304344,
					-1.7391304347826235
				],
				[
					77.39130434782606,
					-1.7391304347826235
				],
				[
					78.26086956521738,
					-1.7391304347826235
				],
				[
					79.13043478260869,
					-1.7391304347826235
				],
				[
					80.43478260869563,
					-1.7391304347826235
				],
				[
					81.30434782608694,
					-1.304347826086996
				],
				[
					82.60869565217388,
					-1.304347826086996
				],
				[
					83.47826086956519,
					-1.304347826086996
				],
				[
					83.91304347826082,
					-1.304347826086996
				],
				[
					84.78260869565213,
					-0.8695652173912549
				],
				[
					85.21739130434781,
					-0.8695652173912549
				],
				[
					83.91304347826082,
					-0.8695652173912549
				],
				[
					82.17391304347825,
					-1.304347826086996
				],
				[
					82.17391304347825,
					-1.304347826086996
				]
			],
			"lastCommittedPoint": null,
			"simulatePressure": true,
			"pressures": []
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "transparent",
		"currentItemFillStyle": "hachure",
		"currentItemStrokeWidth": 1,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%