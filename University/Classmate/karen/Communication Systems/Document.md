# Routers Ciudades
![[Pasted image 20220531063019.png]]
Apagamos y conectamos 2 WIC-2T, y luego volvemos a encenderlo.
![[Pasted image 20220531063111.png]]
Y asi para los cuatros routers. 

![[Pasted image 20220531063307.png]]
Luego conectamos desde Bello a Itagui desde serial (0/0/0 hasta 0/0/1), respectivamente. Etiquetado con consolas.

![[Pasted image 20220531081409.png]]

## Bello
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Bello       | serial 0/0/0 | 10.10.10.1   | 255.255.255.0     | 10.10.10.0          |
| Bello       | serial 0/0/1 | 10.10.13.1   | 255.255.255.0     | 10.10.11.0          |
| Bello       | serial 0/1/0 | 10.10.14.1   | 255.255.255.0     | 10.10.14.0          |
|             |              |              |                   |                     |
Password: 1111

```cisco
Router> enable

Router#configure terminal

Enter configuration commands, one per line. End with CNTL/Z.

Router(config)#hostname Bello

Bello(config)#enable secret 1111

Bello(config)#enable password 1111

The enable password you have chosen is the same as your enable secret.

This is not recommended. Re-enter the enable password.

Bello(config)#line con 0

Bello(config-line)#password 1111

Bello(config-line)#login

Bello(config-line)#exit

Bello(config-if)#interface serial 0/0/0

Bello(config-if)#ip address 10.10.10.1 255.255.255.0

Bello(config-if)#no shutdown

Bello(config-if)#exit

Bello(config-if)#interface serial 0/0/1

Bello(config-if)#ip address 10.10.10.1 255.255.255.0

Bello(config-if)#no shutdown

Bello(config-if)#exit

Bello(config-if)#interface serial 0/1/0

Bello(config-if)#ip address 10.10.14.1 255.255.255.0

Bello(config-if)#no shutdown

Bello(config-if)#exit

Bello(config-if)#router rip

Bello(config-router)#version 2

Bello(config-router)#no auto-summary

Bello(config-router)#network 10.0.0.0

Bello(config-router)#network 130.120.0.0

Bello#copy running-config startup-config

```

Y asi para las siguientes Ciudades.

## Itagui
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Itagui      | serial 0/0/0 | 10.10.11.1   | 255.255.255.0     | 10.10.10.0          |
| Itagui      | serial 0/0/1 | 10.10.10.2   | 255.255.255.0     | 10.10.11.0          |
| Itagui      | serial 0/1/0 | 10.10.15.1   | 255.255.255.0     | 10.10.14.0          |
| Itagui      | serial 0/1/1 | 10.10.17.1   | 255.255.255.0     | 10.10.17.0          |
|             |              |              |                   |                     |
Password: 2222

## Envigado
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Envigado      | serial 0/0/0 | 10.10.12.1   | 255.255.255.0     | 10.10.10.0          |
| Envigado      | serial 0/0/1 | 10.10.11.2   | 255.255.255.0     | 10.10.11.0          |
| Envigado    | serial 0/1/0 | 10.10.18.1   | 255.255.255.0     | 10.10.14.0          |


Password: 3333

## Medellin
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Medellin    | serial 0/0/0 | 10.10.13.1   | 255.255.255.0     | 10.10.10.0          |
| Medellin    | serial 0/0/1 | 10.10.12.2   | 255.255.255.0     | 10.10.11.0          |
| Medellin    | serial 0/1/0 | 10.10.19.1   | 255.255.255.0     | 10.10.14.0          |
| Medellin    | serial 0/1/1 | 10.10.20.1   | 255.255.255.0     | 10.10.20.0           | 


Password: 4444

## Sucursales
## Koaj
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Koaj        | serial 0/0/0 | 10.10.14.2   | 255.255.255.0     | 10.10.10.0          |
| Koaj        | fa 0/0       | 10.10.12.2   | 255.255.255.0     | 10.10.11.0          |
password: 5555

## Adidas
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Adidas        | serial 0/0/0 | 10.10.15.2   | 255.255.255.0     | 10.10.10.0          |
| Adidas        | fa 0/0       | 10.10.12.2   | 255.255.255.0     | 10.10.11.0          |
| Adidas        | serial 0/0/1 | 10.10.16.1   | 255.255.255.0     | 10.10.16.0                    |
password: 6666
## Polo
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Polo        | serial 0/0/0 | 10.10.15.2   | 255.255.255.0     | 10.10.10.0          |
| Polo        | fa 0/0       | 10.10.12.2   | 255.255.255.0     | 10.10.11.0          |
| Polo        | serial 0/0/1 | 10.10.16.1   | 255.255.255.0     | 10.10.16.0                    |
password: 7777

## Arturo Calle
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Arturo Calle        | serial 0/0/1 | 10.10.18.2   | 255.255.255.0     | 10.10.10.0          |

password: 8888


## Koaj Basic
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Koaj Basic        | serial 0/0/0 | 10.10.19.2   | 255.255.255.0     | 10.10.19.0          |
| Koaj Basic        | serial 0/0/1 | 10.10.21.1   | 255.255.255.0     | 10.10.10.0          |
password: 9999


## Patprimo
| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Patprimo        | serial 0/0/0 | 10.10.20.2   | 255.255.255.0     | 10.10.19.0          |
| Patprimo        | serial 0/0/1 | 10.10.21.2   | 255.255.255.0     | 10.10.10.0          |
password: 1999


![[Pasted image 20220531152206.png]]


# Subredes









