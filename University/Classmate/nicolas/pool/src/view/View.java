package view;

import controller.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Date;

public class View extends JFrame{
	private final String app_name = "Pool";
	private int width;
	private int height;

	private JFrame jframe; 
	private JPanel panel;

	public View(int width, int height, JPanel panel) {  
		this.width = width;
		this.height = height;
		this.panel =  panel;

		// JFrame is the Windows
		setSize(width, height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setResizable(true); 

		//Fill grid with userlogin
		add(panel);
	}  


	public void setPanel(JPanel panel){ 
		getContentPane().removeAll();
		repaint();
		add(panel); 
		revalidate();
		repaint();
	}
	
}
