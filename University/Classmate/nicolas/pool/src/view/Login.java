package view;

import controller.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Login extends JPanel implements ActionListener {
	private int width;
	private int height;
	private User user;
	private JPanel grid_items;
	private JTextField username_field;
	private JTextField password_field;

	// Buttons
	JButton login;

	public Login(int width, int height) {
		user = new Controller();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setSize(width, height);

		grid_items = new JPanel();
		grid_items.setLayout(new GridLayout(5, 1, 5, 5));
		fillLogin();
	}

	public void fillLogin() {
		// Grid panel structure
		// Image
		Img img = new Img("./img/unknown.jpg", 50, 50);
		img.setAlignmentX(Component.CENTER_ALIGNMENT);
		add(img);

		// username :labea
		Label username_label = new Label("Username");
		grid_items.add(username_label, CENTER_ALIGNMENT);

		// username_field : textfield
		username_field = new JTextField("Delete this");
		grid_items.add(username_field);
		// password :label
		Label password_label = new Label("password");
		grid_items.add(password_label);
		// password_field : textfield
		password_field = new JTextField("Delete this");
		grid_items.add(password_field);

		// Button Login
		// If user don't sign up, will pass to register
		login = new JButton("Login");
		login.addActionListener(this);
		grid_items.add(login);
		add(grid_items);

		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

	}

	// Action event: buttons, text field and more
	public void actionPerformed(ActionEvent e) {
		View jf = (View) SwingUtilities.getRoot((Component) e.getSource());

		if (e.getSource() == login) {
			if (user.check(username_field.getText(), password_field.getText())) {
				jf.setPanel(new Dashboard(width, height, username_field.getText(), username_field.getText()));
			} else {
				jf.setPanel(new SignUp(width, height));
			}
		} else {

		}
	}

}
