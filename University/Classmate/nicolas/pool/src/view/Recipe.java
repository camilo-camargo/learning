package view;
import controller.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Recipe extends JFrame implements ActionListener{  

	private JButton print;
	private JTextField tickets ; 
	private double value; 
	private UserCard userCard;  
	private JPanel items;
	private JLabel label_value;  
	private JLabel q; 


	public Recipe(String title, String description, double value, String fullname){  
		this.value =  value;
		userCard = new Controller(); 
		items = new JPanel();
		items.setLayout(new BoxLayout(items, BoxLayout.Y_AXIS));

		setSize(200,180); 
		setVisible(true);  
		setResizable(false);

		JLabel label_title_title = new JLabel("Recipe");

		JLabel label_title  = new JLabel(title);  

		JLabel label_description = new JLabel(description);
		label_value = new JLabel(value + ""); 
		JLabel label_fullname = new JLabel(fullname);


		items.add(label_title_title);
		items.add(label_title);
		items.add(label_fullname);
		items.add(label_description);
		items.add(label_value);
 
		tickets = new JTextField();

		q = new JLabel("More tickets? how many?"); 
		add(q);
		items.add(tickets); 
 

		print = new JButton("Print");
		items.add(print);  
		print.addActionListener(this);

		items.setBorder(BorderFactory.createEmptyBorder(20,20,20,20));
		add(items);
	}   

	public void actionPerformed(ActionEvent e){ 
		if(e.getSource() == print){
			value = userCard.ticket(Integer.parseInt(tickets.getText()), value);  
			label_value.setText(value + "");	 

			items.remove(q);
			items.remove(print); 
			items.remove(tickets);
			items.revalidate();
			items.repaint();
		}else{

		}
	}
}
