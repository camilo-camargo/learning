package view;
	
import controller.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import org.w3c.dom.events.MouseEvent;
 
public class Dashboard extends JPanel{  
	private int width;
	private int height; 
	private String username;
	private JButton next;  
	private String fullname;

	//Buttons
	JButton login;	

	public Dashboard(int width, int height, String username, String fullname){        
		this.width = width;
		this.height = height; 
		this.username = username;  
		this.fullname = fullname;
		setLayout(new GridLayout(3,1, 100,50)); 
		add(new Bar(width, 100, username));
	
		JPanel grid_cards = new JPanel();
		grid_cards.setLayout(new GridLayout(1,3, 100, 50)); 

		Card blue = new Card(300, 100, "Azul", "Las Piscinas", 10000, fullname);
	 	Card yellow = new Card(300, 100, "Amarillo", "Las Piscinas + Refrijerio", 15000, fullname);
		Card red = new Card(300, 100, "Rojo", "Las Piscinas +  Masajes  + Refrigerio", 20000, fullname);
		grid_cards.add(blue); 

		grid_cards.add(yellow);
		grid_cards.add(red);

		add(grid_cards);  

		setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

	}  

}
