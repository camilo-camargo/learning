package view;

import controller.*;
import java.awt.*;
import java.awt.event.*;
import java.nio.file.attribute.UserPrincipal;

import javax.swing.*;


public class SignUp extends JPanel  
	implements ActionListener{
	private int width;
	private int height; 
	private JPanel grid_items;
	private User user;

	private JTextField fullname_field ;
	private JTextField id_field;
	private JTextField username_field ;
	private JTextField password_field  ;



	//Buttons
	private JButton register;

	public SignUp(int width, int height){  
		user = new Controller();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setSize(width, height);


		grid_items = new JPanel();
		grid_items.setLayout(new GridLayout(10,1)); 
		fillSignUp();
	}   

	public void fillSignUp(){
		//Structure
		//image
		JLabel img = new JLabel(); 
		img.setIcon(new ImageIcon("./img/unknown.jpg")); 
		img.setPreferredSize(new Dimension(100, 100));
		add(img);
	
		//fullname : label
		Label fullname_label = new Label("Full Name");
		grid_items.add(fullname_label);

		//fullname_fill: textlabel
		fullname_field = new JTextField("fullname ...");
		grid_items.add(fullname_field);
	
				//id :label
		Label id_label = new Label("ID");
		grid_items.add(id_label);

		//id_fill: textlabel	
		id_field = new JTextField("ID ...");
		grid_items.add(id_field);

		// username :label
		Label username_label = new Label("Username");
		grid_items.add(username_label);

		// username_field : textfield
		username_field = new JTextField("username");
		grid_items.add(username_field);
		// password :label
		Label password_label = new Label("password");
		grid_items.add(password_label);
		// password_field : textfield
		password_field = new JTextField("********");
		grid_items.add(password_field); 

		//Button Login
		//If user don't sign up, will pass to register 
		register  = new JButton("Register");
		grid_items.add(register);
		setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

		register.addActionListener(this);

		add(grid_items); 
		
	}

	// Action event: buttons, text field and more
	public void actionPerformed(ActionEvent e) {
		View jf = (View) SwingUtilities.getRoot((Component) e.getSource());  
		if (e.getSource() == register) { 
				if (!user.check(username_field.getText(), password_field.getText())) {
					user.save(username_field.getText(), password_field.getText());  
					jf.setPanel(new Dashboard(width, height, username_field.getText(),fullname_field.getText()));
				}else{
					jf.setPanel(new Login(width, height));
				}
		} else {

		}
	} 

}
