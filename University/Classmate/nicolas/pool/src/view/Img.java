package view;

import controller.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Img extends JLabel{ 
	private String path; 
	private int width; 
	private int height;
	public Img(String path, int width, int height){
		this.path = path;
		setIcon(new ImageIcon(path));  
		setPreferredSize(new Dimension(width, height ));
	}
}
