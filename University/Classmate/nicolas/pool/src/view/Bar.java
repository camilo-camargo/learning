package view; 

import controller.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*; 
import java.util.Date;
import java.text.*;


public class Bar extends JPanel implements ActionListener{
	private int width;
	private int height;
	private String username; 
	private JPanel grid_items;
	public Bar(int width, int height, String username){ 
		this.width = width;
		this.height = height;
		this.username = username;

		setSize(width, height); 

		fillBar();
	}   

	public void fillBar(){
		//structure
		//Username 
		JLabel username_label = new JLabel("Username: "+username);

		//Date
		JLabel date_label = new JLabel(date());

		grid_items = new JPanel();
		grid_items.setLayout(new GridLayout(2,1)); 
		grid_items.add(username_label);
		grid_items.setPreferredSize(new Dimension(width,height));
		grid_items.add(date_label); 

		//image
		Img img = new Img("./img/unknown.jpg", 100, 100);
		add(grid_items);
		add(img);
	}	 

	public String date(){
		    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			 return formatter.format(new Date());
	}

	// Action event: buttons, text field and more
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == null) {

		} else {

		}
	} 



}
