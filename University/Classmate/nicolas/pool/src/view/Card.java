package view;
import controller.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.CompoundBorder;


public class Card extends JPanel{   
	private int width;
	private int height; 
	private String title; 
	private String description;
	private double value;
	private double totalValue; 
	private String fullname;


	public Card(int width, int height, String title, String description, double value, String fullname){ 
		this.width = width;
		this.height = height;	 
		this.title = title; 
		this.description = description; 
		this.value = value;
		this.fullname = fullname;

		setLayout(new GridLayout(3,1));
		setSize(width, height); 
		setPreferredSize(new Dimension(width, height)); 
		setAlignmentY(java.awt.Component.CENTER_ALIGNMENT);
		fillCards(); 

	}    

	public void fillCards(){
		//structure 
		//Title : label
		JLabel title_label = new JLabel(title); 
		add(title_label);
		title_label.setHorizontalAlignment(title_label.CENTER);
		//Description : label
		JLabel description_label = new JLabel(description);
		add(description_label);
		description_label.setHorizontalAlignment(description_label.CENTER);
		//Value	: button 
		JLabel buy = new JLabel(value + ""); 
		buy.setHorizontalAlignment(buy.CENTER); 


		setBorder(BorderFactory.createCompoundBorder(BorderFactory.createEmptyBorder(20,20,20,20), BorderFactory.createLineBorder(Color.RED)));
		add(buy); 
	
		addMouseListener(new MouseAdapter() {
			@Override
				public void mouseClicked(MouseEvent e) {
	  				new Recipe(title, description, value, fullname);
			 }
		});

	} 

	public void setBorder(BorderFactory border){
		setBorder(border);	
	}   
}
