package controller;
import view.*; 
import model.*;

public class Controller implements User, UserCard{  
	private View view; 
	private SignUp signUp;  
	private UserLogin userLogin; 
	private Ticket ticket;

	private String fullname;
	private String username; 

	public Controller(){
		userLogin = new UserLogin();
		ticket = new Ticket();
	} 

	public void init(){  

		int width = 1000;
		int height = (int)(width / (16.0/9.0));

		Login login = new Login(width, height); 
		view = new View(width, height, login); //Start the JFrame
	}  

	@Override
	public boolean save(String username, String password){
		boolean req = userLogin.save(username, password); 
		this.username = username;
		return req;
	} 

	@Override
   public boolean check(String username, String password){
		boolean req = userLogin.check(username, password );
		this.username = username;
		return req;	
	} 

	@Override
	public double ticket(int amount, double value){
		return ticket.amount(amount, value);				
	}

}
