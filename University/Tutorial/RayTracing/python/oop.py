
from multipledispatch import dispatch
class Animal:
    __animal_list = []
    __animal_list_index = 0 

    def __init__(self): 
        pass

    def __animal__(self, type, name, age): 
        self.__type = type 
        self.__name = name
        self.__age = age

    def speak():
        pass
 
    def __str__(self):
        return(f"Animal: {self.__type}  name: {self.__name}  age: {self.__age}")  

    def __iter__(self):
        return self 

    def __next__(self):
        if(self.__animal_list_index == len(self.__animal_list)): 
            self.__animal_list_index = 0
            raise StopIteration 
        ret = self.__animal_list[self.__animal_list_index]
        self.__animal_list_index = self.__animal_list_index + 1 # ++i
        return ret 

    def append(self, animal):
        self.__animal_list.append(animal)

class Dog(Animal):
    def __init__(self, name, age):
        Animal.__animal__(self, "Dog", name, age)

    def speak(self):
        print("Guau Guau")


class Cat(Animal): 
    def __init__(self, name, age): 
        Animal.__animal__(self, "Cat", name, age) 

    @dispatch()
    def speak(self):
        print("Miau Miau")

    @dispatch(int)
    def speak(self, times):
        print("Miau Miau\n"*times)

animals = Animal() 
mickey = Cat("Mickey", 10)
max    = Dog("Max", 11)
animals.append(mickey)
animals.append(max)
for i in animals:
    print(i)

