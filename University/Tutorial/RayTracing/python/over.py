from functools import singledispatch

@singledispatch
def fun(arg, verbose=False):
    if verbose:
        print("Let me just say, ",end=" ")
    print(arg) 

@fun.register(list) 
def _(arg, verbose=False):
    if verbose:
        print("Enumerate this: ")
    for i, elem in enumerate(arg):
        print(i, elem)

@fun.register(int)
def _(arg, verbose=False):
    if verbose: 
        print("Strenght in numbers, eh?: ")
    print(arg) 


if(__name__ == "__main__"): 
    fun(1, verbose=True)
