import math 
from lerp import *

def clamp( value, min, max):
        if (value < min):
            return min
        elif(value > max):
            return max
        return value

def write(file, red, green, blue):
        """ Write the next color pixels until exhausted """
        min = 0
        max = 1

        red = math.floor(clamp(red, min, max) * 255)
        green = math.floor(clamp(green, min, max) * 255)
        blue = math.floor(clamp(blue, min, max) * 255)

        file.write("{0} {1} {2}\n".format(red, green, blue))

def main():
    ppm_file = open("./lerp.ppm", "w+")  
    width = 1280
    height = math.floor(width / (16/9))
    ppm_file.write(f"P3\n{width} {height}\n255\n") 
    for y in reversed(range(height)):
        for x in range(width):
            u = (y / (height-1))
            v = x / (width -1)  
            print(lerp(1,0,v))
            write(ppm_file, lerp(1,0, v), 0, 0) 
    ppm_file.close()

if(__name__ == "__main__"): 
    main()
