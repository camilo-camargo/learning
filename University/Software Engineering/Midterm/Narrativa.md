Se desea construir un sistema de gestión de ventas para comercios. El sistema constará de una base de datos en la que, entre otras cosas, se **almacena** la información del **inventario de productos**  (código de producto, descripción, precio de adquisición, precio de venta, tipo de IVA, cantidad en el  almacén).   El sistema dispondrá de una serie de TPVs (terminales de punto venta), en cada uno de los cuales podrá haber un **empleado** que lleva a cabo las **sesiones de compra**. El empleado se identificará ante el TPV tecleando su código de empleado. El sistema no debe dejar que un mismo empleado esté identificado en más de un TPV simultáneamente. Del mismo modo, la sesión del empleado se cierra  automáticamente a los 20 minutos de inactividad.  

El proceso de compra es el siguiente: un cliente llega a la caja y el empleado va pasando el código  de barras de cada producto por el escáner (o bien lo teclea). Si compra más de una unidad de un producto, el empleado podrá escribir el número de unidades. Cuando se cierra la sesión de venta,  se restarán del inventario el número de unidades vendido y se generará un tique, que tendrá un número, la fecha y hora y el nombre del empleado que ha realizado la venta. La sesión de venta se cierra cuando el cliente ha pagado bien en metálico, bien con tarjeta. Los pagos con tarjeta se  realizan utilizando un servicio remoto que se especificará más adelante. De cada sesión de compra  se almacena también la hora de inicio y la hora de fin.  
El sistema debe ser capaz de **gestionar las devoluciones** de productos: para ello, el cliente se acerca  a un TPV con el producto y el tique, el empleado escribe el número de tique, selecciona de éste el  producto y lleva a cabo la devolución, lo que no significa que se incremente el número de unidades  del producto. En el tique “lógico” (es decir, el almacenado en la base de datos) debe hacerse constar  la devolución.  
El sistema permitirá a los jefes de tienda la modificación de los precios de venta de los productos.  El sistema será también capaz de gestionar ofertas tipo nxm o n+m, que deben ser adecuadamente  
representadas en el tique de compra:  
• nxm: por ejemplo, 3x2 (compra tres unidades de un producto y paga dos).  
	• n+m: por ejemplo, 2+1 (compra dos unidades de un producto y te regalamos una unidad  de otro).9}  
Las ofertas las crean los jefes de tienda.  


A partir del caso de estudio planteado, identificar:  
• Requisitos Funcionales  
• Elaborar Diagrama de contexto  
• Casos de uso  
• Formular ejemplos para relaciones de inclusión, extensión y generalización de existir.

Requisitos funcionales:
RF1: El sistema permitira almacenar informacion del inventario de productos.
RF2: El sistema permitira identificar el inicion de sesion empleado.
RF3: El sistema permitira restringir el inicio de sesion de un empleado.
RF4: El sistema permitira cerrar sesion de un empleado
RF5: El sistema permitira actualizar informacion del inventario de productos.
RF6: El sistema permitira generar un tique.
RF7: El sistema permitira crear una devolucion. 
RF8: El sistema permitira actualizar una devolucion.
RF9: El sistema permitira eliminar una devolucion.
RF10: El sistema permitira modificar productos de venta. 
RF11: El sistema permitira crear ofertas.
RF12: El sistema permitira actualizar ofertas. 
RF13: El sistema permitira modificar ofertas.
RF14: El sistema permitira eliminar ofertas.