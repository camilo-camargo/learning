### Incremental Process Models
It combines the linear and parallel process flows., but is produce increments by evolutionary process flow. Each increment is a new release of the software. 

Core Product: the first released. 

The loop is finished when the product is produced. 
Constraints
1. Initial software requirements are reasonably well-defined.
Uses
1. When staffing is unavailable for a complete implementation. 
2. Early increments can be implemented with fewer people. 
Note
Incremental philosophy is called Agile.