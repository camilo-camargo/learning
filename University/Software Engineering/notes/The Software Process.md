# Process Models
Camilo Andres Camargo Castaneda
**What is it?**
It is a predictable road map.
In the book, software process is a framework of *activities, actions, and tasks.*
**Why is it important?**
It provides controlled, stability, and organization.
**What is the work product?**
Are (programs, documents, and data) defining by the processes.
**How do I ensure that I've done it right?**
If I choose good process, the outcomes are quality, timeliness, and long-term viability of the product.

## Generic Process Model
Activities ↔ Actions ↔ Task 

Framework Activities
* Communication
* Planning
* Modeling
* Construction
* Deployment
## Process flow
How the activities, tasks and actions, are organized. 
### Linear Process Flow
Sequential process of the five framework.
![[Pasted image 20220405094238.png]]
### Interactive Process Flow
Repeat more than one process before to start with another.
![[Pasted image 20220405094336.png]]
### Evolutionary Process Flow
Execute the process circularly. 
![[Pasted image 20220405094433.png]]


### Parallel Process Flow
Executes one activity with another at the same time.
![[Pasted image 20220405095143.png]]

Different projects have different set of task
#### example.
##### One stakeholder
* Contact
* Discuss Requirements
* Organize notes
* Approval 
##### More Stakeholders
* Inception
* Elicitation
* Elaboration
* Negotiation
* Specification 
* Validation

### Task Set
Means the actual work to be done to accomplish the actions of a software engineering action. 
Example
```text
Simple Project){
	(Communication)(Activity){
	(Elicitation)(Action){
		(Task set){
			1. List of Stakeholders
			2. Informal meeting stakeholders
			3. Ask each stakeholders (features and functions required)
			4. Discuss and create final list
			5. Prioritize requirements
			6. Note areas of uncertainty
		}
	}
	}
}
Complex Project){
	(Communication)(Activity){
	(Elicitation)(Action){
		(Task set){
			1. List of Stakeholders
			2. Informal meeting with each stakeholders
			3. Build a  preliminary list based on stakeholders
			4. Schedule a series of meetings
			5. Conduct meeting
			6. Informal user scenarios as part of each meeting
			7. Redefine scenarios based on stakeholders
			8. Build stakeholder requeriments
			9. Use quality function deployment techiques
			10. Pacakege requirements
			11. Note constraints and restrictions
			12. Discuss methods
	}
	}
}
```


## Process Patterns
It is to resolve problems in a quick manner and construct a process.

Template of Ambler
* Pattern Name **TechnicalReviews**
* Forces 
* Type
	* Stage Pattern **EstablishingCommunication**
	* Task Pattern **RequiremetsGathering**
	* Phase Pattern **SpiralModel** or **Prototyping**
* Initial Context

## Process Assessments And Improvement
Assessment attempts to understand the current state of the software process with the intent of improving it.
### Standard CMMI Assessment Method for Process Improvement
Five steps with five  phases. 
* Initiating
* Diagnostic
* Establishing
* Acting
* Learning
### CMM-Based Appraisal for Internal Process Improvement (CBA IPI)
it provided a diagnostic technique for assessing. It is based on SEI CMM.

### SPICE (ISO/IEC 15504)
It defines a set of requirements helps to develop an objective evaluation of the efficacy of any software process.

### ISO 9001:2000 For Software
It improves quality, system or services that provides an organization. 


## Prescriptive Process Models
Prescriptive process models define a prescribed set of process elements and a predictable process work flow. it strives for structure and order.

The edge of chaos: a natural state between order and chaos, a grand compromise between structure and surprise.

### The Waterfall Model (Classic life cycle)
proposed by **Winston Royce**, made provision for feedback loops
When the **requirements** are **well define** and **reasonably stable**. 
![[Pasted image 20220406020713.png]]

Problems: 
1. Real projects rarely follow a linear flow. 
2. Requirements explicitly. 
3. The customer must have patience. Because the outcome software are available at the end. 

Uses: 
1. Requirements are fixed.
2. Work follows the linear manner. 

#### V-Model
A variation of  the waterfall model. 
![[Pasted image 20220406021004.png]]
It performed after code a series of test or quality assurance actions, it is associated with earlier engineering actions. 


### Incremental Process Models
It combines the linear and parallel process flows., but is produce increments by evolutionary process flow. Each increment is a new release of the software. 

Core Product: the first released. 
![[Pasted image 20220406022852.png]]
The loop is finished when the product is produced. 
Constraints
1. Initial software requirements are reasonably well-defined.
Uses
1. When staffing is unavailable for a complete implementation. 
2. Early increments can be implemented with fewer people. 
Note
Incremental philosophy is called Agile. 
### Evolutionary Process Model 
The requirements often change, with each iteration the version are more completed. 
This model is designed to accommodate a product that evolves over time. 

#### Prototyping 
When the requirements aren't well-defined or the customer don't provide functions and features. The prototyping assists you and other stakeholders when requirements are fuzzy. 

![[Pasted image 20220406024144.png]]


Problems: 
1. The stakeholders see that the product don't have all quality, and maintained, they cry foul and demand a few fixes. Hence, the software development management relents.
2. As a software engineer, you often make implementation compromises, example, the language used for prototyping is quickly choose because is known. 

#### The Spiral Model
Proposed by Barry Boehm. 
This model is a risk-driven generator that has two features: 
* Cyclic: decreasing the degree of risk
* Anchor point milestones: feasible and mutually satisfactory solutions. 

Combine prototyping and waterfall model in a controlled and systematic way.

![[Pasted image 20220406025950.png]]
This model is a variation of the original model. 

They can to apply throughout the entire life cycle of an application, from concept development to maintenance. 
Problems: 
1. Difficult to convince customers.

Page 77