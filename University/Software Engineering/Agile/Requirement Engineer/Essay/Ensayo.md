# Ingerieria de requisitos
## Introducción
Principios y desarrollos de software es fundamentalmente constituida por la ingeniería de requisitos, la cual proporciona a una organización de desarrollo la estructura base de un software para su implementación.  Sin embargo, esta estructura está sometida a cambios siendo un proceso dinámico y cíclico.   

### Tesis
Principios y desarrollo de software soluciona de manera aceptable la implementación de un software, basándose, en ingeniería de requisitos. Sin embargo, la problemática a solucionar puede variar en cada etapa de desarrollo.

## Nudo o Cuerpo

Ingeniería de requisitos parte de los requerimientos de los clientes, de esta manera la estructura es dinámica, ya que necesita una interacción constante con el usuario, de tal modo, origina flexibilidad, aunque esto no significa que sea débil. Un requerimiento es un atributo necesario en un sistema, puesto que es una necesidad del cliente, por eso un requerimiento necesita resolverse, además, un requerimiento satisface las necesidades desde la planificación y no en la fase de programación. (Ralph R) 

Dentro de los beneficios de los requerimientos en la fase de planificación que podemos mencionar, se ofrece mejor calidad de los productos, comunicación efectiva entre Ingeniero y cliente, producto moldeado a las ideas del cliente, con ello se evita el rechazo del producto, mejor control de presupuesto, ya que se evita la inversión innecesaria y corregir fallas antes de su implementación.

Los tipos de requisitos son: 
* Requisitos Funcionales 
* Requisitos No Funcionales 
* Requisitos de Usuario
* Requisitos Dominio 
* Requisitos de Sistema

Los requisitos funcionales están basados en el Acrónimo CRUD el cual significa, Create, Read, Update and delete. Además estos son tangibles y visibles para el usuario, al contrario que los requisitos no funcionales, los cuales no se basan con la funcionalidad del sistema. Además los requerimientos no funcionales poseen las siguientes características, cuantificables, restrictivos, críticos y son difíciles de identificar.

Ademas de resolver necesidades, tambien se pueden establecer requerimientos enfocados a cualidades del sistema o  en otras palabras un comportamiento adicional, determinados por el usuario y no por la funcionalidad.  Los requisitos de usuario establecen dos enfoques, los deseables y los obligatorios, cuyo propòsito es establecer criterios de condiciòn, que el usuario quire implementar, proponiendo decisiones.


Los requisitos de Dominio comprende todas las leyes que involucran en su implementacion y ejecuciòn (e.j Licencia GPL), adicionalmente los requisitos de Sistema definen los servicion que la organizaciòn en su implementaciòn desea proveer. Recapitulando lo mencionado anteriormente sobre  la intervencion del cliente, cabe resaltar el papel de los stakeholders, persona de interesa y influyente en la empresa. Para determinar estos requerimientos se utiliza las siguientes tecnicas aplicadas a los usuarios y StakeHolders son:

* Prototipado 
* Revistas 
* Casos de uso 
* Etnografía 
* Escenarios

Las tecnicas que ayudan en el analisis de requerimientos pueden ser un conjunto compuesto, en el cual la Etnografia es utilizada midiendo requerimientos asociados sociales y organizacionales. El caso de uso es una representacion visual en la cual se disponen los requerimientos, entrelazados con distintas clasificaciones de conexiones,  el cual exponen el funcionamiento funcional de el diseno. 

## Conclusión

La implementacion de un software sin un diseno previo esta sometido a pasar por momentos de caos e incentidrumbe, por tal motivo la ingenieria de requisitos establecen clasificacion  de requerimientos tendientes al cambio generados por el usuario o "stakeholders",, para solventar acertivamente problemas que pueden causar una rapida implementacion, sin llegar a asorber una gran cantidad de recursos, como el dinero y tiempo.

### Frase de conclusion
El exito de un proyecto depende de la planificacion inicial que este puede tomar antes de su implementacion, repitiendose ciclicamente. 

# Bibliografia
The Requirements Engenieer HandBook (Ralph R).