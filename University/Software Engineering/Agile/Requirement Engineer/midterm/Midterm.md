---

excalidraw-plugin: parsed

---
==⚠  Switch to EXCALIDRAW VIEW in the MORE OPTIONS menu of this document. ⚠==


# Text Elements
enviar una notificacion via email ^nfnp3IZz


# Embedded files
faf4f41f0d090dd06547f87c4495eaf1bf2a7760: [[Pasted Image 20220310103748_015.png]]

%%
# Drawing
```json
{
	"type": "excalidraw",
	"version": 2,
	"source": "https://excalidraw.com",
	"elements": [
		{
			"type": "image",
			"version": 295,
			"versionNonce": 1143194367,
			"isDeleted": false,
			"id": "8bt7YRg8jM0yTFhoSrb8K",
			"fillStyle": "hachure",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"angle": 0,
			"x": -555.4011018262661,
			"y": -338.1197344322339,
			"strokeColor": "transparent",
			"backgroundColor": "transparent",
			"width": 1300.1187500000003,
			"height": 1777.9401709401714,
			"seed": 904826001,
			"groupIds": [],
			"strokeSharpness": "round",
			"boundElements": [],
			"updated": 1646926806800,
			"link": null,
			"status": "pending",
			"fileId": "faf4f41f0d090dd06547f87c4495eaf1bf2a7760",
			"scale": [
				1,
				1
			]
		},
		{
			"id": "nfnp3IZz",
			"type": "text",
			"x": -247.84172682626564,
			"y": 453.27892246642324,
			"width": 315,
			"height": 25,
			"angle": 0,
			"strokeColor": "#000000",
			"backgroundColor": "#ced4da",
			"fillStyle": "solid",
			"strokeWidth": 2,
			"strokeStyle": "solid",
			"roughness": 1,
			"opacity": 100,
			"groupIds": [],
			"strokeSharpness": "sharp",
			"seed": 196185535,
			"version": 48,
			"versionNonce": 1870352607,
			"isDeleted": false,
			"boundElements": null,
			"updated": 1646926802760,
			"link": null,
			"text": "enviar una notificacion via email",
			"rawText": "enviar una notificacion via email",
			"fontSize": 20,
			"fontFamily": 1,
			"textAlign": "left",
			"verticalAlign": "top",
			"baseline": 18,
			"containerId": null,
			"originalText": "enviar una notificacion via email"
		}
	],
	"appState": {
		"theme": "light",
		"viewBackgroundColor": "#ffffff",
		"currentItemStrokeColor": "#000000",
		"currentItemBackgroundColor": "#ced4da",
		"currentItemFillStyle": "solid",
		"currentItemStrokeWidth": 2,
		"currentItemStrokeStyle": "solid",
		"currentItemRoughness": 1,
		"currentItemOpacity": 100,
		"currentItemFontFamily": 1,
		"currentItemFontSize": 20,
		"currentItemTextAlign": "left",
		"currentItemStrokeSharpness": "sharp",
		"currentItemStartArrowhead": null,
		"currentItemEndArrowhead": "arrow",
		"currentItemLinearStrokeSharpness": "round",
		"gridSize": null,
		"colorPalette": {}
	},
	"files": {}
}
```
%%