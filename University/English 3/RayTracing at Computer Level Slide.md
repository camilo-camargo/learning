



# Ray Tracing at Computer Level
English GATTI Second Part
##### Camilo Andres Camargo Castaneda

---
---
# Introduction
The Ray Tracing works through a **ray** coming from a source light, without a ray from source light the world are difficult to understand, because all are darkness, with one ray we can draw an only piece of an object. To simulate the reality ray process for to see different colors on the computer we need much of a ray, but the technique on the computer change the theory. 

---
--- 

# Simple Algorithm
```
Define a object with a material
Define some light sources
define a window 
for each pixel
	shot a ray towards the objects
	compute the nearest hit point
	if the ray hits an object
		compute the color by material and the light
	else
		set the pixel color to black
```
--- 
---

### Ray Tracing works
![[Pasted image 20220512173322.png]]

---

# Thanks