## Ciudades

![[Pasted image 20220531111039.png]]

```cisco
Router> enable

Router#configure terminal

Enter configuration commands, one per line. End with CNTL/Z.

Router(config)#hostname Bucaramanga

Bucaramanga(config)#enable secret 1111

Bucaramanga(config)#enable password 1111

The enable password you have chosen is the same as your enable secret.

This is not recommended. Re-enter the enable password.

Bucaramanga(config)#line con 0

Bucaramanga(config-line)#password 1111

Bucaramanga(config-line)#login

Bucaramanga(config-line)#exit

Bucaramanga(config-if)#interface serial 0/0/0

Bucaramanga(config-if)#ip address 10.10.10.1 255.255.255.0

Bucaramanga(config-if)#no shutdown

Bucaramanga(config-if)#exit

Bucaramanga(config-if)#interface serial 0/0/1

Bucaramanga(config-if)#ip address 10.10.10.1 255.255.255.0

Bucaramanga(config-if)#no shutdown

Bucaramanga(config-if)#exit

Bucaramanga(config-if)#interface serial 0/1/0

Bucaramanga(config-if)#ip address 10.10.14.1 255.255.255.0

Bucaramanga(config-if)#no shutdown

Bucaramanga(config-if)#exit

Bucaramanga(config-if)#router rip

Bucaramanga(config-router)#version 2

Bucaramanga(config-router)#no auto-summary

Bucaramanga(config-router)#network 10.0.0.0

Bucaramanga(config-router)#network 130.120.0.0

Bucaramanga#copy running-config startup-config

```




| Dispositivo | Interfaz     | Direccion IP | Mascara de subred | Gateway por defecto |
| ----------- | ------------ | ------------ | ----------------- | ------------------- |
| Bucaramanga | serial 0/0/0 | 181.40.104.1 | 255.255.255.252   | 181.40.104.0        |
| Bucaramanga | serial 0/0/1 | 181.40.104.5 | 255.255.255.252   | 181.40.104.4        |
|             |              |              |                   |                     |


## Practica
![[Pasted image 20220425105117.png]]
DNS
![[Pasted image 20220425105148.png]]

![[Pasted image 20220425105205.png]]
ROUTER WIRELESS 
![[Pasted image 20220425105232.png]]

## DHCP
![[Pasted image 20220425105431.png]]

![[Pasted image 20220425105445.png]]
MAIL
![[Pasted image 20220425105505.png]]
![[Pasted image 20220425105517.png]]
## WEB
![[Pasted image 20220425105541.png]]
![[Pasted image 20220425105622.png]]
![[Pasted image 20220425105647.png]]
### Test
Web
![[Pasted image 20220425105746.png]]
mail
![[Pasted image 20220425105825.png]]
dhcp 
![[Pasted image 20220425105849.png]]

**Los routers necesitan conectarles en modo apagado los puertos serial el cual es wic-2t**
![[Pasted image 20220330082518.png]]
### Commands basic
![[Pasted image 20220330082430.png]]
### Command basic 
![[Pasted image 20220330083059.png]]
![[Pasted image 20220330084011.png]]
![[Pasted image 20220330084214.png]]
## IP Routing Dynamic
![[Pasted image 20220403142341.png]]
![[Pasted image 20220403142525.png]]


**Los routers necesitan conectarles en modo apagado los puertos serial el cual es wic-2t**

![[Pasted image 20220404103839.png]]
![[Pasted image 20220404103324.png]]
![[Pasted image 20220404103657.png]]

Test
![[Pasted image 20220404104815.png]]


# Scheme

![[Pasted image 20220502101935.png]]

# Router
![[Pasted image 20220502094303.png]]
![[Pasted image 20220502094621.png]]
![[Pasted image 20220502094804.png]]
![[Pasted image 20220502095226.png]]
![[Pasted image 20220502100253.png]]

# IP Phone Test
![[Pasted image 20220502100533.png]]



![[Pasted image 20220522105155.png]]

![[Pasted image 20220522105213.png]]
![[Pasted image 20220522105228.png]]
![[Pasted image 20220522105241.png]]
![[Pasted image 20220522105318.png]]

# User mode
```bash
show version
```

Este commando muestra la versionde algunos componentes como tambien  las licencias.
![[Pasted image 20220316080850.png]]

```bash
show vlan
```
Este commando muestra la informacion de vlan( Virtual LAN)

![[Pasted image 20220316081046.png]]

```bash
show interfaces
```
Este commando muestra las interfaces y su configuaracion basica 
![[Pasted image 20220316081435.png]]
![[Pasted image 20220316081457.png]]
# Admin Mode
Con `enable` se ingresa en el modo admin.
```bash
clear arp-cache
```
Este commando limpia la cache de arp.

```bash
dir 
``` 
Este commando visualiza los directorios como tambies los permisos para diferentes usuarios
![[Pasted image 20220316081904.png]]
```bash
mkdir test
```
Este commando crea un nuevo directorio con nombre test
![[Pasted image 20220316084409.png]]
```bash
ping 169.254.215.77
```
Este commando prueba transferencia de datos a un host especifico
![[Pasted image 20220316084558.png]]

```bash
reload 
```
Recarga los archivos de nuevo 

# Test Packerttracer
## hostname
![[Pasted image 20220316082613.png]]
## passwords
![[Pasted image 20220316082910.png]]
## Banner
![[Pasted image 20220316083019.png]]
## Interface
![[Pasted image 20220316083519.png]]
## Save 
![[Pasted image 20220316083645.png]]
![[Pasted image 20220316083735.png]]
## Show run command 
![[Pasted image 20220316083911.png]]
![[Pasted image 20220316083928.png]]

## Testing 
![[Pasted image 20220316090304.png]]

![[Pasted image 20220531201906.png]]

![[Pasted image 20220531201920.png]]

![[Pasted image 20220531201944.png]]

![[Pasted image 20220531202001.png]]
![[Pasted image 20220531202016.png]]

