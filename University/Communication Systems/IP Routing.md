It is a process to send packets of one file from one host to another. 

**Features**
* Examine the destination
* Determine the next-hop by routing tables

## Routing Metrics
-   Hops
-   Bandwidth
-   Load
-   Cost
-   Reliability

## Default Gateway? 
It is a router for to communicate to hosts on remote network.

## Routing Work
![[Pasted image 20220329223755.png]]
The router T1 is configured how the default gateway, it looks up in the routeing table is there is a direction of Host y, if there is, send from the Host x.

## Routing Table 
Store in RAM. Its decide the path destination to the destination network. 
### Method for routing
* Directly connected subnets
* Using static routing
* Using dynamic routing

## What are the main routing protocols? 
Helps data packets find their way across the network, path. 

### RIP (Routing Information Protocol)
Determine the best path by Hop counts maximum 15, then is unreachable. 
Port number: 520
Features
* Updates periodically 
*  Updates routing information
* Full routing tables are sent in updates
* Routing on Rumors (trust routing information)

#### Versions
RIP v1 (Classful) : it doesn't send information of subnet mask in its routing update.
RIP v2 (Classless): It sends information of subnet mask in its routing table. 
RIPng: 

![[Pasted image 20220329231348.png]]


### EIGRP (Enhanced Interior Gateway Routing Protocol)
It is a hybrid routing protocol that provides routing protocols, distance vector, and link-state routing protocols. It will route the same protocols that IGRP routes using the same composite metrics as IGRP, which helps the network select the best path destination.

#### Work
keeps a copy of its neighbor's routing tables. If it can't find a route, it queries its neighbors for a route, and then recursive. 

#### Messages and facilitate session management: 
* HELLO packets 
* QUERY packets
* REPLY packets
* REQUEST packets 
* UPDATE packets 

if HELLO packets hasn't been received, inoperative. 
Enhanced Distance vector protocol.

#### Advantages 
* Faster Convergence 
* Improves voice and video quality
* Minimizes network resource usage


### IS-IS (Intermediate System - Intermediate System)
ISIS routing protocol is used on the Internet to send IP routing information. It consists of a range of components, including end systems, intermediate systems, areas, and domains.

* Is a link-state routing protocol

### Routing table Algorithms
* Dijkstra Algorithm
* Shortest Path First  (SPF)

protocol is one of a family of IP Routing protocols, and is an Interior Gateway Protocol (IGP) for the Internet, used to distribute IP routing information throughout a single Autonomous System (AS) in an IP network.





# Bibliography
https://www.guru99.com/ip-routing.html
https://www.cloudflare.com/learning/network-layer/what-is-routing/
https://www.geeksforgeeks.org/routing-information-protocol-rip/
https://www.metaswitch.com/knowledge-center/reference/what-is-intermediate-system-to-intermediate-system-isis
https://www.cisco.com/c/en/us/support/docs/ip/enhanced-interior-gateway-routing-protocol-eigrp/16406-eigrp-toc.html







