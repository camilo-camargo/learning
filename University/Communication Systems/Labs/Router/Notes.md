## Camilo Andrés Camargo Castañeda
# El siguiente informe se refiere al procedimiento hecho para cada router como su tabla de enrutamiento y asignación de IP.
| Dispositivo    | Interfaz      | Direccion IP | Mascara de subred | Gateway por defecto |
| -------------- | ------------- | ------------ | ----------------- | ------------------- |
| Tunja          | serial 0/0/0  | 192.168.1.2  | 255.255.255.0     | 192.168.1.1         |
| Bogota         | serial 0/0/0  | 192.168.3.2  | 255.255.255.0     | 192.168.3.1         |
| Light          | fastEthernet0 | 192.168.1.3  | 255.255.255.0     | 192.168.1.1         |
| Bank           | fastEthernet0 | 192.168.1.4  | 255.255.255.0     | 192.168.1.1         |
| Administration | fastEthernet0 | 192.168.1.5  | 255.255.255.0     | 192.168.1.1         |
| Sarah          | fastEthernet0 | 192.168.3.3  | 255.255.255.0     | 192.168.3.1         |
| Antony         | fastEthernet0 | 192.168.3.4  | 255.255.255.0     | 192.168.3.1         |
| Sarah          | fastEthernet0 | 192.168.3.5  | 255.255.255.0     | 192.168.3.1         |
|                |               |              |                   |                     |
### Diagram screenshot
**Los routers necesitan conectarles en modo apagado los puertos serial el cual es wic-2t**
![[Pasted image 20220330082518.png]]
### Commands basic Router Tunja screenshot
![[Pasted image 20220330082430.png]]
### Command basic Router Bogota 
![[Pasted image 20220330083059.png]]
## IP Routing Static
### IP Routing Tunja
![[Pasted image 20220330084011.png]]
### IP Routing Bogota
![[Pasted image 20220330084214.png]]
## IP Routing Dynamic
### Tunja
![[Pasted image 20220403142341.png]]
### Bogota 
![[Pasted image 20220403142525.png]]


**Los routers necesitan conectarles en modo apagado los puertos serial el cual es wic-2t**
### Bogota -Neiva

![[Pasted image 20220404103839.png]]
## Bogota 
![[Pasted image 20220404103324.png]]
## Neiva 
![[Pasted image 20220404103657.png]]

Test
![[Pasted image 20220404104815.png]]