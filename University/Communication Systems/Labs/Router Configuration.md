# User mode
```bash
show version
```

Este commando muestra la versionde algunos componentes como tambien  las licencias.
![[Pasted image 20220316080850.png]]

```bash
show vlan
```
Este commando muestra la informacion de vlan( Virtual LAN)

![[Pasted image 20220316081046.png]]

```bash
show interfaces
```
Este commando muestra las interfaces y su configuaracion basica 
![[Pasted image 20220316081435.png]]
![[Pasted image 20220316081457.png]]
# Admin Mode
Con `enable` se ingresa en el modo admin.
```bash
clear arp-cache
```
Este commando limpia la cache de arp.

```bash
dir 
``` 
Este commando visualiza los directorios como tambies los permisos para diferentes usuarios
![[Pasted image 20220316081904.png]]
```bash
mkdir test
```
Este commando crea un nuevo directorio con nombre test
![[Pasted image 20220316084409.png]]
```bash
ping 169.254.215.77
```
Este commando prueba transferencia de datos a un host especifico
![[Pasted image 20220316084558.png]]

```bash
reload 
```
Recarga los archivos de nuevo 

# Test Packerttracer
## hostname
![[Pasted image 20220316082613.png]]
## passwords
![[Pasted image 20220316082910.png]]
## Banner
![[Pasted image 20220316083019.png]]
## Interface
![[Pasted image 20220316083519.png]]
## Save 
![[Pasted image 20220316083645.png]]
![[Pasted image 20220316083735.png]]
## Show run command 
![[Pasted image 20220316083911.png]]
![[Pasted image 20220316083928.png]]

## Testing 
![[Pasted image 20220316090304.png]]