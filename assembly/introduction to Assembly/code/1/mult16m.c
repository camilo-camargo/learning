#include <stdio.h> 
#include <time.h> 


int main(void){
	clock_t start, finish;
	int value1=1000, value2=4096;
	int i, j, n;
	extern long mult(int, int); 

	printf("Please input repeat count: ");
	fflush(stdout);
	scanf("%d", &n);

	start = clock();
	for(j=0; j<n; j++){
		for(i=0; i<1000000;i++){
			mult(value1, value2);
		} 
	} 
	finish= clock();
	printf("Multiplication took %f seconds to finish .\n", 
			((double)(finish-start))/CLOCKS_PER_SEC);
	return 0;
}

