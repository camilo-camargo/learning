# Basic Computer Organization 
* CPU (Central Processing Unit)
* Memory Unit
* Input / Output (I/O)

The data bus are the system of communication of the above primary components of the computer. The structure of system bus.
* Address bus: The memory addressing capacity.
* Data bus: The size of the data transferred between the processor and memory or I/O device.
	Example: 20-bits address bus, 16-bits data bus. Since
	> 2^20 = Memory can address  and each data transfer involves 16 bits.
* Control bus: Includes control signals.

bus transactions: are data transfers that involves a *master(initiator)* and *slave(target)*.

When there is more than one master this sends a  *bus request *to the bus *arbiter* is the permission is granted, then sending a signal on the  bus grant.

bus protocol: bus-request-grant.

## The Processor
Fetch-Decode-Execute cycle or execution cycle.
### Fetch
Fetching: Places appropiate address on the address bus, activates the memory read and then places in the data bus.
Access time: Time requires to read the instruction.

### Decoding
Parsers the intruction captured by the fetch. (encoding Scheme)
### Execute
Processors contains ALU  and  contro circuitry for timing.


## The System Clock
Timing signal to syncronize the operations of the system. 
Clock: sequence of 1's and 0's
Frequency: Clock per Seconds Hertz (Hz)
Mhz (10^6), GHz(10^9)
Clock Period: 1 / frequency

## Number of Address
### Three-Address Machines
MIPS processors uses that addresses. 
```c
A = B + C * D - E + F + A
```
```asm
mult T, C, D ; T = C * D
add  T, T, B ; T = B + C * D
sub  T, T, E ; T = B + C * D - E
add  T, T, F ; T = B + C * D - E + F
add  A, A, T ; A = B + c * D - E +  F + A
```

## Two Addressig Machines 
This addresing mode are use with CISC, Here the source are use as destination.
```c
A = B + C * D - E + F + A 
```
```asm
load T, C ; T = C
mult T, D ; T = C * D
add  T, B ; T = B + C * D
sub  T, E ; T = B + C * D - E
add  T, F ; T = B + C * D - E + F
add  A, T ; T = B + C * D - E + F + A
```

## One Address Machines
## Registers
Accumulators: Input Operand and to recieve the result of operation.
## Zero-Address Machines
The both operands are the default location.
## The load/Store Architecture
In this architecture the values are store in the register or memory, this architecture completed two actions load and store.
```c
A = B - C * D - E + F + A
```

```asm
load R1, B 
load R2, C
load R3, D
load R4, E
load R5, F
load R6, A
mult R2, R2, R3
add  R2, R2, R1
sub  R2, R2, R4
add  R2, R2, R5
add  R2, R2, R6
store A, R2
 ```


The RISC has more registers than the CISC processors, since that reduced the size substantially.


## Processor Register
Register are space to hold data, intruction, state information.
General-Purpuse:
Special-Purpose: Accesible for user programs and reserved for the system use.
### Flow of Control
PC = Program Counter
The PC register has an important role because that are the mechaism for flow control.
That register points to the next intruction. 
That couter are incremented after the intruction fetch.


## Branching
More Know how jump intruction. 
### Unconditional Branch
The simplest control flow that has two ways for addressing the target.
MIPS processors only support absolute.
* Absolute Address: Loading the target addresss into the PC register. 
* Relative Address: Added the target address to the PC register. The main advantage is *relocatable code* 
Relocatable code : we can move the code from one block to another without changing the target address.
### Conditional Branch

The Condtitional braches are taken only if the an specific condition is completed. 
**Set-Then-Jump**:	In this way first is set the condition bit to the flag register that record that bit, and then use the condition register depending of the bit condition. 

**Test-and-Jump**: In this way the test and the jump are execute with a only nemonic. 

Condition Code Register: Stores and arithmetics and logical conditional operation, for example overflow that there is when the a value is large to store in the register. 


The MIPS proccesor dont use the flag register it used exception to handled that.

### Procedure Calls
The procedures calls are sligthly different to the brach control  flow, but the two has the same form to initial execution. but that return the control after proceed the procedure and we need to return to the instruction. The return has two elements
* End of Procedure: We must to especified the end of the procedure in assembly language is used a special instruction.
* Return Address: After call the intruction the PC are change with the next instruction but in the same time the address following the call instruction are save in the stack(Pentium) or general purpose register (MIPS). 

### Parameter Passing
Register-Based: The parameters are save in the processor's register.
Stack-Based: The parameters are pushed into the stack.

The Register method is faster than the Stack method. But we cannot used recursive in the register method.


# Memory
The memory are built with switches, that has two state open and close, or 1 or 0. The memory are  organized with a lot of that switches, the readable form is to compound 8 bits called Byte. The memory has regions started with 0 this called Memory Address. 

The total memory  address space (MAS) are determined two power of the amount of address line.
2^32 = 4GB of possible ram. 
## Memory Operations
The memory has to operation *read* and *write*, the first read previosly store data, and write stores a value in memory and specification of the data to be written.

Read are non-destructive.
Write are destructive. 

Metrics
* Access Time: Amount of time that spent to retrieve the data from the address location.
* Memory Cycle time: minimun time between two memory operations.
* Bandwidth: Number of bytes transferred per seconds.

#### Read Cycle (3 Clocks)
1. Place the address (1 clock)
2. Activate Read Signal (1 clock)
3. Wait and place data on the bus the data readed. (2 clock)
4. Read data
5. Drop read signal and terminate the read cycle.

> Each wait cycle introduce a waiting period  equalst to one system clock

#### Write Cycle
1. Place the address (1 clock )
2. Place the data in the data bus (2 clock )
3. Activate write signal (1 clock)
4. Wait for memory to store (2 or 3 clock)
5. Drop the write signal  (2 or 3 clock)


### Types of memory
##### read-only
-> ROM 
-> PROM (Programmable ROM)
-> EPROM ()
-> EEPROMs (Electrically erasable PROMs)
read/write
Volatility :  Power to retain information.
NonVolatile : Doesn't need power to retain.
##### Read/Write
* Static
	* SRAM 
* Dynamic
	* DRAM (Destructive read memory)
	* FPM DRAM
	* EDO DRAM
	* SDRAM
	* DDR SDRAM
	* RDRAMs

### Storing Multibyte Data 
There is a two byte-ordering schemes that are referenced how:
Little Endian (From MSB to LSB) : Pentium used that
Big Endian (From LSB to MSB ):  MIPS used that

The two are not advantage over another, but problems arise when we are working with two machines that  has differents Ordering-Schemes, and for solve that we must make a conversion.


# Input/Output
The Input and Output devices are called peripherical devices that can interact with the outside world. This interaction is gone through I/O Controller that acts as an interface between the system  and the I/O devices. 

The reason for used I/O controller is that he spent time that the cpu may not spent because the cpu spent unneccesary  time with I/O and the another reason is that the another reason is that the electrical power used for send signals is very slow compared is the device has your own controller. The I/O controller has three registers
* Data Register 
* Command Register 
* Status Regsister 

## Mapping 
Memory-Mapped: this way maps the I/O ports to the memory.

Isolated I/O: This way there is a address space that need special instructions to access the I/O.

The pentium has 64KB of I/O address space.

## Accesing I/O Devices 
How a  programmer in assembly there is a problems with direct control in I/O devices because we have total control, but there is routines in operating systems drivers that can help us. Another  mechanism is used  interrupts.

## Performance 

back [[assembly/introduction to Assembly/Introduction]]
next [[]]
