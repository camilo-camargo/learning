# Introduction
## Level of programming
The reason for what users are shields the abstraction of differents layers in programming is that is more easy for users interacted with the application in top level. 
System dependent :  assembly ...
System Independent:  c and java 
![[Level of Programming]]

## Types of processors
CISC (Complex Instruction Set Computers)
RISC (Reduced Instruction Set Computers)

### Complex and Reduced 
The complex instructions has more steps with memory and **updates automatically**. 
The reduced instructions considered that the information cannot be in the **main memory** only in the register and use fixed size. 

## Assembly code  and Assembler 
The Assembler is the program that converts assembly code into binary code. 
Assemblers
* NASM (Netwide Assembler)
* MASM (Microsoft Assembler)
* TASM (Borland Turbo Assembler)

Two address (PENTIUM)
Three address (MIPS)

## Advantages of High-level Languages 
* Program development is faster 
* Programs are easier to maintain
* Programs are portable 
## Why program in the Assembly Language ?
* Efficiecy (space and time)
* Accessibility to system hardware ()
## Typical Application
* Time Convenience (desirable to improve performance)
* Time Critical (Real-Time)
* Accessibilty to hardware

Hybrid or Mixed-Mode 
That con to include a high level programming with assembly language.

Reasons for learn Assembly language 
* Computer Systems itself
* Satisfaction to learn something complex

## Performance c Versus Assembly Languages 
Program
Multiplying two  16-bit integers.

Algorithm 
```
product := 0
for (i = 1 to n)
	if (least significant bit of the multiplier = 1)
	then 
		product := product + multiplicand
	end if
	Left shift the multiplicand by one bit position 
	Right shift the multiplier by one bit position
end for
```


next [[Basic Computer Organization]]