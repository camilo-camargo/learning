
all: init

init:
	git stash
	git pull 
	git stash pop 

documentation:
	cat src/SUMMARY.md > src/.SUMMARY.md
	cat src/.SUMMARY.md > src/SUMMARY.md
	rm src/.SUMMARY.md
