Reduce duplicity 

## IS-A (Inheritance)

```java
// Example Inheritance With Class
public class Person {
	protected String first_name;
	protected String last_name;
	protected int age;
	
	public String first_name(){
		return first_name;
	}
	
	public static void main(String[] args){
	    Manager manager = new Manager("choco");
	    System.out.println(manager.first_name());
	   
	}
}

 class Manager extends Person {
     public Manager(String first_name){
         this.first_name = first_name;
     }
}
```

```java
abstract class Person {
	protected String first_name;
	protected String last_name;
	protected int age;
	
	abstract String first_name();
	
	
}

 public class Manager extends Person {
     public Manager(String first_name){
         this.first_name = first_name;
     }
     
     public static void main(String[] args){
	    Manager manager = new Manager("choco");
	    System.out.println(manager.first_name());
	   
	}
	public String first_name(){
	    
	}
}
```

## Has-A (Association)
Example: Human body has-a heart.


```
```
### Aggregation Vs Composition

### Aggregation (weak)
Exits if doesn't exist another class
Car <- Not (music player) 
### Composition (Strong)
> The **final** keyword is used to represent Composition.

Don't exist if exist another class
Human <- Heart


Example
car <- engine + wheels (composition)
car <- music player (aggregation)

**Example code statements: There is a person if it has a heart **  

```java


```


# Bibliographic
https://www.upgrad.com/blog/what-is-composition-in-java-with-examples/
