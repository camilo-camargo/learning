# Algorithms Binder

> **Algorithm Design** *Jon Kleinberg - Eva Tardos* [[CS345-Algorithms-II-Algorithm-Design-by-Jon-Kleinberg-Eva-Tardos.pdf]]


> **Introduction to Algorithms** *Thamas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, Clifford Stein* [[Introduction_to_algorithms-3rd Edition.pdf]] 