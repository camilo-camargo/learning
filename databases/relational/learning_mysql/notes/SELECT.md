The select command is the basic SQL keyword to explore and manipulate data.


```mysql 
# the asteric indicates that select all atributes in the table
SELECT * FROM table_name;
```

## Choosing Columns 
You specified the columns that you want, separated with comma if is more than one. 

```mysql
SELECT column_name,... FROM table_name;
```

If you repeat the name columns you will see two identicals columns. 
After from mysql keyword you can type the database and the table
```mysql
SELECT column_name, ... FROM database_name.table_name;
```


## Where Clause
The where clause is a filter tool, depending of the value on a columns or another conditions.

```mysql
SELECT column_name, ... FROM table_name WHERE conditions;
```

### Conditions
#### Numbers, Strings
```mysql
# = equals
# > greater than
# < less than
# <= less than or equal
# >= greater than or equal
# <> different
# != different
```

#### Like clause
The like clause performe matches that begin with a prefix, contain string, or end in a suffix. 

```mysql
# %  Wildcard of 0 or any caracters
# _% Only one match 
 
```

The wildcard % has a several performance impact if it are at the beginning. 


## Combining conditions with AND, OR, NO and XOR

For clear when used more than one conditions used cluster indicated by parenthesis.

#### AND Clause
The and clause restrict the output to the both conditions is true.

#### OR Clause
The or clause outputs if in the only one of the two conditions is true. 

#### NOT Clause 
The not clause negates a boolean statement. 

#### XOR Clause 
The xor clause excluded the same boolean for the two conditions.

## Operators Precedence
![[Pasted image 20220514165856.png]]

## ORDER BY Clause
The order clause affects the order that the set of output is view. We can order by one or more than one columns. 

```mysql
SELECT column_name,... FROM table_name 
	ORDER BY column_name, ...;  
```

We can order by descending(DESC) and ascending (ASC) order.
```mysql
SELECT column_name, ... FROM table_name
	ORDER BY column_name DESC, column_name ASC, ...;
```

## LIMIT clause 
The limit clause limit the row output of a query, has two arguments the start row and end row. 
```mysql
SELECT column_name, ... FROM table_name
	LIMIT start_row, end_row;
```

There is another form to specified the range with OFFSET clause
#### OFFSET Clause
The offset clause discard the n values specified in it. 
```mysql
SELECT column_name, ... FROM table_name
	LIMIT limit_n OFFSET offset_n;
```



## Joining
The joining is used we need more than one table at once. 
### Inner Join
The inner join joins to tables with a condition.
```mysql
SELECT column_name, ... 
	FROM table_name INNER JOIN table_name 
	ON condition
```

If the inner join don't type the condition this  is a cartesian product. 

We can use the clause USING if the names of the two attributes in each table has the same name for the join. 

