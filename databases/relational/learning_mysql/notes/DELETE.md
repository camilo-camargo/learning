The delete command deletes the row of a table. 
```mysql
DELETE FROM table_name;
```

## WHERE Clause 
The where clause is used to delete tables with a condition
## LIMIT Clause 
The LIMIT clause has a limit of rows for a delete process. 
## ORDER BY Clause 
The ORDER BY clause provides a order for a delete operation. 

## TRUNCATE
If you want to remove all the rows the TRUNCATE is the faster way to do it. 

```mysql
TRUNCATE DELETE table_name;
```


