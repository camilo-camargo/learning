The insert statement is used to add new data to a table in a  data base. 
```mysql
INSERT INTO table_name VALUES (value1,value2), (), ...
```

You can to write a different order of an attributes in a table where you add new data, the order of the attributes don't important. 


```mysql
INSERT INTO table_name (attribute, ...) VALUES (value1, value2), (), ...
```

## IGNORE Clause 
The ignore clause prevent the error if you want, but the stament ins't complete. 


