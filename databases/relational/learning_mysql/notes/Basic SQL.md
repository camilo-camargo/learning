# Important
* Names with suffix \_name  is an abstract name, you must change that name.

## To create a database 
```mysql
create database database_name
```
## To choose a database 
```mysql
use database_name
```

## To check a active database
```mysql
select database();
```

## To show tables
```mysql
show tables;
```


## To show the columns of a table
```mysql
SHOW COLUMNS FROM table_name;
```

or you can use the following command with the same result
```mysql
#DESCRIBE table_name;
DESC table_name;
```


