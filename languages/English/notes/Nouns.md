Noun means name in Latin "nomen".
Possiblee  nouns list. 
Nouns: Names the following 
* person
* a place
* a thing
* an idea
* an activity
* a quality
![[Pasted image 20220425055506.png]]
Noun Marker (Article) defines the noun from the general to the specific. 
Common Articles: a, an, the, these, those, this, that


## Common Nouns
The nouns are the first word in the sentence. 
> book, author, reader, city, college

The capitalization of the noun is where the noun becomes part of a proper noun. 


## Proper Nouns
The proper noun is the specific name of the above list of possible nouns, but an activity and an idea seldom begins with a capital letter. 

> Taylor University

Tips:
1. Names and family names.
2. Months, days of the week, and holiday names. 
3. Adjectives originating from proper nouns.
> Japanese automobile, Canadian cuisine
4. Geographic names
> Eastern Shore
5. Names of streets, buildings, and parks
6. Titles and historic convention. 
7. The nouns in titles of works or books
8. Names of countries and continents
9. Names of states, provinces, districts, counties, and townships
10. Names of rivers, oceans, seas, and lakes
11. Names of geographical formations
12. Familiar names
13. The shortened form of popular names, often used as the second reference. 

Notes
> Don't capitalize the common noun elements of names in plural usage. The Republican and Democratic parties, the lakes Erie and Huron. 


Incorporated entity of a noun.
![[Pasted image 20220425061802.png]]

While country names are always capitalized
![[Pasted image 20220425062018.png]]
![[Pasted image 20220425062031.png]]
## Mass Nouns 
To measure or classify them, use unit of measurement followed by "of".

1. Food, materials, metals, and natural qualities. 
![[Pasted image 20220425062317.png]]

2. Gases, liquids, and small-particles substances: 

![[Pasted image 20220425062419.png]]
3. Names of Languages: English, German, Yiddish
4.  Abstract nouns, many of which end in -ness, -ance, -ence, ant -ity
![[Pasted image 20220425062553.png]]
6. Most gerunds: waltzing, yelling 
7. Words without a plural form: evidence, furniture 

Rules to keep in mind 
1. A mass noun can be quantified by an amount but not by a number. 
2. A mass noun only has the singular form
3. A mass noun can have "much" as a modifier; it cannot use "a", "an", or "one"
## Plural Nouns
The plural nouns is used to indicate more than one of the above list of nouns. 

1. Add an -s to most nouns
2. Add -es when the noun ends in -s, -sh, -ch, or -x 
3. When a noun ends in the consonant -y, drop the -y and add -ies. 
4. When a noun ends in the vowel -y, add -s
5. Exceptions to 4: when ends in -quy, change  the y to an i and add -es.
 Colloquy -> colloquies
 obloquy -> obloquies
 obsequy -> obsequies 
 soliloquy -> soliloquies 
 6. In most cases, add an s to a word ending in -f. Some exceptions are listed below. 
 calf  -> calves
 self -> selves
 wolf -> wolves 
 7. The main word of a compond word is made plural
![[Pasted image 20220425064136.png]]

8. Exception to 7: If the compound word doesn't contain a noun, add -s to the end of the word.
![[Pasted image 20220425064243.png]]
9. Exception to 7: if the compound word ends in -ful, add -s to the end of the word. 
![[Pasted image 20220425064336.png]]
10. Some nouns remain the same in plural form
![[Pasted image 20220425064427.png]]
11. Irregular noun plurals 
![[Pasted image 20220425064503.png]]
![[Pasted image 20220425064513.png]]


12. Add 's ' to make a number or letter plural
![[Pasted image 20220425064624.png]]
page 72. hour 3 

