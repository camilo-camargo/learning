Every sentence has two essential parts: 
- A complete subject 
- A complete predicate
## The subject of the subject
The complete subjects is the simple subject plus any words or group of words modifying the simple subject.  
![[Pasted image 20220423134347.png]]
![[Pasted image 20220423134604.png]]
## The "Understood you"
### Imperative Sentence
is one that issues a command or makes a request.
![[Pasted image 20220423134914.png]]
## Declarative Sentence
Is one that makes a statement.
![[Pasted image 20220423135219.png]]
## Subject Positioning
### Start with expletive
**expletive**
	an empty word that pretty much just gets the sentence underway. 
![[Pasted image 20220423135704.png]]
### Inverted for effect
![[Pasted image 20220423135838.png]]
## Main clause (independent clause)
A group of word that contains both a subject and verb and expresses a complete thought. The main clause can be placed beginning, middle or end, within a sentence.
![[Pasted image 20220423140538.png]]
## Subordinate Clause (dependent clause)
Is a group of words that does not express a complete thought. 
![[Pasted image 20220423140549.png]]
In that case, the "while" becomes the clause can no longer stand alone as a sentence. 

## The predicate Defined
Is the verb plus its objects, complements, and adverbial modifiers that tell what the complete subject does or is. 
### Complete predicate 
![[Pasted image 20220423141217.png]]
### Simple predicate 
![[Pasted image 20220423141235.png]]
![[Pasted image 20220423141255.png]]


## Compound Subjects and predicates 
### Compound Subject
The compound subject has two or more subjects, joined by a conjunction such as "and" or "or"
![[Pasted image 20220423141503.png]]
### Compound Predicate
The compound predicate has two or more verbs that have the same subject, connect by a conjunction such as "and" or "or".
![[Pasted image 20220423141614.png]]
## Sentences Types
### Simple Sentences
The simple sentences only has
* One main clause
* contains no subordinate clause, and is
* limited to one subject and one predicate. 
![[Pasted image 20220423142344.png]]
### Compound Sentences 
The compound Sentences has two or more main clauses joined by a coordinating conjunction or a semicolon. 
![[Pasted image 20220423142559.png]]

### The Complex Sentences
The complex sentences has one main clause and one or more subordinate clauses. 
Italic: main clause.
Bold: subordinate clause. 
![[Pasted image 20220423142912.png]]
### The Compound-Complex Sentence
Contains one or more than one clauses and one or more subordinate clauses. 
![[Pasted image 20220423143219.png]]

### Sentences Fragment
The sentence fragment is an incomplete sentence. 
![[Pasted image 20220423143823.png]]
Fragment usage is generally not acceptable for **formal English or academic writing**. 
You can use the fragment for exclamation, to emphasize or elaborate on a preceding sentence, or to answer  a question. 
Examples
What a day! (exclamation)
They had money. Lots of it. (elaborate on a preceding sentence)
Where did she see the cat ? In the garden. (Answer)

### Parallel Construction 
Is the use of the same pattern in words, phrases, or clauses. 
A parallel structure must continue and end with the same form with which it begins. 
![[Pasted image 20220423144841.png]]
### Run-On Sentences
Contains two or more complete sentences (main clauses) joined to read as an improperly punctuated single sentence. 
![[Pasted image 20220423145243.png]]
### Fused Sentence Error
This is another way of saying  you have a run-on sentence. 
### Command Splice
The improper use of a comma between two main clauses. 
### Interrogative sentence (Questions)
![[Pasted image 20220423145656.png]]
### Sentence Variation
#### Coordination
Join  short, choppy, complete sentences, clauses and phrases. 
![[Pasted image 20220423145855.png]]
#### Subordination
Link two related sentences to each other so that one contains the main idea and the other is no longer a complete sentence.
![[Pasted image 20220423150203.png]]
#### Replacement
Replace a repeated subject or topic with relative pronouns. 
![[Pasted image 20220423150557.png]]
#### Participles 
Eliminate a form of the verb to be and substitute a present participle or a past participle phrase. 
![[Pasted image 20220423150819.png]]
#### Prepositions and other variations
![[Pasted image 20220423150928.png]]
### Sentences Rules 
* Single-word subject directly in front of the verb, followed by the object or complement. 
> Sandy comes from Scotland. 
* All the words that make up the subject, verb, direct object
> People who have dogs shouldn't leave their homework lying around. 

* Adverbs or adverb phrases of time in one of the following places: 
 ![[Pasted image 20220423151535.png]]
