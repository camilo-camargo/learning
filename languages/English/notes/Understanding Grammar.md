**What is grammar ?**
	The rules that govern it. 
**Why do you need to learn it ?**
	1. How the English sentences work
	2. How the parts of the sentences work together to express a meaning
Grammar helps us. 
 * Identify our shortcomings as writers and speakers. 
 * Gain a common vocabulary for sentences elements, which helps us learn how to address those shortcomings. 

## Grammar's Six Senses 
### Descriptive Grammar
The study of speak  and patterns in that speech. The descriptive grammar is the basis for modern linguistics. 
### Pedagogical Grammar
It stresses and clarifies the systematic nature of the sentence. 
### Prescriptive Grammar
The rules for how people should speak a language.

### Reference Grammar
Sets forth the rules of grammar in a dictionary format.
### Theoretical Grammar
The analysis of the components necessary in any human language. 
### Traditional Grammar
The study of classical grammar used 2,000 years ago. 














## The Logic of Grammar
### Context
time and place in which an utterance occurs. 
#### Cultural Context
This is based on such things as national origin or religion.
#### Linguistic Context
Is the setting in which the text occurs.

#### Social Context 
Includes the identities of the speaker and person or persons to whom he or she is speaking.

### Function 
The purpose for which individual words are chosen and how they relate to each others.

### Pragmatics
Study of language in context.

### Semantics
The meaning of individual words through syntax and context. 
### Wording 
The word order in a sentence. 
### Sound Patterns 
The phonetics, deals with how speech sounds are actually made, transmitted, and received. 
The Phonology a subcategory of phonetics, that deal with the spelling variations we struggle to make sense. 

## Nuances, Trivia, and important stuff
### Alliteration
The repetition of the beginning consonant sound of two or more words that appear close together in speech. 
![[Pasted image 20220423071633.png]]
### Allusion
Is an indirect reference to something else. 
![[Pasted image 20220423071932.png]]

### Anthropomorphism
Is a big word that refers to human characteristics being attributed to a nonhuman object. 

![[Pasted image 20220423072159.png]]
### Antonyms 
Is a word whose meaning is the opposite of another word. 
![[Pasted image 20220423072533.png]]

### Assonance
The repetition of vowel sounds in two or more words that appear close together in speech. 

![[Pasted image 20220423072726.png]]


### Clichés
A cliché is a phrase, saying, or term that has become dated and due to overuse, lacks the creativity that makes language interesting. 
![[Pasted image 20220423073041.png]]

### Colloquialisms
are short-lived fad and slang sayings that are best avoided in formal English usage. 
![[Pasted image 20220423073439.png]]

### Figures OF Speech
Is a nonliteral usage of words employed to achieve and effect beyond the range of ordinary language. 
* Anthropomorphism 
* Metaphors 
* Similes

### Gist
The gist reference to the essence of a text. 
### Homonyms 
A homonym is one of two or more words pronounced the same but spelled differently. 
![[Pasted image 20220423074025.png]]

### Hyperbole
is an exaggeration for emphasis or effect, and its meaning is not meant to be taken literally. 
![[Pasted image 20220423074212.png]]
### Idioms 
is an expression that is unique to a language and cannot be understood simply from the meaning of its individual words. 

Down in the dumps -> feeling very sad 
Fell off the wagon ->  To start drinking alcohol again 
Had a cow -> to become very angry 
Hit the sack -> to go to bed
Pulling your leg -> something that is a joke and isn't true
Skating on thin ice -> is a precarious or risky situation.
Spilled her guts -> tell everything about something secret or private.

### Idiomatic Translations
The meaning of the original is translated into forms that best preserve the meaning of the original form. 
### Irony 
Is criticism or ridicule in which words mean the opposite of what they state. 
![[Pasted image 20220423075622.png]]








### Jargon 
refers to a specialized vocabulary unique to a certain segment of a population for reasons such as trade, occupation, technology, medical science, and academic discipline. 
![[Pasted image 20220423080119.png]]


### Lexicons 
A lexicon is the dictionary and vocabulary used by people speaking a particular language. 
### Malapropisms
Is a word that sound like another word with a totally different meaning. 
![[Pasted image 20220423080325.png]]
### Metaphors 
The metaphors denotes one kind of object or idea in place of another. 
The time is money. 
### Metonymy 
The word is substituted for another with which it is closely associated and with which the audience" is familiar. 
![[Pasted image 20220423081325.png]]

### Morphemes
The smallest unit of meaning in a language. 
If a word has only one meaning part, the morpheme is the definition of the word; 
if each word part has separate meanings, each meaning's part is a morpheme. 
![[Pasted image 20220423081546.png]]

### Onomatopoeia
The ability of a word's sound to suggest its meaning. 
![[Pasted image 20220423081650.png]]
### Puns 
Is the use of a word --- Usually with humorous intent --- in such a way as to suggest two or more of its meanings or the meaning of another word similar in sound. 

### Rhyme 
Is a series of word endings that repeats the same, or similar, sounds. 
![[Pasted image 20220423082458.png]]
### Rhythm 
Is the repetition of stressed and unstressed syllable patterns rather  than sounds. 
![[Pasted image 20220423082652.png]]
### Sarcasm 
Is more intense than irony, and is intended to have a negative impact. 
![[Pasted image 20220423082926.png]]

### Similes
A simile indicates comparison. The comparison is explicitly signaled by a word such as "as" or "like". 
![[Pasted image 20220423083115.png]]

### Synonyms
Is a word that means the same thing as another word. 
![[Pasted image 20220423083221.png]]
### Euphemism
Is a word or phrase whose intent is to "soften" the meaning of the original word.
![[Pasted image 20220423083458.png]]
