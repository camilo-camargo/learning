# Commas
The commas are used to create a soft break or space to breath.

## Errors 
1. When a comma separates a verb from its subject or object. 
> Dogs, Bark. (Error)
> I like, dogs. (Error)

2. Use of a comma in place of a full-stop 
	in These errors is commonly used when are two separate sentences.
## Usage
1. Use commas in lists.
> At the park we saw child's, dogs and woods.
2. Use commas like brackets
 > Data structure is the  best topic programming, write sentences for a computer, because this is related with abstraction.

3. Use commas after an introductory word or phrase, or sometime before a final statement. 
> The computer is a power machine, we assume that it consume a lot of electrical energy. 
4. Use commas to isolate words such as 'however', 'therefore', 'for example'. 
> Laura is a woman. She likes to eat every morning, for example, she eats potatoes every day. 

5. Problems in the use of 'for example'









# Bibliographic
http://archive.bio.ed.ac.uk/jdeacon/writing/stops.htm

