def main():
    while True:
        try: # handled a exception 
            # raise for to execute a exception
            x = int(input("Please enter a number: "))
            break
        except ValueError: # exception to handle
            print("Oops! That was no valid number. Try again...")  
        else: # another exceptions 
            print("Else")
        finally: # clean up exception 
            print("Finally") 

if (__name__ == "__main__"):
    main()
