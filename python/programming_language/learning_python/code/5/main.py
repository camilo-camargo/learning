# lists
def main2():
    items = []
    items.append("H")
    items.append("e")
    items.append("l")
    items.append("l")
    items.append("o")
    items.insert(0, "H")
    items.pop()
    print(items)
    print(items.count("H"))
    items.reverse()
    print(items)

    stack = [3, 4, 5]
    stack.append(6)
    stack.append(7)

    stack.pop()
    print(stack)

    from collections import deque

    queue = deque(["one", "two", "three"])
    queue.append("four")
    print(queue.popleft())

    # List comprehensions
    squares = []
    for x in range(10):
        squares.append(x**2)

    print(squares)

    squares = list(map(lambda x: x**2, range(10)))

    print(squares)
    squares = [x**2 for x in range(10)]
    print(squares)

    list = [(x, y) for x in [1, 2, 3] for y in [3, 1, 4] if x != y]

    print(list)

    vec_sqrt = [(x, x**2, x**3) for x in range(10)]
    print(vec_sqrt)

    print([x for e in vec_sqrt for x in e])
    from math import pi

    print([str(round(pi, i)) for i in range(1, 6)])

    matrix = [[x*4, x*4+1, x*4+2, x*4+3] for x in range(4)]

    transposed = []
    for i in range(4):
        transposed.append([row[i] for row in matrix])

    print(matrix)
    print(transposed)

    # is the same as
    # print(list(zip(matrix)))
    #del matrix
    # print(matrix)


def tuples():
    t = 1, 2, 3
    x, y, z = t
    print(x, y, z) 

def sets():
    set = {1,2,3}  
    print(set) 
    print(1 in set) # fast membership testing 
    # sets comprehensions
    a = {x for x in "Camilo" if x not in "ab"} 
    print(a)

def dic():
    dic = {"Hello": "Hola",
           "Bye"  : "Chao",
           "Happy": "Feliz"}  

    print(dic["Bye"])
    print(list(dic)) 

    ## dict comprehension 
    print(dict(sape="Hello")) 

    for k, v in enumerate(dic.items()):
        print(k, v) 

    for q,a in zip(dic, {"play":"jugar"}):
        print(q,a) 

    for i in reversed(range(1, 10, 2)):
        print(i) 

    for i in sorted(dic): 
        print(i)

    print(dic is dict)
    print((1,2,3) == (1.0, 2.0, 3.0))
if __name__ == "__main__": 
    dic()
