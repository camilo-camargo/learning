print(7 // 2 ) # floor division 
print(2 ** 5 ) # power   

print(r'C:\some\name') # exclude backslash formatting 

print(3 * "Hello, World ")# repeot text with * 
text = "Hello my name is Giovanny Giorgo, but every body call me Giorgo " 

print(text[17:25]) # slicing, the first is always included and  the last is excluded 
print(text[:2] + text[2:]) 
print(len(text)) # len of the string 


# List 
list = ["Carlos", "Miguel", "Mick"]  
print(len(list)) # list size 
#list support slicing
print(list[2:3])
print(list[:]) 

# Fibonacci
a, b = 0, 1
while(a < 10):
    print(a, end = ',')
    a, b = b, a+b 

# If statement 
print("\n") 
x = 0
#x  = int(input("Write a number"))
if (x < 0): 
    print("less than zero")
elif (x > 0):
    print("Greater than zero") 

words = ['cat', 'window', 'defenestrate']
for w in words:
    print(w, len(w))  

for w in words[:]:
    if(len(w)) > 6:
        words.insert(0, w) 

print(words) 

list = range(5, 10, 2) 
print(list) 

# primes
for n in range(2 , 10): 
    for x in range(2,n):
        if(n % x == 0):
            print(n, 'equals', x, '*', n//x)
            break
    else: #when no break occurs
        print(n, 'is a prime number')

    # continue, next iteration. 

#pass statement
#while True:
###    pass # do nothing

# defining functions

def fib(n):
    """ Print a Fibonacci series p to n."""
    a, b = 0, 1
    while a < n:
        print(a, end = ' ')
        a,b = b, a+b
    print()

# function arguments with default value
def wel(msg="Msg function"):
    "print message of the variable msg"
    print(msg)

wel("Hello") 

words = ["cat", "elephant", "mouse"]
#if(input() in words):
###    print("You are guess") 


def dic(*args,**dic):  
    for arg in args: 
        print(arg)
    for dic_v in dic:
        print(dic_v, ":", dic[dic_v]) 

dic("one", "two",men="hello")


def write_multiple_items(file, separator, *args):
    print(separator.join(args)) 

write_multiple_items("hello", ".", "Hello", "world", "myname") 
 
#args = [0,10]
#a = list(range(*args))  

# lambda
def make_incrementor(n): 
    return lambda x: x + n 

f = make_incrementor(42) 
print(f(1))


