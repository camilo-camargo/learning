
def reverse(data):
    for index in range(len(data)-1, -1, -1):
        yield data[index]

## iterable
class Reverse:
    """ Iterator for looping over a sequence backwards """
    def __init__(self, data):
        self.data = data
        self.index = len(data) 

    def __iter__(self):
        return self 

    def __next__(self):
        if(self.index == 0):
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]


class V3:
    """ Vector of 3 Components """ 
    def __init__(self, x, y , z):
        self.__x = x
        self.__y = y
        self.__z = z  

    def __str__(self):
        return f"x: {self.__x} " f"y: {self.__y} " f"z: {self.__z} " 

if(__name__ == "__main__"): 
    v3 = V3(1,1,1)
    print(v3)  

    ## reverse class 
    rev = Reverse("Spam")  
    #for x in rev:
    #    print(x) 

    for char in reverse("spam"):
        print(char)


