import math
import json

def formatted_string_literals():
    x = 5
    y = 10
    z = 15
    print(f'x:{x:10}  y:{y:10} z:{z:10}\n') 
    print(f'pi: {math.pi:.4f}') 

def read_write_file(): 
    with open('workfile', "r+") as f: 
        print(f.read())  

def serializing():
    pass 

def deserializing():
    pass
def main():
    formatted_string_literals() 
    print(json.dumps({"Hello": "Hola"})) 
    # [TODO] search pickle in python

if( __name__ == "__main__"):
    main()
