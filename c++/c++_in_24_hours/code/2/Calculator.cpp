#include <iostream> 

int add(int x, int y){
	//add the numbers x and y together and return the sum 
	std::cout << "Running calculator... \n"; 
	return(x+y);
}

int main(){
	/* this program calls an add() function to add two different
	 * set of numbers together and display the result. The add()
	 * function doesn't do anything unless it is called by  a line in the
	 * main function */ 
	std::cout << add(1,2); 
	return 0;
}
