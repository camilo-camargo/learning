Bjarne Stroustrop was the creator of C++ in 1979, in which to satisfy the next purpose, "I could write programs that were both efficient and elegant."

C++ is a portable language multipurpose that works fine in all modern operating systems, such as, Microsoft Windows, Apple, macOS, Linux and Unix systems. 

The C++ is a compiled language means that the source code is compiled into object file, and then the linker converts into binary format, this improves the performance above interpreters, which execute necessary instruction and is less efficient in time. 

The standard for C++ language is called ANSI/ISO C++. 