The linear interpolation is a method to estimate values between two ranges, and is used for data precision, in 3D graphics for blend, because this fill in the gaps for a set of values. 


The outside of the range of two values ... linear extrapolation. 



## References 
https://www.vedantu.com/formula/linear-interpolation-formula