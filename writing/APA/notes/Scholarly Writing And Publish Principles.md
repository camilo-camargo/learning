## Types of articles and papers
### Quantitative 
Numerically represented. 
Use methods to evaluate this data.
* Statistics
* Data Analyses
* Modeling
The quantitative method have the following sections:
* Introduction: purpose of the investigation.
* Method: full description of the investigation. 
* Results: data analysis and a report of the findings.
* Discussion: a summary of the investigation.


### Qualitative
It generates knowledge about human experience and/or action. 
characteristics: 
* Natural Language
* Iterative process (self-correcting and can produce original knowledge)
* to study experiences and actions, in the way that don't seek to develop laws.

### Mix (quantitative and qualitative)
It merges are with quantitative and qualitative methods, it involves the following: 
* Describing philosophical assumptions or theoretical models.
* Describing methodologies and procedures in relation to the study goal.
* Merge quantitative and qualitative to research aims, questions, or hypotheses. 


### Replication Article's 
Previous investigation
Reevaluates the conclusion of the beginning aim over another variations. 
Internal (not supported)
External: duplication. 
The study according to the external replication articles is divided in: 
* Direct or Exact: Establishing the original study is reliable. 
* Approximate: Alter procedures and adding conditions to the original study. Purpose determines some factors not including in the original study.
* Conceptual: introduce techniques to gain theoretical information. 


### Quantitative and Qualitative Meta-Analyses
Meta-analysis refers to collection of techniques from a group or related studies.
#### Quantitative Meta-Analysis
Determine factors that may be related to the magnitude from effect-size estimates from individual studies. 
#### Qualitative Meta-Analysis
* Metasynthesis
* Metaethnograhy
* Metamethod
* Critical interpretive Synthesis

Features: 
* identify finding and gaps
* develop new understanding
### Literature Review Articles
Narrative summaries and evaluations from the literature base. This type of article has the following features: 
* Define and clarify the problem
* Summarize the state of the research
* Report inconsistent in the literature

### Theoretical Articles
Parts from the existing research to advance theory. 
The theory of the last article can be new, or analyse an existing theory. 

### Methodological Articles
This methodological article is focus on the methodology of the research, provides an assess to determine the applicability and the feasibility of research to study. 

For more readability has been with highly technical materials, these are in the appendix as supplemental material.

### Student Papers, Dissertations, and Theses
* Annotated bibliography: reference list followed by short description.
* Cause-and-effect essays: describing of each cause and its collateral effect, with logical transition.
* Comparative essays: compare and contrast to items.
* Expository essay: Explain or provide information on a specific topic. Includes introduction, body, and a conclusion. 
* Narrative essay: Convey a store. Includes beginning, middle, and end. 
* Persuasive essays: Intended to convince readers to adopt a certain view.
* Précis: It is concise summaries for students.
* Response or reaction papers: Summarizesone or more works and describe students.

#### Dissertations or theses
The student also can to write how professional do. The academic or institution chose the guideline to the dissertations or these. 
The academic papers are more flexible and can be change with the need of the entity for where the document is ship. 



### Professional Standards in Publishing
It, finds ethical and legal principles underlie for all research. 
* Ensuring the accuracy of scientific finding,
* protecting the rights and welfare of research participants and subjects, and
* protecting intellectual property rights. 

Standard APA Ethics Code. 


### Ensuring the Accuracy of Scientific Findings
### Planning for Ethical Compliance
All papers needed to follow the ethical standards. We can start a process with IRB or ethical review group for your approval before to standard with research project. 
### Ethical and Accurate Reporting of Research Results
The reporting must have report the methods and results of their studies fully and accurately. 

Omitting Troublesome Observation (Violates): 
* Selectively failing to report studies.
* Selectively omitting reports of  relevant manipulation, procedures, measures, or findings within a study.
* Selectively excluding participants or other individual data observations.

### Errors, Corrections, and Retractions After Publication
Error can still appear after the publication. The author is responsible to fix and convey the errors. 
* Corrections: The first steps when there is an erratum is broadcast for people responsible for the project. 
* Retractions:  When the article has in step of retraction for something reason, they need to remove from all scientific literature information, so the another scientific to compromise the findings. 
### Data Retention and Sharing
#### Data Retention
How the data will be retained, and sharing.
#### Data Sharing
The APA Ethics Code prohibits authors from withholding data from qualified requesters for verification through reanalysis in most circumstances. 
#### Sharing During Review
Authors shared data, analyses, and/or materials during the review. 
it Is possible if the author don't provide to share requested materials, the journal editor has the right to deny the review. 

#### Sharing After Publication
pag 43





