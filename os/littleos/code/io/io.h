#ifndef _IO_H_
#define _IO_H_   

/* The I/o ports */ 

/* All the I/O ports are calculated relative to the data port. This is because all serial ports (COM1, COM2, COM3, COM4) have 
 * their ports in the same order, but they start at different values.
 * */ 

#define SERIAL_COM1_BASE   0x3F8
#define SERIAL_DATA_PORT(base) 			(base)
#define SERIAL_FIFO_COMMAND_PORT(base) (base + 2)
#define SERIAL_LINE_COMMAND_PORT(base) (base + 3)
#define SERIAL_MODEM_COMMAN_PORT(base) (base + 4) 
#define SERIAL_LINE_STATUS_PORT(base)  (base + 5) 

/* SERIAL_LINE_ENABLE_DLAB:
 * Tells the serial port to expect first the highest 8 bits on the data port, then the lowest 8 bits will follow
 * */ 
#define SERIAL_LINE_ENABLE_DLAB 0x80

/** outb: 
 * Sends the given data to the given I/O port. Defined in io.s
 * @param port The I/O port to send the data to
 * @param data The data to send to the I/O port
 * */ 
void outb(unsigned short port, unsigned char data);
void fb_move_cursor(unsigned short pos); 
int write(char *buf, unsigned int len);

/** serial_configure_baud_rate:
 * Sets the speed of the data being sent. The default speed of a serial port is 115200 bits/s. The argument is a divisor of 
 * that number, hence the resulting speed becomes (115200 / divisor) bits/s. 
 *  
 * @param com The COM port to configure
 * @param divisor The divisor
 * */  


#endif

