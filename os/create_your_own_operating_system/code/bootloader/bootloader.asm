bits 16
start:
	mov ax, 0x07C0 ;adrress start the bootloader
	add ax, 0x20
	mov ss, ax
	mov sp, 4096

	mov ax, 0x07C0
	mov ds, ax
	
	mov si, msg 
	call print
	cli 
	hlt 

data: 
	msg db "Hello world!",0
	
print: 
mov ah, 0x0E

.printchar: 
	lodsb
	cmp al, 0
	je .done
	int 0x10
	jmp .printchar
.done:
	ret
times 510-($-$$) db 0
dw 0xAA55 ;signature of bootloader


